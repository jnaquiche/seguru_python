SET SQL_SAFE_UPDATES = 0;
UPDATE seguru.insurance_vehiclebrand SET name = CONCAT(UCASE(LEFT(name, 1)), LCASE(SUBSTRING(name, 2)));
SET SQL_SAFE_UPDATES = 1;