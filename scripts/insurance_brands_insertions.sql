
SET FOREIGN_KEY_CHECKS = 0; 
TRUNCATE table insurance_vehiclebrand; 
SET FOREIGN_KEY_CHECKS = 1;


INSERT INTO insurance_vehiclebrand(
name,
created_at,
updated_at,
ins_brand_code,
oceanica_brand_code,
qualitas_brand_code
)
SELECT 
    brandDescription,
	now(),
	now(),
	codigo,
	brandcode,
	marca2
FROM 

(select * from

	(
    select min(brandcode) brandcode, brandDescription
		from insurance_oceanicacatalogmodel
		where brandDescription in ("Acura",  "Alfa Romeo",  "AMC","Aro","Asia","Aston Martin","Audi","Austin","Baw","Bentley","Bluebird","BMW","Brilliance","Buick","BYD","Cadillac","Chana","Changan","Chery","Chevrolet","Chrysler","Citroen","Dacia","Daewoo","Daihatsu","Datsun","Dodge","RAM","Donfeng (ZNA)","Eagle","Faw","Ferrari","Fiat","Ford","Foton","Freightliner","Geely","Genesis","Geo","Gmc","Gonow","Great Wall","Hafei","Haima","Heibao","Higer","Hino","Honda","Hummer","Hyundai","Infiniti","International","Isuzu","Iveco","JAC","Jaguar","Jeep","Jinbei","JMC","Jonway","Kenworth","Kia","Lada","Lamborghini","Lancia","Land Rover","Lexus","Lifan","Lincoln","Lotus","Mack","Magiruz","Mahindra","Maserati","Mazda","Mercedes Benz","Mercury","MG","Mini","Mitsubishi","Nissan","Oldsmobile","Opel","Peterbilt","Peugeot","Plymouth","Polarsun","Pontiac","Porsche","Proton","Rambler","Renault","Reva","Rolls Royce","Rover","Saab","Samsung","Saturn","Scania","Scion","Seat","Skoda","Smart","Soueast","Ssang Yong","Subaru","Suzuki","Tianma","Tiger Truck","Toyota","Volkswagen","Volvo","Western Star","Yugo","Zotye")
        group by brandDescription ) as oc
	left join
	(select descripcion, MIN(codigo) codigo
		from insurance_inscatalogmodel 
		where tagxml = 'MARCARSGO'
        group by descripcion) as ins

	on (oc.brandDescription = ins.descripcion)

	  left join
	(select marca2
		from insurance_qualitascatalogmodel
        group by marca2
	) as qa

	on (oc.brandDescription = qa.marca2)
) as result
