
SET FOREIGN_KEY_CHECKS = 0; 
TRUNCATE table insurance_color;
 
SET FOREIGN_KEY_CHECKS = 1;


INSERT INTO `seguru`.`insurance_color`
(`name`,
`ins_color_code`)
select descripcion, codigo from insurance_inscatalogmodel
where tagxml = 'COLORVEHICRSGO';

INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Amarillo','AMA');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Anaranjado','ANA');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Azul','AZU');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Beige','BEI');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Blanco','BLA');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Bronce','BRO');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Cafe Satinado','CAS');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Celeste','CEL');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Champagne','CHA');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Cobre','COB');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Creama','CRE');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Dorado','DOR');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Gris Bari','GRB');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Gris','GRI');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Mamei','MAM');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Cafe','MAR');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Morado','MOR');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Negro','NEG');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Marron','MRR');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Plateado','PLA');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Plata Cosmico','PLC');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Plata metalizado','PLM');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Rojo','ROJ');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Rosado','ROS');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Silver Metalico','SIL');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Turquesa','TUR');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Verde','VER');
INSERT INTO `seguru`.`insurance_color`(`name`,`oceanica_color_code`)VALUES('Vino','VIN');





INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('ALUMINIO METALICO','56');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('AMARILLO','5');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('ANARANJADO','35');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('AQUA/NEGRO','44');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('ARENA','25');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('AZUL','2');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('AZUL ATLANTICO','60');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('AZUL BRILLANTE','48');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('AZUL CLARO','19');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('AZUL MARINO','14');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('AZUL METALICO','67');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('AZUL PLATA','85');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('AZUL POLAR','39');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('AZUL/BLANCO','88');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('AZUL/GRIS','21');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('BEIGE','15');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('BEIGE METALICO','23');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('BEIGE/GRIS','78');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('BLANCO','1');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('BLANCO C/F ROJAS','86');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('BLANCO CON FRANJAS','87');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('BLANCO OSTION','82');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('BLANCO/AMARILLO/VERD','80');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('BLANCO/AZUL','81');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('BLANCO/BEIGE','73');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('BLANCO/ROJO','52');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('BLANCO/VERDE','84');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('BRONCE','34');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('CAFE','12');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('CELESTE','72');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('CEREZA','43');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('CHAMPAGNE','17');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('COBRE','76');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('COBRE Y NEGRO','42');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('CORAL','65');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('CREMA','32');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('DORADO','16');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('ESCARLATA','77');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('GRAFITO METALICO','27');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('GRIS','3');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('GRIS C/F CELESTES','53');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('GRIS CLARO','90');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('GRIS METALICO','63');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('GRIS OXFORD','62');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('GRIS PERLA','54');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('GRIS PLATA','47');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('GUINDA','33');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('HUESO','20');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('HUMO','66');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('JADE','99');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('KHAKI','64');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('MALAQUITA','71');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('MARFIL','69');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('MARRON','7');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('MORA','36');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('MORA METALICO','58');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('MORADO','9');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('MORADO PERLA','28');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('NEGRO','8');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('NEGRO ONIX','59');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('OCRE','40');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('OCRE CON FRANJAS','68');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('OCRE METALICO','91');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('OCRE/BEIGE','29');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('OPALO','55');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('ORO','10');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('ORQUIDEA','75');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('PERLA','74');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('PLATA','18');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('PLATA TORNASOL','37');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('PLATA TURQUESA','49');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('PLATEADO CLARO','46');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('PLATINO','22');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('ROJO','4');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('ROJO BLANCO','94');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('ROJO CEREZA','31');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('ROJO ESCARLATA','89');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('ROJO INDIO','92');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('ROJO METALICO','95');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('ROJO PERLADO','79');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('ROJO TURQUESA','98');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('ROJO VERONA','24');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('ROJO/GRIS','51');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('ROSA','83');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('SATIN METALICO','30');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('SIN COLOR','0');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('TERRACOTA','93');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('TURQUESA','38');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('UVA','50');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('VERCE C/F AMARILLAS','13');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('VERDE','6');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('VERDE BLANCO','96');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('VERDE C/GRIS','41');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('VERDE CLASICO','57');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('VERDE ESMERALDA','97');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('VERDE METALICO','26');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('VERDE OSCURO','70');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('VERDE PERLADO','45');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('VINO','11');
INSERT INTO `seguru`.`insurance_color`(`name`,`qualitas_color_code`)VALUES('ZAFIRO','61');