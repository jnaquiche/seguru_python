/* Showing results for QUALITAS_COSTA RICA_T1801_V (1).xlsx */

/* CREATE TABLE */
/*CREATE TABLE insurance_qualitascatalogmodel(
tarifa DOUBLE,
amis DOUBLE,
clave DOUBLE,
marca1 VARCHAR(100),
marca2 VARCHAR(100),
subtipo VARCHAR(100),
vers VARCHAR(100),
tipo VARCHAR(100),
cate VARCHAR(100),
car DOUBLE,
tras DOUBLE,
cil VARCHAR(100),
ptas DOUBLE,
ocup DOUBLE,
ton DOUBLE,
subr VARCHAR(100),
tarvig DOUBLE,
gdm DOUBLE,
grt DOUBLE,
grc DOUBLE,
marca VARCHAR(100),
descripcionmarca VARCHAR(100)
);*/

/* INSERT QUERY NO: 1 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00001, 1, 'NN', 'NISSAN', '370Z', '3.7L SEDAN AUT.', ' 02 OCUP.', '370Z', 100, 18, 'A', 06, 2, 2, '', 01, 1003, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 2 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00002, 2, 'NN', 'NISSAN', 'MURANO', '3.5L V6 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', 'MURANO', 105, 22, 'A', 06, 4, 5, '', 01, 1003, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 3 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00003, 3, 'NN', 'NISSAN', 'PATHFINDER', 'ADVANCE 3.5L V6 AUT.', ' 07 OCUP.', 'PATHFINDER', 105, 22, 'A', 04, 4, 7, '', 01, 1003, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 4 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00004, 4, 'NN', 'NISSAN', 'PATHFINDER', '2.5L 4X4 DIESEL AUT.', ' 07 OCUP.', 'PATHFINDER', 105, 22, 'A', 04, 4, 7, '', 01, 1003, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 5 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00005, 5, 'NN', 'NISSAN', 'PATHFINDER', '2.5L 4X4 DIESEL STD.', ' 07 OCUP.', 'PATHFINDER', 105, 22, 'S', 04, 4, 7, '', 01, 1003, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 6 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00006, 6, 'NN', 'NISSAN', 'PATROL', '5.6L V8 4X4 GASOLINA AUT.', ' 08 OCUP.', 'PATROL', 105, 22, 'A', 08, 4, 8, '', 01, 1003, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 7 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00007, 7, 'NN', 'NISSAN', 'PATROL', '3.0L 4X2 TDI FULL AUT.', ' 08 OCUP.', 'PATROL', 105, 22, 'A', 04, 4, 8, '', 01, 1003, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 8 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00008, 8, 'NN', 'NISSAN', 'PATROL', '3.0L 4X2 TDI FULL STD.', ' 08 OCUP.', 'PATROL', 105, 22, 'S', 04, 4, 8, '', 01, 1003, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 9 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00009, 9, 'NN', 'NISSAN', 'QASHQAI', '1.6L GASOLINA FULL STD.', ' 05 OCUP.', 'QASHQAI', 100, 27, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 10 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00010, 10, 'NN', 'NISSAN', 'QASHQAI', '2.0L GASOLINA FULL AUT.', ' 05 OCUP.', 'QASHQAI', 100, 27, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 11 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00011, 11, 'NN', 'NISSAN', 'TIIDA', 'HB 1.6L GASOLINA FULL AUT.', ' 05 OCUP.', 'TIIDA', 100, 27, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 12 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00012, 12, 'NN', 'NISSAN', 'TIIDA', 'HB 1.6L GASOLINA FULL STD.', ' 05 OCUP.', 'TIIDA', 100, 27, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 13 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00014, 14, 'NN', 'NISSAN', 'TIIDA', 'SEDAN 1.6L GASOLINA FULL AUT.', ' 05 OCUP.', 'TIIDA', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 14 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00015, 15, 'NN', 'NISSAN', 'TIIDA', 'SEDAN 1.6L GASOLINA FULL STD.', ' 05 OCUP.', 'TIIDA', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 15 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00018, 18, 'NN', 'NISSAN', 'URVAN', 'STANDAD ROOF 2.5L DIESEL STD.', ' 15 OCUP.', 'URVAN', 105, 11, 'S', 04, 4, 15, '', 01, 1003, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 16 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00019, 19, 'NN', 'NISSAN', 'XTRAIL', 'MT 2.4L 4X2 GASOLINA STD.', ' 05 OCUP.', 'XTRAIL', 105, 22, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 17 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00020, 20, 'NN', 'NISSAN', 'XTRAIL', 'LIMITED 2.5L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'XTRAIL', 105, 22, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 18 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00021, 21, 'NN', 'NISSAN', 'XTRAIL', '2.5L 4X4 GASOLINA FULL STD.', ' 05 OCUP.', 'XTRAIL', 105, 22, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 19 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00022, 22, 'NN', 'NISSAN', 'XTRAIL', '2.5L 4X2 GASOLINA FULL AUT.', ' 05 OCUP.', 'XTRAIL', 105, 22, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 20 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00026, 26, 'NN', 'NISSAN', 'DATSUN', 'SEDAN 1.2L STD.', ' 05 OCUP.', 'DATSUN', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 21 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00027, 27, 'NN', 'NISSAN', 'XTERRA', '4X4 STD.', ' 05 OCUP.', 'XTERRA', 105, 22, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 22 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00028, 28, 'NN', 'NISSAN', 'SENTRA', 'ADVANCE 1.8L GASOLINA STD.', ' 05 OCUP.', 'SENTRA', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 23 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00031, 31, 'NN', 'NISSAN', 'ALMERA', 'SEDAN 1.8L STD.', ' 05 OCUP.', 'ALMERA', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 24 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00032, 32, 'NN', 'NISSAN', 'PLATINA', 'SEDAN 1.3L STD.', ' 05 OCUP.', 'PLATINA', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 25 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00034, 34, 'NN', 'NISSAN', 'PRIMERA', 'SEDAN 1.9L GASOLINA STD.', ' 05 OCUP.', 'PRIMERA', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 26 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00035, 35, 'NN', 'NISSAN', 'TERRANO', '2.7L 4X4 DIESEL STD.', ' 07 OCUP.', 'TERRANO', 105, 22, 'S', 04, 5, 7, '', 01, 1106, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 27 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00036, 36, 'NN', 'NISSAN', 'PULSAR', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'PULSAR', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 28 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00037, 37, 'NN', 'NISSAN', 'MICRA', 'SEDAN 1.4L GASOLINA STD.', ' 05 OCUP.', 'MICRA', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 29 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00038, 38, 'NN', 'NISSAN', 'C20', 'SEDAN 1.5L GASOLINA STD.', ' 09 OCUP.', 'C20', 105, 11, 'S', 04, 4, 9, '', 01, 1106, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 30 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00039, 39, 'NN', 'NISSAN', 'MARCH', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'MARCH', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 31 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00040, 40, 'NN', 'NISSAN', '240 Z', 'EDAN 2.4L V6 GASOLINA STD.', ' 02 OCUP.', '240 Z', 100, 18, 'S', 06, 2, 2, '', 01, 1112, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 32 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00041, 41, 'NN', 'NISSAN', 'VERSA', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'VERSA', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 33 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00042, 42, 'NN', 'NISSAN', '200SX', 'SEDAN 2.0L GASOLINA STD.', ' 04 OCUP.', '200SX', 100, 18, 'S', 04, 2, 4, '', 01, 1112, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 34 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00043, 43, 'NN', 'NISSAN', 'NX', 'SEDAN 1.6L GASOLINA STD.', ' 04 OCUP.', 'NX', 100, 18, 'S', 04, 2, 4, '', 01, 1112, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 35 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00044, 44, 'NN', 'NISSAN', '300ZX', 'SEDAN 3.0L V6 GASOLINA STD.', ' 02 OCUP.', '300ZX', 100, 18, 'S', 04, 2, 2, '', 01, 1112, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 36 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00045, 45, 'NN', 'NISSAN', 'PATHFINDER', '3.5L 4X4 DIESEL AUT.', ' 05 OCUP.', 'PATHFINDER', 105, 22, 'A', 06, 5, 5, '', 01, 1112, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 37 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00046, 46, 'NN', 'NISSAN', 'LEAF', 'SEDAN 1.5L AUT.', ' 05 OCUP.', 'LEAF', 100, 01, 'A', 00, 5, 5, '', 01, 1112, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 38 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00047, 47, 'NN', 'NISSAN', 'JUKE', '1.6L GASOLINA AUT.', ' 04 OCUP.', 'JUKE', 105, 22, 'A', 04, 5, 4, '', 01, 1112, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 39 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00048, 48, 'NN', 'NISSAN', 'MAXIMA', 'SEDAN 3.0L V6 GASOLINA STD.', ' 05 OCUP.', 'MAXIMA', 100, 01, 'S', 06, 4, 5, '', 01, 1112, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 40 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00049, 49, 'NN', 'NISSAN', 'VANETTE', '1.8L GASOLINA STD.', ' 08 OCUP.', 'VANETTE', 105, 11, 'S', 04, 4, 8, '', 01, 1112, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 41 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00050, 50, 'NN', 'NISSAN', 'PATHFINDER', 'EXCLUSIVE 3.5L V6 STD.', ' 05 OCUP.', 'PATHFINDER', 105, 22, 'S', 06, 5, 5, '', 01, 1112, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 42 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00051, 51, 'NN', 'NISSAN', 'ALTIMA', 'SEDAN 2.4L GASOLINA STD.', ' 05 OCUP.', 'ALTIMA', 100, 01, 'S', 06, 4, 5, '', 01, 1112, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 43 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00052, 52, 'NN', 'NISSAN', 'INFINITI', 'I30 SEDAN 3.0L GASOLINA STD.', ' 05 OCUP.', 'INFINITI', 100, 01, 'S', 06, 4, 5, '', 01, 1112, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 44 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00053, 53, 'NN', 'NISSAN', 'SENTRA', '1.6L GASOLINA STD.', ' 04 OCUP.', 'SENTRA', 100, 01, 'S', 04, 4, 4, '', 01, 1112, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 45 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00054, 54, 'NN', 'NISSAN', 'ROGUE', '2.5L 4X4 GASOLINA STD.', ' 05 OCUP.', 'ROGUE', 105, 22, 'S', 04, 5, 5, '', 01, 1112, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 46 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00055, 55, 'NN', 'NISSAN', 'AD WAGON ', '1.6L GASOLINA STD.', ' 05 OCUP.', 'AD WAGON', 100, 02, 'S', 04, 5, 5, '', 01, 1112, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 47 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00056, 56, 'NN', 'NISSAN', 'PATROL', 'GRX 3.0L 4X4 DIESEL STD.', ' 07 OCUP.', 'PATROL', 105, 22, 'S', 04, 5, 7, '', 01, 1212, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 48 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00057, 57, 'NN', 'NISSAN', 'QUEST', 'STATION WAGON V6 GASOLINA AUT.', ' 07 OCUP.', 'QUEST', 105, 24, 'A', 06, 5, 7, '', 01, 1212, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 49 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00058, 58, 'NN', 'NISSAN', 'NOTE', 'ADVANCE 1.6L GASOLINA AUT.', ' 05 OCUP.', 'NOTE', 100, 01, 'A', 04, 4, 5, '', 01, 1212, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 50 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00059, 59, 'NN', 'NISSAN', '350Z', '3.7L V6 GASOLINA COUPE AUT.', ' 02 OCUP.', '350Z', 100, 18, 'A', 06, 2, 2, '', 01, 1212, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 51 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00060, 60, 'NN', 'NISSAN', 'SENTRA', 'EXCLUSIVE CUERO 1.8L GASOLINA FULL AUT.', ' 05 OCUP.', 'SENTRA', 100, 01, 'A', 04, 4, 5, '', 01, 1402, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 52 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00061, 61, 'NN', 'NISSAN', 'XTERRA', 'SE STATION WAGON 3.3L 4X2 GASOLINA STD.', ' 05 OCUP.', 'XTERRA', 105, 22, 'S', 06, 4, 5, '', 01, 1402, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 53 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00062, 62, 'NN', 'NISSAN', 'ARMADA', 'LE 5.5L 4X2 DISEL STD.', ' 07 OCUP.', 'ARMADA', 105, 22, 'S', 08, 4, 7, '', 01, 1402, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 54 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00063, 63, 'NN', 'NISSAN', 'MARCH', 'SEDAN 1.6L GASOLINA AUT.', ' 05 OCUP.', 'MARCH', 100, 27, 'A', 04, 4, 5, '', 01, 1412, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 55 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00064, 64, 'NN', 'NISSAN', 'MURANO', '3.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'MURANO', 105, 22, 'S', 06, 4, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 56 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00065, 65, 'NN', 'NISSAN', 'TIIDA', 'SALOON SAFE 1.6L GASOLINA STD.', ' 05 OCUP.', 'TIIDA', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 57 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00066, 66, 'NN', 'NISSAN', 'NOTE', 'ADVANCE 1.6L GASOLINA STD.', ' 05 OCUP.', 'NOTE', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 58 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00067, 67, 'NN', 'NISSAN', 'NOTE', 'SR SPORT 1.6L GASOLINA STD.', ' 05 OCUP.', 'NOTE', 100, 27, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 59 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00068, 68, 'NN', 'NISSAN', 'VERSA', 'SALOON 1.6L GASOLINA STD.', ' 05 OCUP.', 'VERSA', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 60 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00069, 69, 'NN', 'NISSAN', 'VERSA', 'SALOON 1.6L GASOLINA AUT.', ' 05 OCUP.', 'VERSA', 100, 01, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 61 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00070, 70, 'NN', 'NISSAN', 'VERSA', 'SUPER SALOON 1.6L GASOLINA STD.', ' 05 OCUP.', 'VERSA', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 62 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00071, 71, 'NN', 'NISSAN', 'VERSA', 'SUPER SALOON 1.6L GASOLINA AUT.', ' 05 OCUP.', 'VERSA', 100, 01, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 63 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00072, 72, 'NN', 'NISSAN', 'SENTRA', 'ADVANCE 1.8L GASOLINA AUT.', ' 05 OCUP.', 'SENTRA', 100, 01, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 64 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00073, 73, 'NN', 'NISSAN', 'SENTRA', 'SR SPORT 1.8L GASOLINA AUT.', ' 05 OCUP.', 'SENTRA', 100, 01, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 65 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00074, 74, 'NN', 'NISSAN', 'QASHQAI', 'LUX 1.6L 4X2 GASOLINA STD.', ' 05 OCUP.', 'QASHQAI', 105, 22, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 66 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00075, 75, 'NN', 'NISSAN', 'QASHQAI', 'LIMITED 1.6L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'QASHQAI', 105, 22, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 67 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00076, 76, 'NN', 'NISSAN', 'XTRAIL', 'LUX 2.5L 4X2 GASOLINA AUT.', ' 07 OCUP.', 'XTRAIL', 105, 22, 'A', 04, 4, 7, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 68 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00077, 77, 'NN', 'NISSAN', 'XTRAIL', 'LUX 2.5L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'XTRAIL', 105, 22, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 69 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00078, 78, 'NN', 'NISSAN', 'XTRAIL', 'SUPER LUX 2.5L 4X2 GASOLINA AUT.', ' 07 OCUP.', 'XTRAIL', 105, 22, 'A', 04, 4, 7, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 70 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00079, 79, 'NN', 'NISSAN', 'XTRAIL', 'LIMITED 2.5L 4X4 GASOLINA AUT.', ' 07 OCUP.', 'XTRAIL', 105, 22, 'A', 04, 4, 7, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 71 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00080, 80, 'NN', 'NISSAN', 'PATHFINDER', 'LIMITED 3.5L V6 AUT.', ' 05 OCUP.', 'PATHFINDER', 105, 22, 'A', 06, 4, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 72 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00081, 81, 'NN', 'NISSAN', 'URVAN', 'LONG HI ROOF 2.5L DIESEL STD.', ' 15 OCUP.', 'URVAN', 100, 20, 'S', 04, 4, 15, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 73 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00082, 82, 'NN', 'NISSAN', 'URVAN', 'GX LONG HI ROOF 3.0L DIESEL STD.', ' 15 OCUP.', 'URVAN', 100, 20, 'S', 04, 4, 15, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 74 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00083, 83, 'NN', 'NISSAN', 'SENTRA', 'LUX 1.8L GASOLINA STD.', ' 05 OCUP.', 'SENTRA', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 75 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00084, 84, 'NN', 'NISSAN', 'SENTRA', 'LUX 1.8L GASOLINA AUT.', ' 05 OCUP.', 'SENTRA', 100, 01, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 76 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00085, 85, 'NN', 'NISSAN', 'QASHQAI', 'ADVANCE 2.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'QASHQAI', 105, 22, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 77 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00086, 86, 'NN', 'NISSAN', 'QASHQAI', 'ADVANCE 2.0L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'QASHQAI', 105, 22, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 78 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00087, 87, 'NN', 'NISSAN', 'SKYLINE GT-R', '2.5L GASOLINA STD.', ' 05 OCUP.', 'SKYLINE GT-R', 100, 18, 'S', 06, 2, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 79 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00088, 88, 'NN', 'NISSAN', 'MARCH', 'ADVANCE 1.6L GASOLINA STD.', ' 05 OCUP.', 'MARCH', 100, 27, 'S', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 80 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00089, 89, 'NN', 'NISSAN', 'MARCH', 'ADVANCE 1.6L GASOLINA AUT.', ' 05 OCUP.', 'MARCH', 100, 27, 'A', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 81 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00090, 90, 'NN', 'NISSAN', 'SENTRA', 'LIMITED 1.8L GASOLINA AUT.', ' 05 OCUP.', 'SENTRA', 100, 01, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 82 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00091, 91, 'NN', 'NISSAN', 'PATHFINDER', 'EXCLUSIVE 3.5L AUT.', ' 07 OCUP.', 'PATHFINDER', 105, 22, 'A', 06, 5, 7, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 83 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00092, 92, 'NN', 'NISSAN', 'PATROL', 'LIMITED ADVANCE 5.7L GASOLINA AUT.', ' 08 OCUP.', 'PATROL', 105, 22, 'A', 08, 5, 8, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 84 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00093, 93, 'NN', 'NISSAN', 'URVAN', 'GX WIDE LONG HI ROOF 2.5L DIESEL STD.', ' 15 OCUP.', 'URVAN', 105, 11, 'S', 04, 4, 15, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 85 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00094, 94, 'NN', 'NISSAN', 'URVAN', 'DX ROOF 2.5L TURBO DIESEL STD.', ' 15 OCUP.', 'URVAN', 105, 11, 'S', 04, 4, 15, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 86 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00095, 95, 'NN', 'NISSAN', 'URVAN', 'DX LONG HIGH ROOF 2.5L TURBO DIESEL STD.', ' 15 OCUP.', 'URVAN', 105, 11, 'S', 04, 4, 15, '', 01, 1501, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 87 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00096, 96, 'NN', 'NISSAN', 'SENTRA', '1.8L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'SENTRA', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 88 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00097, 97, 'NN', 'NISSAN', 'KICKS', '1.6L GASOLINA STD.', ' 05 OCUP.', 'KICKS', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 89 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00098, 98, 'NN', 'NISSAN', 'QASHQAI', '2.0L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'QASHQAI', 105, 22, 'A', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 90 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00099, 99, 'NN', 'NISSAN', 'I30', '3.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'I30', 100, 01, 'S', 06, 4, 5, '', 01, 1601, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 91 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00100, 100, 'NN', 'NISSAN', 'TSURU', 'II 1.6L GASOLINA STD.', ' 05 OCUP.', 'TSURU', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 92 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00101, 101, 'NN', 'NISSAN', 'MARCH', 'ACTIVE 1.6L GASOLINA STD.', ' 05 OCUP.', 'MARCH', 100, 27, 'S', 04, 5, 5, '', 01, 1601, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 93 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00102, 102, 'NN', 'NISSAN', 'KICKS', 'LUX 1.6L 4X2 GASOLINA STD.', ' 05 OCUP.', 'KICKS', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 94 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00103, 103, 'NN', 'NISSAN', 'KICKS', 'ADVANCE 1.6L 4X2 GASOLINA STD.', ' 05 OCUP.', 'KICKS', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 95 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00104, 104, 'NN', 'NISSAN', 'KICKS', 'ADVANCE 1.6L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'KICKS', 105, 22, 'A', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 96 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00105, 105, 'NN', 'NISSAN', 'KICKS', 'LIMITED 1.6L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'KICKS', 105, 22, 'A', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 97 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00106, 106, 'NN', 'NISSAN', 'MURANO', 'LIMITED 3.5L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'MURANO', 105, 22, 'A', 06, 4, 5, '', 01, 1601, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 98 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00107, 107, 'NN', 'NISSAN', 'SUNNY ALMERA', 'SG 1.6L 4X2 GASOLINA STD.', ' 05 OCUP.', 'SUNNY ALMERA', 100, 01, 'S', 04, 4, 5, '', 01, 1705, 0003, 0003, '', 'NISSAN'
);

/* INSERT QUERY NO: 99 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00501, 501, 'TY', 'TOYOTA', 'YARIS', '1.3L GASOLINA STD.', ' 05 OCUP.', 'YARIS', 100, 27, 'S', 04, 5, 5, '', 01, 1003, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 100 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00503, 503, 'TY', 'TOYOTA', 'YARIS', '1.3L GASOLINA AUT.', ' 05 OCUP.', 'YARIS', 100, 27, 'A', 04, 5, 5, '', 01, 1003, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 101 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00505, 505, 'TY', 'TOYOTA', 'YARIS', '1.5L GASOLINA FULL STD.', ' 05 OCUP.', 'YARIS', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 102 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00506, 506, 'TY', 'TOYOTA', 'YARIS', '1.5L GASOLINA SUPERFULL STD.', ' 05 OCUP.', 'YARIS', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 103 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00507, 507, 'TY', 'TOYOTA', 'YARIS', 'HB 1.5L GASOLINA AUT.', ' 05 OCUP.', 'YARIS', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 104 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00509, 509, 'TY', 'TOYOTA', 'COROLLA', '1.8L GASOLINA STD.', ' 05 OCUP.', 'COROLLA', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 105 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00510, 510, 'TY', 'TOYOTA', 'COROLLA', '1.8L GASOLINA AUT.', ' 05 OCUP.', 'COROLLA', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 106 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00512, 512, 'TY', 'TOYOTA', 'COROLLA', '2.0L DIESEL STD.', ' 05 OCUP.', 'COROLLA', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 107 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00514, 514, 'TY', 'TOYOTA', 'CAMRY', '2.4L V/T GASOLINA AUT.', ' 05 OCUP.', 'CAMRY', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 108 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00515, 515, 'TY', 'TOYOTA', 'CAMRY', '2.4L GASOLINA AUT.', ' 05 OCUP.', 'CAMRY', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 109 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00516, 516, 'TY', 'TOYOTA', 'PRIUS', '1.8L V/T GASOLINA AUT.', ' 05 OCUP.', 'PRIUS', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 110 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00517, 517, 'TY', 'TOYOTA', 'PRIUS', '1.8L V/P GASOLINA AUT.', ' 05 OCUP.', 'PRIUS', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 111 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00518, 518, 'TY', 'TOYOTA', '4RUNNER', '4.0L V6 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', '4RUNNER', 105, 22, 'A', 06, 5, 5, '', 01, 1003, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 112 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00520, 520, 'TY', 'TOYOTA', '4RUNNER', 'SR5 4.0L V6 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', '4RUNNER', 105, 22, 'A', 06, 5, 5, '', 01, 1003, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 113 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00522, 522, 'TY', 'TOYOTA', 'FJ CRUISER', '4.0L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'FJ CRUISER', 105, 22, 'A', 06, 5, 5, '', 01, 1003, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 114 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00523, 523, 'TY', 'TOYOTA', 'FORTUNER', '3.0L 4X4 DIESEL STD.', ' 07 OCUP.', 'FORTUNER', 105, 22, 'S', 04, 4, 7, '', 01, 1003, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 115 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00524, 524, 'TY', 'TOYOTA', 'FORTUNER', '3.0L 4X4 DIESEL AUT.', ' 07 OCUP.', 'FORTUNER', 105, 22, 'A', 04, 4, 7, '', 01, 1003, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 116 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00525, 525, 'TY', 'TOYOTA', 'PRADO', '4.0L V6 4X4 GASOLINA SUPERFULL AUT.', ' 07 OCUP.', 'PRADO', 105, 22, 'A', 06, 5, 7, '', 01, 1003, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 117 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00526, 526, 'TY', 'TOYOTA', 'PRADO', '4.0L V6 4X4 GASOLINA AUT.', ' 07 OCUP.', 'PRADO', 105, 22, 'A', 06, 5, 7, '', 01, 1003, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 118 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00527, 527, 'TY', 'TOYOTA', 'PRADO', '4.0L V6 4X4 GASOLINA FULL AUT.', ' 07 OCUP.', 'PRADO', 105, 22, 'A', 06, 5, 7, '', 01, 1003, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 119 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00528, 528, 'TY', 'TOYOTA', 'RAV4', '2.4L 4X2 GASOLINA FULL STD.', ' 05 OCUP.', 'RAV4', 105, 22, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 120 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00529, 529, 'TY', 'TOYOTA', 'RAV4', '2.5L 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', 'RAV4', 105, 22, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 121 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00530, 530, 'TY', 'TOYOTA', 'RAV4', '2.4L 4X2 GASOLINA SUPERFULL AUT.', ' 05 OCUP.', 'RAV4', 105, 22, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 122 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00531, 531, 'TY', 'TOYOTA', 'RAV4', '2.4L 4X4 GASOLINA FULL STD.', ' 05 OCUP.', 'RAV4', 105, 22, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 123 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00532, 532, 'TY', 'TOYOTA', 'RAV4', '2.4L 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', 'RAV4', 105, 22, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 124 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00533, 533, 'TY', 'TOYOTA', 'RAV4', '2.4L 4X4 GASOLINA SUPERFULL AUT.', ' 05 OCUP.', 'RAV4', 105, 22, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 125 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00534, 534, 'TY', 'TOYOTA', 'SEQUOIA', '4.6L V8 4X4 GASOLINA AUT.', ' 07 OCUP.', 'SEQUOIA', 105, 22, 'A', 08, 5, 7, '', 01, 1003, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 126 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00535, 535, 'TY', 'TOYOTA', 'SEQUOIA', '4.6L V8 4X4 GASOLINA FULL AUT.', ' 07 OCUP.', 'SEQUOIA', 105, 22, 'A', 08, 5, 7, '', 01, 1003, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 127 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00537, 537, 'TY', 'TOYOTA', 'LAND CRUISER', '2.7L 4X4 SUPERFULL AUT.', ' 05 OCUP.', 'LAND CRUISER', 105, 22, 'A', 04, 3, 5, '', 01, 1106, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 128 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00538, 538, 'TY', 'TOYOTA', 'TERCEL', '1.4L STD.', ' 05 OCUP.', 'TERCEL', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 129 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00539, 539, 'TY', 'TOYOTA', 'CORONA', 'SEDAN 2.0L STD.', ' 05 OCUP.', 'CORONA', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 130 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00540, 540, 'TY', 'TOYOTA', 'PREVIA', 'STATION WAGON 2.4L GASOLINA AUT.', ' 09 OCUP.', 'PREVIA', 100, 11, 'A', 04, 4, 9, '', 01, 1106, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 131 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00541, 541, 'TY', 'TOYOTA', 'LAND CRUISER', 'STW 3.0L 4X4 DIESEL SUPERFULL STD.', ' 05 OCUP.', 'LAND CRUISER', 105, 11, 'S', 06, 4, 5, '', 01, 1106, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 132 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00542, 542, 'TY', 'TOYOTA', 'MR2', 'SPYDER COUPE 1.8L GASOLINA STD.', ' 02 OCUP.', 'MR2', 100, 18, 'S', 04, 2, 2, '', 01, 1106, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 133 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00543, 543, 'TY', 'TOYOTA', 'ECHO', 'COUPE 1.5L GASOLINA STD.', ' 05 OCUP.', 'ECHO', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 134 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00544, 544, 'TY', 'TOYOTA', 'STARLET', 'SEDAN 1.3L GASOLINA STD.', ' 05 OCUP.', 'STARLET', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 135 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00545, 545, 'TY', 'TOYOTA', 'MATRIX', 'SEDAN 1.8L GASOLINA STD.', ' 05 OCUP.', 'MATRIX', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 136 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00546, 546, 'TY', 'TOYOTA', 'LAND CRUISER', '2.7L TDI AUT.', ' 07 OCUP.', 'LAND CRUISER', 105, 22, 'A', 04, 5, 7, '', 01, 1112, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 137 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00547, 547, 'TY', 'TOYOTA', 'LAND CRUISER', '3.0L 4X4 TDI AUT.', ' 07 OCUP.', 'LAND CRUISER', 105, 01, 'A', 04, 5, 7, '', 01, 1112, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 138 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00549, 549, 'TY', 'TOYOTA', 'AVENSIS', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'AVENSIS', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 139 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00550, 550, 'TY', 'TOYOTA', 'RAV4', '2.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'RAV4', 105, 22, 'S', 04, 5, 5, '', 01, 1112, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 140 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00551, 551, 'TY', 'TOYOTA', 'CRESSIDA', '2.4L DIESEL STD.', ' 05 OCUP.', 'CRESSIDA', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 141 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00552, 552, 'TY', 'TOYOTA', 'SIENNA', '2.5L V6 GASOLINA AUT.', ' 08 OCUP.', 'SIENNA', 100, 02, 'A', 06, 5, 8, '', 01, 1112, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 142 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00553, 553, 'TY', 'TOYOTA', 'ZELAS', 'COUPE 2.5L GASOLINA STD.', ' 05 OCUP.', 'ZELAS', 100, 18, 'S', 04, 2, 5, '', 01, 1112, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 143 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00555, 555, 'TY', 'TOYOTA', 'CELICA', 'CELICA COUPE 2.0L GASOLINA STD.', ' 04 OCUP.', 'CELICA', 100, 08, 'S', 04, 2, 4, '', 01, 1112, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 144 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00556, 556, 'TY', 'TOYOTA', 'PRADO', '3.0L 4X4 DIESEL STD.', ' 05 OCUP.', 'PRADO', 105, 22, 'S', 04, 5, 5, '', 01, 1112, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 145 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00558, 558, 'TY', 'TOYOTA', 'VAN LE', '2.0L GASOLINA AUT.', ' 07 OCUP.', 'VAN LE', 105, 11, 'A', 04, 3, 7, '', 01, 1112, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 146 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00559, 559, 'TY', 'TOYOTA', 'PASEO', '1.5L GASOLINA STD.', ' 05 OCUP.', 'PASEO', 100, 08, 'S', 04, 2, 5, '', 01, 1112, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 147 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00560, 560, 'TY', 'TOYOTA', 'CARINA', 'XL SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'CARINA', 100, 01, 'S', 04, 4, 5, '', 01, 1212, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 148 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00561, 561, 'TY', 'TOYOTA', 'CROWN', 'SEDAN 2.5L DIESEL STD.', ' 05 OCUP.', 'CROWN', 100, 01, 'S', 04, 4, 5, '', 01, 1212, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 149 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00562, 562, 'TY', 'TOYOTA', 'HIGHLANDER', '3.5L GASOLINA STD.', ' 07 OCUP.', 'HIGHLANDER', 105, 22, 'S', 06, 4, 7, '', 01, 1212, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 150 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00563, 563, 'TY', 'TOYOTA', '4RUNNER', '4.0L 4X2 GASOLINA STD.', ' 05 OCUP.', '4RUNNER', 105, 22, 'S', 04, 5, 5, '', 01, 1212, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 151 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00564, 564, 'TY', 'TOYOTA', 'HIACE', '4X2 STD.', ' 15 OCUP.', 'HIACE', 105, 11, 'S', 04, 3, 15, '', 02, 1106, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 152 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00565, 565, 'TY', 'TOYOTA', 'SCION', 'XB STATION WAGON 1.5L STD.', ' 05 OCUP.', 'SCION', 105, 22, 'S', 04, 4, 5, '', 01, 1402, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 153 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00566, 566, 'TY', 'TOYOTA', 'COROLLA ', 'L 1.8L GASOLINA STD.', ' 05 OCUP.', 'COROLLA', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 154 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00567, 567, 'TY', 'TOYOTA', 'COROLLA ', 'L 1.8L GASOLINA AUT.', ' 05 OCUP.', 'COROLLA', 100, 01, 'A', 04, 4, 5, '', 01, 1501, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 155 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00568, 568, 'TY', 'TOYOTA', 'COROLLA ', 'LE 1.8L GASOLINA STD.', ' 05 OCUP.', 'COROLLA', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 156 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00569, 569, 'TY', 'TOYOTA', 'COROLLA ', 'LE 1.8L GASOLINA AUT.', ' 05 OCUP.', 'COROLLA', 100, 01, 'A', 04, 4, 5, '', 01, 1501, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 157 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00570, 570, 'TY', 'TOYOTA', 'COROLLA ', 'S 1.8L GASOLINA STD.', ' 05 OCUP.', 'COROLLA', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 158 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00571, 571, 'TY', 'TOYOTA', 'COROLLA ', 'S 1.8L GASOLINA AUT.', ' 05 OCUP.', 'COROLLA', 100, 01, 'A', 04, 4, 5, '', 01, 1501, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 159 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00572, 572, 'TY', 'TOYOTA', 'PRIUS', 'C HIBRIDO 1.5L GASOLINA AUT.', ' 05 OCUP.', 'PRIUS', 100, 27, 'A', 04, 5, 5, '', 01, 1501, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 160 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00573, 573, 'TY', 'TOYOTA', 'PRIUS', 'HIBRIDO 1.8L GASOLINA AUT.', ' 05 OCUP.', 'PRIUS', 100, 27, 'A', 04, 5, 5, '', 01, 1501, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 161 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00574, 574, 'TY', 'TOYOTA', 'CAMRY', 'HIBRIDO 2.5L GASOLINA AUT.', ' 05 OCUP.', 'CAMRY', 100, 01, 'A', 04, 4, 5, '', 01, 1501, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 162 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00575, 575, 'TY', 'TOYOTA', 'FORTUNER', 'SR5 3.0L 4X4 DIESEL STD.', ' 07 OCUP.', 'FORTUNER', 105, 22, 'S', 04, 5, 7, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 163 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00576, 576, 'TY', 'TOYOTA', 'FORTUNER', 'SR5 3.0L 4X4 DIESEL AUT.', ' 07 OCUP.', 'FORTUNER', 105, 22, 'A', 04, 5, 7, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 164 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00577, 577, 'TY', 'TOYOTA', 'FORTUNER', 'LIMITED 3.0L 4X4 DIESEL AUT.', ' 07 OCUP.', 'FORTUNER', 105, 22, 'A', 04, 5, 7, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 165 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00578, 578, 'TY', 'TOYOTA', 'RAV4', '2.5L 4X2 GASOLINA FULL EXTRAS STD.', ' 05 OCUP.', 'RAV4', 105, 22, 'S', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 166 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00579, 579, 'TY', 'TOYOTA', 'RAV4', '2.5L 4X2 GASOLINA FULL EXTRAS AUT.', ' 05 OCUP.', 'RAV4', 105, 22, 'A', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 167 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00580, 580, 'TY', 'TOYOTA', 'RAV4', '2.5L 4X4 GASOLINA FULL EXTRAS STD.', ' 05 OCUP.', 'RAV4', 105, 22, 'S', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 168 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00581, 581, 'TY', 'TOYOTA', 'RAV4', '2.5L 4X4 GASOLINA FULL EXTRAS AUT.', ' 05 OCUP.', 'RAV4', 105, 22, 'A', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 169 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00582, 582, 'TY', 'TOYOTA', 'RAV4', 'LIMITED 2.5L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'RAV4', 105, 22, 'A', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 170 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00583, 583, 'TY', 'TOYOTA', 'RAV4', 'SX 2.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'RAV4', 105, 22, 'S', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 171 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00584, 584, 'TY', 'TOYOTA', 'RAV4', 'SX 2.5L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'RAV4', 105, 22, 'A', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 172 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00585, 585, 'TY', 'TOYOTA', 'RAV4', 'SX 2.5L 4X4 GASOLINA STD.', ' 05 OCUP.', 'RAV4', 105, 22, 'S', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 173 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00586, 586, 'TY', 'TOYOTA', 'RAV4', 'SX 2.5L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'RAV4', 105, 22, 'A', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 174 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00587, 587, 'TY', 'TOYOTA', '4RUNNER', 'SR5 4.0L 4X4 GASOLINA AUT.', ' 07 OCUP.', '4RUNNER', 105, 22, 'A', 06, 5, 7, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 175 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00588, 588, 'TY', 'TOYOTA', 'LC PRADO', 'X-L LIMITED 3.0L 4X4 DIESEL STD.', ' 07 OCUP.', 'L.C.PRADO', 105, 22, 'S', 04, 5, 7, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 176 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00589, 589, 'TY', 'TOYOTA', 'LC PRADO', 'X-L LIMITED 3.0L 4X4 DIESEL AUT.', ' 07 OCUP.', 'L.C.PRADO', 105, 22, 'A', 04, 5, 7, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 177 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00590, 590, 'TY', 'TOYOTA', 'LC PRADO', 'X-L 3.0L 4X4 DIESEL AUT.', ' 07 OCUP.', 'L.C.PRADO', 105, 22, 'A', 04, 5, 7, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 178 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00591, 591, 'TY', 'TOYOTA', 'LC PRADO', 'X 3.0L 4X4 DIESEL STD.', ' 07 OCUP.', 'L.C.PRADO', 105, 22, 'S', 04, 5, 7, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 179 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00592, 592, 'TY', 'TOYOTA', 'LC PRADO', 'X 3.0L 4X4 DIESEL AUT.', ' 07 OCUP.', 'L.C.PRADO', 105, 22, 'A', 04, 5, 7, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 180 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00593, 593, 'TY', 'TOYOTA', 'LC PRADO', 'X PLATINUM 3.0L DIESEL AUT.', ' 07 OCUP.', 'L.C.PRADO', 105, 22, 'A', 04, 5, 7, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 181 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00594, 594, 'TY', 'TOYOTA', 'LAND CRUISER', 'STW PLATINUM 4.5L DIESEL AUT.', ' 08 OCUP.', 'LAND CRUISER', 105, 22, 'A', 08, 5, 8, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 182 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00595, 595, 'TY', 'TOYOTA', 'LAND CRUISER', 'HARD TOP 4.2L DIESEL STD.', ' 07 OCUP.', 'LAND CRUISER', 105, 22, 'S', 06, 3, 7, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 183 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00596, 596, 'TY', 'TOYOTA', 'LAND CRUISER', 'HARD TOP 4.2L DIESEL FULL STD.', ' 05 OCUP.', 'LAND CRUISER', 105, 22, 'S', 06, 5, 5, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 184 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00597, 597, 'TY', 'TOYOTA', 'LAND CRUISER', 'HARD TOP 4.2L DIESEL STD.', ' 05 OCUP.', 'LAND CRUISER', 105, 22, 'S', 06, 5, 5, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 185 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00598, 598, 'TY', 'TOYOTA', 'SEQUOIA', 'LIMITED 5.7L GASOLINA AUT.', ' 07 OCUP.', 'SEQUOIA', 105, 22, 'A', 08, 5, 7, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 186 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00599, 599, 'TY', 'TOYOTA', 'YARIS', 'HATCHBACK E 1.5L GASOLINA STD.', ' 05 OCUP.', 'YARIS', 100, 27, 'S', 04, 5, 5, '', 01, 1501, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 187 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00600, 600, 'TY', 'TOYOTA', 'YARIS', 'HATCHBACK E 1.5L GASOLINA AUT.', ' 05 OCUP.', 'YARIS', 100, 27, 'A', 04, 5, 5, '', 01, 1501, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 188 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00601, 601, 'TY', 'TOYOTA', 'YARIS', 'HATCHBACK S 1.5L GASOLINA STD.', ' 05 OCUP.', 'YARIS', 100, 27, 'S', 04, 5, 5, '', 01, 1501, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 189 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00602, 602, 'TY', 'TOYOTA', 'YARIS', 'HATCHBACK S 1.5L GASOLINA AUT.', ' 05 OCUP.', 'YARIS', 100, 27, 'A', 04, 5, 5, '', 01, 1501, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 190 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00603, 603, 'TY', 'TOYOTA', 'YARIS', 'SEDAN E 1.5L GASOLINA STD.', ' 05 OCUP.', 'YARIS', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 191 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00604, 604, 'TY', 'TOYOTA', 'YARIS', 'SEDAN G 1.5L GASOLINA STD.', ' 05 OCUP.', 'YARIS', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 192 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00605, 605, 'TY', 'TOYOTA', 'YARIS', 'SEDAN G 1.5L GASOLINA AUT.', ' 05 OCUP.', 'YARIS', 100, 01, 'A', 04, 4, 5, '', 01, 1501, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 193 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00606, 606, 'TY', 'TOYOTA', 'LAND CRUISER', 'HARD TOP LONG 4.2L DIESEL STD.', ' 11 OCUP.', 'LAND CRUISER', 105, 22, 'S', 06, 3, 11, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 194 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00607, 607, 'TY', 'TOYOTA', 'AVANZA', '1.5L GASOLINA AUT.', ' 07 OCUP.', 'AVANZA', 105, 22, 'A', 04, 4, 7, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 195 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00608, 608, 'TY', 'TOYOTA', 'COASTER', 'HZ01 4.2L DIESEL STD.', ' 30 OCUP.', 'COASTER', 105, 11, 'S', 06, 2, 30, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 196 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00609, 609, 'TY', 'TOYOTA', 'RAV4', '2.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'RAV4', 105, 22, 'S', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 197 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00610, 610, 'TY', 'TOYOTA', 'RAV4', '2.0L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'RAV4', 105, 22, 'A', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 198 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00611, 611, 'TY', 'TOYOTA', 'RAV4', '2.0L 4X4 GASOLINA STD.', ' 05 OCUP.', 'RAV4', 105, 22, 'S', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 199 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00612, 612, 'TY', 'TOYOTA', 'RAV4', '2.0L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'RAV4', 105, 22, 'A', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 200 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00613, 613, 'TY', 'TOYOTA', 'RAV4', 'SPORT 2.0L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'RAV4', 105, 22, 'A', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 201 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00614, 614, 'TY', 'TOYOTA', 'SEQUOIA', 'PLATINUM 5.7L GASOLINA AUT.', ' 07 OCUP.', 'SEQUOIA', 105, 22, 'A', 08, 5, 7, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 202 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00615, 615, 'TY', 'TOYOTA', 'COASTER', 'BB51 4.0L TURBO DIESEL STD.', ' 29 OCUP.', 'COASTER', 105, 11, 'S', 04, 2, 29, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 203 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00616, 616, 'TY', 'TOYOTA', 'HIACE', 'LHB6 3.0L DIESEL STD.', ' 15 OCUP.', 'HIACE', 105, 11, 'S', 04, 4, 15, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 204 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00617, 617, 'TY', 'TOYOTA', 'HIACE', 'LHB7 DE LUJO 3.0L DIESEL STD.', ' 15 OCUP.', 'HIACE', 105, 11, 'S', 04, 4, 15, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 205 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00618, 618, 'TY', 'TOYOTA', 'HIACE', 'LHB8 TECHO ALTO 3.0L DIESEL STD.', ' 16 OCUP.', 'HIACE', 105, 11, 'S', 04, 4, 16, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 206 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00619, 619, 'TY', 'TOYOTA', 'HIACE', 'KD01 2.5L TURBO DIESEL STD.', ' 12 OCUP.', 'HIACE', 105, 11, 'S', 04, 4, 12, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 207 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00620, 620, 'TY', 'TOYOTA', 'HIACE', 'KD03 DE LUJO 2.5L TURBO DIESEL STD.', ' 15 OCUP.', 'HIACE', 105, 11, 'S', 04, 4, 15, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 208 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00621, 621, 'TY', 'TOYOTA', 'HIACE', 'KD08 TECHO ALTO 2.5L TURBO DIESEL STD.', ' 16 OCUP.', 'HIACE', 105, 11, 'S', 04, 4, 16, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 209 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00622, 622, 'TY', 'TOYOTA', 'LITEACE', '2.0L GASOLINA STD.', ' 8 OCUP.', 'LITEACE', 105, 11, 'S', 04, 4, 8, '', 01, 1501, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 210 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00623, 623, 'TY', 'TOYOTA', 'AVALON', '3.0L 4X2 GASOLINA AUT.', ' 5 OCUP.', 'AVALON', 100, 01, 'A', 06, 4, 5, '', 01, 1501, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 211 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00624, 624, 'TY', 'TOYOTA', 'TOWN ACE', '2.0L 4X2 DIESEL STD.', ' 9 OCUP.', 'TOWN ACE', 100, 24, 'S', 04, 4, 9, '', 01, 1601, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 212 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00625, 625, 'TY', 'TOYOTA', 'LAND CRUISER', 'SW 5.7L 4X4 DIESEL STD.', ' 05 OCUP.', 'LAND CRUISER', 105, 22, 'S', 08, 4, 5, '', 01, 1601, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 213 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00626, 626, 'TY', 'TOYOTA', 'LC PRADO', 'GX 2.7L 4X4 GASOLINA STD.', ' 05 OCUP.', 'LC PRADO', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 214 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00627, 627, 'TY', 'TOYOTA', 'RUSH', '1.5L 4X4 GASOLINA STD.', ' 05 OCUP.', 'RUSH', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 215 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00628, 628, 'TY', 'TOYOTA', 'RUSH', '1.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'RUSH', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 216 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00629, 629, 'TY', 'TOYOTA', 'FJ CRUISER', '4.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'FJ CRUISER', 105, 22, 'S', 06, 4, 5, '', 01, 1601, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 217 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00630, 630, 'TY', 'TOYOTA', '86', '2.0L 4X2 GASOLINA STD.', ' 04 OCUP.', '86', 100, 18, 'S', 04, 2, 4, '', 01, 1601, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 218 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00631, 631, 'TY', 'TOYOTA', 'RAV4', 'HIBRIDO 2.5L GASOLINA AUT.', ' 05 OCUP.', 'RAV4', 105, 22, 'A', 04, 5, 5, '', 01, 1601, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 219 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00632, 632, 'TY', 'TOYOTA', 'ETIOS', '1.5L GASOLINA STD.', ' 05 OCUP.', 'ETIOS', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 220 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00633, 633, 'TY', 'TOYOTA', 'LAND CRUISER', 'VX 4.7L 4X4 GASOLINA AUT.', ' 08 OCUP.', 'LAND CRUISER', 105, 22, 'A', 08, 5, 8, '', 01, 1601, 0003, 0003, '', 'TOYOTA'
);

/* INSERT QUERY NO: 221 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 00634, 634, 'TY', 'TOYOTA', 'AGYA', '1.0L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'AGYA', 100, 01, 'A', 03, 4, 5, '', 01, 1705, 0002, 0002, '', 'TOYOTA'
);

/* INSERT QUERY NO: 222 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01001, 1001, 'HI', 'HYUNDAI', 'ACCENT', 'BLUE SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'ACCENT', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 223 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01002, 1002, 'HI', 'HYUNDAI', 'ACCENT', 'SEDAN 1.4L GASOLINA AUT.', ' 05 OCUP.', 'ACCENT', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 224 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01003, 1003, 'HI', 'HYUNDAI', 'ELANTRA', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'ELANTRA', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 225 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01004, 1004, 'HI', 'HYUNDAI', 'ELANTRA', 'COUPE 1.8L GASOLINA AUT.', ' 05 OCUP.', 'ELANTRA', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 226 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01007, 1007, 'HI', 'HYUNDAI', 'ELANTRA', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'ELANTRA', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 227 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01008, 1008, 'HI', 'HYUNDAI', 'GETZ', 'SEDAN 1.4L GASOLINA STD.', ' 05 OCUP.', 'GETZ', 100, 27, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 228 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01009, 1009, 'HI', 'HYUNDAI', 'GETZ', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'GETZ', 100, 27, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 229 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01010, 1010, 'HI', 'HYUNDAI', 'GRAN STAREX', '2.5L TURBO DIESEL STD.', ' 12 OCUP.', 'GRAN STAREX', 105, 11, 'S', 04, 2, 12, '', 01, 1003, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 230 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01011, 1011, 'HI', 'HYUNDAI', 'GRAN STAREX', '2.5L TURBO DIESEL AUT.', ' 12 OCUP.', 'GRAN STAREX', 105, 11, 'A', 04, 2, 12, '', 01, 1003, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 231 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01013, 1013, 'HI', 'HYUNDAI', 'I10', 'SEDAN 1.3L GASOLINA STD.', ' 05 OCUP.', 'I10', 100, 27, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 232 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01014, 1014, 'HI', 'HYUNDAI', 'I10', 'SEDAN 1.3L GASOLINA AUT.', ' 05 OCUP.', 'I10', 100, 27, 'A', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 233 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01017, 1017, 'HI', 'HYUNDAI', 'I30', 'SEDAN 1.8L GASOLINA STD.', ' 05 OCUP.', 'I30', 100, 27, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 234 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01018, 1018, 'HI', 'HYUNDAI', 'I30', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'I30', 100, 27, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 235 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01019, 1019, 'HI', 'HYUNDAI', 'SANTA FE', '2.2L 4X4 TDI STD.', ' 07 OCUP.', 'SANTA FE', 105, 22, 'S', 04, 4, 7, '', 01, 1003, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 236 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01020, 1020, 'HI', 'HYUNDAI', 'SANTA FE', '2.2L 4X4 TDI AUT.', ' 07 OCUP.', 'SANTA FE', 105, 22, 'A', 04, 4, 7, '', 01, 1003, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 237 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01023, 1023, 'HI', 'HYUNDAI', 'SANTA FE', '2.4L 4X4 GASOLINA STD.', ' 05 OCUP.', 'SANTA FE', 105, 22, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 238 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01024, 1024, 'HI', 'HYUNDAI', 'SANTA FE', '2.4L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'SANTA FE', 105, 22, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 239 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01028, 1028, 'HI', 'HYUNDAI', 'SANTA FE', '3.5L V6 4X4 GASOLINA AUT.', ' 07 OCUP.', 'SANTA FE', 105, 22, 'A', 06, 4, 7, '', 01, 1003, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 240 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01029, 1029, 'HI', 'HYUNDAI', 'SONATA', 'SEDAN 2.4L GASOLINA STD.', ' 05 OCUP.', 'SONATA', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 241 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01030, 1030, 'HI', 'HYUNDAI', 'SONATA', 'SEDAN 2.4L GASOLINA FULL AUT.', ' 05 OCUP.', 'SONATA', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 242 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01031, 1031, 'HI', 'HYUNDAI', 'TUCSON', '2.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'TUCSON', 105, 22, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 243 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01032, 1032, 'HI', 'HYUNDAI', 'TUCSON', 'SPORT GL 2.0L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'TUCSON', 105, 22, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 244 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01033, 1033, 'HI', 'HYUNDAI', 'TUCSON', '2.0L 4X4 GASOLINA STD.', ' 05 OCUP.', 'TUCSON', 105, 22, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 245 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01034, 1034, 'HI', 'HYUNDAI', 'TUCSON', '2.0L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'TUCSON', 105, 22, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 246 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01037, 1037, 'HI', 'HYUNDAI', 'TUCSON', '2.0L 4X4 GASOLINA FULL STD.', ' 05 OCUP.', 'TUCSON', 105, 22, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 247 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01038, 1038, 'HI', 'HYUNDAI', 'TUCSON', '2.0L 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', 'TUCSON', 105, 22, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 248 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01039, 1039, 'HI', 'HYUNDAI', 'VERACRUZ', '3.0L V6 4X4 TDI AUT.', ' 07 OCUP.', 'VERACRUZ', 105, 22, 'A', 06, 4, 7, '', 01, 1003, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 249 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01040, 1040, 'HI', 'HYUNDAI', 'VERACRUZ', '3.0L V6 4X4 TDI FULL AUT.', ' 07 OCUP.', 'VERACRUZ', 105, 22, 'A', 06, 4, 7, '', 01, 1003, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 250 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01041, 1041, 'HI', 'HYUNDAI', 'TERRACAN', '2.5L 4X4 FULL STD.', ' 07 OCUP.', 'TERRACAN', 105, 22, 'S', 04, 4, 7, '', 01, 1106, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 251 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01042, 1042, 'HI', 'HYUNDAI', 'TERRACAN', '3.0L 4X4 STD.', ' 07 OCUP.', 'TERRACAN', 105, 22, 'S', 04, 4, 7, '', 01, 1106, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 252 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01043, 1043, 'HI', 'HYUNDAI', 'VERNA', 'SEDAN 1.5L GASOLINA STD.', ' 05 OCUP.', 'VERNA', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 253 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01044, 1044, 'HI', 'HYUNDAI', 'EXCEL', 'SEDAN 1.5L GASOLINA STD.', ' 05 OCUP.', 'EXCEL', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 254 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01045, 1045, 'HI', 'HYUNDAI', 'H100', '2.5L STD.', ' 15 OCUP.', 'H100', 105, 11, 'S', 04, 3, 15, '', 01, 1106, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 255 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01046, 1046, 'HI', 'HYUNDAI', 'GENESIS', 'COUPE 3.8L V6 STD.', ' 05 OCUP.', 'GENESIS', 100, 01, 'S', 06, 4, 5, '', 01, 1106, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 256 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01047, 1047, 'HI', 'HYUNDAI', 'GENESIS', 'COUPE 2.0L GASOLINA STD.', ' 04 OCUP.', 'GENESIS', 100, 18, 'S', 04, 2, 4, '', 01, 1106, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 257 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01048, 1048, 'HI', 'HYUNDAI', 'GALLOPER', '2.5L 4X4 STD.', ' 07 OCUP.', 'GALLOPER', 105, 22, 'S', 05, 5, 7, '', 01, 1106, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 258 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01049, 1049, 'HI', 'HYUNDAI', 'TIBURON', 'SEDAN 2.0L GASOLINA STD.', ' 04 OCUP.', 'TIBURON', 100, 18, 'S', 04, 2, 4, '', 01, 1106, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 259 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01050, 1050, 'HI', 'HYUNDAI', 'ATOS', 'HB 1.1L GASOLINA STD.', ' 05 OCUP.', 'ATOS', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 260 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01051, 1051, 'HI', 'HYUNDAI', 'TERRACAN', '2.5L V6 GASOLINA STD.', ' 07 OCUP.', 'TERRACAN', 105, 22, 'S', 06, 5, 7, '', 01, 1112, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 261 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01052, 1052, 'HI', 'HYUNDAI', 'MATRIX', 'HB 1.6L GASOLINA STD.', ' 05 OCUP.', 'MATRIX', 105, 01, 'S', 04, 5, 5, '', 01, 1112, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 262 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01053, 1053, 'HI', 'HYUNDAI', 'VERACRUZ', '3.0L V6 4X4 DIESEL AUT.', ' 07 OCUP.', 'VERACRUZ', 105, 22, 'A', 06, 5, 7, '', 01, 1112, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 263 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01054, 1054, 'HI', 'HYUNDAI', 'AVANTE', 'SEDAN 1.5L GASOLINA STD.', ' 05 OCUP.', 'AVANTE', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 264 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01055, 1055, 'HI', 'HYUNDAI', 'VELOSTER', 'COUPE 1.6L GASOLINA STD.', ' 04 OCUP.', 'VELOSTER', 100, 01, 'S', 04, 3, 4, '', 01, 1112, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 265 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01056, 1056, 'HI', 'HYUNDAI', 'AVANTE', 'SEDAN 1.8L GASOLINA STD.', ' 05 OCUP.', 'AVANTE', 100, 01, 'S', 04, 5, 5, '', 01, 1112, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 266 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01058, 1058, 'HI', 'HYUNDAI', 'TERRACAN', '2.7L 4X4 STD.', ' 06 OCUP.', 'TERRACAN', 105, 22, 'S', 04, 5, 6, '', 01, 1112, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 267 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01060, 1060, 'HI', 'HYUNDAI', 'ACCENT', 'HB 1.6L GASOLINA STD.', ' 05 OCUP.', 'ACCENT', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 268 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01061, 1061, 'HI', 'HYUNDAI', 'SANTA FE', '2.4L 4X2 GASOLINA STD.', ' 05 OCUP.', 'SANTA FE', 105, 22, 'S', 04, 4, 5, '', 01, 1112, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 269 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01062, 1062, 'HI', 'HYUNDAI', 'GETZ', 'HB 1.5L DIESEL STD.', ' 05 OCUP.', 'GETZ', 100, 27, 'S', 04, 5, 5, '', 01, 1212, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 270 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01063, 1063, 'HI', 'HYUNDAI', 'I40', '2.0L GASOLINA AUT.', ' 05 OCUP.', 'I40', 100, 01, 'A', 04, 5, 5, '', 01, 1402, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 271 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01064, 1064, 'HI', 'HYUNDAI', 'CRETA', '1.6L 4X2 GASOLINA STD.', ' 05 OCUP.', 'CRETA', 105, 22, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 272 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01065, 1065, 'HI', 'HYUNDAI', 'I20', '1.2L 4X2 GASOLINA STD.', ' 05 OCUP.', 'I20', 100, 01, 'S', 04, 5, 5, '', 01, 1501, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 273 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01066, 1066, 'HI', 'HYUNDAI', 'CRETA GL', '1.6L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'CRETA GL', 105, 22, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 274 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01067, 1067, 'HI', 'HYUNDAI', 'TUCSON GL', '2.0L 4X4 DIESEL AUT.', ' 05 OCUP.', 'TUCSON GL', 105, 22, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 275 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01068, 1068, 'HI', 'HYUNDAI', 'TUCSON GL', '2.0L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'TUCSON GL', 105, 22, 'A', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 276 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01069, 1069, 'HI', 'HYUNDAI', 'STAREX', 'GRX 2.5L 4X4 DIESEL STD.', ' 09 OCUP.', 'STAREX', 105, 11, 'S', 04, 3, 9, '', 01, 1601, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 277 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01070, 1070, 'HI', 'HYUNDAI', 'IONIQ', 'HYBRID 1.6L GASOLINA AUT.', ' 05 OCUP.', 'IONIQ', 100, 27, 'A', 04, 5, 5, '', 01, 1601, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 278 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01071, 1071, 'HI', 'HYUNDAI', 'GRAND I10', 'GLS 1.3L GASOLINA STD.', ' 05 OCUP.', 'GRAND I10', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 279 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01072, 1072, 'HI', 'HYUNDAI', 'GRACE', 'SALOOM 2.5L DIESEL STD.', ' 15 OCUP.', 'GRACE', 105, 11, 'S', 04, 3, 15, '', 01, 1601, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 280 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01073, 1073, 'HI', 'HYUNDAI', 'H1', '2.5L DIESEL AUT.', ' 12 OCUP.', 'H1', 105, 11, 'A', 04, 3, 12, '', 01, 1705, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 281 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01074, 1074, 'HI', 'HYUNDAI', 'ELANTRA', 'GLS 1.6L 4X2 DIESEL STD.', ' 05 OCUP.', 'ELANTRA', 100, 01, 'S', 04, 4, 5, '', 01, 1705, 0001, 0001, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 282 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01075, 1075, 'HI', 'HYUNDAI', 'STAREX', 'SVX 2.5L 4X2 DIESEL STD.', ' 12 OCUP.', 'STAREX', 105, 11, 'S', 04, 3, 12, '', 01, 1705, 0003, 0003, '', 'HYUNDAI'
);

/* INSERT QUERY NO: 283 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01501, 1501, 'HA', 'HONDA', 'ACCORD', 'SEDAN 2.4L GASOLINA STD.', ' 05 OCUP.', 'ACCORD', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 284 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01502, 1502, 'HA', 'HONDA', 'ACCORD', 'SEDAN 2.4L GASOLINA AUT.', ' 05 OCUP.', 'ACCORD', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 285 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01505, 1505, 'HA', 'HONDA', 'ACCORD', 'SEDAN 1.8L GASOLINA AUT.', ' 05 OCUP.', 'ACCORD', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 286 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01506, 1506, 'HA', 'HONDA', 'CIVIC', 'COUPE 2.0L GASOLINA STD.', ' 05 OCUP.', 'CIVIC', 100, 18, 'S', 04, 2, 5, '', 01, 1003, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 287 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01507, 1507, 'HA', 'HONDA', 'CIVIC', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'CIVIC', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 288 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01508, 1508, 'HA', 'HONDA', 'CIVIC', 'COUPE 1.8L GASOLINA STD.', ' 05 OCUP.', 'CIVIC', 100, 18, 'S', 04, 2, 5, '', 01, 1003, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 289 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01509, 1509, 'HA', 'HONDA', 'CIVIC', 'COUPE 1.8L GASOLINA AUT.', ' 05 OCUP.', 'CIVIC', 100, 18, 'A', 04, 2, 5, '', 01, 1003, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 290 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01510, 1510, 'HA', 'HONDA', 'CIVIC', 'SEDAN 1.8L GASOLINA STD.', ' 05 OCUP.', 'CIVIC', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 291 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01511, 1511, 'HA', 'HONDA', 'CIVIC', 'SEDAN 1.8L GASOLINA AUT.', ' 05 OCUP.', 'CIVIC', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 292 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01514, 1514, 'HA', 'HONDA', 'CR-V', '2.4L 4X2 GASOLINA STD STD.', ' 05 OCUP.', 'CR-V', 105, 22, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 293 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01515, 1515, 'HA', 'HONDA', 'CR-V', '2.4L 4X4 GASOLINA FULL STD.', ' 05 OCUP.', 'CR-V', 105, 22, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 294 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01516, 1516, 'HA', 'HONDA', 'FIT', 'SEDAN 1.4L GASOLINA STD.', ' 05 OCUP.', 'FIT', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 295 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01517, 1517, 'HA', 'HONDA', 'FIT', 'SEDAN 1.4L GASOLINA FULL AUT.', ' 05 OCUP.', 'FIT', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 296 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01518, 1518, 'HA', 'HONDA', 'LEGEND', 'SEDAN 1.9L V6 GASOLINA AUT.', ' 05 OCUP.', 'LEGEND', 100, 01, 'A', 06, 4, 5, '', 01, 1003, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 297 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01519, 1519, 'HA', 'HONDA', 'ODYSSEY', '3.5L V6 GASOLINA FULL AUT.', ' 07 OCUP.', 'ODYSSEY', 105, 24, 'A', 06, 4, 7, '', 01, 1003, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 298 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01520, 1520, 'HA', 'HONDA', 'PILOT', '3.5L 4X4 GASOLINA AUT.', ' 08 OCUP.', 'PILOT', 105, 22, 'A', 06, 4, 8, '', 01, 1003, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 299 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01521, 1521, 'HA', 'HONDA', 'CITY', 'SEDAN 2.0L GASOLINA AUT.', ' 05 OCUP.', 'CITY', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 300 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01522, 1522, 'HA', 'HONDA', 'PRELUDE', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'PRELUDE', 100, 01, 'S', 04, 2, 5, '', 01, 1003, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 301 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01523, 1523, 'HA', 'HONDA', 'CITY', 'SEDAN 1.5L GASOLINA FULL STD.', ' 05 OCUP.', 'CITY', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 302 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01524, 1524, 'HA', 'HONDA', 'ELEMENT', '2.4L 4X4 GASOLINA FULL STD.', ' 04 OCUP.', 'ELEMENT', 105, 22, 'S', 04, 5, 4, '', 01, 1112, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 303 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01525, 1525, 'HA', 'HONDA', 'CR-V', 'LX 2.4L 4X2 GASOLINA STD.', ' 05 OCUP.', 'CR-V', 105, 22, 'S', 04, 5, 5, '', 01, 1212, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 304 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01526, 1526, 'HA', 'HONDA', 'CROSSTOUR', '1.8L 4X4 SUPER FULL GASOLINA STD.', ' 05 OCUP.', 'CROSSTOUR', 100, 01, 'S', 06, 5, 5, '', 01, 1212, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 305 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01527, 1527, 'HA', 'HONDA', 'HR-V', '1.8L 4X2 GASOLINA STD.', ' 05 OCUP.', 'HR-V', 105, 22, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 306 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01528, 1528, 'HA', 'HONDA', 'PASSPORT LX', '1.6L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'PASSPORT LX', 105, 22, 'A', 06, 4, 5, '', 01, 1501, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 307 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01529, 1529, 'HA', 'HONDA', 'HR-V', 'EXL 1.8L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'HR-V', 105, 22, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 308 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01530, 1530, 'HA', 'HONDA', 'S 2000', '2.2L 4X2 GASOLINA STD.', ' 02 OCUP.', 'S 2000', 100, 04, 'S', 04, 2, 2, '', 01, 1601, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 309 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01531, 1531, 'HA', 'HONDA', 'CR-V', 'EXL 2.4L 4X4 GASOLINA STD.', ' 05 OCUP.', 'CR-V', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 310 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01532, 1532, 'HA', 'HONDA', 'CIVIC', 'EXT-L 1.5L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'CIVIC', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 311 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01533, 1533, 'HA', 'HONDA', 'ELEMENT', 'DX 2.4L 4X2 GASOLINA STD.', ' 05 OCUP.', 'ELEMENT', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 312 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01534, 1534, 'HA', 'HONDA', 'CIVIC', 'TYPE-R 2.0L GASOLINA STD.', ' 04 OCUP.', 'CIVIC', 100, 27, 'S', 04, 5, 4, '', 01, 1601, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 313 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01535, 1535, 'HA', 'HONDA', 'INSIGHT', 'LX 1.3L GASOLINA AUT.', ' 05 OCUP.', 'INSIGHT', 100, 27, 'A', 04, 5, 5, '', 01, 1705, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 314 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 01536, 1536, 'HA', 'HONDA', 'CR-V', 'EX 2.4L 4X2 GASOLINA STD.', ' 05 OCUP.', 'CR-V', 105, 22, 'S', 04, 4, 5, '', 01, 1705, 0003, 0003, '', 'HONDA'
);

/* INSERT QUERY NO: 315 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02001, 2001, 'MI', 'MITSUBISHI', 'MIRAGE', 'SEDAN 1.5L FULL STD.', ' 05 OCUP.', 'MIRAGE', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 316 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02002, 2002, 'MI', 'MITSUBISHI', 'LANCER', 'SEDAN 1.5L GASOLINA FULL STD.', ' 05 OCUP.', 'LANCER', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 317 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02003, 2003, 'MI', 'MITSUBISHI', 'LANCER', 'SEDAN 1.5L GASOLINA STD.', ' 05 OCUP.', 'LANCER', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 318 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02005, 2005, 'MI', 'MITSUBISHI', 'LANCER', 'SEDAN 2.0L GASOLINA AUT.', ' 05 OCUP.', 'LANCER', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 319 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02007, 2007, 'MI', 'MITSUBISHI', 'LANCER', 'SEDAN 1.6L GASOLINA AUT.', ' 05 OCUP.', 'LANCER', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 320 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02009, 2009, 'MI', 'MITSUBISHI', 'MONTERO', '3.8L V6 4X4 GASOLINA STD.', ' 05 OCUP.', 'MONTERO', 105, 22, 'S', 06, 4, 5, '', 01, 1003, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 321 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02010, 2010, 'MI', 'MITSUBISHI', 'MONTERO', '2.8L 4X4 DIESEL STD.', ' 05 OCUP.', 'MONTERO', 105, 22, 'S', 04, 4, 5, '', 01, 1003, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 322 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02011, 2011, 'MI', 'MITSUBISHI', 'MONTERO', '2.8L 4X4 DIESEL FULL STD.', ' 07 OCUP.', 'MONTERO', 105, 22, 'S', 04, 4, 7, '', 01, 1003, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 323 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02013, 2013, 'MI', 'MITSUBISHI', 'MONTERO', '2.8L 4X4 DIESEL FULL AUT.', ' 05 OCUP.', 'MONTERO', 105, 22, 'A', 04, 4, 5, '', 01, 1003, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 324 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02014, 2014, 'MI', 'MITSUBISHI', 'MONTERO', '3.0L V6 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', 'MONTERO', 105, 22, 'A', 06, 5, 5, '', 01, 1003, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 325 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02015, 2015, 'MI', 'MITSUBISHI', 'MONTERO', '3.0L V6 4X4 GASOLINA FULL STD.', ' 05 OCUP.', 'MONTERO', 105, 22, 'S', 06, 5, 5, '', 01, 1003, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 326 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02020, 2020, 'MI', 'MITSUBISHI', 'MONTERO', '2.8L V6 4X4 DIESEL AUT.', ' 05 OCUP.', 'MONTERO', 105, 22, 'A', 04, 5, 5, '', 01, 1003, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 327 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02023, 2023, 'MI', 'MITSUBISHI', 'OUTLANDER', '2.4L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'OUTLANDER', 105, 22, 'A', 04, 5, 5, '', 01, 1003, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 328 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02024, 2024, 'MI', 'MITSUBISHI', 'OUTLANDER', '3.0L V6 4X4 GASOLINA AUT.', ' 05 OCUP.', 'OUTLANDER', 105, 22, 'A', 06, 5, 5, '', 01, 1003, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 329 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02025, 2025, 'MI', 'MITSUBISHI', 'ZINGER', '2.4L 4X2 GASOLINA FULL AUT.', ' 05 OCUP.', 'ZINGER', 105, 22, 'A', 04, 4, 5, '', 01, 1003, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 330 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02026, 2026, 'MI', 'MITSUBISHI', 'ZINGER', '2.4L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'ZINGER', 105, 22, 'A', 04, 4, 5, '', 01, 1003, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 331 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02028, 2028, 'MI', 'MITSUBISHI', 'NATIVA', '3.0L V6 4X4 GASOLINA AUT.', ' 05 OCUP.', 'NATIVA', 105, 22, 'A', 06, 4, 5, '', 01, 1106, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 332 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02029, 2029, 'MI', 'MITSUBISHI', 'NATIVA', 'GLS 2.8L 4X4 DIESEL AUT.', ' 05 OCUP.', 'NATIVA', 105, 22, 'A', 06, 4, 5, '', 01, 1106, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 333 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02030, 2030, 'MI', 'MITSUBISHI', 'EAGLE SUMMIT', '.8L GASOLINA STD.', ' 05 OCUP.', 'EAGLE SUMMIT', 100, 02, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 334 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02032, 2032, 'MI', 'MITSUBISHI', 'GALANT', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'GALANT', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 335 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02033, 2033, 'MI', 'MITSUBISHI', 'ECLIPSE', 'SEDAN 2.4L GASOLINA STD.', ' 05 OCUP.', 'ECLIPSE', 100, 01, 'S', 04, 2, 5, '', 01, 1106, 0001, 0001, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 336 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02034, 2034, 'MI', 'MITSUBISHI', 'ASX', '2.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'ASX', 105, 22, 'S', 04, 4, 5, '', 01, 1106, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 337 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02035, 2035, 'MI', 'MITSUBISHI', 'LANCER', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'LANCER', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 338 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02036, 2036, 'MI', 'MITSUBISHI', 'LANCER', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'LANCER', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 339 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02039, 2039, 'MI', 'MITSUBISHI', 'PAJERO', '2.6L 4X4 DIESEL STD.', ' 05 OCUP.', 'PAJERO', 105, 22, 'S', 04, 5, 5, '', 01, 1112, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 340 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02040, 2040, 'MI', 'MITSUBISHI', 'MIEV', 'ELECTRICO 16 KWH 330V LITIO AUT.', ' 05 OCUP.', 'MIEV', 100, 01, 'A', 00, 0, 5, '', 01, 1212, 0001, 0001, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 341 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02041, 2041, 'MI', 'MITSUBISHI', 'LIONCEL', 'SEDAN 1.5L V4 GASOLINA STD.', ' 05 OCUP.', 'LIONCEL', 100, 01, 'S', 04, 4, 5, '', 01, 1212, 0001, 0001, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 342 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02043, 2043, 'MI', 'MITSUBISHI', 'ASX', 'GLS 2.0L 4X4 GASOLINA STD.', ' 05 OCUP.', 'ASX', 105, 22, 'S', 04, 5, 5, '', 01, 1212, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 343 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02044, 2044, 'MI', 'MITSUBISHI', 'PRECIS', '1.4L GASOLINA STD.', ' 05 OCUP.', 'PRECIS', 105, 27, 'S', 04, 2, 5, '', 01, 1212, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 344 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02045, 2045, 'MI', 'MITSUBISHI', 'MIRAGE', '1.2L GASOLINA STD.', ' 05 OCUP.', 'MIRAGE', 100, 01, 'S', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 345 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02046, 2046, 'MI', 'MITSUBISHI', 'ENDAVOR', 'LS 3.8L 4X4 GASOLINA STD.', ' 05 OCUP.', 'ENDAVOR', 105, 22, 'S', 06, 4, 5, '', 01, 1402, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 346 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02047, 2047, 'MI', 'MITSUBISHI', 'OUTLANDER', '2.4L 4X2 GASOLINA STD.', ' 05 OCUP.', 'OUTLANDER', 105, 22, 'S', 04, 5, 5, '', 01, 1402, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 347 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02048, 2048, 'MI', 'MITSUBISHI', 'MONTERO', '2.5L 4X2 DIESEL FULL STD.', ' 05 OCUP.', 'MONTERO', 105, 22, 'S', 04, 4, 5, '', 01, 1412, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 348 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02049, 2049, 'MI', 'MITSUBISHI', 'EXPO', '2.4L 4X2 GASOLINA STD.', ' 05 OCUP.', 'EXPO', 105, 22, 'S', 04, 4, 5, '', 01, 1501, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 349 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02050, 2050, 'MI', 'MITSUBISHI', 'FREECA', 'GLX 2.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'FREECA', 105, 22, 'S', 04, 4, 5, '', 01, 1501, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 350 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02051, 2051, 'MI', 'MITSUBISHI', 'GRUNDER', '2.4L 4X2 GASOLINA STD.', ' 05 OCUP.', 'GRUNDER', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 351 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02052, 2052, 'MI', 'MITSUBISHI', 'COLT PLUS', '1.6L 4X2 GASOLINA STD.', ' 05 OCUP.', 'COLT PLUS', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 352 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02053, 2053, 'MI', 'MITSUBISHI', 'MONTERO', 'SPORT 2.4L 4X4 DIESEL STD.', ' 05 OCUP.', 'MONTERO', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 353 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02054, 2054, 'MI', 'MITSUBISHI', 'MONTERO', 'SPORT 3.5L V6 GASOLINA STD.', ' 05 OCUP.', 'MONTERO', 105, 22, 'S', 06, 5, 5, '', 01, 1601, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 354 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02055, 2055, 'MI', 'MITSUBISHI', '3000 GT', '3.0L 4X2 GASOLINA STD.', ' 04 OCUP.', '3000 GT', 100, 01, 'S', 06, 2, 4, '', 01, 1601, 0001, 0001, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 355 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02056, 2056, 'MI', 'MITSUBISHI', 'LANCER', 'EVOLUTION 2.0L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'LANCER', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 356 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02057, 2057, 'MI', 'MITSUBISHI', 'MONTERO', 'SPORT 2.5L 4X2 DIESEL STD.', ' 07 OCUP.', 'MONTERO', 105, 22, 'S', 04, 4, 7, '', 01, 1705, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 357 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02058, 2058, 'MI', 'MITSUBISHI', 'MONTERO', 'SPORT 2.5L 4X2 DIESEL AUT.', ' 07 OCUP.', 'MONTERO', 105, 22, 'A', 04, 4, 7, '', 01, 1705, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 358 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02059, 2059, 'MI', 'MITSUBISHI', 'I MIEV', 'SEDAN ELECTRICO AUT.', ' 04 OCUP.', 'I MIEV', 100, 01, 'A', 00, 4, 4, '', 01, 1705, 0001, 0001, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 359 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02060, 2060, 'MI', 'MITSUBISHI', 'OUTLANDER', 'HIBRIDO 2.0L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'OUTLANDER', 105, 22, 'A', 04, 5, 5, '', 01, 1705, 0002, 0002, '', 'MITSUBISHI'
);

/* INSERT QUERY NO: 360 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02501, 2501, 'SI', 'SUZUKI', 'SWIFT', 'HB 1.3L GASOLINA STD.', ' 05 OCUP.', 'SWIFT', 100, 27, 'S', 04, 5, 5, '', 01, 1003, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 361 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02502, 2502, 'SI', 'SUZUKI', 'SWIFT', 'HB 1.5L GASOLINA STD.', ' 05 OCUP.', 'SWIFT', 100, 27, 'S', 04, 5, 5, '', 01, 1003, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 362 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02503, 2503, 'SI', 'SUZUKI', 'SWIFT', 'HB 1.5L GASOLINA AUT.', ' 05 OCUP.', 'SWIFT', 100, 27, 'A', 04, 5, 5, '', 01, 1003, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 363 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02504, 2504, 'SI', 'SUZUKI', 'SX4', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'SX4', 100, 27, 'S', 04, 5, 5, '', 01, 1003, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 364 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02505, 2505, 'SI', 'SUZUKI', 'SX4', 'SEDAN 1.6L GASOLINA AUT.', ' 05 OCUP.', 'SX4', 100, 27, 'A', 04, 5, 5, '', 01, 1003, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 365 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02506, 2506, 'SI', 'SUZUKI', 'CELERIO', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'CELERIO', 100, 27, 'S', 03, 5, 5, '', 01, 1003, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 366 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02507, 2507, 'SI', 'SUZUKI', 'CELERIO', 'SEDAN 1.6L GASOLINA AUT.', ' 05 OCUP.', 'CELERIO', 100, 27, 'A', 03, 5, 5, '', 01, 1003, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 367 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02508, 2508, 'SI', 'SUZUKI', 'GRAND VITARA', '3.2L 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', 'GRAND VITARA', 105, 22, 'A', 04, 5, 5, '', 01, 1003, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 368 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02509, 2509, 'SI', 'SUZUKI', 'GRAND VITARA', '1.6L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'GRAND VITARA', 105, 22, 'A', 04, 3, 5, '', 01, 1003, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 369 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02510, 2510, 'SI', 'SUZUKI', 'GRAND VITARA', '2.4L 4X4 GASOLINA FULL STD.', ' 05 OCUP.', 'GRAND VITARA', 105, 22, 'S', 04, 5, 5, '', 01, 1003, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 370 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02511, 2511, 'SI', 'SUZUKI', 'GRAND VITARA', '2.4L 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', 'GRAND VITARA', 105, 22, 'A', 04, 5, 5, '', 01, 1003, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 371 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02512, 2512, 'SI', 'SUZUKI', 'JIMNY', '1.3L 4X4 GASOLINA FULL STD.', ' 04 OCUP.', 'JIMNY', 105, 22, 'S', 04, 3, 4, '', 01, 1003, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 372 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02513, 2513, 'SI', 'SUZUKI', 'SIDEKICK', '1.6L 4X2 GASOLINA STD.', ' 05 OCUP.', 'SIDEKICK', 105, 22, 'S', 04, 4, 5, '', 01, 1106, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 373 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02514, 2514, 'SI', 'SUZUKI', 'ALTO', 'SEDAN 1.0L GASOLINA STD.', ' 05 OCUP.', 'ALTO', 100, 01, 'S', 03, 4, 5, '', 01, 1106, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 374 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02515, 2515, 'SI', 'SUZUKI', 'SAMURAI', '1.3L 4X4 GASOLINA STD.', ' 05 OCUP.', 'SAMURAI', 105, 03, 'S', 04, 4, 5, '', 01, 1106, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 375 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02516, 2516, 'SI', 'SUZUKI', 'AERIO', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'AERIO', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 376 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02517, 2517, 'SI', 'SUZUKI', 'IGNIS', 'SEDAN 1.3L GASOLINA AUT.', ' 05 OCUP.', 'IGNIS', 100, 01, 'A', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 377 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02518, 2518, 'SI', 'SUZUKI', 'SIDEKICK', '1.8L 4X4 GASOLINA STD.', ' 05 OCUP.', 'SIDEKICK', 105, 22, 'S', 04, 5, 5, '', 01, 1106, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 378 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02520, 2520, 'SI', 'SUZUKI', 'BALENO', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'BALENO', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 379 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02521, 2521, 'SI', 'SUZUKI', 'ESTEEM', 'SEDAN 1.8L GASOLINA STD.', ' 05 OCUP.', 'ESTEEM', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 380 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02522, 2522, 'SI', 'SUZUKI', 'GRAND VITARA', '2.0L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'GRAND VITARA', 105, 22, 'A', 04, 5, 5, '', 01, 1112, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 381 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02523, 2523, 'SI', 'SUZUKI', 'XL7', '3.6L V6 GASOLINA AUT.', ' 07 OCUP.', 'XL7', 105, 22, 'A', 06, 5, 7, '', 01, 1112, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 382 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02524, 2524, 'SI', 'SUZUKI', 'GRAND VITARA', '2.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'GRAND VITARA', 105, 22, 'S', 04, 4, 5, '', 01, 1112, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 383 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02525, 2525, 'SI', 'SUZUKI', 'APV', '1.6L GASOLINA STD.', ' 07 OCUP.', 'APV', 100, 02, 'S', 04, 5, 7, '', 01, 1112, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 384 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02526, 2526, 'SI', 'SUZUKI', 'GRAND VITARA', '2.5L V6 4X4 GASOLINA AUT.', ' 05 OCUP.', 'GRAND VITARA', 105, 22, 'A', 06, 5, 5, '', 01, 1112, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 385 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02527, 2527, 'SI', 'SUZUKI', 'FORENZA', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'FORENZA', 100, 01, 'S', 04, 4, 5, '', 01, 1212, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 386 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02528, 2528, 'SI', 'SUZUKI', 'MARUTI', 'STATION WAGON 0.8L GASOLINA STD.', ' 05 OCUP.', 'MARUTI', 100, 02, 'S', 03, 5, 5, '', 01, 1212, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 387 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02529, 2529, 'SI', 'SUZUKI', 'KIZASHI', '2.4L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'KIZASHI', 100, 01, 'A', 04, 5, 5, '', 01, 1402, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 388 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02530, 2530, 'SI', 'SUZUKI', 'ERTIGA', '1.4L GASOLINA AUT.', ' 07 OCUP.', 'ERTIGA', 105, 22, 'A', 04, 4, 7, '', 01, 1402, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 389 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02531, 2531, 'SI', 'SUZUKI', 'S-CROSS', 'HB 1.6L GASOLINA STD.', ' 05 OCUP.', 'S-CROSS', 105, 22, 'S', 04, 4, 5, '', 01, 1402, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 390 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02532, 2532, 'SI', 'SUZUKI', 'CIAZ', 'GLX 1.2L GASOLINA AUT.', ' 05 OCUP.', 'CIAZ', 100, 01, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 391 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02533, 2533, 'SI', 'SUZUKI', 'RENO', '2.0L GASOLINA STD.', ' 05 OCUP.', 'RENO', 100, 27, 'S', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 392 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02534, 2534, 'SI', 'SUZUKI', 'WAGON', 'R 1.2L 4X2 GASOLINA AUT.', ' 04 OCUP.', 'WAGON', 105, 22, 'A', 04, 4, 4, '', 01, 1501, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 393 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02535, 2535, 'SI', 'SUZUKI', 'DZIRE', 'SEDAN 1.2L GASOLINA STD.', ' 05 OCUP.', 'DZIRE', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 394 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02536, 2536, 'SI', 'SUZUKI', 'S-CROSS', 'GL SEDAN 1.6L 4X2 GASOLINA STD.', ' 05 OCUP.', 'S-CROSS', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 395 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02537, 2537, 'SI', 'SUZUKI', 'RS', '413 GL 1.3L 4X2 GASOLINA STD.', ' 05 OCUP.', 'RS', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 396 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02538, 2538, 'SI', 'SUZUKI', 'VITARA', 'GL 1.6L 4X2 GASOLINA STD.', ' 05 OCUP.', 'VITARA', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 397 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02539, 2539, 'SI', 'SUZUKI', 'VITARA', 'TURBO 1.4L 4X4 GASOLINA STD.', ' 05 OCUP.', 'VITARA', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 398 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02540, 2540, 'SI', 'SUZUKI', 'CARRY', '1.3L 4X2 GASOLINA STD.', ' 07 OCUP.', 'CARRY', 105, 11, 'S', 04, 3, 7, '', 01, 1601, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 399 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02541, 2541, 'SI', 'SUZUKI', 'VITARA', 'GLX 1.6L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'VITARA', 105, 22, 'A', 04, 4, 5, '', 01, 1601, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 400 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02542, 2542, 'SI', 'SUZUKI', 'VITARA', 'GL PLUS 1.6L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'VITARA', 105, 22, 'A', 04, 4, 5, '', 01, 1601, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 401 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02543, 2543, 'SI', 'SUZUKI', 'ALTO', '800 1.0L GASOLINA STD.', ' 04 OCUP.', 'ALTO', 100, 27, 'S', 03, 5, 4, '', 01, 1601, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 402 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02544, 2544, 'SI', 'SUZUKI', 'ALTO', 'K10 1.0L GASOLINA STD.', ' 04 OCUP.', 'ALTO', 100, 27, 'S', 03, 5, 4, '', 01, 1601, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 403 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02545, 2545, 'SI', 'SUZUKI', 'BALENO', 'GL 1.4L GASOLINA STD.', ' 05 OCUP.', 'BALENO', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 404 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02546, 2546, 'SI', 'SUZUKI', 'BALENO', 'GLX 1.4L GASOLINA AUT.', ' 05 OCUP.', 'BALENO', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 405 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02547, 2547, 'SI', 'SUZUKI', 'BALENO', 'GLX 1.4L GASOLINA STD.', ' 05 OCUP.', 'BALENO', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 406 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02548, 2548, 'SI', 'SUZUKI', 'CIAZ', 'GL PLUS 1.4L GASOLINA STD.', ' 05 OCUP.', 'CIAZ', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 407 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02549, 2549, 'SI', 'SUZUKI', 'CIAZ', 'GLX 1.4L GASOLINA STD.', ' 05 OCUP.', 'CIAZ', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 408 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02550, 2550, 'SI', 'SUZUKI', 'SWIFT', 'DZIRE GL 1.2L GASOLINA AUT.', ' 05 OCUP.', 'SWIFT', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 409 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02551, 2551, 'SI', 'SUZUKI', 'SWIFT', 'DZIRE GL 1.2L GASOLINA STD.', ' 05 OCUP.', 'SWIFT', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 410 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02552, 2552, 'SI', 'SUZUKI', 'ERTIGA', 'GLX 1.4L GASOLINA AUT.', ' 07 OCUP.', 'ERTIGA', 105, 24, 'A', 04, 5, 7, '', 01, 1601, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 411 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02553, 2553, 'SI', 'SUZUKI', 'ERTIGA', 'GLX 1.4L GASOLINA STD.', ' 07 OCUP.', 'ERTIGA', 105, 24, 'S', 04, 5, 7, '', 01, 1601, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 412 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02554, 2554, 'SI', 'SUZUKI', 'GRAND VITARA', '2.4L 4X2 GASOLINA STD.', ' 05 OCUP.', 'GRAND VITARA', 105, 22, 'S', 04, 5, 5, '', 01, 1601, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 413 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02555, 2555, 'SI', 'SUZUKI', 'GRAND VITARA', '2.4L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'GRAND VITARA', 105, 22, 'A', 04, 5, 5, '', 01, 1601, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 414 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02556, 2556, 'SI', 'SUZUKI', 'IGNIS', 'GLX HB 1.2L GASOLINA AUT.', ' 05 OCUP.', 'IGNIS', 105, 22, 'A', 04, 5, 5, '', 01, 1601, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 415 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02557, 2557, 'SI', 'SUZUKI', 'IGNIS', 'GLX HB 1.2L GASOLINA STD.', ' 05 OCUP.', 'IGNIS', 105, 22, 'S', 04, 5, 5, '', 01, 1601, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 416 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02558, 2558, 'SI', 'SUZUKI', 'JIMNY', 'JX 1.3L GASOLINA STD.', ' 04 OCUP.', 'JIMNY', 105, 22, 'S', 04, 5, 4, '', 01, 1601, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 417 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02559, 2559, 'SI', 'SUZUKI', 'CELERIO', 'GL 1.0L GASOLINA AUT.', ' 05 OCUP.', 'CELERIO', 100, 27, 'A', 03, 5, 5, '', 01, 1601, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 418 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02560, 2560, 'SI', 'SUZUKI', 'CELERIO', 'GL 1.0L GASOLINA STD.', ' 05 OCUP.', 'CELERIO', 100, 27, 'S', 03, 5, 5, '', 01, 1601, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 419 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02561, 2561, 'SI', 'SUZUKI', 'S-CROSS', 'GL HB 1.6L GASOLINA AUT.', ' 05 OCUP.', 'S-CROSS', 105, 22, 'A', 04, 5, 5, '', 01, 1601, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 420 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02562, 2562, 'SI', 'SUZUKI', 'SWIFT', 'REVOLUTION GL 1.2L GASOLINA AUT.', ' 05 OCUP.', 'SWIFT', 100, 27, 'A', 04, 5, 5, '', 01, 1601, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 421 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02563, 2563, 'SI', 'SUZUKI', 'SWIFT', 'REVOLUTION GL 1.2L GASOLINA STD.', ' 05 OCUP.', 'SWIFT', 100, 27, 'S', 04, 5, 5, '', 01, 1601, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 422 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02564, 2564, 'SI', 'SUZUKI', 'SWIFT', 'REVOLUTION GLX 1.2L GASOLINA AUT.', ' 05 OCUP.', 'SWIFT', 100, 27, 'A', 04, 5, 5, '', 01, 1601, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 423 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02565, 2565, 'SI', 'SUZUKI', 'SWIFT', 'REVOLUTION GLX 1.2L GASOLINA STD.', ' 05 OCUP.', 'SWIFT', 100, 27, 'S', 04, 5, 5, '', 01, 1601, 0003, 0003, '', 'SUZUKI'
);

/* INSERT QUERY NO: 424 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02566, 2566, 'SI', 'SUZUKI', 'VITARA', 'GL PLUS 1.6L 4X2 GASOLINA STD.', ' 05 OCUP.', 'VITARA', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 425 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02567, 2567, 'SI', 'SUZUKI', 'VITARA', 'GL PLUS 1.6L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'VITARA', 105, 22, 'A', 04, 4, 5, '', 01, 1601, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 426 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 02568, 2568, 'SI', 'SUZUKI', 'VITARA', 'GLX 1.6L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'VITARA', 105, 22, 'A', 04, 4, 5, '', 01, 1601, 0002, 0002, '', 'SUZUKI'
);

/* INSERT QUERY NO: 427 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03001, 3001, 'MA', 'MAZDA', 'MAZDA 2', 'SEDAN 1.5L STD.', ' 05 OCUP.', 'MAZDA 2', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'MAZDA'
);

/* INSERT QUERY NO: 428 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03002, 3002, 'MA', 'MAZDA', 'MAZDA 3', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'MAZDA 3', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'MAZDA'
);

/* INSERT QUERY NO: 429 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03003, 3003, 'MA', 'MAZDA', 'MAZDA 6', 'SEDAN 2.0L STD.', ' 05 OCUP.', 'MAZDA 6', 100, 01, 'S', 06, 4, 5, '', 01, 1106, 0001, 0001, '', 'MAZDA'
);

/* INSERT QUERY NO: 430 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03004, 3004, 'MA', 'MAZDA', 'CX-7', '2.5L 4X2 FULL AUT.', ' 05 OCUP.', 'CX-7', 105, 22, 'A', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'MAZDA'
);

/* INSERT QUERY NO: 431 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03005, 3005, 'MA', 'MAZDA', 'CX-9', '3.8L 4X4 FULL AUT.', ' 07 OCUP.', 'CX-9', 105, 22, 'A', 06, 4, 7, '', 01, 1106, 0003, 0003, '', 'MAZDA'
);

/* INSERT QUERY NO: 432 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03006, 3006, 'MA', 'MAZDA', 'RX-8', 'COUPE 1.3L FULL AUT.', ' 04 OCUP.', 'RX-8', 100, 08, 'A', 06, 2, 4, '', 01, 1106, 0001, 0001, '', 'MAZDA'
);

/* INSERT QUERY NO: 433 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03007, 3007, 'MA', 'MAZDA', 'PROTEGE LX', 'AN 1.5L STD.', ' 05 OCUP.', 'PROTEGE LX', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'MAZDA'
);

/* INSERT QUERY NO: 434 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03008, 3008, 'MA', 'MAZDA', 'MIATA', 'MX5 1.6L GASOLINA STD.', ' 02 OCUP.', 'MIATA', 100, 08, 'S', 04, 2, 2, '', 01, 1106, 0001, 0001, '', 'MAZDA'
);

/* INSERT QUERY NO: 435 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03009, 3009, 'MA', 'MAZDA', 'CX-9', '3.7L 4X2 FULL AUT.', ' 07 OCUP.', 'CX-9', 105, 22, 'A', 06, 4, 7, '', 01, 1106, 0003, 0003, '', 'MAZDA'
);

/* INSERT QUERY NO: 436 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03010, 3010, 'MA', 'MAZDA', 'CX-7', '2.5L 4X4 FULL AUT.', ' 07 OCUP.', 'CX-7', 105, 22, 'A', 04, 5, 7, '', 01, 1106, 0003, 0003, '', 'MAZDA'
);

/* INSERT QUERY NO: 437 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03011, 3011, 'MA', 'MAZDA', 'MAZDA 2', 'SEDAN 1.5L FULL STD.', ' 05 OCUP.', 'MAZDA 2', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'MAZDA'
);

/* INSERT QUERY NO: 438 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03012, 3012, 'MA', 'MAZDA', 'MAZDA 3', 'SEDAN 1.6L FULL STD.', ' 05 OCUP.', 'MAZDA 3', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'MAZDA'
);

/* INSERT QUERY NO: 439 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03013, 3013, 'MA', 'MAZDA', '626', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', '626', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'MAZDA'
);

/* INSERT QUERY NO: 440 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03014, 3014, 'MA', 'MAZDA', '232', 'HB 1.6L FULL STD.', ' 05 OCUP.', '232', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'MAZDA'
);

/* INSERT QUERY NO: 441 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03015, 3015, 'MA', 'MAZDA', 'TRIBUTE', '3.0L V6 FULL STD.', ' 05 OCUP.', 'TRIBUTE', 105, 22, 'S', 04, 4, 5, '', 01, 1112, 0003, 0003, '', 'MAZDA'
);

/* INSERT QUERY NO: 442 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03016, 3016, 'MA', 'MAZDA', 'MVP', 'MINIVAN 2.5L V6 GASOLINA FULL AUT.', ' 07 OCUP.', 'MVP', 105, 02, 'A', 06, 4, 7, '', 01, 1112, 0003, 0003, '', 'MAZDA'
);

/* INSERT QUERY NO: 443 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03017, 3017, 'MA', 'MAZDA', '323', 'HB 1.6L GASOLINA FULL STD.', ' 05 OCUP.', '323', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'MAZDA'
);

/* INSERT QUERY NO: 444 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03018, 3018, 'MA', 'MAZDA', 'MAZDA 5', 'MINIVAN 2.5L GASOLINA FULL AUT.', ' 06 OCUP.', 'MAZDA 5', 100, 02, 'A', 04, 5, 6, '', 01, 1112, 0001, 0001, '', 'MAZDA'
);

/* INSERT QUERY NO: 445 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03019, 3019, 'MA', 'MAZDA', 'CX-5', '2.0L 4X2 FULL STD.', ' 05 OCUP.', 'CX-5', 105, 22, 'S', 04, 5, 5, '', 01, 1212, 0003, 0003, '', 'MAZDA'
);

/* INSERT QUERY NO: 446 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03020, 3020, 'MA', 'MAZDA', 'PREMACY', 'VAN 2.0L GASOLINA STD.', ' 07 OCUP.', 'PREMACY', 105, 11, 'S', 04, 5, 7, '', 01, 1212, 0003, 0003, '', 'MAZDA'
);

/* INSERT QUERY NO: 447 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03021, 3021, 'MA', 'MAZDA', 'CX-5', '2.0L 4X4 GASOLINA AUT.', ' 03 OCUP.', 'CX-5', 105, 22, 'A', 04, 4, 3, '', 01, 1212, 0003, 0003, '', 'MAZDA'
);

/* INSERT QUERY NO: 448 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03022, 3022, 'MA', 'MAZDA', 'MAZDA 6', '2.3L 4x4 STD.', ' 05 OCUP.', 'MAZDA 6', 100, 01, 'S', 06, 4, 5, '', 01, 1412, 0001, 0001, '', 'MAZDA'
);

/* INSERT QUERY NO: 449 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03023, 3023, 'MA', 'MAZDA', 'MILLENIA', '2.5L GASOLINA STD.', ' 05 OCUP.', 'MILLENIA', 100, 01, 'S', 06, 4, 5, '', 01, 1501, 0001, 0001, '', 'MAZDA'
);

/* INSERT QUERY NO: 450 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03024, 3024, 'MA', 'MAZDA', 'CX-3', '2.0L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'CX-3', 105, 22, 'A', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'MAZDA'
);

/* INSERT QUERY NO: 451 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03025, 3025, 'MA', 'MAZDA', 'E2200', '2.2L 4X2 DIESEL STD.', ' 15 OCUP.', 'E2200', 105, 11, 'S', 04, 2, 15, '', 01, 1601, 0003, 0003, '', 'MAZDA'
);

/* INSERT QUERY NO: 452 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03026, 3026, 'MA', 'MAZDA', 'MX-5', '2.0L 4X2 GASOLINA STD.', ' 02 OCUP.', 'MX-5', 100, 04, 'S', 04, 2, 2, '', 01, 1705, 0001, 0001, '', 'MAZDA'
);

/* INSERT QUERY NO: 453 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03501, 3501, 'FD', 'FORD', 'EDGE', '4WD 3.5L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'EDGE', 105, 22, 'A', 06, 4, 5, '', 01, 1003, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 454 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03502, 3502, 'FD', 'FORD', 'EDGE', '3.5L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'EDGE', 105, 22, 'A', 06, 4, 5, '', 01, 1003, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 455 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03503, 3503, 'FD', 'FORD', 'ESCAPE', '1.6L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'ESCAPE', 105, 22, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 456 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03504, 3504, 'FD', 'FORD', 'ESCAPE', '2.5L 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', 'ESCAPE', 105, 22, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 457 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03505, 3505, 'FD', 'FORD', 'EVEREST', '2.5L 4X2 DIESEL FULL AUT.', ' 07 OCUP.', 'EVEREST', 105, 22, 'A', 04, 4, 7, '', 01, 1003, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 458 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03506, 3506, 'FD', 'FORD', 'EVEREST', '2.5L 4X4 DIESEL FULL AUT.', ' 07 OCUP.', 'EVEREST', 105, 22, 'A', 04, 4, 7, '', 01, 1003, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 459 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03507, 3507, 'FD', 'FORD', 'EVEREST', '3.0L 4X4 DIESEL FULL AUT.', ' 07 OCUP.', 'EVEREST', 105, 22, 'A', 04, 4, 7, '', 01, 1003, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 460 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03508, 3508, 'FD', 'FORD', 'EXPLORER', '4.0L V6 4X2 GASOLINA FULL AUT.', ' 05 OCUP.', 'EXPLORER', 105, 22, 'A', 06, 4, 5, '', 01, 1003, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 461 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03509, 3509, 'FD', 'FORD', 'EXPLORER', '3.5L 4X2 GASOLINA FULL AUT.', ' 07 OCUP.', 'EXPLORER', 105, 22, 'A', 06, 4, 7, '', 01, 1003, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 462 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03510, 3510, 'FD', 'FORD', 'EXPLORER', '4.0L V6 4X4 GASOLINA FULL AUT.', ' 07 OCUP.', 'EXPLORER', 105, 22, 'A', 06, 4, 7, '', 01, 1003, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 463 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03511, 3511, 'FD', 'FORD', 'FUSION', 'SEDAN 2.5L GASOLINA FULL STD.', ' 05 OCUP.', 'FUSION', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'FORD'
);

/* INSERT QUERY NO: 464 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03513, 3513, 'FD', 'FORD', 'MUSTANG', 'SEDAN 4.0L V6 GASOLINA FULL STD.', ' 05 OCUP.', 'MUSTANG', 100, 18, 'S', 06, 2, 5, '', 01, 1003, 0001, 0001, '', 'FORD'
);

/* INSERT QUERY NO: 465 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03514, 3514, 'FD', 'FORD', 'MUSTANG', 'SEDAN 4.0L V6 GASOLINA FULL AUT.', ' 05 OCUP.', 'MUSTANG', 100, 18, 'A', 06, 2, 5, '', 01, 1003, 0001, 0001, '', 'FORD'
);

/* INSERT QUERY NO: 466 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03515, 3515, 'FD', 'FORD', 'MUSTANG', 'SEDAN 4.6L V8 GASOLINA FULL STD.', ' 05 OCUP.', 'MUSTANG', 100, 18, 'S', 08, 2, 5, '', 01, 1003, 0001, 0001, '', 'FORD'
);

/* INSERT QUERY NO: 467 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03516, 3516, 'FD', 'FORD', 'MUSTANG', 'SEDAN 4.6L V8 GASOLINA FULL AUT.', ' 05 OCUP.', 'MUSTANG', 100, 18, 'A', 08, 2, 5, '', 01, 1003, 0001, 0001, '', 'FORD'
);

/* INSERT QUERY NO: 468 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03517, 3517, 'FD', 'FORD', 'EXPEDITION', '5.4L V8 4X4 FULL STD.', ' 05 OCUP.', 'EXPEDITION', 105, 22, 'S', 06, 4, 5, '', 01, 1106, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 469 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03518, 3518, 'FD', 'FORD', 'ECOSPORT', 'TREND 1.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'ECOSPORT', 105, 22, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 470 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03519, 3519, 'FD', 'FORD', 'ASPIRE', 'HB 1.3L GASOLINA FULL STD.', ' 05 OCUP.', 'ASPIRE', 100, 01, 'S', 04, 2, 5, '', 01, 1106, 0001, 0001, '', 'FORD'
);

/* INSERT QUERY NO: 471 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03520, 3520, 'FD', 'FORD', 'FOCUS', 'ST TURBO SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'FOCUS', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'FORD'
);

/* INSERT QUERY NO: 472 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03521, 3521, 'FD', 'FORD', 'EXPLORER', '4WD 3.5L 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', 'EXPLORER', 105, 22, 'A', 06, 4, 5, '', 01, 1106, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 473 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03522, 3522, 'FD', 'FORD', 'CLUB WAGON', '5.8L V8 GASOLINA AUT.', ' 12 OCUP.', 'CLUB WAGON', 105, 11, 'A', 08, 5, 12, '', 01, 1106, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 474 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03524, 3524, 'FD', 'FORD', 'FESTIVA', 'HB 1.3L GASOLINA STD.', ' 05 OCUP.', 'FESTIVA', 100, 01, 'S', 04, 3, 5, '', 01, 1112, 0001, 0001, '', 'FORD'
);

/* INSERT QUERY NO: 475 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03525, 3525, 'FD', 'FORD', 'WINDSTAR', 'MINIVAN 3.8L V6 GASOLINA AUT.', ' 06 OCUP.', 'WINDSTAR', 105, 11, 'A', 06, 5, 6, '', 01, 1112, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 476 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03526, 3526, 'FD', 'FORD', 'FIESTA', 'HB 1.0L GASOLINA STD.', ' 05 OCUP.', 'FIESTA', 100, 01, 'S', 04, 2, 5, '', 01, 1112, 0001, 0001, '', 'FORD'
);

/* INSERT QUERY NO: 477 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03527, 3527, 'FD', 'FORD', 'ESCORT', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'ESCORT', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'FORD'
);

/* INSERT QUERY NO: 478 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03528, 3528, 'FD', 'FORD', 'BRONCO II', '2.9L V6 4X4 GASOLINA AUT.', ' 05 OCUP.', 'BRONCO II', 105, 22, 'A', 06, 3, 5, '', 01, 1112, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 479 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03529, 3529, 'FD', 'FORD', 'TAURUS', 'SEDAN 3.0L V6 GASOLINA AUT.', ' 05 OCUP.', 'TAURUS', 100, 01, 'A', 06, 3, 5, '', 01, 1212, 0001, 0001, '', 'FORD'
);

/* INSERT QUERY NO: 480 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03530, 3530, 'FD', 'FORD', 'MERCURY TRACER', 'SEDAN 1.9L GASOLINA AUT.', ' 05 OCUP.', 'MERCURY TRACER', 100, 01, 'A', 04, 4, 5, '', 01, 1212, 0001, 0001, '', 'FORD'
);

/* INSERT QUERY NO: 481 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03531, 3531, 'FD', 'FORD', 'ESCAPE', '4WD 1.6L GASOLINA AUT.', ' 05 OCUP.', 'ESCAPE', 105, 22, 'A', 04, 4, 5, '', 01, 1204, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 482 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03532, 3532, 'FD', 'FORD', 'EXPEDITION', '5.4L V8 4X2 STD.', ' 08 OCUP.', 'EXPEDITION', 105, 22, 'S', 08, 4, 8, '', 01, 1402, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 483 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03533, 3533, 'FD', 'FORD', 'MUSTANG', 'SEDAN 2.3L GASOLINA AUT.', ' 05 OCUP.', 'MUSTANG', 100, 01, 'A', 04, 4, 5, '', 01, 1412, 0001, 0001, '', 'FORD'
);

/* INSERT QUERY NO: 484 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03534, 3534, 'FD', 'FORD', 'FUSION', 'HYBRID 2.0L STD.', ' 05 OCUP.', 'FUSION', 100, 01, 'S', 04, 4, 5, '', 01, 1412, 0001, 0001, '', 'FORD'
);

/* INSERT QUERY NO: 485 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03535, 3535, 'FD', 'FORD', 'FIGO', 'SEDAN 1.5L GASOLINA STD.', ' 05 OCUP.', 'FIGO', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'FORD'
);

/* INSERT QUERY NO: 486 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03536, 3536, 'FD', 'FORD', 'EDGE', '2.0L GASOLINA AUT.', ' 05 OCUP.', 'EDGE', 105, 22, 'A', 04, 5, 5, '', 01, 1601, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 487 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03537, 3537, 'FD', 'FORD', 'EXPLORER', 'XLT 2.3L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'EXPLORER', 105, 22, 'A', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 488 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03538, 3538, 'FD', 'FORD', 'TRANSIT', '2.2L DIESEL AUT.', ' 15 OCUP.', 'TRANSIT', 105, 11, 'A', 04, 3, 15, '', 01, 1601, 0003, 0003, '', 'FORD'
);

/* INSERT QUERY NO: 489 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03901, 3901, 'CT', 'CHEVROLET', 'AVEO', 'SEDAN 1.5L GASOLINA STD.', ' 05 OCUP.', 'AVEO', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 490 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03902, 3902, 'CT', 'CHEVROLET', 'AVEO', 'SEDAN 1.6L GASOLINA AUT.', ' 05 OCUP.', 'AVEO', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 491 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03903, 3903, 'CT', 'CHEVROLET', 'AVEO', 'HB 1.5L GASOLINA STD.', ' 05 OCUP.', 'AVEO', 100, 27, 'S', 04, 5, 5, '', 01, 1003, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 492 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03904, 3904, 'CT', 'CHEVROLET', 'CAPTIVA', '2.4L 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', 'CAPTIVA', 105, 22, 'A', 04, 5, 5, '', 01, 1003, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 493 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03906, 3906, 'CT', 'CHEVROLET', 'EQUINOX', '3.4L V6 4X4 GASOLINA AUT.', ' 05 OCUP.', 'EQUINOX', 105, 22, 'A', 06, 5, 5, '', 01, 1003, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 494 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03907, 3907, 'CT', 'CHEVROLET', 'OPTRA', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'OPTRA', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 495 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03908, 3908, 'CT', 'CHEVROLET', 'OPTRA', 'SEDAN 1.6L GASOLINA AUT.', ' 05 OCUP.', 'OPTRA', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 496 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03909, 3909, 'CT', 'CHEVROLET', 'OPTRA', 'HB 1.6L GASOLINA STD.', ' 05 OCUP.', 'OPTRA', 100, 27, 'S', 04, 5, 5, '', 01, 1003, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 497 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03910, 3910, 'CT', 'CHEVROLET', 'SPARK', 'HB 1.0L GASOLINA STD.', ' 05 OCUP.', 'SPARK', 100, 27, 'S', 04, 5, 5, '', 01, 1003, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 498 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03911, 3911, 'CT', 'CHEVROLET', 'SPARK', 'HB 0.8L GASOLINA STD.', ' 05 OCUP.', 'SPARK', 100, 27, 'S', 03, 5, 5, '', 01, 1003, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 499 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03912, 3912, 'CT', 'CHEVROLET', 'TAHOE', '5.3L V8 4X4 GASOLINA AUT.', ' 07 OCUP.', 'TAHOE', 105, 22, 'A', 08, 5, 7, '', 01, 1003, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 500 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03915, 3915, 'CT', 'CHEVROLET', 'TRAVERSE', '3.6L V6 4X4 GASOLINA AUT.', ' 07 OCUP.', 'TRAVERSE', 105, 22, 'A', 06, 4, 7, '', 01, 1003, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 501 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03919, 3919, 'CT', 'CHEVROLET', 'BLAZER', '4.3L V6 4X2 GASOLINA STD.', ' 05 OCUP.', 'BLAZER', 105, 22, 'S', 06, 4, 5, '', 01, 1106, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 502 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03920, 3920, 'CT', 'CHEVROLET', 'CRUZE', 'SEDAN 1.8L GASOLINA STD.', ' 05 OCUP.', 'CRUZE', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 503 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03921, 3921, 'CT', 'CHEVROLET', 'CORSA', 'SEDAN 1.0L GASOLINA STD.', ' 05 OCUP.', 'CORSA', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 504 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03922, 3922, 'CT', 'CHEVROLET', 'CELTA', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'CELTA', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 505 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03924, 3924, 'CT', 'CHEVROLET', 'ASTRA', 'SEDAN 1.8L GASOLINA STD.', ' 05 OCUP.', 'ASTRA', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 506 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03925, 3925, 'CT', 'CHEVROLET', 'CHEVY MONZA', '1.6L GASOLINA STD.', ' 05 OCUP.', 'CHEVY MONZA', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 507 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03926, 3926, 'CT', 'CHEVROLET', 'CHEVY VAN', '5.7L V8 GASOLINA AUT.', ' 12 OCUP.', 'CHEVY VAN', 105, 11, 'A', 08, 4, 12, '', 01, 1112, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 508 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03927, 3927, 'CT', 'CHEVROLET', 'CAVALIER', 'SEDAN 2.2L GASOLINA STD.', ' 05 OCUP.', 'CAVALIER', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 509 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03928, 3928, 'CT', 'CHEVROLET', 'SUBURBAN', '5.3L V8 GASOLINA AUT.', ' 08 OCUP.', 'SUBURBAN', 105, 22, 'A', 08, 5, 8, '', 01, 1112, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 510 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03929, 3929, 'CT', 'CHEVROLET', 'MALIBU', 'SEDAN 3.1L V6 GASOLINA AUT.', ' 05 OCUP.', 'MALIBU', 100, 01, 'A', 06, 4, 5, '', 01, 1112, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 511 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03930, 3930, 'CT', 'CHEVROLET', 'CAMARO', 'SEDAN 3.0L V6 GASOLINA STD.', ' 05 OCUP.', 'CAMARO', 100, 01, 'S', 06, 5, 5, '', 01, 1112, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 512 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03931, 3931, 'CT', 'CHEVROLET', 'SONIC', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'SONIC', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 513 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03932, 3932, 'CT', 'CHEVROLET', 'CAPTIVA', '2.4L 4X2 GASOLINA STD.', ' 05 OCUP.', 'CAPTIVA', 105, 22, 'S', 04, 5, 5, '', 01, 1112, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 514 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03933, 3933, 'CT', 'CHEVROLET', 'TRACKER', 'SUV 1.6L 4X4 GASOLINA STD.', ' 04 OCUP.', 'TRACKER', 105, 03, 'S', 04, 2, 4, '', 01, 1212, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 515 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03934, 3934, 'CT', 'CHEVROLET', 'TRACKER', '1.6L TECHO DE LONA 4X4 GASOLINA STD.', ' 05 OCUP.', 'TRACKER', 105, 03, 'S', 04, 3, 5, '', 01, 1212, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 516 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03935, 3935, 'CT', 'CHEVROLET', 'CORVETTE', 'STRINGRAY CABRIO 5.7L V8 GLINA AUT.', ' 02 OCUP.', 'CORVETTE', 100, 04, 'A', 08, 2, 2, '', 01, 1212, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 517 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03936, 3936, 'CT', 'CHEVROLET', 'BLAZER', '4.3L V6 4X4 GASOLINA AUT.', ' 05 OCUP.', 'BLAZER', 105, 22, 'A', 06, 4, 5, '', 01, 1212, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 518 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03937, 3937, 'CT', 'CHEVROLET', 'VENTURE', 'STATION WAGON 3.4L GASOLINA AUT.', ' 07 OCUP.', 'VENTURE', 100, 02, 'A', 06, 5, 7, '', 01, 1212, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 519 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03938, 3938, 'CT', 'CHEVROLET', 'TRAX', '1.8L 4X4 GASOLINA STD.', ' 05 OCUP.', 'TRAX', 105, 22, 'S', 04, 5, 5, '', 01, 1212, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 520 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03939, 3939, 'CT', 'CHEVROLET', 'VECTRA', 'ELEGANCE 3.2L 4X2 GASOLINA STD.', ' 05 OCUP.', 'VECTRA', 100, 01, 'S', 06, 5, 5, '', 01, 1402, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 521 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03940, 3940, 'CT', 'CHEVROLET', 'TRAX', 'LTZ 1.8L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'TRAX', 105, 22, 'A', 04, 5, 5, '', 01, 1402, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 522 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03941, 3941, 'CT', 'CHEVROLET', 'CAPTIVA', '2.2L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'CAPTIVA', 105, 22, 'A', 04, 5, 5, '', 01, 1412, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 523 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03942, 3942, 'CT', 'CHEVROLET', 'ASTRO VAN', '4.3L GASOLINA STD.', ' 08 OCUP.', 'ASTRO VAN', 105, 11, 'S', 06, 4, 8, '', 01, 1501, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 524 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03943, 3943, 'CT', 'CHEVROLET', 'HHR LT', '2.2L GASOLINA STD.', ' 05 OCUP.', 'HHR LT', 105, 22, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 525 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03944, 3944, 'CT', 'CHEVROLET', 'TRACKER', '2.0L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'TRACKER', 105, 22, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 526 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03945, 3945, 'CT', 'CHEVROLET', 'N300', '1.2L 4X2 GASOLINA STD.', ' 07 OCUP.', 'N300', 105, 11, 'S', 04, 5, 7, '', 01, 1501, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 527 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03946, 3946, 'CT', 'CHEVROLET', 'MERIVA', '1.8L 4X2 GASOLINA STD.', ' 05 OCUP.', 'MERIVA', 105, 22, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 528 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03947, 3947, 'CT', 'CHEVROLET', 'IMPALA LS', '3.5L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'IMPALA LS', 100, 01, 'A', 06, 4, 5, '', 01, 1501, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 529 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03948, 3948, 'CT', 'CHEVROLET', 'EQUINOX', 'LT 3.4L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'EQUINOX', 105, 22, 'A', 06, 4, 5, '', 01, 1601, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 530 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03949, 3949, 'CT', 'CHEVROLET', 'PRIZM', '1.8L 4X2 GASOLINA STD.', ' 05 OCUP.', 'PRIZM', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 531 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03950, 3950, 'CT', 'CHEVROLET', 'PRIZM', '1.8L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'PRIZM', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 532 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03951, 3951, 'CT', 'CHEVROLET', 'TAHOE', '5.3L 4X2 GASOLINA STD.', ' 07 OCUP.', 'TAHOE', 105, 22, 'S', 08, 4, 7, '', 01, 1601, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 533 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03952, 3952, 'CT', 'CHEVROLET', 'TRACKER', '2.0L 4X4 GASOLINA STD.', ' 05 OCUP.', 'TRACKER', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 534 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03953, 3953, 'CT', 'CHEVROLET', 'METRO', '1.3L GASOLINA AUT.', ' 05 OCUP.', 'METRO', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 535 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03954, 3954, 'CT', 'CHEVROLET', 'COBALT', 'LS 2.2L GASOLINA STD.', ' 05 OCUP.', 'COBALT', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 536 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03955, 3955, 'CT', 'CHEVROLET', 'TRAILBLAZER', '2.8L 4X4 DIESEL AUT.', ' 07 OCUP.', 'TRAILBLAZER', 105, 22, 'A', 04, 4, 7, '', 01, 1601, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 537 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03956, 3956, 'CT', 'CHEVROLET', 'SPARK', 'LTZ 1.4L GASOLINA STD.', ' 05 OCUP.', 'SPARK', 100, 27, 'S', 04, 5, 5, '', 01, 1601, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 538 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03957, 3957, 'CT', 'CHEVROLET', 'BEAT', '1.2L GASOLINA STD.', ' 05 OCUP.', 'BEAT', 100, 27, 'S', 04, 5, 5, '', 01, 1601, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 539 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03958, 3958, 'CT', 'CHEVROLET', 'CAMARO', '2 LT 3.6L GASOLINA AUT.', ' 04 OCUP.', 'CAMARO', 100, 04, 'A', 06, 2, 4, '', 01, 1705, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 540 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03959, 3959, 'CT', 'CHEVROLET', 'BLAZER', 'ST 2.8L 4X4 DIESEL STD.', ' 05 OCUP.', 'BLAZER', 105, 22, 'S', 04, 4, 5, '', 01, 1705, 0003, 0003, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 541 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03960, 3960, 'CT', 'CHEVROLET', 'BEAT', 'LT 1.2L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'BEAT', 100, 27, 'A', 04, 5, 5, '', 01, 1705, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 542 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 03961, 3961, 'CT', 'CHEVROLET', 'SPARK', 'LS 1.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'SPARK', 100, 27, 'S', 03, 5, 5, '', 01, 1705, 0001, 0001, '', 'CHEVROLET'
);

/* INSERT QUERY NO: 543 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04301, 4301, 'KA', 'KIA', 'SPORTAGE', '2.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'SPORTAGE', 105, 22, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 544 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04302, 4302, 'KA', 'KIA', 'SORENTO', '2.4L 4X4 GASOLINA STD.', ' 05 OCUP.', 'SORENTO', 105, 22, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 545 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04303, 4303, 'KA', 'KIA', 'MOHAVE', '3.8L 4X4 GASOLINA AUT.', ' 07 OCUP.', 'MOHAVE', 105, 22, 'A', 06, 4, 7, '', 01, 1106, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 546 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04304, 4304, 'KA', 'KIA', 'CERATO', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'CERATO', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 547 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04305, 4305, 'KA', 'KIA', 'PICANTO', 'SEDAN 1.2L GASOLINA STD.', ' 05 OCUP.', 'PICANTO', 100, 27, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 548 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04306, 4306, 'KA', 'KIA', 'RIO', 'SEDAN 1.4L GASOLINA STD.', ' 05 OCUP.', 'RIO', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 549 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04307, 4307, 'KA', 'KIA', 'CARENS', '2.0L GASOLINA STD.', ' 07 OCUP.', 'CARENS', 100, 02, 'S', 04, 4, 7, '', 01, 1106, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 550 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04308, 4308, 'KA', 'KIA', 'CARNIVAL', '2.9L DIESEL STD.', ' 07 OCUP.', 'CARNIVAL', 100, 24, 'S', 04, 4, 7, '', 01, 1106, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 551 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04309, 4309, 'KA', 'KIA', 'CLARUS', 'GLX SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'CLARUS', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 552 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04310, 4310, 'KA', 'KIA', 'SOUL', '1.6L GASOLINA STD.', ' 05 OCUP.', 'SOUL', 105, 01, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 553 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04311, 4311, 'KA', 'KIA', 'SORENTO', '2.5L 4X4 DIESEL AUT.', ' 05 OCUP.', 'SORENTO', 105, 22, 'A', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 554 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04312, 4312, 'KA', 'KIA', 'SPECTRA', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'SPECTRA', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 555 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04313, 4313, 'KA', 'KIA', 'SEPHIA', 'SEDAN 1.5L GASOLINA STD.', ' 05 OCUP.', 'SEPHIA', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 556 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04314, 4314, 'KA', 'KIA', 'OPTIMA', 'SEDAN 2.4L GASOLINA STD.', ' 05 OCUP.', 'OPTIMA', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 557 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04315, 4315, 'KA', 'KIA', 'JOICE', 'LS 2.0L GASOLINA STD.', ' 07 OCUP.', 'JOICE', 105, 11, 'S', 04, 5, 7, '', 01, 1112, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 558 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04316, 4316, 'KA', 'KIA', 'SHUMA', 'SEDAN 1.8L GASOLINA STD.', ' 05 OCUP.', 'SHUMA', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 559 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04317, 4317, 'KA', 'KIA', 'PREGIO', '3.0L DIESEL STD.', ' 15 OCUP.', 'PREGIO', 105, 11, 'S', 04, 3, 15, '', 01, 1112, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 560 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04318, 4318, 'KA', 'KIA', 'SEDONA', '3.5L 4X4 GASOLINA STD.', ' 07 OCUP.', 'SEDONA', 100, 24, 'S', 06, 5, 7, '', 01, 1112, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 561 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04319, 4319, 'KA', 'KIA', 'SPORTAGE', '2.4L 4X4 GASOLINA STD.', ' 05 OCUP.', 'SPORTAGE', 105, 22, 'S', 04, 4, 5, '', 01, 1112, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 562 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04320, 4320, 'KA', 'KIA', 'OPIRUS', 'SEDAN 3.5L GASOLINA AUT.', ' 05 OCUP.', 'OPIRUS', 105, 04, 'A', 06, 4, 5, '', 01, 1112, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 563 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04321, 4321, 'KA', 'KIA', 'MOHAVE', '3.0L 4X4 DIESEL AUT.', ' 05 OCUP.', 'MOHAVE', 105, 22, 'A', 05, 5, 5, '', 01, 1402, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 564 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04322, 4322, 'KA', 'KIA', 'VISTO', 'HB 0.8L STD.', ' 05 OCUP.', 'VISTO', 100, 27, 'S', 05, 4, 5, '', 01, 1402, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 565 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04323, 4323, 'KA', 'KIA', 'OPTIMA', 'HYBRIDO SEDAN 2.4L STD.', ' 05 OCUP.', 'OPTIMA', 100, 01, 'S', 04, 4, 5, '', 01, 1412, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 566 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04324, 4324, 'KA', 'KIA', 'QUORIS', 'SEDAN 3.8L AUT.', ' 05 OCUP.', 'QUORIS', 100, 01, 'A', 04, 4, 5, '', 01, 1412, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 567 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04325, 4325, 'KA', 'KIA', 'SORENTO', '2.4L 4X2 AUT.', ' 05 OCUP.', 'SORENTO', 105, 22, 'A', 04, 4, 5, '', 01, 1412, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 568 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04326, 4326, 'KA', 'KIA', 'CARNIVAL', 'EX 2.9L DIESEL AUT.', ' 08 OCUP.', 'CARNIVAL', 100, 24, 'A', 04, 4, 8, '', 01, 1501, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 569 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04327, 4327, 'KA', 'KIA', 'CARENS', '2.0L 4X2 GASOLINA STD.', ' 07 OCUP.', 'CARENS', 100, 02, 'S', 04, 4, 7, '', 01, 1601, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 570 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04328, 4328, 'KA', 'KIA', 'FORTE', '1.6L GASOLINA AUT.', ' 05 OCUP.', 'FORTE', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 571 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04329, 4329, 'KA', 'KIA', 'AVELLA', 'GLX 1.5L GASOLINA STD.', ' 05 OCUP.', 'AVELLA', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 572 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04330, 4330, 'KA', 'KIA', 'FORTE', '2.0L GASOLINA STD.', ' 05 OCUP.', 'FORTE', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 573 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04331, 4331, 'KA', 'KIA', 'SORENTO', '3.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'SORENTO', 105, 22, 'S', 06, 4, 5, '', 01, 1601, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 574 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04332, 4332, 'KA', 'KIA', 'PRIDE', 'BGTX 1.3L 4X2 GASOLINA STD.', ' 05 OCUP.', 'PRIDE', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 575 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04333, 4333, 'KA', 'KIA', 'SORENTO', '3.5L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'SORENTO', 105, 22, 'A', 06, 4, 5, '', 01, 1705, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 576 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04334, 4334, 'KA', 'KIA', 'RETONA', '2.0L 4X4 DIESEL STD.', ' 05 OCUP.', 'RETONA', 105, 22, 'S', 04, 3, 5, '', 01, 1705, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 577 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04335, 4335, 'KA', 'KIA', 'RIO', 'LX 1.6L GASOLINA STD.', ' 05 OCUP.', 'RIO', 100, 01, 'S', 04, 4, 5, '', 01, 1705, 0003, 0003, '', 'KIA'
);

/* INSERT QUERY NO: 578 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04701, 4701, 'RT', 'RENAULT', 'KOLEOS', '2.5L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'KOLEOS', 105, 22, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'RENAULT'
);

/* INSERT QUERY NO: 579 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04703, 4703, 'RT', 'RENAULT', 'KOLEOS', '2.5L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'KOLEOS', 105, 22, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'RENAULT'
);

/* INSERT QUERY NO: 580 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04704, 4704, 'RT', 'RENAULT', 'LOGAN', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'LOGAN', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'RENAULT'
);

/* INSERT QUERY NO: 581 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04705, 4705, 'RT', 'RENAULT', 'MEGANE', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'MEGANE', 100, 27, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'RENAULT'
);

/* INSERT QUERY NO: 582 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04706, 4706, 'RT', 'RENAULT', 'SANDERO', '1.6L 4X4 GASOLINA STD.', ' 05 OCUP.', 'SANDERO', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'RENAULT'
);

/* INSERT QUERY NO: 583 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04707, 4707, 'RT', 'RENAULT', 'STEPWAY', '1.6L 4X4 GASOLINA STD.', ' 05 OCUP.', 'STEPWAY', 100, 27, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'RENAULT'
);

/* INSERT QUERY NO: 584 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04708, 4708, 'RT', 'RENAULT', 'CLIO', '1.2L GASOLINA STD.', ' 04 OCUP.', 'CLIO', 100, 01, 'S', 04, 4, 4, '', 01, 1106, 0001, 0001, '', 'RENAULT'
);

/* INSERT QUERY NO: 585 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04709, 4709, 'RT', 'RENAULT', 'SCENIC', '2.0L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'SCENIC', 105, 11, 'A', 04, 5, 5, '', 01, 1106, 0003, 0003, '', 'RENAULT'
);

/* INSERT QUERY NO: 586 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04710, 4710, 'RT', 'RENAULT', 'LAGUNA', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'LAGUNA', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'RENAULT'
);

/* INSERT QUERY NO: 587 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04711, 4711, 'RT', 'RENAULT', 'SAMSUNG', '2.0L 4X4 GASOLINA STD.', ' 05 OCUP.', 'SAMSUNG', 100, 01, 'S', 04, 5, 5, '', 01, 1106, 0001, 0001, '', 'RENAULT'
);

/* INSERT QUERY NO: 588 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04714, 4714, 'RT', 'RENAULT', 'FLUENCE', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'FLUENCE', 100, 01, 'S', 04, 4, 5, '', 01, 1212, 0001, 0001, '', 'RENAULT'
);

/* INSERT QUERY NO: 589 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04715, 4715, 'RT', 'RENAULT', 'DUSTER', '2.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'DUSTER', 105, 22, 'S', 04, 5, 5, '', 01, 1212, 0003, 0003, '', 'RENAULT'
);

/* INSERT QUERY NO: 590 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04716, 4716, 'RT', 'RENAULT', 'DUSTER', '1.6L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'DUSTER', 105, 22, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'RENAULT'
);

/* INSERT QUERY NO: 591 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04717, 4717, 'RT', 'RENAULT', 'SANDERO', 'STEPWAY 1.6L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'SANDERO', 100, 27, 'A', 04, 5, 5, '', 01, 1501, 0001, 0001, '', 'RENAULT'
);

/* INSERT QUERY NO: 592 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04718, 4718, 'RT', 'RENAULT', 'SAMSUNG', 'SM3 SE 1.5L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'SAMSUNG', 100, 01, 'A', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'RENAULT'
);

/* INSERT QUERY NO: 593 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04719, 4719, 'RT', 'RENAULT', 'SCENIC', '2.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'SCENIC', 105, 22, 'S', 04, 5, 5, '', 01, 1601, 0003, 0003, '', 'RENAULT'
);

/* INSERT QUERY NO: 594 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04720, 4720, 'RT', 'RENAULT', 'CAPTUR', 'INTENS 1.0L GASOLINA STD.', ' 05 OCUP.', 'CAPTUR', 105, 22, 'S', 04, 5, 5, '', 01, 1601, 0003, 0003, '', 'RENAULT'
);

/* INSERT QUERY NO: 595 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04721, 4721, 'RT', 'RENAULT', 'CAPTUR', 'INTENS 1.0L GASOLINA AUT.', ' 05 OCUP.', 'CAPTUR', 105, 22, 'A', 04, 5, 5, '', 01, 1601, 0003, 0003, '', 'RENAULT'
);

/* INSERT QUERY NO: 596 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04722, 4722, 'RT', 'RENAULT', 'CAPTUR', 'ICONIC 1.2L GASOLINA AUT.', ' 05 OCUP.', 'CAPTUR', 105, 22, 'A', 04, 5, 5, '', 01, 1601, 0003, 0003, '', 'RENAULT'
);

/* INSERT QUERY NO: 597 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 04723, 4723, 'RT', 'RENAULT', 'DOKKER', 'VAN 1.5L DIESEL STD.', ' 05 OCUP.', 'DOKKER', 105, 11, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'RENAULT'
);

/* INSERT QUERY NO: 598 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05001, 5001, 'GO', 'GEO', 'PRIZM', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'PRIZM', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'GEO'
);

/* INSERT QUERY NO: 599 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05002, 5002, 'GO', 'GEO', 'METRO', 'SEDAN 1.3L GASOLINA STD.', ' 04 OCUP.', 'METRO', 100, 01, 'S', 04, 2, 4, '', 01, 1112, 0001, 0001, '', 'GEO'
);

/* INSERT QUERY NO: 600 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05003, 5003, 'GO', 'GEO', 'TRACKER', '1.6L 4X2 GASOLINA STD.', ' 04 OCUP.', 'TRACKER', 105, 03, 'S', 04, 2, 4, '', 01, 1112, 0003, 0003, '', 'GEO'
);

/* INSERT QUERY NO: 601 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05004, 5004, 'GO', 'GEO', 'TRACKER', 'LSI 1.6L 4X4 GASOLINA STD.', ' 05 OCUP.', 'TRACKER', 105, 03, 'S', 04, 3, 5, '', 01, 1212, 0003, 0003, '', 'GEO'
);

/* INSERT QUERY NO: 602 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05301, 5301, 'VW', 'VOLKSWAGEN', 'BEETLE', 'CABRIO 2.5L GASOLINA STD.', ' 05 OCUP.', 'BEETLE', 100, 04, 'S', 05, 2, 5, '', 01, 1003, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 603 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05302, 5302, 'VW', 'VOLKSWAGEN', 'BEETLE', 'CABRIO 2.5L GASOLINA AUT.', ' 05 OCUP.', 'BEETLE', 100, 04, 'A', 05, 2, 5, '', 01, 1003, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 604 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05303, 5303, 'VW', 'VOLKSWAGEN', 'BEETLE', '2.0L GASOLINA STD.', ' 05 OCUP.', 'BEETLE', 100, 01, 'S', 04, 2, 5, '', 01, 1003, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 605 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05304, 5304, 'VW', 'VOLKSWAGEN', 'BEETLE', '2.0L GASOLINA AUT.', ' 05 OCUP.', 'BEETLE', 100, 01, 'A', 04, 2, 5, '', 01, 1003, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 606 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05305, 5305, 'VW', 'VOLKSWAGEN', 'BEETLE', '2.5L GASOLINA STD.', ' 05 OCUP.', 'BEETLE', 100, 01, 'S', 05, 2, 5, '', 01, 1003, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 607 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05306, 5306, 'VW', 'VOLKSWAGEN', 'BEETLE', '2.5L GASOLINA AUT.', ' 05 OCUP.', 'BEETLE', 100, 01, 'A', 05, 2, 5, '', 01, 1003, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 608 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05307, 5307, 'VW', 'VOLKSWAGEN', 'BORA', 'SEDAN 2.5L GASOLINA STD.', ' 05 OCUP.', 'BORA', 100, 01, 'S', 05, 4, 5, '', 01, 1003, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 609 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05308, 5308, 'VW', 'VOLKSWAGEN', 'BORA', 'SEDAN 2.5L GASOLINA AUT.', ' 05 OCUP.', 'BORA', 100, 01, 'A', 05, 4, 5, '', 01, 1003, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 610 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05315, 5315, 'VW', 'VOLKSWAGEN', 'CROSSFOX', '1.6L 4X2 GASOLINA STD.', ' 05 OCUP.', 'CROSSFOX', 100, 27, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 611 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05316, 5316, 'VW', 'VOLKSWAGEN', 'JETTA', 'SEDAN 2.5L GASOLINA STD.', ' 05 OCUP.', 'JETTA', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 612 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05317, 5317, 'VW', 'VOLKSWAGEN', 'JETTA', 'SEDAN 2.5L GASOLINA AUT.', ' 05 OCUP.', 'JETTA', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 613 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05318, 5318, 'VW', 'VOLKSWAGEN', 'JETTA', 'SEDAN 1.8L GASOLINA STD.', ' 05 OCUP.', 'JETTA', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 614 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05319, 5319, 'VW', 'VOLKSWAGEN', 'JETTA', 'TRENDLINE SEDAN 2.0L GASOLINA AUT.', ' 05 OCUP.', 'JETTA', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 615 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05320, 5320, 'VW', 'VOLKSWAGEN', 'JETTA', 'COMFORLINE SEDAN 2.5L GASOLINA STD.', ' 05 OCUP.', 'JETTA', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 616 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05321, 5321, 'VW', 'VOLKSWAGEN', 'JETTA', 'SEDAN 2.0L GASOLINA AUT.', ' 05 OCUP.', 'JETTA', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 617 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05322, 5322, 'VW', 'VOLKSWAGEN', 'PASSAT', 'SEDAN 1.8L GASOLINA AUT.', ' 05 OCUP.', 'PASSAT', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 618 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05323, 5323, 'VW', 'VOLKSWAGEN', 'TIGUAN', '2.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'TIGUAN', 105, 22, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 619 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05324, 5324, 'VW', 'VOLKSWAGEN', 'TIGUAN', '2.0L 4X4 GASOLINA STD.', ' 05 OCUP.', 'TIGUAN', 105, 22, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 620 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05325, 5325, 'VW', 'VOLKSWAGEN', 'TIGUAN', '2.0L 4X4 DIESEL STD.', ' 05 OCUP.', 'TIGUAN', 105, 22, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 621 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05327, 5327, 'VW', 'VOLKSWAGEN', 'TOUAREG', '3.0L V6 4X4 TDI STD.', ' 05 OCUP.', 'TOUAREG', 105, 22, 'S', 06, 4, 5, '', 01, 1003, 0003, 0003, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 622 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05328, 5328, 'VW', 'VOLKSWAGEN', 'TOUAREG', '2.5L 4X4 TDI STD.', ' 05 OCUP.', 'TOUAREG', 105, 22, 'S', 05, 4, 5, '', 01, 1003, 0003, 0003, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 623 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05330, 5330, 'VW', 'VOLKSWAGEN', 'TOUAREG', '4.2L V8 4X4 GASOLINA STD.', ' 05 OCUP.', 'TOUAREG', 105, 22, 'S', 08, 4, 5, '', 01, 1003, 0003, 0003, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 624 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05331, 5331, 'VW', 'VOLKSWAGEN', 'TOUAREG', '3.6L V6 4X4 GASOLINA STD.', ' 05 OCUP.', 'TOUAREG', 105, 22, 'S', 06, 4, 5, '', 01, 1003, 0003, 0003, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 625 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05334, 5334, 'VW', 'VOLKSWAGEN', 'TOUAREG', '3.6L V6 4X4 TDI STD.', ' 05 OCUP.', 'TOUAREG', 105, 22, 'S', 06, 4, 5, '', 01, 1003, 0003, 0003, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 626 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05335, 5335, 'VW', 'VOLKSWAGEN', 'TRANSPORTER', '2.0L TDI STD.', ' 10 OCUP.', 'TRANSPORTER', 100, 24, 'S', 04, 3, 10, '', 01, 1003, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 627 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05336, 5336, 'VW', 'VOLKSWAGEN', 'JETTA MK6', 'SEDAN 2.5L GASOLINA STD.', ' 05 OCUP.', 'JETTA MK6', 100, 01, 'S', 05, 4, 5, '', 01, 1106, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 628 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05338, 5338, 'VW', 'VOLKSWAGEN', 'GOL', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'GOL', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 629 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05339, 5339, 'VW', 'VOLKSWAGEN', 'POLO', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'POLO', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 630 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05340, 5340, 'VW', 'VOLKSWAGEN', 'VENTO', 'SEDAN 1.8L GASOLINA STD.', ' 05 OCUP.', 'VENTO', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 631 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05342, 5342, 'VW', 'VOLKSWAGEN', 'FOX', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'FOX', 100, 01, 'S', 04, 2, 5, '', 01, 1112, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 632 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05343, 5343, 'VW', 'VOLKSWAGEN', 'KOMBI', '2.0L GASOLINA STD.', ' 05 OCUP.', 'KOMBI', 100, 20, 'S', 04, 5, 5, '', 01, 1112, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 633 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05344, 5344, 'VW', 'VOLKSWAGEN', 'ESCARABAJO', 'COUPE 1.5L GASOLINA STD.', ' 05 OCUP.', 'ESCARABAJO', 100, 01, 'S', 04, 2, 5, '', 01, 1112, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 634 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05345, 5345, 'VW', 'VOLKSWAGEN', 'JETTA', 'GLI TSI 2.0L GASOLINA STD.', ' 05 OCUP.', 'JETTA', 100, 01, 'S', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 635 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05346, 5346, 'VW', 'VOLKSWAGEN', 'GOLF', 'HATCHBACK 1.6L GASOLINA STD.', ' 05 OCUP.', 'GOLF', 100, 27, 'S', 04, 2, 5, '', 01, 1402, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 636 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05347, 5347, 'VW', 'VOLKSWAGEN', 'HIGH', 'SEDAN 1.0L GASOLINA AUT.', ' 05 OCUP.', 'HIGH', 100, 01, 'A', 03, 4, 5, '', 01, 1601, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 637 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05348, 5348, 'VW', 'VOLKSWAGEN', 'RABBIT', '2.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'RABBIT', 100, 27, 'S', 05, 5, 5, '', 01, 1601, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 638 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05349, 5349, 'VW', 'VOLKSWAGEN', 'UP', '1.0L GASOLINA STD.', ' 05 OCUP.', 'UP', 100, 27, 'S', 03, 5, 5, '', 01, 1601, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 639 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05350, 5350, 'VW', 'VOLKSWAGEN', 'GOLF', 'A4 GTI 2.0L GASOLINA AUT.', ' 05 OCUP.', 'GOLF', 100, 27, 'A', 04, 5, 5, '', 01, 1705, 0001, 0001, '', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 640 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05601, 5601, 'SU', 'SUBARU', 'FORESTER', 'XS 2.0L 4X4 GASOLINA STD.', ' 05 OCUP.', 'FORESTER', 105, 22, 'S', 04, 5, 5, '', 01, 1003, 0003, 0003, '', 'SUBARU'
);

/* INSERT QUERY NO: 641 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05603, 5603, 'SU', 'SUBARU', 'FORESTER', 'XT 2.5L 4X4 GASOLINA STD.', ' 05 OCUP.', 'FORESTER', 105, 22, 'S', 04, 5, 5, '', 01, 1003, 0003, 0003, '', 'SUBARU'
);

/* INSERT QUERY NO: 642 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05605, 5605, 'SU', 'SUBARU', 'FORESTER', 'S-EDITION 2.5L 4X4 GASOLINA STD.', ' 05 OCUP.', 'FORESTER', 105, 22, 'S', 04, 5, 5, '', 01, 1003, 0003, 0003, '', 'SUBARU'
);

/* INSERT QUERY NO: 643 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05606, 5606, 'SU', 'SUBARU', 'IMPREZA', 'SEDAN 2.5L GASOLINA STD.', ' 05 OCUP.', 'IMPREZA', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'SUBARU'
);

/* INSERT QUERY NO: 644 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05607, 5607, 'SU', 'SUBARU', 'IMPREZA', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'IMPREZA', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'SUBARU'
);

/* INSERT QUERY NO: 645 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05610, 5610, 'SU', 'SUBARU', 'IMPREZA', 'SEDAN 2.0L GASOLINA AUT.', ' 05 OCUP.', 'IMPREZA', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'SUBARU'
);

/* INSERT QUERY NO: 646 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05611, 5611, 'SU', 'SUBARU', 'IMPREZA', 'HB 1.5L GASOLINA STD.', ' 05 OCUP.', 'IMPREZA', 100, 27, 'S', 04, 5, 5, '', 01, 1003, 0001, 0001, '', 'SUBARU'
);

/* INSERT QUERY NO: 647 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05612, 5612, 'SU', 'SUBARU', 'IMPREZA', 'HB 1.5L GASOLINA AUT.', ' 05 OCUP.', 'IMPREZA', 100, 27, 'A', 04, 5, 5, '', 01, 1003, 0001, 0001, '', 'SUBARU'
);

/* INSERT QUERY NO: 648 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05613, 5613, 'SU', 'SUBARU', 'IMPREZA', 'HB 2.0L GASOLINA STD.', ' 05 OCUP.', 'IMPREZA', 100, 27, 'S', 04, 5, 5, '', 01, 1003, 0001, 0001, '', 'SUBARU'
);

/* INSERT QUERY NO: 649 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05614, 5614, 'SU', 'SUBARU', 'IMPREZA', 'HB 2.0L GASOLINA AUT.', ' 05 OCUP.', 'IMPREZA', 100, 27, 'A', 04, 5, 5, '', 01, 1003, 0001, 0001, '', 'SUBARU'
);

/* INSERT QUERY NO: 650 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05615, 5615, 'SU', 'SUBARU', 'IMPREZA', '4X4 GASOLINA STD.', ' 05 OCUP.', 'IMPREZA', 105, 22, 'S', 04, 5, 5, '', 01, 1003, 0003, 0003, '', 'SUBARU'
);

/* INSERT QUERY NO: 651 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05616, 5616, 'SU', 'SUBARU', 'IMPREZA', 'XV 2.0L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'IMPREZA', 105, 22, 'A', 04, 5, 5, '', 01, 1003, 0003, 0003, '', 'SUBARU'
);

/* INSERT QUERY NO: 652 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05617, 5617, 'SU', 'SUBARU', 'IMPREZA', 'STI SEDAN 2.5L GASOLINA STD.', ' 05 OCUP.', 'IMPREZA', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'SUBARU'
);

/* INSERT QUERY NO: 653 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05618, 5618, 'SU', 'SUBARU', 'LEGACY', 'SEDAN 2.5L GASOLINA AUT.', ' 05 OCUP.', 'LEGACY', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'SUBARU'
);

/* INSERT QUERY NO: 654 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05620, 5620, 'SU', 'SUBARU', 'OUTBACK', '2.5L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'OUTBACK', 105, 22, 'A', 04, 5, 5, '', 01, 1003, 0003, 0003, '', 'SUBARU'
);

/* INSERT QUERY NO: 655 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05621, 5621, 'SU', 'SUBARU', 'OUTBACK', '3.6L V6 4X4 GASOLINA AUT.', ' 05 OCUP.', 'OUTBACK', 105, 22, 'A', 06, 5, 5, '', 01, 1003, 0003, 0003, '', 'SUBARU'
);

/* INSERT QUERY NO: 656 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05623, 5623, 'SU', 'SUBARU', 'JUSTY', 'HB 1.2L GASOLINA STD.', ' 05 OCUP.', 'JUSTY', 100, 01, 'S', 03, 2, 5, '', 01, 1112, 0001, 0001, '', 'SUBARU'
);

/* INSERT QUERY NO: 657 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05624, 5624, 'SU', 'SUBARU', 'VAN', '1.2L GASOLINA STD.', ' 07 OCUP.', 'VAN', 100, 11, 'S', 03, 5, 7, '', 01, 1112, 0001, 0001, '', 'SUBARU'
);

/* INSERT QUERY NO: 658 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05625, 5625, 'SU', 'SUBARU', 'CROSSOVER', '2.0L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'CROSSOVER', 105, 22, 'A', 04, 4, 5, '', 01, 1212, 0003, 0003, '', 'SUBARU'
);

/* INSERT QUERY NO: 659 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05626, 5626, 'SU', 'SUBARU', 'LEGACY', 'GT 2.5L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'LEGACY', 100, 01, 'A', 04, 5, 5, '', 01, 1402, 0001, 0001, '', 'SUBARU'
);

/* INSERT QUERY NO: 660 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05627, 5627, 'SU', 'SUBARU', 'BRZ', '2.0L 4X4 GASOLINA STD.', ' 05 OCUP.', 'BRZ', 100, 01, 'S', 04, 5, 5, '', 01, 1402, 0001, 0001, '', 'SUBARU'
);

/* INSERT QUERY NO: 661 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05628, 5628, 'SU', 'SUBARU', 'TRIBECA', '3.6L V6 4X4 GASOLINA AUT.', ' 05 OCUP.', 'TRIBECA', 105, 22, 'A', 06, 5, 5, '', 01, 1501, 0003, 0003, '', 'SUBARU'
);

/* INSERT QUERY NO: 662 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05629, 5629, 'SU', 'SUBARU', 'J10', 'GL 1.0L GASOLINA STD.', ' 05 OCUP.', 'J10', 100, 01, 'S', 03, 4, 5, '', 01, 1601, 0001, 0001, '', 'SUBARU'
);

/* INSERT QUERY NO: 663 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05901, 5901, 'DE', 'DODGE', 'DURANGO', '5.7L V8 4X4 GASOLINA AUT.', ' 07 OCUP.', 'DURANGO', 105, 22, 'A', 08, 4, 7, '', 01, 1106, 0003, 0003, '', 'DODGE'
);

/* INSERT QUERY NO: 664 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05902, 5902, 'DE', 'DODGE', 'JOURNEY', '2.4L 4X2 AUT.', ' 07 OCUP.', 'JOURNEY', 105, 22, 'A', 06, 4, 7, '', 01, 1106, 0003, 0003, '', 'DODGE'
);

/* INSERT QUERY NO: 665 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05903, 5903, 'DE', 'DODGE', 'NEON', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'NEON', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'DODGE'
);

/* INSERT QUERY NO: 666 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05905, 5905, 'DE', 'DODGE', 'JOURNEY', '2.4L V6 4X4 AUT.', ' 05 OCUP.', 'JOURNEY', 105, 22, 'A', 06, 5, 5, '', 01, 1106, 0003, 0003, '', 'DODGE'
);

/* INSERT QUERY NO: 667 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05906, 5906, 'DE', 'DODGE', 'VIPER', 'COUPE 8.3L V10 FULL AUT.', ' 02 OCUP.', 'VIPER', 100, 08, 'A', 10, 2, 2, '', 01, 1106, 0001, 0001, '', 'DODGE'
);

/* INSERT QUERY NO: 668 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05907, 5907, 'DE', 'DODGE', 'GRAND CARAVAN', '3.0L V6 GASOLINA AUT.', ' 07 OCUP.', 'GRAND CARAVAN', 105, 11, 'A', 06, 5, 7, '', 01, 1112, 0003, 0003, '', 'DODGE'
);

/* INSERT QUERY NO: 669 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05908, 5908, 'DE', 'DODGE', 'NITRO', '2.8L 4X2 DIESEL AUT.', ' 05 OCUP.', 'NITRO', 105, 22, 'A', 04, 5, 5, '', 01, 1112, 0003, 0003, '', 'DODGE'
);

/* INSERT QUERY NO: 670 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05909, 5909, 'DE', 'DODGE', 'COLT', 'SEDAN 1.5L STD.', ' 05 OCUP.', 'COLT', 100, 01, 'S', 04, 3, 5, '', 01, 1112, 0001, 0001, '', 'DODGE'
);

/* INSERT QUERY NO: 671 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05911, 5911, 'DE', 'DODGE', 'RAIDER', '2.4L 4X4 DIESEL STD.', ' 06 OCUP.', 'RAIDER', 105, 22, 'S', 04, 3, 6, '', 01, 1212, 0003, 0003, '', 'DODGE'
);

/* INSERT QUERY NO: 672 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05912, 5912, 'DE', 'DODGE', 'NITRO', '2.8L 4X4 STD.', ' 05 OCUP.', 'NITRO', 105, 22, 'S', 06, 4, 5, '', 01, 1112, 0003, 0003, '', 'DODGE'
);

/* INSERT QUERY NO: 673 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05913, 5913, 'DE', 'DODGE', 'INTREPID', 'SEDAN 3.5L STD.', ' 05 OCUP.', 'INTREPID', 100, 01, 'S', 06, 4, 5, '', 01, 1402, 0001, 0001, '', 'DODGE'
);

/* INSERT QUERY NO: 674 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05914, 5914, 'DE', 'DODGE', 'CHALENGER', '6.4L 4X2 STD.', ' 05 OCUP.', 'CHALENGER', 100, 01, 'S', 08, 4, 5, '', 01, 1402, 0001, 0001, '', 'DODGE'
);

/* INSERT QUERY NO: 675 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05915, 5915, 'DE', 'DODGE', 'CALIBER', '2.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'CALIBER', 105, 22, 'S', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'DODGE'
);

/* INSERT QUERY NO: 676 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05916, 5916, 'DE', 'DODGE', 'RAM B', '3500 5.9L 4X2 GASOLINA STD.', ' 15 OCUP.', 'RAM B', 105, 11, 'S', 08, 4, 15, '', 01, 1601, 0003, 0003, '', 'DODGE'
);

/* INSERT QUERY NO: 677 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 05917, 5917, 'DE', 'DODGE', 'CHALLENGER', 'SRT HELLCAT 6.2L GASOLINA AUT.', ' 05 OCUP.', 'CHALLENGER', 100, 01, 'A', 08, 2, 5, '', 01, 1601, 0001, 0001, '', 'DODGE'
);

/* INSERT QUERY NO: 678 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06101, 6101, 'DO', 'DAEWOO', 'MUSSO', '3.0L V6 4X4 STD.', ' 05 OCUP.', 'MUSSO', 105, 22, 'S', 06, 5, 5, '', 01, 1106, 0003, 0003, '', 'DAEWOO'
);

/* INSERT QUERY NO: 679 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06102, 6102, 'DO', 'DAEWOO', 'NUBIRA', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'NUBIRA', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'DAEWOO'
);

/* INSERT QUERY NO: 680 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06103, 6103, 'DO', 'DAEWOO', 'MAGNUS', 'SEDAN 2.0L V/P D/C V/E GASOLINA STD.', ' 05 OCUP.', 'MAGNUS', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'DAEWOO'
);

/* INSERT QUERY NO: 681 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06104, 6104, 'DO', 'DAEWOO', 'LANOS', 'SEDAN 1.6L GASOLINA AUT.', ' 05 OCUP.', 'LANOS', 100, 01, 'A', 04, 4, 5, '', 01, 1212, 0001, 0001, '', 'DAEWOO'
);

/* INSERT QUERY NO: 682 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06105, 6105, 'DO', 'DAEWOO', 'MATIZ', 'SEDAN HB 1.0L GASOLINA STD.', ' 05 OCUP.', 'MATIZ', 100, 27, 'S', 03, 4, 5, '', 01, 1402, 0001, 0001, '', 'DAEWOO'
);

/* INSERT QUERY NO: 683 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06106, 6106, 'DO', 'DAEWOO', 'LACETTI', '1.5L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'LACETTI', 100, 01, 'A', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'DAEWOO'
);

/* INSERT QUERY NO: 684 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06107, 6107, 'DO', 'DAEWOO', 'CIELO', '1.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'CIELO', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'DAEWOO'
);

/* INSERT QUERY NO: 685 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06301, 6301, 'PT', 'PEUGEOT', '206', 'HB 1.6L GASOLINA STD.', ' 05 OCUP.', '206', 100, 27, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 686 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06302, 6302, 'PT', 'PEUGEOT', '206', 'HB 1.4L GASOLINA STD.', ' 05 OCUP.', '206', 100, 27, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 687 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06305, 6305, 'PT', 'PEUGEOT', '206', 'HB 1.9L DIESEL STD.', ' 05 OCUP.', '206', 100, 27, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 688 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06306, 6306, 'PT', 'PEUGEOT', '206', 'HB 1.4L DIESEL STD.', ' 05 OCUP.', '206', 100, 27, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 689 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06308, 6308, 'PT', 'PEUGEOT', '307', 'SEDAN 1.6L DIESEL STD.', ' 05 OCUP.', '307', 100, 02, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 690 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06310, 6310, 'PT', 'PEUGEOT', '307', 'SEDAN 2.0L DIESEL STD.', ' 05 OCUP.', '307', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 691 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06314, 6314, 'PT', 'PEUGEOT', '407', 'SEDAN 2.2L GASOLINA STD.', ' 05 OCUP.', '407', 100, 01, 'S', 04, 5, 5, '', 01, 1003, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 692 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06323, 6323, 'PT', 'PEUGEOT', 'PARTNER', 'SEDAN 1.4L STD.', ' 05 OCUP.', 'PARTNER', 100, 02, 'S', 04, 3, 5, '', 01, 1106, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 693 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06324, 6324, 'PT', 'PEUGEOT', '206', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', '206', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 694 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06327, 6327, 'PT', 'PEUGEOT', '207', 'SEDAN 2.0L STD.', ' 05 OCUP.', '207', 100, 27, 'S', 04, 3, 5, '', 01, 1106, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 695 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06328, 6328, 'PT', 'PEUGEOT', '207', 'SEDAN 1.6L STD.', ' 05 OCUP.', '207', 100, 04, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 696 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06332, 6332, 'PT', 'PEUGEOT', '3008', '1.9L DIESEL STD.', ' 05 OCUP.', '3008', 100, 22, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 697 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06333, 6333, 'PT', 'PEUGEOT', '3008', 'SEDAN 3.0L GASOLINA STD.', ' 05 OCUP.', '3008', 100, 22, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 698 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06334, 6334, 'PT', 'PEUGEOT', 'PARTNER', '1.9L GASOLINA STD.', ' 05 OCUP.', 'PARTNER', 100, 02, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 699 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06335, 6335, 'PT', 'PEUGEOT', '607', 'SEDAN 1.7L GASOLINA STD.', ' 05 OCUP.', '607', 100, 01, 'S', 06, 4, 5, '', 01, 1106, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 700 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06338, 6338, 'PT', 'PEUGEOT', '406', 'SEDAN 1.7L GASOLINA STD.', ' 05 OCUP.', '406', 100, 01, 'S', 04, 4, 5, '', 01, 1212, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 701 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06339, 6339, 'PT', 'PEUGEOT', '107', 'TR SEDAN 1.9L GASOLINA STD.', ' 03 OCUP.', '107', 100, 01, 'S', 04, 2, 3, '', 01, 1212, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 702 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06340, 6340, 'PT', 'PEUGEOT', '107', 'URBAN 1.0L GASOLINA STD.', ' 04 OCUP.', '107', 100, 27, 'S', 03, 3, 4, '', 01, 1402, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 703 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06341, 6341, 'PT', 'PEUGEOT', '208', 'ACTIVE 1.6L GASOLINA STD.', ' 05 OCUP.', '208', 100, 27, 'S', 04, 3, 5, '', 01, 1402, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 704 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06343, 6343, 'PT', 'PEUGEOT', '208', 'ACTIVE TECHO P. 1.6L GASOLINA FULL STD.', ' 05 OCUP.', '208', 100, 27, 'S', 04, 3, 5, '', 01, 1402, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 705 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06344, 6344, 'PT', 'PEUGEOT', '208', 'ALLURE 1.6L GASOLINA FULL AUT.', ' 05 OCUP.', '208', 100, 27, 'A', 04, 3, 5, '', 01, 1402, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 706 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06346, 6346, 'PT', 'PEUGEOT', '208', 'GTI 1.6L GASOLINA FULL STD.', ' 05 OCUP.', '208', 100, 27, 'S', 04, 3, 5, '', 01, 1402, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 707 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06347, 6347, 'PT', 'PEUGEOT', '301', 'ACTIVE SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', '301', 100, 01, 'S', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 708 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06348, 6348, 'PT', 'PEUGEOT', '301', 'ACTIVE SEDAN 1.6L GASOLINA AUT.', ' 05 OCUP.', '301', 100, 01, 'A', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 709 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06349, 6349, 'PT', 'PEUGEOT', '2008', 'ACTIVE HALOG-LED 1.6L GASOLINA STD.', ' 05 OCUP.', '2008', 105, 22, 'S', 03, 5, 5, '', 01, 1402, 0003, 0003, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 710 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06350, 6350, 'PT', 'PEUGEOT', '2008', 'ALLURE G. CONTROL T.P. 1.6L GASOLINA STD.', ' 05 OCUP.', '2008', 105, 22, 'S', 03, 5, 5, '', 01, 1402, 0003, 0003, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 711 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06352, 6352, 'PT', 'PEUGEOT', '308', 'PREMIUM 1.6L TECHO P. GASOLINA FULL AUT.', ' 05 OCUP.', '308', 100, 27, 'A', 04, 5, 5, '', 01, 1402, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 712 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06353, 6353, 'PT', 'PEUGEOT', '308', 'CC CUERO 1.6L GASOLINA FULL AUT.', ' 04 OCUP.', '308', 100, 04, 'A', 04, 2, 4, '', 01, 1402, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 713 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06354, 6354, 'PT', 'PEUGEOT', '308', 'RCZ CUERO XENON 1.6L GASOLINA FULL STD.', ' 04 OCUP.', '308', 100, 08, 'S', 04, 2, 4, '', 01, 1402, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 714 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06356, 6356, 'PT', 'PEUGEOT', '3008', 'PREMIUM TECHO P. 1.6L GASOLINA FULL AUT.', ' 05 OCUP.', '3008', 100, 22, 'A', 04, 5, 5, '', 01, 1402, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 715 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06357, 6357, 'PT', 'PEUGEOT', '508', 'ALLURE CUERO S.R 1.6L GASOLINA FULL AUT.', ' 05 OCUP.', '508', 100, 27, 'A', 04, 5, 5, '', 01, 1402, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 716 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06358, 6358, 'PT', 'PEUGEOT', 'BERLINA', 'SEDAN 1.6L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'BERLINA', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 717 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06359, 6359, 'PT', 'PEUGEOT', 'BREAK', '306 XR 1.9L 4X2 DIESEL STD.', ' 05 OCUP.', 'BREAK', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 718 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06360, 6360, 'PT', 'PEUGEOT', '5008', 'TECHO DURO 1.6L 4X2 GASOLINA STD.', ' 07 OCUP.', '5008', 105, 22, 'S', 04, 4, 7, '', 01, 1705, 0003, 0003, '', 'PEUGEOT'
);

/* INSERT QUERY NO: 719 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06501, 6501, 'BW', 'BMW', 'SERIE 1', 'COUPE 1.8L STD.', ' 04 OCUP.', 'SERIE 1', 100, 18, 'S', 06, 2, 4, '', 01, 1106, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 720 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06502, 6502, 'BW', 'BMW', 'SERIE 3', 'SEDAN 2.0L STD.', ' 05 OCUP.', 'SERIE 3', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 721 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06503, 6503, 'BW', 'BMW', 'SERIE 5', 'SEDAN 2.5L STD.', ' 05 OCUP.', 'SERIE 5', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 722 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06504, 6504, 'BW', 'BMW', 'SERIE 5', 'SEDAN STD.', ' 05 OCUP.', 'SERIE 5', 100, 01, 'S', 06, 4, 5, '', 01, 1106, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 723 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06505, 6505, 'BW', 'BMW', 'SERIE 5', 'SEDAN STD.', ' 05 OCUP.', 'SERIE 5', 100, 01, 'S', 06, 4, 5, '', 01, 1106, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 724 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06506, 6506, 'BW', 'BMW', 'SERIE 5', 'SEDAN STD.', ' 05 OCUP.', 'SERIE 5', 100, 01, 'S', 08, 4, 5, '', 01, 1106, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 725 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06507, 6507, 'BW', 'BMW', 'SERIE 7', 'SEDAN STD.', ' 05 OCUP.', 'SERIE 7', 100, 01, 'S', 08, 4, 5, '', 01, 1106, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 726 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06508, 6508, 'BW', 'BMW', 'SERIE M', 'COUPE STD.', ' 04 OCUP.', 'SERIE M', 100, 18, 'S', 06, 2, 4, '', 01, 1106, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 727 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06509, 6509, 'BW', 'BMW', 'SERIE M', 'SEDAN STD.', ' 04 OCUP.', 'SERIE M', 100, 01, 'S', 08, 4, 4, '', 01, 1106, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 728 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06510, 6510, 'BW', 'BMW', 'SERIE X', '2.8L 4X4 AUT.', ' 05 OCUP.', 'SERIE X', 105, 22, 'A', 06, 4, 5, '', 01, 1106, 0003, 0003, '', 'BMW'
);

/* INSERT QUERY NO: 729 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06511, 6511, 'BW', 'BMW', 'SERIE X', '2.0L 4X4 DIESEL AUT.', ' 05 OCUP.', 'SERIE X', 105, 22, 'A', 06, 4, 5, '', 01, 1106, 0003, 0003, '', 'BMW'
);

/* INSERT QUERY NO: 730 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06514, 6514, 'BW', 'BMW', 'SERIE X', '3.5L 4X4 AUT.', ' 04 OCUP.', 'SERIE X', 105, 22, 'A', 08, 4, 4, '', 01, 1106, 0003, 0003, '', 'BMW'
);

/* INSERT QUERY NO: 731 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06515, 6515, 'BW', 'BMW', 'SERIE X', '4X4 AUT.', ' 04 OCUP.', 'SERIE X', 105, 22, 'A', 08, 4, 4, '', 01, 1106, 0003, 0003, '', 'BMW'
);

/* INSERT QUERY NO: 732 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06516, 6516, 'BW', 'BMW', 'SERIE Z', '3.5L AUT.', ' 02 OCUP.', 'SERIE Z', 100, 04, 'A', 06, 2, 2, '', 01, 1106, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 733 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06517, 6517, 'BW', 'BMW', 'SERIE 5', 'SEDAN AUT.', ' 05 OCUP.', 'SERIE 5', 100, 01, 'A', 06, 4, 5, '', 01, 1106, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 734 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06518, 6518, 'BW', 'BMW', 'SERIE 8', 'SEDAN V12 5.0L GASOLINA STD.', ' 04 OCUP.', 'SERIE 8', 100, 01, 'S', 12, 2, 4, '', 01, 1106, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 735 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06519, 6519, 'BW', 'BMW', 'SERIE 3', 'SEDAN AUT.', ' 05 OCUP.', 'SERIE 3', 100, 01, 'A', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 736 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06520, 6520, 'BW', 'BMW', 'SERIE Z', '2.8L V6 STD.', ' 02 OCUP.', 'SERIE Z', 100, 04, 'S', 06, 3, 2, '', 01, 1106, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 737 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06521, 6521, 'BW', 'BMW', 'SERIE 5', '3.0L V6 AUT.', ' 05 OCUP.', 'SERIE 5', 100, 01, 'A', 06, 4, 5, '', 01, 1106, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 738 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06522, 6522, 'BW', 'BMW', 'SERIE 7', 'SEDAN STD.', ' 05 OCUP.', 'SERIE 7', 100, 01, 'S', 06, 4, 5, '', 01, 1112, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 739 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06523, 6523, 'BW', 'BMW', 'SERIE 3', 'SEDAN STD.', ' 05 OCUP.', 'SERIE 3', 100, 01, 'S', 06, 4, 5, '', 01, 1112, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 740 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06524, 6524, 'BW', 'BMW', 'ALPINA', '3L V6 GASOLINA STD.', ' 05 OCUP.', 'ALPINA', 100, 08, 'S', 06, 4, 5, '', 01, 1112, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 741 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06525, 6525, 'BW', 'BMW', 'SERIE 3', 'SEDAN 2.5L V6 STD.', ' 05 OCUP.', 'SERIE 3', 100, 01, 'S', 06, 4, 5, '', 01, 1112, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 742 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06526, 6526, 'BW', 'BMW', 'SERIE 3', 'SEDAN 2.0L AUT.', ' 05 OCUP.', 'SERIE 3', 100, 01, 'A', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 743 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06527, 6527, 'BW', 'BMW', 'SERIE 3', 'SEDAN 2.8L V6 STD.', ' 05 OCUP.', 'SERIE 3', 100, 01, 'S', 06, 4, 5, '', 01, 1112, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 744 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06528, 6528, 'BW', 'BMW', 'SERIE 3', 'SEDAN 3.0L GASOLINA STD.', ' 05 OCUP.', 'SERIE 3', 100, 01, 'S', 04, 5, 5, '', 01, 1112, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 745 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06529, 6529, 'BW', 'BMW', 'SERIE 5', 'SEDAN 1.8L GASOLINA STD.', ' 05 OCUP.', 'SERIE 5', 100, 01, 'S', 04, 5, 5, '', 01, 1112, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 746 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06530, 6530, 'BW', 'BMW', 'SERIE 5', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'SERIE 5', 100, 01, 'S', 04, 5, 5, '', 01, 1112, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 747 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06531, 6531, 'BW', 'BMW', 'SERIE X', '3.0L 4X4 DIESEL STD.', ' 05 OCUP.', 'SERIE X', 105, 22, 'S', 04, 5, 5, '', 01, 1112, 0003, 0003, '', 'BMW'
);

/* INSERT QUERY NO: 748 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06532, 6532, 'BW', 'BMW', 'SERIE 1', 'I 1.8L GASOLINA STD.', ' 05 OCUP.', 'SERIE 1', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 749 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06533, 6533, 'BW', 'BMW', 'SERIE 1', 'I 1.6L GASOLINA STD.', ' 05 OCUP.', 'SERIE 1', 100, 01, 'S', 04, 5, 5, '', 01, 1112, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 750 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06534, 6534, 'BW', 'BMW', 'SERIE 1', 'I 2.0L GASOLINA STD.', ' 05 OCUP.', 'SERIE 1', 100, 18, 'S', 04, 3, 5, '', 01, 1112, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 751 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06535, 6535, 'BW', 'BMW', 'SERIE 1', 'I 3.0L V6 GASOLINA STD.', ' 05 OCUP.', 'SERIE 1', 100, 18, 'S', 06, 3, 5, '', 01, 1112, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 752 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06536, 6536, 'BW', 'BMW', 'SERIE 7', 'I 4.4L V8 GASOLINA AUT.', ' 05 OCUP.', 'SERIE 7', 100, 01, 'A', 08, 5, 5, '', 01, 1112, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 753 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06537, 6537, 'BW', 'BMW', 'SERIE M', '5.0L V10 GASOLINA AUT.', ' 04 OCUP.', 'SERIE M', 100, 18, 'A', 10, 3, 4, '', 01, 1112, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 754 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06538, 6538, 'BW', 'BMW', 'SERIE 6', '650IA 4.8L V8 GASOLINA AUT.', ' 04 OCUP.', 'SERIE 6', 100, 01, 'A', 08, 2, 4, '', 01, 1112, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 755 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06539, 6539, 'BW', 'BMW', 'SERIE 6', '645 CABRIOLET 4.4L V8 GASOLINA AUT.', ' 04 OCUP.', 'SERIE 6', 100, 04, 'A', 08, 2, 4, '', 01, 1112, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 756 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06540, 6540, 'BW', 'BMW', 'SERIE 3', '330I COUPE 3.0L GASOLINA AUT.', ' 05 OCUP.', 'SERIE 3', 100, 18, 'A', 04, 2, 5, '', 01, 1212, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 757 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06541, 6541, 'BW', 'BMW', 'SERIE 7', 'I SEDAN 3.5L V8 GASOLINA AUT.', ' 05 OCUP.', 'SERIE 7', 100, 01, 'A', 08, 4, 5, '', 01, 1212, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 758 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06542, 6542, 'BW', 'BMW', 'SERIE 3', '316 SEDAN 1.6L GASOLINA AUT.', ' 05 OCUP.', 'SERIE 3', 100, 01, 'A', 04, 4, 5, '', 01, 1212, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 759 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06543, 6543, 'BW', 'BMW', 'SERIE 4', '428I GRAND COUPE 2.8L GASOLINA AUT.', ' 05 OCUP.', 'SERIE 4', 100, 01, 'A', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 760 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06544, 6544, 'BW', 'BMW', 'SERIE X', 'X4 3.0L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'SERIE X', 105, 22, 'A', 04, 4, 5, '', 01, 1402, 0003, 0003, '', 'BMW'
);

/* INSERT QUERY NO: 761 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06545, 6545, 'BW', 'BMW', 'SERIE M', 'M4 SEDAN 3.0L GASOLINA STD.', ' 05 OCUP.', 'SERIE M', 100, 01, 'S', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 762 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06547, 6547, 'BW', 'BMW', 'SERIE Z', 'Z4 3.0L V6 STD.', ' 02 OCUP.', 'SERIE Z', 100, 04, 'S', 04, 4, 2, '', 01, 1402, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 763 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06548, 6548, 'BW', 'BMW', 'SERIE 4', '428I 2.0L 4x2 GASOLINA AUT.', ' 04 OCUP.', 'SERIE 4', 100, 01, 'A', 04, 4, 4, '', 01, 1402, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 764 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06549, 6549, 'BW', 'BMW', 'SERIE X', 'X4 2.0L 4x4 GASOLINA AUT.', ' 05 OCUP.', 'SERIE X', 105, 22, 'A', 04, 4, 5, '', 01, 1412, 0003, 0003, '', 'BMW'
);

/* INSERT QUERY NO: 765 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06550, 6550, 'BW', 'BMW', 'SERIE 3', '320 GT SEDAN 2.0L GASOLINA AUT.', ' 05 OCUP.', 'SERIE 3', 100, 01, 'A', 04, 4, 5, '', 01, 1412, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 766 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06551, 6551, 'BW', 'BMW', 'SERIE 4', '420I SEDAN 2.0L GASOLINA AUT.', ' 05 OCUP.', 'SERIE 4', 100, 01, 'A', 04, 4, 5, '', 01, 1412, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 767 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06552, 6552, 'BW', 'BMW', 'SERIE X', 'X4 2.0L 4x4 GASOLINA STD.', ' 05 OCUP.', 'SERIE X', 105, 22, 'S', 04, 4, 5, '', 01, 1412, 0003, 0003, '', 'BMW'
);

/* INSERT QUERY NO: 768 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06553, 6553, 'BW', 'BMW', 'SERIE M', 'M5 3.0L 4x2 GASOLINA STD.', ' 04 OCUP.', 'SERIE M', 100, 01, 'S', 08, 4, 4, '', 01, 1501, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 769 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06554, 6554, 'BW', 'BMW', 'SERIE X', 'X5 2.0L 4X2 DIESEL AUT.', ' 05 OCUP.', 'SERIE X', 105, 22, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'BMW'
);

/* INSERT QUERY NO: 770 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06555, 6555, 'BW', 'BMW', 'SERIE X', '3.0L 4X4 DIESEL AUT.', ' 05 OCUP.', 'SERIE X', 105, 22, 'A', 08, 4, 5, '', 01, 1501, 0003, 0003, '', 'BMW'
);

/* INSERT QUERY NO: 771 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06556, 6556, 'BW', 'BMW', 'SERIE M', 'M135i 3.0L 4x2 GASOLINA AUT.', ' 05 OCUP.', 'SERIE M', 100, 27, 'A', 06, 5, 5, '', 01, 1501, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 772 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06557, 6557, 'BW', 'BMW', 'SERIE X', 'X1 2.0L 4X2 DIESEL AUT.', ' 05 OCUP.', 'SERIE X', 105, 22, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'BMW'
);

/* INSERT QUERY NO: 773 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06558, 6558, 'BW', 'BMW', 'SERIE 8', '840 CI 4.0L GASOLINA AUT.', ' 04 OCUP.', 'SERIE 8', 100, 01, 'A', 08, 2, 4, '', 01, 1601, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 774 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06559, 6559, 'BW', 'BMW', 'SERIE 2', '220I CABRIOLET 2.0L GASOLINA STD.', ' 04 OCUP.', 'SERIE 2', 100, 04, 'S', 04, 2, 4, '', 01, 1601, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 775 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06560, 6560, 'BW', 'BMW', 'SERIE 5', '530DA 3.0L 4X2 DIESEL AUT.', ' 05 OCUP.', 'SERIE 5', 100, 01, 'A', 06, 4, 5, '', 01, 1601, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 776 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06561, 6561, 'BW', 'BMW', 'I3', 'CARRO ELECTRICO AUT.', ' 04 OCUP.', 'I3', 100, 27, 'A', 01, 5, 4, '', 01, 1601, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 777 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06562, 6562, 'BW', 'BMW', 'SERIE 1', '114 1.6L 4X2 GASOLINA STD.', ' 05 OCUP.', 'SERIE 1', 100, 27, 'S', 04, 3, 5, '', 01, 1601, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 778 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06563, 6563, 'BW', 'BMW', 'SERIE M', 'M135 3.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'SERIE M', 100, 27, 'S', 06, 5, 5, '', 01, 1601, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 779 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06564, 6564, 'BW', 'BMW', 'SERIE 4', '430 2.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'SERIE 4', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'BMW'
);

/* INSERT QUERY NO: 780 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06565, 6565, 'BW', 'BMW', 'SERIE X', 'X3 2.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'SERIE X', 105, 22, 'S', 04, 4, 5, '', 01, 1705, 0003, 0003, '', 'BMW'
);

/* INSERT QUERY NO: 781 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06701, 6701, 'IS', 'ISUZU', 'GEMINI', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'GEMINI', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'ISUZU'
);

/* INSERT QUERY NO: 782 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06702, 6702, 'IS', 'ISUZU', 'RODEO', '2.5L 4X4 GASOLINA STD.', ' 05 OCUP.', 'RODEO', 105, 22, 'S', 06, 4, 5, '', 01, 1106, 0003, 0003, '', 'ISUZU'
);

/* INSERT QUERY NO: 783 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06703, 6703, 'IS', 'ISUZU', 'TROOPER', '3.2L 4X4 GASOLINA STD.', ' 05 OCUP.', 'TOOPER', 105, 22, 'S', 06, 5, 5, '', 01, 1106, 0003, 0003, '', 'ISUZU'
);

/* INSERT QUERY NO: 784 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06704, 6704, 'IS', 'ISUZU', 'RODEO', '3.2L 4X4 GASOLINA STD.', ' 05 OCUP.', 'RODEO', 105, 22, 'S', 06, 4, 5, '', 01, 1106, 0003, 0003, '', 'ISUZU'
);

/* INSERT QUERY NO: 785 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06705, 6705, 'IS', 'ISUZU', 'AMIGO', '2.2L 4X2 GASOLINA STD.', ' 05 OCUP.', 'AMIGO', 105, 22, 'S', 04, 4, 5, '', 01, 1112, 0003, 0003, '', 'ISUZU'
);

/* INSERT QUERY NO: 786 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06706, 6706, 'IS', 'ISUZU', 'RODEO', 'LS 3.2L 4X2 GASOLINA STD.', ' 05 OCUP.', 'RODEO', 105, 22, 'S', 06, 5, 5, '', 01, 1212, 0003, 0003, '', 'ISUZU'
);

/* INSERT QUERY NO: 787 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06707, 6707, 'IS', 'ISUZU', 'MUX', '2.5L 4X2 DIESEL AUT.', ' 07 OCUP.', 'MUX', 105, 22, 'A', 04, 5, 7, '', 01, 1501, 0003, 0003, '', 'ISUZU'
);

/* INSERT QUERY NO: 788 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06708, 6708, 'IS', 'ISUZU', 'MUX', '2.5L 4X4 DIESEL AUT.', ' 07 OCUP.', 'MUX', 105, 22, 'A', 04, 5, 7, '', 01, 1501, 0003, 0003, '', 'ISUZU'
);

/* INSERT QUERY NO: 789 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06709, 6709, 'IS', 'ISUZU', 'AMIGO', 'XS 2.6L 4X4 GASOLINA STD.', ' 04 OCUP.', 'AMIGO', 105, 22, 'S', 04, 2, 4, '', 01, 1601, 0003, 0003, '', 'ISUZU'
);

/* INSERT QUERY NO: 790 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06901, 6901, 'MZ', 'MBENZ', 'CLASE A', '1.7L GASOLINA STD.', ' 05 OCUP.', 'CLASE A', 100, 27, 'S', 04, 3, 5, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 791 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06902, 6902, 'MZ', 'MBENZ', 'CLASE A', '1.7L GASOLINA AUT.', ' 05 OCUP.', 'CLASE A', 100, 27, 'A', 04, 3, 5, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 792 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06903, 6903, 'MZ', 'MBENZ', 'CLASE A', '1.6L GASOLINA STD.', ' 05 OCUP.', 'CLASE A', 100, 27, 'S', 04, 5, 5, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 793 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06904, 6904, 'MZ', 'MBENZ', 'CLASE A', '2.0L GASOLINA AUT.', ' 05 OCUP.', 'CLASE A', 100, 27, 'A', 04, 5, 5, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 794 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06905, 6905, 'MZ', 'MBENZ', 'CLASE B', '2.0L GASOLINA STD.', ' 05 OCUP.', 'CLASE B', 100, 27, 'S', 04, 5, 5, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 795 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06906, 6906, 'MZ', 'MBENZ', 'CLASE B', '2.0L GASOLINA AUT.', ' 05 OCUP.', 'CLASE B', 100, 27, 'A', 04, 5, 5, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 796 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06909, 6909, 'MZ', 'MBENZ', 'CLASE C', '1.8L GASOLINA STD.', ' 05 OCUP.', 'CLASE C', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 797 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06910, 6910, 'MZ', 'MBENZ', 'CLASE C', '1.8L GASOLINA AUT.', ' 05 OCUP.', 'CLASE C', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 798 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06911, 6911, 'MZ', 'MBENZ', 'CLASE C', '1.8L GASOLINA STD.', ' 05 OCUP.', 'CLASE C', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 799 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06912, 6912, 'MZ', 'MBENZ', 'CLASE C', '1.8L GASOLINA AUT.', ' 05 OCUP.', 'CLASE C', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 800 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06913, 6913, 'MZ', 'MBENZ', 'CLASE C', '3.0L V6 GASOLINA AUT.', ' 05 OCUP.', 'CLASE C', 100, 01, 'A', 06, 4, 5, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 801 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06914, 6914, 'MZ', 'MBENZ', 'CLASE C', '3.5L V6 GASOLINA AUT.', ' 05 OCUP.', 'CLASE C', 100, 01, 'A', 06, 4, 5, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 802 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06915, 6915, 'MZ', 'MBENZ', 'CLASE CLK', '3.5L V6 GASOLINA AUT.', ' 04 OCUP.', 'CLASE CLK', 100, 04, 'A', 06, 2, 4, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 803 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06916, 6916, 'MZ', 'MBENZ', 'CLASE CLK', '3.0L V6 GASOLINA AUT.', ' 04 OCUP.', 'CLASE CLK', 100, 04, 'A', 06, 2, 4, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 804 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06917, 6917, 'MZ', 'MBENZ', 'CLASE CLK', 'COUPE 3.0L V6 GASOLINA AUT.', ' 05 OCUP.', 'CLASE CLK', 100, 18, 'A', 06, 2, 5, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 805 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06918, 6918, 'MZ', 'MBENZ', 'CLASE CLK', 'COUPE 3.5L V6 GASOLINA AUT.', ' 05 OCUP.', 'CLASE CLK', 100, 18, 'A', 06, 2, 5, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 806 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06919, 6919, 'MZ', 'MBENZ', 'CLASE CLS', 'SEDAN 3.5L V6 GASOLINA AUT.', ' 04 OCUP.', 'CLASE CLS', 100, 01, 'A', 06, 4, 4, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 807 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06920, 6920, 'MZ', 'MBENZ', 'CLASE CLS', 'SEDAN 5.5L V8 GASOLINA AUT.', ' 04 OCUP.', 'CLASE CLS', 100, 01, 'A', 08, 4, 4, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 808 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06921, 6921, 'MZ', 'MBENZ', 'CLASE E', 'SEDAN 3.0L V6 GASOLINA AUT.', ' 05 OCUP.', 'CLASE E', 100, 01, 'A', 06, 4, 5, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 809 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06923, 6923, 'MZ', 'MBENZ', 'CLASE E', 'SEDAN 3.5L V6 GASOLINA AUT.', ' 05 OCUP.', 'CLASE E', 100, 01, 'A', 06, 4, 5, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 810 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06924, 6924, 'MZ', 'MBENZ', 'CLASE E', 'SEDAN 5.5L V8 GASOLINA AUT.', ' 05 OCUP.', 'CLASE E', 100, 01, 'A', 08, 4, 5, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 811 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06925, 6925, 'MZ', 'MBENZ', 'CLASE G', '5.0L V8 4X4 GASOLINA AUT.', ' 05 OCUP.', 'CLASE G', 105, 22, 'A', 08, 5, 5, '', 01, 1003, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 812 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06926, 6926, 'MZ', 'MBENZ', 'CLASE G', '5.5L V8 4X4 GASOLINA AUT.', ' 05 OCUP.', 'CLASE G', 105, 22, 'A', 08, 5, 5, '', 01, 1003, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 813 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06927, 6927, 'MZ', 'MBENZ', 'CLASE GL', '3.0L V6 4X4 GASOLINA AUT.', ' 07 OCUP.', 'CLASE GL', 105, 22, 'A', 06, 5, 7, '', 01, 1003, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 814 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06928, 6928, 'MZ', 'MBENZ', 'CLASE GL', '4.5L V8 4X4 GASOLINA AUT.', ' 07 OCUP.', 'CLASE GL', 105, 22, 'A', 08, 5, 7, '', 01, 1003, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 815 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06929, 6929, 'MZ', 'MBENZ', 'CLASE GL', '5.5L V8 4X4 GASOLINA AUT.', ' 07 OCUP.', 'CLASE GL', 105, 22, 'A', 08, 5, 7, '', 01, 1003, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 816 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06930, 6930, 'MZ', 'MBENZ', 'CLASE ML', '3.0L V6 4X4 AUT.', ' 05 OCUP.', 'CLASE ML', 105, 22, 'A', 06, 5, 5, '', 01, 1003, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 817 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06931, 6931, 'MZ', 'MBENZ', 'CLASE ML', '3.0L V6 4X4 AUT.', ' 05 OCUP.', 'CLASE ML', 105, 22, 'A', 06, 5, 5, '', 01, 1003, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 818 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06932, 6932, 'MZ', 'MBENZ', 'CLASE ML', '3.5L V6 4X4 AUT.', ' 05 OCUP.', 'CLASE ML', 105, 22, 'A', 06, 5, 5, '', 01, 1003, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 819 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06933, 6933, 'MZ', 'MBENZ', 'CLASE ML', '5.5L V8 4X4 GASOLINA AUT.', ' 05 OCUP.', 'CLASE ML', 105, 22, 'A', 08, 5, 5, '', 01, 1003, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 820 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06934, 6934, 'MZ', 'MBENZ', 'CLASE S', 'SEDAN 3.5L V6 GASOLINA AUT.', ' 05 OCUP.', 'CLASE S', 100, 01, 'A', 06, 4, 5, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 821 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06935, 6935, 'MZ', 'MBENZ', 'CLASE S', 'SEDAN 5.5L V8 GASOLINA AUT.', ' 05 OCUP.', 'CLASE S', 100, 01, 'A', 08, 4, 5, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 822 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06936, 6936, 'MZ', 'MBENZ', 'CLASE SL', 'CABRIO 5.5L V8 GASOLINA AUT.', ' 02 OCUP.', 'CLASE SL', 100, 04, 'A', 08, 2, 2, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 823 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06937, 6937, 'MZ', 'MBENZ', 'CLASE SL', 'CABRIO 6.2L V8 GASOLINA AUT.', ' 02 OCUP.', 'CLASE SL', 100, 04, 'A', 08, 2, 2, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 824 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06938, 6938, 'MZ', 'MBENZ', 'CLASE SLK', '1.8L GASOLINA AUT.', ' 02 OCUP.', 'CLASE SLK', 100, 04, 'A', 04, 2, 2, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 825 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06939, 6939, 'MZ', 'MBENZ', 'CLASE SLK', '3.5L V6 GASOLINA AUT.', ' 02 OCUP.', 'CLASE SLK', 100, 04, 'A', 06, 2, 2, '', 01, 1003, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 826 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06940, 6940, 'MZ', 'MBENZ', 'CLASE E', 'SEDAN 3.0L V6 AUT.', ' 05 OCUP.', 'CLASE E', 100, 01, 'A', 06, 4, 5, '', 01, 1106, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 827 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06941, 6941, 'MZ', 'MBENZ', 'CLASE ML', '3.0L V6 4X4 AUT.', ' 05 OCUP.', 'CLASE ML', 105, 22, 'A', 06, 5, 5, '', 01, 1106, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 828 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06942, 6942, 'MZ', 'MBENZ', 'CLASE E', 'COUPE 3.5L V6 AUT.', ' 05 OCUP.', 'CLASE E', 100, 18, 'A', 06, 2, 5, '', 01, 1106, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 829 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06943, 6943, 'MZ', 'MBENZ', 'CLASE E', 'COUPE 1.8L AUT.', ' 05 OCUP.', 'CLASE E', 100, 18, 'A', 04, 2, 5, '', 01, 1106, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 830 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06944, 6944, 'MZ', 'MBENZ', 'CLASE GL', '3.0L V6 4X4 AUT.', ' 07 OCUP.', 'CLASE GL', 105, 22, 'A', 06, 5, 7, '', 01, 1106, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 831 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06945, 6945, 'MZ', 'MBENZ', 'CLASE C', '2.6L V6 GASOLINA AUT.', ' 05 OCUP.', 'CLASE C', 100, 01, 'A', 06, 4, 5, '', 01, 1106, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 832 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06946, 6946, 'MZ', 'MBENZ', 'CLASE CLK', 'SEDAN 3.0L GASOLINA AUT.', ' 04 OCUP.', 'CLASE CLK', 100, 01, 'A', 04, 2, 4, '', 01, 1106, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 833 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06947, 6947, 'MZ', 'MBENZ', 'CLASE E', 'SEDAN 3.2L V6 AUT.', ' 05 OCUP.', 'CLASE E', 100, 01, 'A', 06, 4, 5, '', 01, 1106, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 834 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06948, 6948, 'MZ', 'MBENZ', 'CLASE ML', '5.5L V6 4X4 AUT.', ' 05 OCUP.', 'CLASE ML', 105, 22, 'A', 06, 4, 5, '', 01, 1106, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 835 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06949, 6949, 'MZ', 'MBENZ', 'CLASE G', '4.0L V8 4X4 AUT.', ' 05 OCUP.', 'CLASE G', 105, 22, 'A', 08, 5, 5, '', 01, 1106, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 836 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06951, 6951, 'MZ', 'MBENZ', 'CLASE C', '2.2L STD.', ' 05 OCUP.', 'CLASE C', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 837 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06952, 6952, 'MZ', 'MBENZ', 'CLASE C', '8L AUT.', ' 05 OCUP.', 'CLASE C', 100, 01, 'A', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 838 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06953, 6953, 'MZ', 'MBENZ', 'CLASE G', '4.0L V8 GASOLINA AUT.', ' 05 OCUP.', 'CLASE G', 105, 22, 'A', 08, 5, 5, '', 01, 1112, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 839 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06954, 6954, 'MZ', 'MBENZ', 'CLASE ML', '4.3L V8 4X4 AUT.', ' 05 OCUP.', 'CLASE ML', 105, 22, 'A', 08, 5, 5, '', 01, 1112, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 840 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06955, 6955, 'MZ', 'MBENZ', 'CLASE ML', '3.0L V6 4X4 AUT.', ' 05 OCUP.', 'CLASE ML', 105, 22, 'A', 06, 5, 5, '', 01, 1112, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 841 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06956, 6956, 'MZ', 'MBENZ', 'CLASE R', '3.0L V6 AUT.', ' 05 OCUP.', 'CLASE R', 105, 01, 'A', 06, 5, 5, '', 01, 1112, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 842 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06957, 6957, 'MZ', 'MBENZ', 'CLASE R', '3.5L V6 AUT.', ' 05 OCUP.', 'CLASE R', 105, 01, 'A', 06, 5, 5, '', 01, 1112, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 843 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06958, 6958, 'MZ', 'MBENZ', 'CLASE GLK', '2.0L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'CLASE GLK', 105, 22, 'A', 06, 5, 5, '', 01, 1112, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 844 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06959, 6959, 'MZ', 'MBENZ', 'CLASE E', '2.0L GASOLINA AUT.', ' 05 OCUP.', 'CLASE E', 100, 01, 'A', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 845 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06960, 6960, 'MZ', 'MBENZ', 'CLASE E', '4.2L V8 GASOLINA STD.', ' 05 OCUP.', 'CLASE E', 100, 01, 'S', 08, 5, 5, '', 01, 1112, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 846 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06961, 6961, 'MZ', 'MBENZ', 'CLASE G', '3.5L V6 DIESEL AUT.', ' 05 OCUP.', 'CLASE G', 105, 22, 'A', 06, 5, 5, '', 01, 1112, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 847 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06962, 6962, 'MZ', 'MBENZ', 'CLASE E', 'SEDAN 2.1L DIESEL AUT.', ' 05 OCUP.', 'CLASE E', 100, 01, 'A', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 848 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06963, 6963, 'MZ', 'MBENZ', 'CLASE E', 'SEDAN 2.3L GASOLINA STD.', ' 05 OCUP.', 'CLASE E', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 849 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06964, 6964, 'MZ', 'MBENZ', 'CLASE C', 'SEDAN 1.8L GASOLINA AUT.', ' 05 OCUP.', 'CLASE C', 100, 01, 'A', 04, 5, 5, '', 01, 1112, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 850 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06965, 6965, 'MZ', 'MBENZ', 'CLASE C', '6.2L V8 GASOLINA AUT.', ' 05 OCUP.', 'CLASE C', 100, 01, 'A', 08, 5, 5, '', 01, 1112, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 851 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06966, 6966, 'MZ', 'MBENZ', 'CLASE E', 'SEDAN 1.8L GASOLINA AUT.', ' 05 OCUP.', 'CLASE E', 100, 01, 'A', 04, 5, 5, '', 01, 1112, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 852 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06967, 6967, 'MZ', 'MBENZ', 'CLASE ML', '6.2L V8 AUT.', ' 05 OCUP.', 'CLASE ML', 105, 22, 'A', 08, 5, 5, '', 01, 1112, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 853 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06968, 6968, 'MZ', 'MBENZ', 'CLASE C', '3.0L V6 GASOLINA AUT.', ' 05 OCUP.', 'CLASE C', 100, 01, 'A', 06, 4, 5, '', 01, 1112, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 854 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06969, 6969, 'MZ', 'MBENZ', 'CLASE G', '270 2.7L V5 4X4 DIESEL AUT.', ' 07 OCUP.', 'CLASE G', 105, 01, 'A', 05, 5, 7, '', 01, 1112, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 855 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06970, 6970, 'MZ', 'MBENZ', 'CLASE SLK', '1.8L GASOLINA AUT.', ' 02 OCUP.', 'CLASE SLK', 100, 04, 'A', 06, 2, 2, '', 01, 1112, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 856 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06971, 6971, 'MZ', 'MBENZ', 'CLASE ML', '5.4L GASOLINA AUT.', ' 05 OCUP.', 'CLASE ML', 105, 22, 'A', 08, 5, 5, '', 01, 1212, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 857 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06972, 6972, 'MZ', 'MBENZ', 'KOMPRESSOR', '2.3L GASOLINA STD.', ' 02 OCUP.', 'KOMPRESSOR', 100, 01, 'S', 04, 2, 2, '', 01, 1212, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 858 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06973, 6973, 'MZ', 'MBENZ', 'CLASE E', '3.2L STD.', '05 OCUP.', 'CLASE E', 100, 01, 'S', 06, 4, 5, '', 01, 1212, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 859 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06974, 6974, 'MZ', 'MBENZ', 'CLASE CLS', '5.0L GASOLINA STD.', ' 05 OCUP.', 'CLASE CLS', 100, 01, 'S', 08, 4, 5, '', 01, 1212, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 860 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06975, 6975, 'MZ', 'MBENZ', 'CLASE CLA', '1.6L GASOLINA AUT.', ' 05 OCUP.', 'CLASE CLS', 100, 01, 'A', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 861 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06976, 6976, 'MZ', 'MBENZ', 'S63', 'SEDAN 6.3L GASOLINA STD.', ' 05 OCUP.', 'S63', 100, 01, 'S', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 862 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06977, 6977, 'MZ', 'MBENZ', '300', 'SE SEDAN 3.2L 4X2 GASOLINA AUT.', ' 05 OCUP.', '300', 100, 01, 'A', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 863 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06978, 6978, 'MZ', 'MBENZ', 'CLASE CLA', '2.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'CLASE CLA', 100, 01, 'S', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 864 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06979, 6979, 'MZ', 'MBENZ', 'CLASE G', '1.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'GLA', 105, 22, 'S', 04, 4, 5, '', 01, 1412, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 865 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06980, 6980, 'MZ', 'MBENZ', 'CLASE G', '290 GDT 2.8L 4X4 DIESEL STD.', ' 05 OCUP.', 'CLASE G', 105, 03, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 866 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06981, 6981, 'MZ', 'MBENZ', '280 S', 'S SEDAN 2.8L 4X2 GASOLINA STD.', ' 05 OCUP.', '280 S', 100, 01, 'S', 06, 4, 5, '', 01, 1501, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 867 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06982, 6982, 'MZ', 'MBENZ', 'CLASE GLC', '2.0L 4X4 GASOLINA STD.', ' 05 OCUP.', 'CLASE GLC', 105, 22, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 868 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06983, 6983, 'MZ', 'MBENZ', 'CLASE GLE', '3.5L 4X4 DIESEL AUT.', ' 05 OCUP.', 'CLASE GLE', 105, 22, 'A', 06, 4, 5, '', 01, 1501, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 869 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06984, 6984, 'MZ', 'MBENZ', 'CLASE C', '2.0L GASOLINA AUT.', ' 05 OCUP.', 'CLASE C', 100, 01, 'A', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 870 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06985, 6985, 'MZ', 'MBENZ', 'CLASE G', '4.0L DIESEL STD.', ' 07 OCUP.', 'CLASE G', 105, 22, 'S', 08, 5, 7, '', 01, 1501, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 871 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06986, 6986, 'MZ', 'MBENZ', 'CLASE C', '1.8L GASOLINA STD.', ' 05 OCUP.', 'CLASE C', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 872 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06987, 6987, 'MZ', 'MBENZ', 'CLASE GLK', '2.1L 4X4 DIESEL AUT.', ' 05 OCUP.', 'CLASE GLK', 105, 22, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 873 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06988, 6988, 'MZ', 'MBENZ', '200', '2.0L GASOLINA STD.', ' 05 OCUP.', '200', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 874 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06989, 6989, 'MZ', 'MBENZ', '190D', '2.0L DIESEL STD.', ' 05 OCUP.', '190D', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 875 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06990, 6990, 'MZ', 'MBENZ', 'CLASE GLA', '2.0L GASOLINA SUPER STD.', ' 05 OCUP.', 'CLASE GLA', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 876 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06991, 6991, 'MZ', 'MBENZ', 'CLASE C', 'AMG 3.0L GASOLINA STD.', ' 05 OCUP.', 'CLASE C', 100, 01, 'S', 06, 4, 5, '', 01, 1601, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 877 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06992, 6992, 'MZ', 'MBENZ', 'CLASE GL', '4MATIC 5.5L 4X4 GASOLINA AUT.', ' 07 OCUP.', 'CLASE GL', 105, 22, 'A', 08, 4, 7, '', 01, 1601, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 878 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06993, 6993, 'MZ', 'MBENZ', 'CLASE GLE', 'COUPE 3.0L 4X4 DIESEL AUT.', ' 05 OCUP.', 'CLASE GLE', 105, 22, 'A', 06, 5, 5, '', 01, 1601, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 879 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06994, 6994, 'MZ', 'MBENZ', 'CLASE GLK', '350 3.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'CLASE GLK', 105, 22, 'S', 06, 4, 5, '', 01, 1601, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 880 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06995, 6995, 'MZ', 'MBENZ', 'CLASE GLA', '180 1.6L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'CLASE GLA', 100, 27, 'A', 04, 5, 5, '', 01, 1601, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 881 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06996, 6996, 'MZ', 'MBENZ', 'CLASE ML', '250 2.1L 4X4 DIESEL AUT.', ' 05 OCUP.', 'CLASE ML', 105, 22, 'A', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 882 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06997, 6997, 'MZ', 'MBENZ', 'CLASE G', '320L 3.2L 4X4 GASOLINA AUT.', ' 07 OCUP.', 'CLASE G', 105, 22, 'A', 06, 4, 7, '', 01, 1601, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 883 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06998, 6998, 'MZ', 'MBENZ', 'CLASE GLE', '250D 2.1L 4X4 DIESEL AUT.', ' 05 OCUP.', 'CLASE GLE', 105, 22, 'A', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 884 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 06999, 6999, 'MZ', 'MBENZ', 'CLASE GLE', '250D 2.1L 4X4 DIESEL STD.', ' 05 OCUP.', 'CLASE GLE', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 885 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07000, 7000, 'MZ', 'MBENZ', 'CLASE CLA', '180 1.6L 4X2 GASOLINA STD.', ' 05 OCUP.', 'CLASE CLA', 100, 01, 'S', 04, 4, 5, '', 01, 1705, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 886 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07001, 7001, 'MZ', 'MBENZ', 'CLASE GLK', '300 3.0L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'CLASE GLK', 105, 22, 'A', 06, 4, 5, '', 01, 1705, 0003, 0003, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 887 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07002, 7002, 'MZ', 'MBENZ', '300', 'SEL 3.0L GASOLINA STD.', ' 05 OCUP.', '300', 100, 01, 'S', 06, 4, 5, '', 01, 1705, 0001, 0001, '', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 888 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07048, 7048, 'RC', 'RC EXCESO', 'UMBRELLA', 'AUT.', ' 00 OCUP.', 'UMBRELLA', 100, 01, 'A', 04, 2, 0, '', 01, 1212, 0000, 0000, '', 'RC EXCESO'
);

/* INSERT QUERY NO: 889 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07101, 7101, 'DU', 'DAIHATSU', 'TERIOS BEGO', '1.5L 4X2 FULL STD.', ' 05 OCUP.', 'TERIOS BEGO', 105, 22, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'DAIHATSU'
);

/* INSERT QUERY NO: 890 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07102, 7102, 'DU', 'DAIHATSU', 'TERIOS BEGO', '1.5L 4X4 FULL STD.', ' 05 OCUP.', 'TERIOS BEGO', 105, 22, 'S', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'DAIHATSU'
);

/* INSERT QUERY NO: 891 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07104, 7104, 'DU', 'DAIHATSU', 'SIRION', '1.0L STD.', ' 05 OCUP.', 'SIRION', 100, 27, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'DAIHATSU'
);

/* INSERT QUERY NO: 892 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07105, 7105, 'DU', 'DAIHATSU', 'CHARADE', '1.0L GASOLINA STD.', ' 05 OCUP.', 'CHARADE', 100, 27, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'DAIHATSU'
);

/* INSERT QUERY NO: 893 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07106, 7106, 'DU', 'DAIHATSU', 'CUORE', '1.0L GASOLINA STD.', ' 05 OCUP.', 'CUORE', 100, 01, 'S', 03, 4, 5, '', 01, 1106, 0001, 0001, '', 'DAIHATSU'
);

/* INSERT QUERY NO: 894 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07107, 7107, 'DU', 'DAIHATSU', 'GRAND MOVE', '1.6L GASOLINA STD.', ' 05 OCUP.', 'GRAND MOVE', 100, 20, 'S', 04, 5, 5, '', 01, 1106, 0001, 0001, '', 'DAIHATSU'
);

/* INSERT QUERY NO: 895 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07108, 7108, 'DU', 'DAIHATSU', 'FEROZA', '1.6L 4X4 GASOLINA STD.', ' 05 OCUP.', 'FEROZA', 105, 22, 'S', 04, 3, 5, '', 01, 1106, 0003, 0003, '', 'DAIHATSU'
);

/* INSERT QUERY NO: 896 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07109, 7109, 'DU', 'DAIHATSU', 'GRAN MAX', '1.5L GASOLINA STD.', ' 05 OCUP.', 'GRAN MAX', 100, 02, 'S', 04, 5, 5, '', 01, 1112, 0001, 0001, '', 'DAIHATSU'
);

/* INSERT QUERY NO: 897 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07110, 7110, 'DU', 'DAIHATSU', 'ROCKY', '1.7L GASOLINA STD.', ' 04 OCUP.', 'ROCKY', 105, 22, 'S', 04, 3, 4, '', 01, 1112, 0003, 0003, '', 'DAIHATSU'
);

/* INSERT QUERY NO: 898 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07111, 7111, 'DU', 'DAIHATSU', 'TERIOS', '1.3L 4X4 GASOLINA STD.', ' 05 OCUP.', 'TERIOS', 105, 22, 'S', 04, 4, 5, '', 01, 1112, 0003, 0003, '', 'DAIHATSU'
);

/* INSERT QUERY NO: 899 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07112, 7112, 'DU', 'DAIHATSU', 'YRV', 'STATION WAGON 1.3L GASOLINA STD.', ' 05 OCUP.', 'YRV', 100, 27, 'S', 04, 5, 5, '', 01, 1212, 0001, 0001, '', 'DAIHATSU'
);

/* INSERT QUERY NO: 900 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07113, 7113, 'DU', 'DAIHATSU', 'TERIOS BEGO', '1.5L 4X2 GASOLINA FULL STD.', ' 05 OCUP.', 'TERIOS BEGO', 105, 22, 'S', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'DAIHATSU'
);

/* INSERT QUERY NO: 901 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07114, 7114, 'DU', 'DAIHATSU', 'TERIOS BEGO', '1.5L 4X2 ED ESP GASOLINA STD.', ' 05 OCUP.', 'TERIOS BEGO', 105, 22, 'S', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'DAIHATSU'
);

/* INSERT QUERY NO: 902 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07115, 7115, 'DU', 'DAIHATSU', 'TERIOS BEGO', '1.5L 4X4 ED ESP GASOLINA STD.', ' 05 OCUP.', 'TERIOS BEGO', 105, 22, 'S', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'DAIHATSU'
);

/* INSERT QUERY NO: 903 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07116, 7116, 'DU', 'DAIHATSU', 'TERIOS BEGO', '1.5L 4X4 GASOLINA SUPERFULL STD.', ' 05 OCUP.', 'TERIOS BEGO', 105, 22, 'S', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'DAIHATSU'
);

/* INSERT QUERY NO: 904 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07117, 7117, 'DU', 'DAIHATSU', 'TERIOS BEGO', '1.5L 4X4 GASOLINA SUPERFULL AUT.', ' 05 OCUP.', 'TERIOS BEGO', 105, 22, 'A', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'DAIHATSU'
);

/* INSERT QUERY NO: 905 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07118, 7118, 'DU', 'DAIHATSU', 'TERIOS BEGO', '1.5L 4X4 ED ESP GASOLINA AUT.', ' 05 OCUP.', 'TERIOS BEGO', 105, 22, 'A', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'DAIHATSU'
);

/* INSERT QUERY NO: 906 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07119, 7119, 'DU', 'DAIHATSU', 'TERIOS BEGO', '1.5L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'TERIOS BEGO', 105, 22, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'DAIHATSU'
);

/* INSERT QUERY NO: 907 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07301, 7301, 'AA', 'ACURA', 'MDX', '4X4 AUT.', ' 07 OCUP.', 'MDX', 105, 22, 'A', 04, 4, 7, '', 01, 1106, 0003, 0003, '', 'ACURA'
);

/* INSERT QUERY NO: 908 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07302, 7302, 'AA', 'ACURA', 'CL', '2.3L GASOLINA STD.', ' 05 OCUP.', 'CL', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'ACURA'
);

/* INSERT QUERY NO: 909 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07303, 7303, 'AA', 'ACURA', 'INTEGRA', '1.8L GASOLINA STD.', ' 05 OCUP.', 'INTEGRA', 100, 01, 'S', 04, 2, 5, '', 01, 1112, 0001, 0001, '', 'ACURA'
);

/* INSERT QUERY NO: 910 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07304, 7304, 'AA', 'ACURA', 'LEGEND', '3.2L V6 GASOLINA AUT.', ' 05 OCUP.', 'LEGEND', 100, 01, 'A', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'ACURA'
);

/* INSERT QUERY NO: 911 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07305, 7305, 'AA', 'ACURA', 'RSX', 'S SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'RSX', 100, 01, 'S', 04, 2, 5, '', 01, 1212, 0001, 0001, '', 'ACURA'
);

/* INSERT QUERY NO: 912 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07401, 7401, 'VO', 'VOLVO', 'C30', 'HB 2.0L GASOLINA STD.', ' 04 OCUP.', 'C30', 100, 27, 'S', 04, 2, 4, '', 01, 1003, 0001, 0001, '', 'VOLVO'
);

/* INSERT QUERY NO: 913 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07402, 7402, 'VO', 'VOLVO', 'C30', 'HB 2.0L GASOLINA AUT.', ' 04 OCUP.', 'C30', 100, 27, 'A', 05, 2, 4, '', 01, 1003, 0001, 0001, '', 'VOLVO'
);

/* INSERT QUERY NO: 914 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07403, 7403, 'VO', 'VOLVO', 'C70', 'CABRIO 2.5L GASOLINA AUT.', ' 04 OCUP.', 'C70', 100, 04, 'A', 05, 2, 4, '', 01, 1003, 0001, 0001, '', 'VOLVO'
);

/* INSERT QUERY NO: 915 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07404, 7404, 'VO', 'VOLVO', 'S40', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'S40', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'VOLVO'
);

/* INSERT QUERY NO: 916 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07405, 7405, 'VO', 'VOLVO', 'S40', 'SEDAN 2.5L GASOLINA AUT.', ' 05 OCUP.', 'S40', 100, 01, 'A', 05, 4, 5, '', 01, 1003, 0001, 0001, '', 'VOLVO'
);

/* INSERT QUERY NO: 917 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07406, 7406, 'VO', 'VOLVO', 'S60', 'SEDAN 2.0T GASOLINA AUT.', ' 05 OCUP.', 'S60', 100, 01, 'A', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'VOLVO'
);

/* INSERT QUERY NO: 918 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07407, 7407, 'VO', 'VOLVO', 'S60', 'SEDAN 3.0L GASOLINA AUT.', ' 05 OCUP.', 'S60', 100, 01, 'A', 06, 4, 5, '', 01, 1003, 0001, 0001, '', 'VOLVO'
);

/* INSERT QUERY NO: 919 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07408, 7408, 'VO', 'VOLVO', 'S80', 'SEDAN 2.4L DIESEL AUT.', ' 05 OCUP.', 'S80', 100, 01, 'A', 05, 4, 5, '', 01, 1003, 0001, 0001, '', 'VOLVO'
);

/* INSERT QUERY NO: 920 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07409, 7409, 'VO', 'VOLVO', 'S80', 'SEDAN 2.5L GASOLINA AUT.', ' 05 OCUP.', 'S80', 100, 01, 'A', 05, 4, 5, '', 01, 1003, 0001, 0001, '', 'VOLVO'
);

/* INSERT QUERY NO: 921 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07410, 7410, 'VO', 'VOLVO', 'XC60', '2.0L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'XC60', 105, 22, 'A', 04, 4, 5, '', 01, 1003, 0003, 0003, '', 'VOLVO'
);

/* INSERT QUERY NO: 922 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07411, 7411, 'VO', 'VOLVO', 'XC60', '3.0L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'XC60', 105, 22, 'A', 06, 4, 5, '', 01, 1003, 0003, 0003, '', 'VOLVO'
);

/* INSERT QUERY NO: 923 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07412, 7412, 'VO', 'VOLVO', 'XC60', '2.4L 4X4 DIESEL AUT.', ' 05 OCUP.', 'XC60', 105, 22, 'A', 05, 4, 5, '', 01, 1003, 0003, 0003, '', 'VOLVO'
);

/* INSERT QUERY NO: 924 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07413, 7413, 'VO', 'VOLVO', 'XC90', '3.2L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'XC90', 105, 22, 'A', 06, 4, 5, '', 01, 1003, 0003, 0003, '', 'VOLVO'
);

/* INSERT QUERY NO: 925 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07414, 7414, 'VO', 'VOLVO', 'XC90', '2.4L 4X4 DIESEL AUT.', ' 05 OCUP.', 'XC90', 105, 22, 'A', 05, 4, 5, '', 01, 1003, 0003, 0003, '', 'VOLVO'
);

/* INSERT QUERY NO: 926 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07415, 7415, 'VO', 'VOLVO', 'XC90', '4.4L V8 4X4 GASOLINA AUT.', ' 05 OCUP.', 'XC90', 105, 22, 'A', 08, 4, 5, '', 01, 1003, 0003, 0003, '', 'VOLVO'
);

/* INSERT QUERY NO: 927 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07417, 7417, 'VO', 'VOLVO', 'XC70', '2.5L GASOLINA AUT.', ' 05 OCUP.', 'XC70', 105, 02, 'A', 05, 5, 5, '', 01, 1112, 0003, 0003, '', 'VOLVO'
);

/* INSERT QUERY NO: 928 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07418, 7418, 'VO', 'VOLVO', 'V40', 'SEDAN 1.7L GASOLINA STD.', ' 05 OCUP.', 'V40', 105, 01, 'S', 04, 4, 5, '', 01, 1112, 0003, 0003, '', 'VOLVO'
);

/* INSERT QUERY NO: 929 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07419, 7419, 'VO', 'VOLVO', 'C30', 'T5 SEDAN 2.5L 4X2 GASOLINA AUT.', ' 04 OCUP.', 'C30', 100, 01, 'A', 05, 2, 4, '', 01, 1402, 0001, 0001, '', 'VOLVO'
);

/* INSERT QUERY NO: 930 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07420, 7420, 'VO', 'VOLVO', 'V70', '2.5L DIESEL STD.', ' 05 OCUP.', 'V70', 105, 22, 'S', 05, 4, 5, '', 01, 1501, 0003, 0003, '', 'VOLVO'
);

/* INSERT QUERY NO: 931 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07421, 7421, 'VO', 'VOLVO', 'V50', '2.4L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'V50', 105, 22, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'VOLVO'
);

/* INSERT QUERY NO: 932 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07422, 7422, 'VO', 'VOLVO', 'S90', 'T5 2.0L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'S90', 100, 01, 'A', 04, 4, 5, '', 01, 1705, 0001, 0001, '', 'VOLVO'
);

/* INSERT QUERY NO: 933 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07423, 7423, 'VO', 'VOLVO', 'XC40', 'T5 2.0L GASOLINA AUT.', ' 05 OCUP.', 'XC40', 105, 22, 'A', 04, 4, 5, '', 01, 1705, 0003, 0003, '', 'VOLVO'
);

/* INSERT QUERY NO: 934 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07501, 7501, 'AI', 'AUDI', 'A1', '1.4L TURBO AUT.', ' 04 OCUP.', 'A1', 100, 01, 'A', 04, 2, 4, '', 01, 1106, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 935 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07502, 7502, 'AI', 'AUDI', 'A3', '1.2L GASOLINA STD.', ' 05 OCUP.', 'A3', 100, 01, 'S', 04, 2, 5, '', 01, 1106, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 936 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07503, 7503, 'AI', 'AUDI', 'A4', '1.8L TURBO STD.', ' 05 OCUP.', 'A4', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 937 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07504, 7504, 'AI', 'AUDI', 'A4', '1.8L TURBO AUT.', ' 05 OCUP.', 'A4', 100, 01, 'A', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 938 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07505, 7505, 'AI', 'AUDI', 'A5', '1.8L TURBO GASOLINA AUT.', ' 04 OCUP.', 'A5', 100, 01, 'A', 04, 4, 4, '', 01, 1106, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 939 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07506, 7506, 'AI', 'AUDI', 'A7', '3.0L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'A7', 100, 01, 'A', 06, 4, 5, '', 01, 1106, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 940 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07507, 7507, 'AI', 'AUDI', 'A8', '3.0L V6 AUT.', ' 05 OCUP.', 'A8', 100, 01, 'A', 08, 4, 5, '', 01, 1106, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 941 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07508, 7508, 'AI', 'AUDI', 'Q5', '2.0L TURBO 4X4 AUT.', ' 05 OCUP.', 'Q5', 105, 22, 'A', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'AUDI'
);

/* INSERT QUERY NO: 942 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07509, 7509, 'AI', 'AUDI', 'Q7', '3.0L 4X4 TDI AUT.', ' 07 OCUP.', 'Q7', 105, 22, 'A', 06, 4, 7, '', 01, 1106, 0003, 0003, '', 'AUDI'
);

/* INSERT QUERY NO: 943 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07510, 7510, 'AI', 'AUDI', 'A6', '2.8L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'A6', 105, 01, 'A', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'AUDI'
);

/* INSERT QUERY NO: 944 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07511, 7511, 'AI', 'AUDI', 'A6', '2.4L GASOLINA AUT.', ' 05 OCUP.', 'A6', 100, 01, 'A', 06, 4, 5, '', 01, 1106, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 945 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07512, 7512, 'AI', 'AUDI', 'A6', '4.2L V8 GASOLINA AUT.', ' 05 OCUP.', 'A6', 100, 01, 'A', 08, 4, 5, '', 01, 1106, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 946 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07513, 7513, 'AI', 'AUDI', 'A6', 'TURBO 2.0L GASOLINA AUT.', ' 05 OCUP.', 'A6', 100, 01, 'A', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 947 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07514, 7514, 'AI', 'AUDI', 'A6', 'TURBO 3.0L V6 GASOLINA AUT.', ' 05 OCUP.', 'A6', 100, 01, 'A', 06, 4, 5, '', 01, 1106, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 948 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07515, 7515, 'AI', 'AUDI', 'R8', 'COUPE 4.2L V8 GASOLINA AUT.', ' 02 OCUP.', 'R8', 100, 18, 'A', 08, 2, 2, '', 01, 1106, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 949 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07516, 7516, 'AI', 'AUDI', 'TT', 'COUPE 2.0L GASOLINA AUT.', ' 05 OCUP.', 'TT', 100, 18, 'A', 06, 4, 5, '', 01, 1106, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 950 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07517, 7517, 'AI', 'AUDI', 'RS5', 'COUPE 4.2L V6 GASOLINA AUT.', ' 05 OCUP.', 'RS5', 100, 18, 'A', 06, 4, 5, '', 01, 1106, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 951 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07518, 7518, 'AI', 'AUDI', 'S3', '2.0L GASOLINA AUT.', ' 05 OCUP.', 'S3', 100, 01, 'A', 04, 3, 5, '', 01, 1112, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 952 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07519, 7519, 'AI', 'AUDI', 'TT', 'COUPE 1.8L GASOLINA STD.', ' 02 OCUP.', 'TT', 100, 18, 'S', 04, 2, 2, '', 01, 1112, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 953 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07520, 7520, 'AI', 'AUDI', 'Q3', '2.0L 4X4 GASOLINA STD.', ' 05 OCUP.', 'Q7', 105, 22, 'S', 04, 5, 5, '', 01, 1112, 0003, 0003, '', 'AUDI'
);

/* INSERT QUERY NO: 954 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07521, 7521, 'AI', 'AUDI', 'S7', '4.0L GASOLINA AUT.', ' 05 OCUP.', 'S7', 100, 01, 'A', 08, 4, 5, '', 01, 1212, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 955 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07522, 7522, 'AI', 'AUDI', 'R8', 'COUPE 5.2L V8 GASOLINA AUT.', ' 02 OCUP.', 'R8', 100, 01, 'A', 08, 4, 2, '', 01, 1412, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 956 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07523, 7523, 'AI', 'AUDI', 'S6', '4.0L GASOLINA AUT.', ' 05 OCUP.', 'S6', 100, 01, 'A', 08, 4, 5, '', 01, 1412, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 957 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07524, 7524, 'AI', 'AUDI', 'S4', '4.2L GASOLINA STD.', ' 05 OCUP.', 'S4', 100, 01, 'S', 08, 4, 5, '', 01, 1501, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 958 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07525, 7525, 'AI', 'AUDI', 'Q3', '1.4L 4X2 GASOLINA STD.', ' 05 OCUP.', 'Q3', 105, 22, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'AUDI'
);

/* INSERT QUERY NO: 959 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07526, 7526, 'AI', 'AUDI', 'S3', 'SEDAN 2.0L GASOLINA AUT.', ' 05 OCUP.', 'S3', 100, 01, 'A', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 960 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07527, 7527, 'AI', 'AUDI', 'RS7', '4.0L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'RS7', 100, 27, 'A', 08, 5, 5, '', 01, 1601, 0001, 0001, '', 'AUDI'
);

/* INSERT QUERY NO: 961 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07528, 7528, 'AI', 'AUDI', 'Q2', '1.4L 4X2 GASOLINA STD.', ' 05 OCUP.', 'Q2', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'AUDI'
);

/* INSERT QUERY NO: 962 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07529, 7529, 'AI', 'AUDI', 'Q7', '3.0L 4X4 DIESEL AUT.', ' 07 OCUP.', 'Q7', 105, 22, 'A', 06, 4, 7, '', 01, 1705, 0003, 0003, '', 'AUDI'
);

/* INSERT QUERY NO: 963 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07530, 7530, 'AI', 'AUDI', 'A6', 'AVANT 2.5L 4X2 DIESEL AUT.', ' 05 OCUP.', 'A6', 105, 22, 'A', 06, 4, 5, '', 01, 1705, 0003, 0003, '', 'AUDI'
);

/* INSERT QUERY NO: 964 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07601, 7601, 'CC', 'CADILLAC', 'S.T.S.', 'SEDAN 4.9L GASOLINA AUT.', ' 05 OCUP.', 'S.T.S.', 100, 01, 'A', 08, 4, 5, '', 01, 1106, 0001, 0001, '', 'CADILLAC'
);

/* INSERT QUERY NO: 965 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07602, 7602, 'CC', 'CADILLAC', 'ESCALADE', 'ESV 6.2L 4X4 GASOLINA AUT.', ' 07 OCUP.', 'ESCALADE', 105, 22, 'A', 08, 4, 7, '', 01, 1402, 0003, 0003, '', 'CADILLAC'
);

/* INSERT QUERY NO: 966 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07603, 7603, 'CC', 'CADILLAC', 'SRX', '4.6L 4X4 GASOLINA STD.', ' 07 OCUP.', 'SRX', 105, 22, 'S', 08, 4, 7, '', 01, 1501, 0003, 0003, '', 'CADILLAC'
);

/* INSERT QUERY NO: 967 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07604, 7604, 'CC', 'CADILLAC', 'ESCALADE', '6.2L 4X2 GASOLINA AUT.', ' 07 OCUP.', 'ESCALADE', 105, 22, 'A', 08, 5, 7, '', 01, 1601, 0003, 0003, '', 'CADILLAC'
);

/* INSERT QUERY NO: 968 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07701, 7701, 'CR', 'CHRYSLER', 'CARAVAN', 'MINIVAN 3.0L V6 AUT.', ' 07 OCUP.', 'CARAVAN', 100, 11, 'A', 06, 4, 7, '', 01, 1106, 0001, 0001, '', 'CHRYSLER'
);

/* INSERT QUERY NO: 969 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07702, 7702, 'CR', 'CHRYSLER', '300C', 'SEDAN 5.7L V8 FULL STD.', ' 05 OCUP.', '300C', 100, 01, 'S', 08, 4, 5, '', 01, 1112, 0001, 0001, '', 'CHRYSLER'
);

/* INSERT QUERY NO: 970 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07704, 7704, 'CR', 'CHRYSLER', 'TOWN & COUNTRY', 'MINIVAN 3.8L V6 AUT.', ' 07 OCUP.', 'TOWN & COUNTRY', 105, 11, 'A', 06, 4, 7, '', 01, 1112, 0003, 0003, '', 'CHRYSLER'
);

/* INSERT QUERY NO: 971 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07705, 7705, 'CR', 'CHRYSLER', 'RAIDER', '3.0L V6 V/P AC 4X4 STD.', ' 04 OCUP.', 'RAIDER', 100, 03, 'S', 06, 3, 4, '', 01, 1112, 0001, 0001, '', 'CHRYSLER'
);

/* INSERT QUERY NO: 972 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07706, 7706, 'CR', 'CHRYSLER', 'NEON', 'LX SEDAN 2.0L STD.', ' 05 OCUP.', 'NEON', 100, 01, 'S', 04, 4, 5, '', 01, 1212, 0001, 0001, '', 'CHRYSLER'
);

/* INSERT QUERY NO: 973 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07707, 7707, 'CR', 'CHRYSLER', 'PACIFICA', 'TOURING FWD 3.8L STD.', ' 06 OCUP.', 'PACIFICA', 105, 22, 'S', 06, 4, 6, '', 01, 1402, 0003, 0003, '', 'CHRYSLER'
);

/* INSERT QUERY NO: 974 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07708, 7708, 'CR', 'CHRYSLER', 'SEBRING', '2.5L GASOLINA STD.', ' 04 OCUP.', 'SEBRING', 100, 04, 'S', 04, 2, 4, '', 01, 1501, 0001, 0001, '', 'CHRYSLER'
);

/* INSERT QUERY NO: 975 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07801, 7801, 'LR', 'LAND ROVER', 'DISCOVERY', '4.4L 4X4 FULL AUT.', ' 07 OCUP.', 'DISCOVERY', 105, 22, 'A', 08, 4, 7, '', 01, 1106, 0003, 0003, '', 'LAND ROVER'
);

/* INSERT QUERY NO: 976 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07802, 7802, 'LR', 'LAND ROVER', 'RANGE SPORT', 'SPORT 3.0L 4X4 FULL AUT.', ' 05 OCUP.', 'RANGE SPORT', 105, 22, 'A', 06, 5, 5, '', 01, 1106, 0003, 0003, '', 'LAND ROVER'
);

/* INSERT QUERY NO: 977 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07803, 7803, 'LR', 'LAND ROVER', 'DEFENDER', '2.2L 4X4 DIESEL AUT.', ' 06 OCUP.', 'DEFENDER', 105, 22, 'A', 06, 4, 6, '', 01, 1106, 0003, 0003, '', 'LAND ROVER'
);

/* INSERT QUERY NO: 978 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07805, 7805, 'LR', 'LAND ROVER', 'FREELANDER', '3.2L 4X4 GASOLINA FULL AUT.', ' 07 OCUP.', 'FREELANDER', 105, 22, 'A', 06, 5, 7, '', 01, 1106, 0003, 0003, '', 'LAND ROVER'
);

/* INSERT QUERY NO: 979 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07806, 7806, 'LR', 'LAND ROVER', 'LR3', '4.0L V6 GASOLINA FULL STD.', ' 07 OCUP.', 'LR3', 105, 22, 'S', 06, 4, 7, '', 01, 1112, 0003, 0003, '', 'LAND ROVER'
);

/* INSERT QUERY NO: 980 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07807, 7807, 'LR', 'LAND ROVER', 'SERIE IIA', '2.5L 4X4 GASOLINA FULL STD.', ' 03 OCUP.', 'SERIE IIA', 105, 22, 'S', 04, 4, 3, '', 01, 1112, 0003, 0003, '', 'LAND ROVER'
);

/* INSERT QUERY NO: 981 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07808, 7808, 'LR', 'LAND ROVER', '400', '1.6L 4X4 GASOLINA FULL STD.', ' 05 OCUP.', '400', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'LAND ROVER'
);

/* INSERT QUERY NO: 982 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07809, 7809, 'LR', 'LAND ROVER', 'RANGE ROVER', '3.0L 4X4 FULL STD.', ' 05 OCUP.', 'RANGE ROVER', 105, 22, 'S', 08, 5, 5, '', 01, 1112, 0003, 0003, '', 'LAND ROVER'
);

/* INSERT QUERY NO: 983 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07810, 7810, 'LR', 'LAND ROVER', 'RANGE ROVER', '2.0L 4X4 GASOLINA STD.', ' 05 OCUP.', 'RANGE ROVER', 105, 22, 'S', 04, 3, 5, '', 01, 1112, 0003, 0003, '', 'LAND ROVER'
);

/* INSERT QUERY NO: 984 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07811, 7811, 'LR', 'LAND ROVER', 'FREELANDER', '2.0L 4X4 GASOLINA FULL STD.', ' 05 OCUP.', 'FREELANDER', 105, 22, 'S', 04, 5, 5, '', 01, 1112, 0003, 0003, '', 'LAND ROVER'
);

/* INSERT QUERY NO: 985 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07812, 7812, 'LR', 'LAND ROVER', 'RANGE ROVER', '3.6L V8 4X4 GLINA FULL STD.', ' 05 OCUP.', 'RANGE ROVER', 105, 22, 'S', 08, 4, 5, '', 01, 1112, 0003, 0003, '', 'LAND ROVER'
);

/* INSERT QUERY NO: 986 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07813, 7813, 'LR', 'LAND ROVER', 'EVOQUE', 'DINAMIC 2.0L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'EVOQUE', 105, 22, 'A', 04, 5, 5, '', 01, 1212, 0003, 0003, '', 'LAND ROVER'
);

/* INSERT QUERY NO: 987 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07814, 7814, 'LR', 'LAND ROVER', 'DEFENDER', '110 2.2L 4X4 DIESEL AUT.', ' 05 OCUP.', 'DEFENDER', 105, 22, 'A', 04, 5, 5, '', 01, 1402, 0003, 0003, '', 'LAND ROVER'
);

/* INSERT QUERY NO: 988 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07815, 7815, 'LR', 'LAND ROVER', 'RANGE ROVER', 'VOGUE 5.0L 4X4 FULL STD.', ' 05 OCUP.', 'RANGE ROVER', 105, 22, 'S', 08, 5, 5, '', 01, 1402, 0003, 0003, '', 'LAND ROVER'
);

/* INSERT QUERY NO: 989 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07816, 7816, 'LR', 'LAND ROVER', 'DISCOVERY', '3.0L 4X4 AUT.', ' 05 OCUP.', 'DISCOVERY', 105, 22, 'A', 06, 5, 5, '', 01, 1412, 0003, 0003, '', 'LAND ROVER'
);

/* INSERT QUERY NO: 990 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07817, 7817, 'LR', 'LAND ROVER', 'RANGE SPORT', '2.7L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'RANGE SPORT', 105, 22, 'A', 06, 5, 5, '', 01, 1601, 0003, 0003, '', 'LAND ROVER'
);

/* INSERT QUERY NO: 991 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07818, 7818, 'LR', 'LAND ROVER', 'LR4', '5.0L 4X4 GASOLINA AUT.', ' 07 OCUP.', 'LR4', 105, 22, 'A', 08, 4, 7, '', 01, 1705, 0003, 0003, '', 'LAND ROVER'
);

/* INSERT QUERY NO: 992 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07819, 7819, 'LR', 'LAND ROVER', 'ROVER', '2.3L 4X4 DIESEL STD.', ' 07 OCUP.', 'ROVER', 105, 22, 'S', 04, 4, 7, '', 01, 1705, 0003, 0003, '', 'LAND ROVER'
);

/* INSERT QUERY NO: 993 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07901, 7901, 'ZP', 'ZAP', 'A380', '1.8L 4X4 STD.', ' 05 OCUP.', 'A380', 105, 22, 'S', 04, 4, 5, '', 01, 1212, 0003, 0003, '', 'ZAP'
);

/* INSERT QUERY NO: 994 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07902, 7902, 'ZP', 'ZAP', 'A380', '2.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'A380', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'ZAP'
);

/* INSERT QUERY NO: 995 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 07951, 7951, 'HG', 'HIGER', 'H5C', '2.5L DIESEL STD.', ' 15 OCUP.', 'H5C', 105, 11, 'S', 04, 3, 15, '', 01, 1601, 0003, 0003, '', 'HIGER'
);

/* INSERT QUERY NO: 996 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08001, 8001, 'OL', 'OPEL', 'ASTRA', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'ASTRA', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'OPEL'
);

/* INSERT QUERY NO: 997 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08002, 8002, 'OL', 'OPEL', 'VECTRA', 'SEDAN 2.0L V/P GASOLINA STD.', ' 05 OCUP.', 'VECTRA', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'OPEL'
);

/* INSERT QUERY NO: 998 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08003, 8003, 'OL', 'OPEL', 'CORSA', 'WING COUPE 1.4L GASOLINA AUT.', ' 05 OCUP.', 'CORSA', 100, 27, 'A', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'OPEL'
);

/* INSERT QUERY NO: 999 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08004, 8004, 'OL', 'OPEL', 'CALIBRA', '2.0L GASOLINA STD.', ' 04 OCUP.', 'CALIBRA', 100, 18, 'S', 04, 2, 4, '', 01, 1601, 0001, 0001, '', 'OPEL'
);

/* INSERT QUERY NO: 1000 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08005, 8005, 'OL', 'OPEL', 'MERIVA', '1.8L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'MERIVA', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'OPEL'
);

/* INSERT QUERY NO: 1001 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08101, 8101, 'PE', 'PORSCHE', 'CAYENNE S', '4.8L 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', 'CAYENNE S', 105, 22, 'A', 08, 4, 5, '', 01, 1106, 0003, 0003, '', 'PORSCHE'
);

/* INSERT QUERY NO: 1002 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08102, 8102, 'PE', 'PORSCHE', '911', 'COUPE 3.0L GASOLINA FULL STD.', ' 02 OCUP.', '911', 100, 18, 'S', 06, 2, 2, '', 01, 1106, 0001, 0001, '', 'PORSCHE'
);

/* INSERT QUERY NO: 1003 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08103, 8103, 'PE', 'PORSCHE', 'BOXSTER ', 'COUPE 2.5L GASOLINA FULL STD.', ' 02 OCUP.', 'BOXSTER', 100, 18, 'S', 06, 2, 2, '', 01, 1106, 0001, 0001, '', 'PORSCHE'
);

/* INSERT QUERY NO: 1004 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08104, 8104, 'PE', 'PORSCHE', '911', 'COUPE 3.8L V6 GASOLINA FULL STD.', ' 02 OCUP.', '911', 100, 18, 'S', 06, 2, 2, '', 01, 1112, 0001, 0001, '', 'PORSCHE'
);

/* INSERT QUERY NO: 1005 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08105, 8105, 'PE', 'PORSCHE', 'CAYENNE', 'CAYENNE 3.0L V/P 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', 'CAYENNE', 105, 22, 'A', 06, 5, 5, '', 01, 1112, 0003, 0003, '', 'PORSCHE'
);

/* INSERT QUERY NO: 1006 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08106, 8106, 'PE', 'PORSCHE', 'PANAMERA', '4 DEPORTIVO 3.6L V6 GASOLINA AUT.', ' 04 OCUP.', 'PANAMERA', 100, 08, 'A', 06, 4, 4, '', 01, 1212, 0001, 0001, '', 'PORSCHE'
);

/* INSERT QUERY NO: 1007 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08107, 8107, 'PE', 'PORSCHE', 'CAYMAN', 'S DEPORTIVO 3.4L 4x4 GASOLINA STD.', ' 02 OCUP.', 'CAYMAN', 100, 08, 'S', 06, 2, 2, '', 01, 1402, 0001, 0001, '', 'PORSCHE'
);

/* INSERT QUERY NO: 1008 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08108, 8108, 'PE', 'PORSCHE', 'MACAN', 'S 3.0L 4x4 GASOLINA AUT.', ' 05 OCUP.', 'MACAN', 100, 22, 'A', 06, 4, 5, '', 01, 1402, 0001, 0001, '', 'PORSCHE'
);

/* INSERT QUERY NO: 1009 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08201, 8201, 'SM', 'SMART', 'PASSION', 'HB 1.0L GASOLINA FULL AUT.', ' 02 OCUP.', 'PASSION', 100, 27, 'A', 03, 2, 2, '', 01, 1106, 0001, 0001, '', 'SMART'
);

/* INSERT QUERY NO: 1010 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08202, 8202, 'SM', 'SMART', 'FORFOUR', 'PASSION 1.0L GASOLINA SUPER STD.', ' 04 OCUP.', 'FORFOUR', 100, 01, 'S', 03, 4, 4, '', 01, 1601, 0001, 0001, '', 'SMART'
);

/* INSERT QUERY NO: 1011 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08203, 8203, 'SM', 'SMART', 'FORFOUR', 'PRIME 1.0L GASOLINA SUPER STD.', ' 04 OCUP.', 'FORFOUR', 100, 01, 'S', 03, 4, 4, '', 01, 1601, 0001, 0001, '', 'SMART'
);

/* INSERT QUERY NO: 1012 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08204, 8204, 'SM', 'SMART', 'FORFOUR', 'PROXY 1.0L GASOLINA SUPER STD.', ' 04 OCUP.', 'FORFOUR', 100, 01, 'S', 03, 4, 4, '', 01, 1601, 0001, 0001, '', 'SMART'
);

/* INSERT QUERY NO: 1013 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08301, 8301, 'MN', 'MINI', 'MINI COOPER', 'HB 1.6L GASOLINA FULL STD.', ' 04 OCUP.', 'COOPER', 100, 18, 'S', 04, 2, 4, '', 01, 1106, 0001, 0001, '', 'MINI'
);

/* INSERT QUERY NO: 1014 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08302, 8302, 'MN', 'MINI', 'MINI COOPER', 'HB 1.6L GASOLINA FULL AUT.', ' 04 OCUP.', 'COOPER', 100, 18, 'A', 04, 2, 4, '', 01, 1112, 0001, 0001, '', 'MINI'
);

/* INSERT QUERY NO: 1015 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08303, 8303, 'MN', 'MINI', 'MINI COOPER', 'CLUBMAN STATION WAGON 1.6L GASOLINA AUT.', ' 04 OCUP.', 'COOPER', 100, 18, 'A', 04, 3, 4, '', 01, 1212, 0001, 0001, '', 'MINI'
);

/* INSERT QUERY NO: 1016 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08304, 8304, 'MN', 'MINI', 'MINI COOPER', 'COUNTRYMAN ALL4 1.6L GASOLINA AUT.', ' 05 OCUP.', 'COOPER', 100, 18, 'A', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'MINI'
);

/* INSERT QUERY NO: 1017 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08305, 8305, 'MN', 'MINI', 'MINI COOPER', 'PACEMAN 1.6L GASOLINA AUT.', ' 05 OCUP.', 'COOPER', 100, 18, 'A', 04, 2, 5, '', 01, 1402, 0001, 0001, '', 'MINI'
);

/* INSERT QUERY NO: 1018 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08306, 8306, 'MN', 'MINI', 'MINI COOPER', 'JCW COUPE 1.6L GASOLINA AUT.', ' 05 OCUP.', 'COOPER', 100, 18, 'A', 04, 2, 5, '', 01, 1402, 0001, 0001, '', 'MINI'
);

/* INSERT QUERY NO: 1019 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08307, 8307, 'MN', 'MINI', 'MINI COOPER', 'S CABRIO 1.6L 4X2 GASOLINA AUT.', ' 04 OCUP.', 'COOPER', 100, 18, 'A', 04, 2, 4, '', 01, 1402, 0001, 0001, '', 'MINI'
);

/* INSERT QUERY NO: 1020 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08308, 8308, 'MN', 'MINI', 'MINI COOPER', 'JCW 2.0L 4X2 GASOLINA STD.', ' 04 OCUP.', 'COOPER', 100, 01, 'S', 04, 2, 4, '', 01, 1601, 0001, 0001, '', 'MINI'
);

/* INSERT QUERY NO: 1021 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08501, 8501, 'CY', 'CHERY', 'QQ3', 'HB 1.1L GASOLINA FULL STD.', ' 05 OCUP.', 'QQ3', 100, 01, 'S', 02, 4, 5, '', 01, 1112, 0001, 0001, '', 'CHERY'
);

/* INSERT QUERY NO: 1022 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08502, 8502, 'CY', 'CHERY', 'A11', 'SEDAN 1.6L 4X2 GASOLINA STD.', ' 05 OCUP.', 'A11', 100, 01, 'S', 01, 4, 5, '', 01, 1402, 0001, 0001, '', 'CHERY'
);

/* INSERT QUERY NO: 1023 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08601, 8601, 'DM', 'DFM', 'MINI PANEL', 'MINIVAN 1.1L STD.', ' 07 OCUP.', 'MINI PANEL', 100, 24, 'S', 04, 3, 7, '', 01, 1003, 0001, 0001, '', 'DFM'
);

/* INSERT QUERY NO: 1024 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08602, 8602, 'DM', 'DFM', 'A-30 ELEGANT', '1.6L GASOLINA STD.', ' 05 OCUP.', 'A-30 ELEGANT', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'DFM'
);

/* INSERT QUERY NO: 1025 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08701, 8701, 'FW', 'FAW', 'BESTURN', '1.6L GASOLINA STD.', ' 05 OCUP.', 'BESTURN', 100, 01, 'S', 04, 4, 5, '', 01, 1212, 0001, 0001, '', 'FAW'
);

/* INSERT QUERY NO: 1026 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08702, 8702, 'FW', 'FAW', 'OLEY', 'CONFORT 1.5L HIBRIDO STD.', ' 05 OCUP.', 'OLEY', 100, 01, 'S', 04, 4, 5, '', 01, 1412, 0001, 0001, '', 'FAW'
);

/* INSERT QUERY NO: 1027 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08703, 8703, 'FW', 'FAW', 'S80', '1.5L GASOLINA STD.', ' 07 OCUP.', 'S80', 105, 22, 'S', 04, 4, 7, '', 01, 1501, 0003, 0003, '', 'FAW'
);

/* INSERT QUERY NO: 1028 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08801, 8801, 'GY', 'GEELY', 'CK', 'SEDAN 1.3L GASOLINA STD.', ' 05 OCUP.', 'CK', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'GEELY'
);

/* INSERT QUERY NO: 1029 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08802, 8802, 'GY', 'GEELY', 'HA', 'SEDAN 1.3L GASOLINA STD.', ' 05 OCUP.', 'HA', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'GEELY'
);

/* INSERT QUERY NO: 1030 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08803, 8803, 'GY', 'GEELY', 'MK', 'SEDAN 1.3L GASOLINA STD.', ' 05 OCUP.', 'MK', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'GEELY'
);

/* INSERT QUERY NO: 1031 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08804, 8804, 'GY', 'GEELY', 'LC GT', 'HB 1.3L GASOLINA STD.', ' 05 OCUP.', 'LC GT', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'GEELY'
);

/* INSERT QUERY NO: 1032 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08805, 8805, 'GY', 'GEELY', 'EMGRAND', '1.8L 4X2 GASOLINA STD.', ' 05 OCUP.', 'EMGRAND', 100, 01, 'S', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'GEELY'
);

/* INSERT QUERY NO: 1033 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08806, 8806, 'GY', 'GEELY', 'EC7', '1.8L 4X2 GASOLINA STD.', ' 05 OCUP.', 'EC7', 100, 01, 'S', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'GEELY'
);

/* INSERT QUERY NO: 1034 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08807, 8807, 'GY', 'GEELY', 'SL', '1.8L 4X2 GASOLINA STD.', ' 05 OCUP.', 'SL', 100, 01, 'S', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'GEELY'
);

/* INSERT QUERY NO: 1035 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08808, 8808, 'GY', 'GEELY', 'GX2', '1.3L 4X2 GASOLINA STD.', ' 05 OCUP.', 'GX2', 105, 22, 'S', 04, 4, 5, '', 01, 1402, 0003, 0003, '', 'GEELY'
);

/* INSERT QUERY NO: 1036 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08809, 8809, 'GY', 'GEELY', 'SC5', '1.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'SC5', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'GEELY'
);

/* INSERT QUERY NO: 1037 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08810, 8810, 'GY', 'GEELY', 'GC5', '1.5L GASOLINA STD.', ' 05 OCUP.', 'GC5', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'GEELY'
);

/* INSERT QUERY NO: 1038 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08811, 8811, 'GY', 'GEELY', 'SC7', '1.8L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'SC7', 100, 01, 'A', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'GEELY'
);

/* INSERT QUERY NO: 1039 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08812, 8812, 'GY', 'GEELY', 'GC2', '1.3L GASOLINA AUT.', ' 05 OCUP.', 'GC2', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'GEELY'
);

/* INSERT QUERY NO: 1040 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08813, 8813, 'GY', 'GEELY', 'GC6', '1.5L GASOLINA STD.', ' 05 OCUP.', 'GC6', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'GEELY'
);

/* INSERT QUERY NO: 1041 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08901, 8901, 'GW', 'GREAT WALL', 'FLORID', 'GASOLINA STD.', ' 04 OCUP.', 'FLORID', 100, 27, 'S', 04, 4, 4, '', 01, 1003, 0001, 0001, '', 'GREAT WALL'
);

/* INSERT QUERY NO: 1042 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08902, 8902, 'GW', 'GREAT WALL', 'PERI', 'C20R 1.3L GASOLINA FULL STD.', ' 04 OCUP.', 'PERI', 100, 01, 'S', 04, 2, 4, '', 01, 1112, 0001, 0001, '', 'GREAT WALL'
);

/* INSERT QUERY NO: 1043 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08903, 8903, 'GW', 'GREAT WALL', 'HOVER', '2.8L GASOLINA FULL STD.', ' 05 OCUP.', 'HOVER', 105, 22, 'S', 04, 5, 5, '', 01, 1112, 0003, 0003, '', 'GREAT WALL'
);

/* INSERT QUERY NO: 1044 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08904, 8904, 'GW', 'GREAT WALL', 'SING', '2.8L GASOLINA FULL STD.', ' 07 OCUP.', 'SING', 105, 22, 'S', 04, 5, 7, '', 01, 1112, 0003, 0003, '', 'GREAT WALL'
);

/* INSERT QUERY NO: 1045 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08905, 8905, 'GW', 'GREAT WALL', 'HAVAL 5', '4X4 GASOLINA FULL STD.', ' 05 OCUP.', 'HAVAL 5', 105, 22, 'S', 04, 5, 5, '', 01, 1112, 0003, 0003, '', 'GREAT WALL'
);

/* INSERT QUERY NO: 1046 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08906, 8906, 'GW', 'GREAT WALL', 'HAVAL 5', '4X2 GASOLINA FULL STD.', ' 05 OCUP.', 'HAVAL 5', 105, 22, 'S', 04, 5, 5, '', 01, 1112, 0003, 0003, '', 'GREAT WALL'
);

/* INSERT QUERY NO: 1047 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08907, 8907, 'GW', 'GREAT WALL', 'SAFE', '2.2L GASOLINA FULL STD.', ' 05 OCUP.', 'SAFE', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'GREAT WALL'
);

/* INSERT QUERY NO: 1048 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08908, 8908, 'GW', 'GREAT WALL', 'VOLEEX', 'C30 SEDAN 1.5L GASOLINA STD.', ' 05 OCUP.', 'VOLEEX', 100, 01, 'S', 04, 4, 5, '', 01, 1212, 0001, 0001, '', 'GREAT WALL'
);

/* INSERT QUERY NO: 1049 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08909, 8909, 'GW', 'GREAT WALL', 'VOLEEX', 'C50 SEDAN 1.5L GASOLINA STD.', ' 05 OCUP.', 'VOLEEX', 100, 01, 'S', 04, 4, 5, '', 01, 1412, 0001, 0001, '', 'GREAT WALL'
);

/* INSERT QUERY NO: 1050 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08910, 8910, 'GW', 'GREAT WALL', 'H6', '4X2 GASOLINA FULL STD.', ' 05 OCUP.', 'VOLEEX', 105, 22, 'S', 04, 4, 5, '', 01, 1412, 0003, 0003, '', 'GREAT WALL'
);

/* INSERT QUERY NO: 1051 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08911, 8911, 'GW', 'GREAT WALL', 'H6', '4X2 GASOLINA FULL STD.', ' 05 OCUP.', 'VOLEEX', 105, 22, 'S', 04, 4, 5, '', 01, 1412, 0003, 0003, '', 'GREAT WALL'
);

/* INSERT QUERY NO: 1052 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08912, 8912, 'GW', 'GREAT WALL', 'HAVAL M2', '4X2 GASOLINA STD.', ' 05 OCUP.', 'HAVAL M2', 105, 22, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'GREAT WALL'
);

/* INSERT QUERY NO: 1053 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08913, 8913, 'GW', 'GREAT WALL', 'M4', '1.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'M4', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'GREAT WALL'
);

/* INSERT QUERY NO: 1054 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08914, 8914, 'GW', 'GREAT WALL', 'HAVAL', 'H9 2.0L 4X4 GASOLINA STD.', ' 07 OCUP.', 'HAVAL', 105, 22, 'S', 04, 4, 7, '', 01, 1601, 0003, 0003, '', 'GREAT WALL'
);

/* INSERT QUERY NO: 1055 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08915, 8915, 'GW', 'GREAT WALL', 'VOLEEX', 'C30 SEDAN 1.5L GASOLINA AUT.', ' 05 OCUP.', 'VOLEEX', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'GREAT WALL'
);

/* INSERT QUERY NO: 1056 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08916, 8916, 'GW', 'GREAT WALL', 'HAVAL M4', '1.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'HAVAL M4', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'GREAT WALL'
);

/* INSERT QUERY NO: 1057 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08917, 8917, 'GW', 'GREAT WALL', 'FLORID', '1.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'FLORID', 100, 27, 'S', 04, 5, 5, '', 01, 1705, 0001, 0001, '', 'GREAT WALL'
);

/* INSERT QUERY NO: 1058 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 08918, 8918, 'GW', 'GREAT WALL', 'HAVAL', 'H6 SPORT 1.5L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'HAVAL', 105, 22, 'A', 04, 4, 5, '', 01, 1705, 0003, 0003, '', 'GREAT WALL'
);

/* INSERT QUERY NO: 1059 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09001, 9001, 'JR', 'JAGUAR', 'XF', 'SEDAN 3.0L GASOLINA FULL AUT.', ' 05 OCUP.', 'XF', 100, 01, 'A', 06, 4, 5, '', 01, 1003, 0001, 0001, '', 'JAGUAR'
);

/* INSERT QUERY NO: 1060 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09002, 9002, 'JR', 'JAGUAR', 'S', 'SEDAN 4.2L V8 GASOLINA FULL AUT.', ' 05 OCUP.', 'S', 100, 01, 'A', 08, 4, 5, '', 01, 1112, 0001, 0001, '', 'JAGUAR'
);

/* INSERT QUERY NO: 1061 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09003, 9003, 'JR', 'JAGUAR', 'XKR', 'COUPE 4.0L V8 GASOLINA AUT.', ' 04 OCUP.', 'XKR', 100, 18, 'A', 08, 2, 4, '', 01, 1212, 0001, 0001, '', 'JAGUAR'
);

/* INSERT QUERY NO: 1062 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09004, 9004, 'JR', 'JAGUAR', 'XKR', 'CABRIOLET 4.0L V8 GASOLINA AUT.', ' 04 OCUP.', 'XKR', 100, 04, 'A', 08, 2, 4, '', 01, 1212, 0001, 0001, '', 'JAGUAR'
);

/* INSERT QUERY NO: 1063 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09005, 9005, 'JR', 'JAGUAR', 'XFR', 'SEDAN 5.0L 4X2 V8 GASOLINA AUT.', ' 05 OCUP.', 'XFR', 100, 01, 'A', 08, 4, 5, '', 01, 1402, 0001, 0001, '', 'JAGUAR'
);

/* INSERT QUERY NO: 1064 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09006, 9006, 'JR', 'JAGUAR', 'XE', '2.0L GTDI 4X2 GASOLINA AUT.', ' 05 OCUP.', 'XE', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'JAGUAR'
);

/* INSERT QUERY NO: 1065 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09007, 9007, 'JR', 'JAGUAR', 'S', 'TYPE 3.0L 4X2 V6 GASOLINA AUT.', ' 05 OCUP.', 'S', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'JAGUAR'
);

/* INSERT QUERY NO: 1066 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09008, 9008, 'JR', 'JAGUAR', 'F-PACE', '35T 3.0L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'F-PACE', 105, 22, 'A', 06, 4, 5, '', 01, 1601, 0003, 0003, '', 'JAGUAR'
);

/* INSERT QUERY NO: 1067 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09009, 9009, 'JR', 'JAGUAR', 'X', 'TYPE 2.0L 4X2 V6 GASOLINA STD.', ' 05 OCUP.', 'X', 100, 01, 'S', 06, 4, 5, '', 01, 1601, 0001, 0001, '', 'JAGUAR'
);

/* INSERT QUERY NO: 1068 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09101, 9101, 'FT', 'FIAT', '500', 'LOUNGE 1.4L GASOLINA STD.', ' 05 OCUP.', '500', 100, 27, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'FIAT'
);

/* INSERT QUERY NO: 1069 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09102, 9102, 'FT', 'FIAT', 'BRAVO', 'SEDAN 1.4L GASOLINA STD.', ' 05 OCUP.', 'BRAVO', 100, 27, 'S', 04, 2, 5, '', 01, 1003, 0001, 0001, '', 'FIAT'
);

/* INSERT QUERY NO: 1070 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09103, 9103, 'FT', 'FIAT', 'GRANDE PUNTO', 'SEDAN 1.8L GASOLINA STD.', ' 05 OCUP.', 'GRANDE PUNTO', 100, 27, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'FIAT'
);

/* INSERT QUERY NO: 1071 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09104, 9104, 'FT', 'FIAT', 'GRANDE PUNTO', '1.4L GASOLINA STD.', ' 05 OCUP.', 'GRANDE PUNTO', 100, 27, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'FIAT'
);

/* INSERT QUERY NO: 1072 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09105, 9105, 'FT', 'FIAT', 'LINEA', 'SEDAN 1.4L GASOLINA STD.', ' 05 OCUP.', 'LINEA', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'FIAT'
);

/* INSERT QUERY NO: 1073 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09106, 9106, 'FT', 'FIAT', 'PALIO', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'PALIO', 100, 27, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'FIAT'
);

/* INSERT QUERY NO: 1074 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09107, 9107, 'FT', 'FIAT', 'PALIO', 'STATION WAGON 1.8L GASOLINA STD.', ' 05 OCUP.', 'PALIO', 100, 02, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'FIAT'
);

/* INSERT QUERY NO: 1075 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09108, 9108, 'FT', 'FIAT', 'SIENA', 'SEDAN 1.4L GASOLINA STD.', ' 05 OCUP.', 'SIENA', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'FIAT'
);

/* INSERT QUERY NO: 1076 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09109, 9109, 'FT', 'FIAT', 'UNO', '1.2L GASOLINA STD.', ' 05 OCUP.', 'UNO', 100, 27, 'S', 04, 2, 5, '', 01, 1003, 0001, 0001, '', 'FIAT'
);

/* INSERT QUERY NO: 1077 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09111, 9111, 'FT', 'FIAT', '500', 'POP 1.2L GASOLINA STD.', ' 05 OCUP.', '500', 100, 01, 'S', 04, 2, 5, '', 01, 1003, 0001, 0001, '', 'FIAT'
);

/* INSERT QUERY NO: 1078 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09112, 9112, 'FT', 'FIAT', 'PALIO', 'STATION WAGON 1.4L GASOLINA STD.', ' 05 OCUP.', 'PALIO', 100, 02, 'S', 04, 5, 5, '', 01, 1003, 0001, 0001, '', 'FIAT'
);

/* INSERT QUERY NO: 1079 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09113, 9113, 'FT', 'FIAT', 'LINEA', '1.4L GASOLINA STD.', ' 05 OCUP.', 'LINEA', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'FIAT'
);

/* INSERT QUERY NO: 1080 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09114, 9114, 'FT', 'FIAT', 'BRAVO', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', 'BRAVO', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'FIAT'
);

/* INSERT QUERY NO: 1081 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09115, 9115, 'FT', 'FIAT', 'DOBLO', '1.9L GASOLINA STD.', ' 07 OCUP.', 'DOBLO', 105, 01, 'S', 04, 5, 7, '', 01, 1112, 0003, 0003, '', 'FIAT'
);

/* INSERT QUERY NO: 1082 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09116, 9116, 'FT', 'FIAT', 'PANDA', 'SEDAN 1.2L GASOLINA STD.', ' 05 OCUP.', 'PANDA', 105, 01, 'S', 04, 4, 5, '', 01, 1112, 0003, 0003, '', 'FIAT'
);

/* INSERT QUERY NO: 1083 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09117, 9117, 'FT', 'FIAT', 'REGATA', '60 SEDAN 1.1L GASOLINA STD.', ' 05 OCUP.', 'REGATA', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'FIAT'
);

/* INSERT QUERY NO: 1084 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09118, 9118, 'FT', 'FIAT', 'STILO', 'CONFORT 1.8L GASOLINA STD.', ' 05 OCUP.', 'STILO', 100, 27, 'S', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'FIAT'
);

/* INSERT QUERY NO: 1085 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09119, 9119, 'FT', 'FIAT', 'IDEA', 'ADVENTURE 1.8L 4X2 GASOLINA STD.', ' 05 OCUP.', 'IDEA', 105, 22, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'FIAT'
);

/* INSERT QUERY NO: 1086 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09120, 9120, 'FT', 'FIAT', '500', 'SPORT X 1.4L 4X4 GASOLINA AUT.', ' 05 OCUP.', '500', 105, 22, 'A', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'FIAT'
);

/* INSERT QUERY NO: 1087 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09201, 9201, 'LD', 'LANDWIND', 'X6', 'FLAGSHIP 4X4 2.8L DIESEL STD.', ' 05 OCUP.', 'X6', 100, 22, 'S', 04, 5, 5, '', 01, 1112, 0001, 0001, '', 'LANDWIND'
);

/* INSERT QUERY NO: 1088 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09301, 9301, 'GG', 'GONOW', 'JETSTAR', '2.8L DIESEL STD.', ' 05 OCUP.', 'JETSTAR', 105, 22, 'S', 04, 4, 5, '', 01, 1212, 0003, 0003, '', 'GONOW'
);

/* INSERT QUERY NO: 1089 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09302, 9302, 'GG', 'GONOW', 'JETSTAR', 'GA6490 2.2L GASOLINA STD.', ' 07 OCUP.', 'JETSTAR', 105, 22, 'S', 04, 4, 7, '', 01, 1501, 0003, 0003, '', 'GONOW'
);

/* INSERT QUERY NO: 1090 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09701, 9701, 'FN', 'FOTON', 'VIEW', 'CS2 2.8L DIESEL AUT.', ' 16 OCUP.', 'VIEW', 105, 11, 'A', 04, 4, 16, '', 01, 1501, 0003, 0003, '', 'FOTON'
);

/* INSERT QUERY NO: 1091 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09702, 9702, 'FN', 'FOTON', 'VIEW', 'CS2 2.8L DIESEL STD.', ' 16 OCUP.', 'VIEW', 105, 11, 'S', 04, 4, 16, '', 01, 1601, 0003, 0003, '', 'FOTON'
);

/* INSERT QUERY NO: 1092 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09801, 9801, 'HM', 'HAIMA', 'HAIMA 7', '2.0L 4x4 GASOLINA AUT.', ' 05 OCUP.', 'HAIMA 7', 105, 22, 'A', 04, 4, 5, '', 01, 1212, 0003, 0003, '', 'HAIMA'
);

/* INSERT QUERY NO: 1093 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09802, 9802, 'HM', 'HAIMA', 'M3', 'SEDAN 1.5L GASOLINA STD.', ' 05 OCUP.', 'M3', 100, 01, 'S', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'HAIMA'
);

/* INSERT QUERY NO: 1094 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09803, 9803, 'HM', 'HAIMA', 'HAIMA 1', 'STANDARD 1.0L GASOLINA STD.', ' 05 OCUP.', 'HAIMA 1', 100, 27, 'S', 04, 5, 5, '', 01, 1501, 0001, 0001, '', 'HAIMA'
);

/* INSERT QUERY NO: 1095 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 09804, 9804, 'HM', 'HAIMA', 'HAIMA 3', 'GLS DELUXE 1.6L GASOLINA STD.', ' 05 OCUP.', 'HAIMA 3', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'HAIMA'
);

/* INSERT QUERY NO: 1096 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10001, 10001, 'CG', 'CHANGAN', 'SC35', '1.6L GASOLINA STD.', ' 05 OCUP.', 'SC35', 105, 22, 'S', 04, 4, 5, '', 01, 1212, 0003, 0003, '', 'CHANGAN'
);

/* INSERT QUERY NO: 1097 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10002, 10002, 'CG', 'CHANGAN', 'HONOR', '1.5L GASOLINA STD.', ' 05 OCUP.', 'HONOR', 105, 11, 'S', 04, 4, 5, '', 01, 1402, 0003, 0003, '', 'CHANGAN'
);

/* INSERT QUERY NO: 1098 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10003, 10003, 'CG', 'CHANGAN', 'EADO', 'XT HB 1.6L GASOLINA AUT.', ' 05 OCUP.', 'EADO', 100, 27, 'A', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'CHANGAN'
);

/* INSERT QUERY NO: 1099 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10004, 10004, 'CG', 'CHANGAN', 'BENNI', 'MINI HB 1.0L 4x2 GASOLINA AUT.', ' 05 OCUP.', 'BENNI', 100, 27, 'A', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'CHANGAN'
);

/* INSERT QUERY NO: 1100 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10005, 10005, 'CG', 'CHANGAN', 'CS75', '2L GASOLINA STD.', ' 05 OCUP.', 'CS75', 105, 22, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'CHANGAN'
);

/* INSERT QUERY NO: 1101 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10006, 10006, 'CG', 'CHANGAN', 'BENNI', '1.4L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'BENNI', 100, 27, 'A', 04, 5, 5, '', 01, 1601, 0001, 0001, '', 'CHANGAN'
);

/* INSERT QUERY NO: 1102 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10007, 10007, 'CG', 'CHANGAN', 'STAR', '3 1.3L 4X2 GASOLINA STD.', ' 07 OCUP.', 'STAR', 105, 11, 'S', 04, 4, 7, '', 01, 1601, 0003, 0003, '', 'CHANGAN'
);

/* INSERT QUERY NO: 1103 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10008, 10008, 'CG', 'CHANGAN', 'CS15', '1.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'CS15', 105, 22, 'S', 04, 5, 5, '', 01, 1705, 0003, 0003, '', 'CHANGAN'
);

/* INSERT QUERY NO: 1104 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10009, 10009, 'CG', 'CHANGAN', 'CS35', '1.6L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'SC35', 105, 22, 'A', 04, 4, 5, '', 01, 1705, 0003, 0003, '', 'CHANGAN'
);

/* INSERT QUERY NO: 1105 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10201, 10201, 'SG', 'SSANGYONG', 'KYRON', '4L 4X4 DIESEL AUT.', ' 05 OCUP.', 'KYRON', 105, 22, 'A', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'SSANGYONG'
);

/* INSERT QUERY NO: 1106 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10202, 10202, 'SG', 'SSANGYONG', 'REXTON', '2.7L 4X4 DIESEL STD.', ' 07 OCUP.', 'REXTON', 105, 22, 'S', 05, 4, 7, '', 01, 1106, 0003, 0003, '', 'SSANGYONG'
);

/* INSERT QUERY NO: 1107 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10203, 10203, 'SG', 'SSANGYONG', 'KYRON', '2.0L 4X4 DIESEL STD.', ' 05 OCUP.', 'KYRON', 105, 22, 'S', 04, 4, 5, '', 01, 1112, 0003, 0003, '', 'SSANGYONG'
);

/* INSERT QUERY NO: 1108 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10204, 10204, 'SG', 'SSANGYONG', 'ISTANA', '2.9L DIESEL STD.', ' 12 OCUP.', 'ISTANA', 105, 11, 'S', 04, 4, 12, '', 01, 1112, 0003, 0003, '', 'SSANGYONG'
);

/* INSERT QUERY NO: 1109 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10205, 10205, 'SG', 'SSANGYONG', 'MUSSO', '2.9L DIESEL STD.', ' 07 OCUP.', 'MUSSO', 105, 22, 'S', 05, 5, 7, '', 01, 1112, 0003, 0003, '', 'SSANGYONG'
);

/* INSERT QUERY NO: 1110 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10206, 10206, 'SG', 'SSANGYONG', 'ACTION', '2.0L DIESEL STD.', ' 05 OCUP.', 'ACTION', 105, 22, 'S', 05, 5, 5, '', 01, 1112, 0003, 0003, '', 'SSANGYONG'
);

/* INSERT QUERY NO: 1111 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10207, 10207, 'SG', 'SSANGYONG', 'KORANDO', 'SUV 2.0L V4 4X4 DIESEL STD.', ' 05 OCUP.', 'KORANDO', 105, 22, 'S', 05, 5, 5, '', 01, 1212, 0003, 0003, '', 'SSANGYONG'
);

/* INSERT QUERY NO: 1112 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10208, 10208, 'SG', 'SSANGYONG', 'KORANDO', 'SUV 2.0L V4 4X2 DIESEL STD.', ' 05 OCUP.', 'KORANDO', 105, 22, 'S', 05, 5, 5, '', 01, 1212, 0003, 0003, '', 'SSANGYONG'
);

/* INSERT QUERY NO: 1113 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10209, 10209, 'SG', 'SSANGYONG', 'TIVOLI', '1.6L 4X2 GASOLINA STD.', ' 05 OCUP.', 'TIVOLI', 105, 22, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'SSANGYONG'
);

/* INSERT QUERY NO: 1114 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10210, 10210, 'SG', 'SSANGYONG', 'STAVIC', '2.0L 4X2 DIESEL AUT.', ' 09 OCUP.', 'STAVIC', 105, 22, 'A', 04, 5, 9, '', 01, 1501, 0003, 0003, '', 'SSANGYONG'
);

/* INSERT QUERY NO: 1115 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10211, 10211, 'SG', 'SSANGYONG', 'XLV', '1.6L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'XLV', 105, 22, 'A', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'SSANGYONG'
);

/* INSERT QUERY NO: 1116 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10212, 10212, 'SG', 'SSANGYONG', 'TIVOLI', '1.6L 4X4 DIESEL STD.', ' 05 OCUP.', 'TIVOLI', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'SSANGYONG'
);

/* INSERT QUERY NO: 1117 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10301, 10301, 'ST', 'SEAT', 'IBIZA', 'HB 1.6L DH AA VE VP GASOLINA STD.', ' 05 OCUP.', 'IBIZA', 100, 27, 'S', 04, 4, 5, '', 01, 1212, 0001, 0001, '', 'SEAT'
);

/* INSERT QUERY NO: 1118 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10401, 10401, 'BK', 'BUICK', 'REGAL', 'SEDAN 3.8L GASOLINA AUT.', ' 05 OCUP.', 'REGAL', 100, 01, 'A', 06, 4, 5, '', 01, 1106, 0001, 0001, '', 'BUICK'
);

/* INSERT QUERY NO: 1119 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10402, 10402, 'BK', 'BUICK', 'ROADMASTER', '5.7L GASOLINA AUT.', ' 06 OCUP.', 'ROADMASTER', 100, 02, 'A', 08, 4, 6, '', 01, 1601, 0001, 0001, '', 'BUICK'
);

/* INSERT QUERY NO: 1120 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10502, 10502, 'GS', 'GMOTORS', 'SAVANA G1500', '5.3L 4X2 DIESEL STD.', ' 07 OCUP.', 'SAVANA G1500', 105, 11, 'S', 08, 4, 7, '', 01, 1501, 0003, 0003, '', 'GENERAL MOTORS'
);

/* INSERT QUERY NO: 1121 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10503, 10503, 'GS', 'GMOTORS', 'ACADIA', 'DENALI 3.6L 4X4 GASOLINA STD.', ' 07 OCUP.', 'ACADIA', 105, 22, 'S', 06, 4, 7, '', 01, 1601, 0003, 0003, '', 'GENERAL MOTORS'
);

/* INSERT QUERY NO: 1122 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10601, 10601, 'HR', 'HUMMER', 'H2', '6.0L V8 4X4 GASOLINA FULL AUT.', ' 06 OCUP.', 'H2', 105, 22, 'A', 08, 5, 6, '', 01, 1112, 0003, 0003, '', 'HUMMER'
);

/* INSERT QUERY NO: 1123 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10602, 10602, 'HR', 'HUMMER', 'H3', '3.5L V6 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', 'H3', 105, 22, 'A', 06, 5, 5, '', 01, 1112, 0003, 0003, '', 'HUMMER'
);

/* INSERT QUERY NO: 1124 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10701, 10701, 'II', 'INFINITI', 'Q45', 'SEDAN 4.5L GASOLINA FULL STD.', ' 05 OCUP.', 'Q45', 100, 01, 'S', 08, 4, 5, '', 01, 1106, 0001, 0001, '', 'INFINITI'
);

/* INSERT QUERY NO: 1125 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10702, 10702, 'II', 'INFINITI', 'M37', 'SEDAN V6 3.7L GASOLINA AUT.', ' 05 OCUP.', 'M37', 100, 01, 'A', 06, 4, 5, '', 01, 1112, 0001, 0001, '', 'INFINITI'
);

/* INSERT QUERY NO: 1126 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10703, 10703, 'II', 'INFINITI', 'FX35', '3.5L V6 4X4 GASOLINA AUT.', ' 05 OCUP.', 'FX35', 105, 22, 'A', 06, 5, 5, '', 01, 1112, 0003, 0003, '', 'INFINITI'
);

/* INSERT QUERY NO: 1127 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10704, 10704, 'II', 'INFINITI', 'QX4', '3.5L V6 4X4 GASOLINA AUT.', ' 05 OCUP.', 'QX4', 105, 22, 'A', 06, 5, 5, '', 01, 1212, 0003, 0003, '', 'INFINITI'
);

/* INSERT QUERY NO: 1128 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10705, 10705, 'II', 'INFINITI', 'G35', '3.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'G35', 100, 18, 'S', 06, 2, 5, '', 01, 1501, 0001, 0001, '', 'INFINITI'
);

/* INSERT QUERY NO: 1129 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10801, 10801, 'LN', 'LINCOLN', 'NAVIGATOR', '5.4L V8 4X2 GASOLINA AUT.', ' 07 OCUP.', 'NAVIGATOR', 105, 22, 'A', 08, 5, 7, '', 01, 1112, 0003, 0003, '', 'LINCOLN'
);

/* INSERT QUERY NO: 1130 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10901, 10901, 'MY', 'MERCURY', 'VILLAGER', '3.0L V6 GASOLINA AUT.', ' 07 OCUP.', 'VILLAGER', 105, 11, 'A', 06, 5, 7, '', 01, 1112, 0003, 0003, '', 'MERCURY'
);

/* INSERT QUERY NO: 1131 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10902, 10902, 'MY', 'MERCURY', 'GRAND MARQUIS', 'SEDAN 5.0L V8 GASOLINA AUT.', ' 05 OCUP.', 'GRAND MARQUIS', 100, 01, 'A', 08, 4, 5, '', 01, 1112, 0001, 0001, '', 'MERCURY'
);

/* INSERT QUERY NO: 1132 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 10903, 10903, 'MY', 'MERCURY', 'SABLE', '3.0L GASOLINA STD.', ' 05 OCUP.', 'SABLE', 100, 01, 'S', 06, 4, 5, '', 01, 1501, 0001, 0001, '', 'MERCURY'
);

/* INSERT QUERY NO: 1133 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11000, 11000, 'PO', 'PONTIAC', 'FIREBIRD', 'COUPE 5.0L GASOLINA FULL STD.', ' 05 OCUP.', 'FIREBIRD', 105, 01, 'S', 08, 2, 5, '', 01, 1112, 0003, 0003, '', 'PONTIAC'
);

/* INSERT QUERY NO: 1134 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11301, 11301, 'MG', 'MORRIS GARAGES', '3', 'COM HB 1.5L GASOLINA STD.', ' 05 OCUP.', '3', 100, 27, 'S', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'MORRIS GARAGES'
);

/* INSERT QUERY NO: 1135 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11302, 11302, 'MG', 'MORRIS GARAGES', 'MG3', '3 HB 1.5L GASOLINA STD.', ' 05 OCUP.', '3', 100, 27, 'S', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'MORRIS GARAGES'
);

/* INSERT QUERY NO: 1136 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11303, 11303, 'MG', 'MORRIS GARAGES', 'MG 550 GD', '1.8L GASOLINA STD.', ' 05 OCUP.', 'MG 550 GD', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'MORRIS GARAGES'
);

/* INSERT QUERY NO: 1137 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11304, 11304, 'MG', 'MORRIS GARAGES', 'MG 350', '1.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'MG 350', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'MORRIS GARAGES'
);

/* INSERT QUERY NO: 1138 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11305, 11305, 'MG', 'MORRIS GARAGES', 'MG GT', 'COM 1.4L GASOLINA AUT.', ' 05 OCUP.', 'MG GT', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'MORRIS GARAGES'
);

/* INSERT QUERY NO: 1139 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11306, 11306, 'MG', 'MORRIS GARAGES', 'MG GS', '1.5L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'MG GS', 105, 22, 'A', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'MORRIS GARAGES'
);

/* INSERT QUERY NO: 1140 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11307, 11307, 'MG', 'MORRIS GARAGES', 'MG GS LUX', '2.0L 4X4 GASOLINA STD.', ' 05 OCUP.', 'MG GS LUX', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'MORRIS GARAGES'
);

/* INSERT QUERY NO: 1141 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11308, 11308, 'MG', 'MORRIS GARAGES', 'MG 5', 'COM 1.5L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'MG 5', 100, 27, 'A', 04, 5, 5, '', 01, 1601, 0001, 0001, '', 'MORRIS GARAGES'
);

/* INSERT QUERY NO: 1142 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11309, 11309, 'MG', 'MORRIS GARAGES', 'MG GS', 'DEL 2.0L 4X4 GASOLINA STD.', ' 05 OCUP.', 'MG GS', 105, 22, 'S', 04, 4, 5, '', 01, 1705, 0003, 0003, '', 'MORRIS GARAGES'
);

/* INSERT QUERY NO: 1143 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11401, 11401, 'CN', 'CITROEN', 'BERLINGO', '1.9L DIESEL STD.', ' 05 OCUP.', 'BERLINGO', 100, 20, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'CITROEN'
);

/* INSERT QUERY NO: 1144 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11402, 11402, 'CN', 'CITROEN', 'C3', 'SEDAN 1.4L GASOLINA STD.', ' 05 OCUP.', 'C3', 100, 27, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'CITROEN'
);

/* INSERT QUERY NO: 1145 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11403, 11403, 'CN', 'CITROEN', 'C3', 'SEDAN 1.4L GASOLINA FULL STD.', ' 05 OCUP.', 'C3', 100, 27, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'CITROEN'
);

/* INSERT QUERY NO: 1146 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11405, 11405, 'CN', 'CITROEN', 'C4', 'COUPE 1.6L DIESEL FULL STD.', ' 04 OCUP.', 'C4', 100, 01, 'S', 04, 4, 4, '', 01, 1003, 0001, 0001, '', 'CITROEN'
);

/* INSERT QUERY NO: 1147 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11406, 11406, 'CN', 'CITROEN', 'C4', '1.6L GASOLINA FULL STD.', ' 05 OCUP.', 'C4', 100, 01, 'S', 04, 4, 5, '', 01, 1003, 0001, 0001, '', 'CITROEN'
);

/* INSERT QUERY NO: 1148 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11407, 11407, 'CN', 'CITROEN', 'C4', 'STATION WAGON 1.6L STD.', ' 07 OCUP.', 'C4', 100, 24, 'S', 04, 4, 7, '', 01, 1003, 0001, 0001, '', 'CITROEN'
);

/* INSERT QUERY NO: 1149 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11409, 11409, 'CN', 'CITROEN', 'C4', 'SEDAN 2.0L GASOLINA AUT.', ' 07 OCUP.', 'C4', 100, 24, 'A', 04, 4, 7, '', 01, 1003, 0001, 0001, '', 'CITROEN'
);

/* INSERT QUERY NO: 1150 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11411, 11411, 'CN', 'CITROEN', 'JUMPY', '1.6L DIESEL STD.', ' 08 OCUP.', 'JUMPY', 100, 20, 'S', 04, 4, 8, '', 01, 1003, 0001, 0001, '', 'CITROEN'
);

/* INSERT QUERY NO: 1151 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11412, 11412, 'CN', 'CITROEN', 'XSARA', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'XSARA', 100, 02, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'CITROEN'
);

/* INSERT QUERY NO: 1152 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11413, 11413, 'CN', 'CITROEN', 'DS3', 'COUPE 1.6L GASOLINA STD.', ' 05 OCUP.', 'DS3', 100, 08, 'S', 04, 3, 5, '', 01, 1106, 0001, 0001, '', 'CITROEN'
);

/* INSERT QUERY NO: 1153 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11414, 11414, 'CN', 'CITROEN', 'C2', 'COUPE 1.4L GASOLINA STD.', ' 05 OCUP.', 'C2', 100, 01, 'S', 04, 5, 5, '', 01, 1112, 0001, 0001, '', 'CITROEN'
);

/* INSERT QUERY NO: 1154 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11416, 11416, 'CN', 'CITROEN', 'C-ELYSEE', 'VTI SEDAN 1.2L V4 GASOLINA STD.', ' 05 OCUP.', 'C-ELYSEE', 100, 01, 'S', 04, 5, 5, '', 01, 1212, 0001, 0001, '', 'CITROEN'
);

/* INSERT QUERY NO: 1155 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11417, 11417, 'CN', 'CITROEN', 'C-ELYSEE', 'VTI SEDAN 1.6L V4 GASOLINA STD.', ' 05 OCUP.', 'C-ELYSEE', 100, 01, 'S', 04, 5, 5, '', 01, 1412, 0001, 0001, '', 'CITROEN'
);

/* INSERT QUERY NO: 1156 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11418, 11418, 'CN', 'CITROEN', 'C5', '2.0L GASOLINA STD.', ' 05 OCUP.', 'C5', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'CITROEN'
);

/* INSERT QUERY NO: 1157 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11419, 11419, 'CN', 'CITROEN', 'NEMO', '1.4L DIESEL STD.', ' 02 OCUP.', 'NEMO', 105, 02, 'S', 04, 4, 2, '', 01, 1501, 0003, 0003, '', 'CITROEN'
);

/* INSERT QUERY NO: 1158 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11420, 11420, 'CN', 'CITROEN', 'DS4', 'HB 1.6L GASOLINA AUT.', ' 05 OCUP.', 'DS4', 100, 27, 'A', 04, 3, 5, '', 01, 1501, 0001, 0001, '', 'CITROEN'
);

/* INSERT QUERY NO: 1159 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11421, 11421, 'CN', 'CITROEN', 'C1', '1.0L GASOLINA STD.', ' 05 OCUP.', 'C1', 100, 27, 'S', 03, 5, 5, '', 01, 1501, 0001, 0001, '', 'CITROEN'
);

/* INSERT QUERY NO: 1160 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11422, 11422, 'CN', 'CITROEN', 'DS4', '1.6L GASOLINA STD.', ' 05 OCUP.', 'DS4', 100, 27, 'S', 04, 3, 5, '', 01, 1601, 0001, 0001, '', 'CITROEN'
);

/* INSERT QUERY NO: 1161 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11501, 11501, 'JP', 'JEEP', 'CHEROKEE', 'ROKEE LAREDO 3.6L V6 4X4 FULL AUT.', ' 05 OCUP.', 'CHEROKEE', 105, 22, 'A', 06, 5, 5, '', 01, 1003, 0003, 0003, '', 'JEEP'
);

/* INSERT QUERY NO: 1162 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11502, 11502, 'JP', 'JEEP', 'CHEROKEE', '2.8L 4X4 DIESEL FULL AUT.', ' 05 OCUP.', 'CHEROKEE', 105, 22, 'A', 04, 5, 5, '', 01, 1003, 0003, 0003, '', 'JEEP'
);

/* INSERT QUERY NO: 1163 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11503, 11503, 'JP', 'JEEP', 'COMPASS', '2.0L GASOLINA AUT.', ' 05 OCUP.', 'COMPASS', 105, 22, 'A', 04, 5, 5, '', 01, 1003, 0003, 0003, '', 'JEEP'
);

/* INSERT QUERY NO: 1164 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11504, 11504, 'JP', 'JEEP', 'GRAND CHEROKEE', '3.6L V6 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', 'GRAND CHEROKEE', 105, 22, 'A', 06, 5, 5, '', 01, 1003, 0003, 0003, '', 'JEEP'
);

/* INSERT QUERY NO: 1165 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11505, 11505, 'JP', 'JEEP', 'GRAND CHEROKEE', 'LIMITED 5.7L V8 4X4 FULL AUT.', ' 05 OCUP.', 'GRAND CHEROKEE', 105, 22, 'A', 08, 5, 5, '', 01, 1003, 0003, 0003, '', 'JEEP'
);

/* INSERT QUERY NO: 1166 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11506, 11506, 'JP', 'JEEP', 'PATRIOT', '2.4L 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', 'PATRIOT', 105, 22, 'A', 04, 5, 5, '', 01, 1003, 0003, 0003, '', 'JEEP'
);

/* INSERT QUERY NO: 1167 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11507, 11507, 'JP', 'JEEP', 'WRANGLER', 'SAHARA 3.6L 4X4 AUT.', ' 05 OCUP.', 'WRANGLER', 105, 22, 'A', 06, 4, 5, '', 01, 1003, 0003, 0003, '', 'JEEP'
);

/* INSERT QUERY NO: 1168 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11509, 11509, 'JP', 'JEEP', 'WRANGLER', '3.8L V6 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', 'WRANGLER', 105, 22, 'A', 06, 4, 5, '', 01, 1003, 0003, 0003, '', 'JEEP'
);

/* INSERT QUERY NO: 1169 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11511, 11511, 'JP', 'JEEP', 'LIBERTY', '3.7L V6 4X2 GASOLINA FULL STD.', ' 05 OCUP.', 'LIBERTY', 105, 22, 'S', 06, 4, 5, '', 01, 1106, 0003, 0003, '', 'JEEP'
);

/* INSERT QUERY NO: 1170 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11512, 11512, 'JP', 'JEEP', 'LIBERTY', '2.8L 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', 'LIBERTY', 105, 22, 'A', 04, 4, 5, '', 01, 1106, 0003, 0003, '', 'JEEP'
);

/* INSERT QUERY NO: 1171 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11513, 11513, 'JP', 'JEEP', 'CJ-7', '2.5L V6 4X4 GASOLINA FULL STD.', ' 05 OCUP.', 'CJ-7', 105, 03, 'S', 06, 3, 5, '', 01, 1106, 0003, 0003, '', 'JEEP'
);

/* INSERT QUERY NO: 1172 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11514, 11514, 'JP', 'JEEP', 'RENEGADO', '3.8L V6 4X4 GASOLINA FULL STD.', ' 04 OCUP.', 'RENEGADO', 105, 03, 'S', 06, 2, 4, '', 01, 1112, 0003, 0003, '', 'JEEP'
);

/* INSERT QUERY NO: 1173 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11515, 11515, 'JP', 'JEEP', 'COMMANDER', '5.7L V8 GASOLINA FULL STD.', ' 07 OCUP.', 'COMMANDER', 105, 03, 'S', 22, 5, 7, '', 01, 1112, 0003, 0003, '', 'JEEP'
);

/* INSERT QUERY NO: 1174 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11516, 11516, 'JP', 'JEEP', 'WRANGLER', 'RUBICON 3.6L 4X4 STD.', ' 05 OCUP.', 'WRANGLER', 105, 03, 'S', 04, 3, 5, '', 01, 1112, 0003, 0003, '', 'JEEP'
);

/* INSERT QUERY NO: 1175 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11517, 11517, 'JP', 'JEEP', 'WRANGLER', 'SPORT 4.0L V6 4X4 GASOLINA STD.', ' 05 OCUP.', 'WRANGLER', 105, 03, 'S', 06, 4, 5, '', 01, 1412, 0003, 0003, '', 'JEEP'
);

/* INSERT QUERY NO: 1176 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11518, 11518, 'JP', 'JEEP', 'RENEGADE', '2.4L 4X4 GASOLINA STD.', ' 05 OCUP.', 'RENEGADE', 105, 03, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'JEEP'
);

/* INSERT QUERY NO: 1177 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11519, 11519, 'JP', 'JEEP', 'GRAND CHEROKEE', '3.7L 4X2 GASOLINA STD.', ' 05 OCUP.', 'GRAND CHEROKEE', 105, 22, 'S', 06, 4, 5, '', 01, 1601, 0003, 0003, '', 'JEEP'
);

/* INSERT QUERY NO: 1178 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11520, 11520, 'JP', 'JEEP', 'WRANGLER', 'UNLIMITED SAHARA 3.6L 4X4 GASOLINA STD.', ' 05 OCUP.', 'WRANGLER', 105, 22, 'S', 06, 4, 5, '', 01, 1601, 0003, 0003, '', 'JEEP'
);

/* INSERT QUERY NO: 1179 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11521, 11521, 'JP', 'JEEP', 'SPORT', '2.4L 4X4 GASOLINA STD.', ' 05 OCUP.', 'SPORT', 105, 22, 'S', 04, 4, 5, '', 01, 1705, 0003, 0003, '', 'JEEP'
);

/* INSERT QUERY NO: 1180 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11522, 11522, 'JP', 'JEEP', 'LIBERTY', 'SPORT 3.7L 4X4 GASOLINA STD.', ' 05 OCUP.', 'LIBERTY', 105, 22, 'S', 06, 4, 5, '', 01, 1705, 0003, 0003, '', 'JEEP'
);

/* INSERT QUERY NO: 1181 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11701, 11701, 'PU', 'PLYMOUTH', 'PROWLER', 'CONVERTIBLE V6 3.5L GLINA FULL AUT.', ' 02 OCUP.', 'PROWLER', 100, 04, 'A', 06, 2, 2, '', 01, 1106, 0001, 0001, '', 'PLYMOUTH'
);

/* INSERT QUERY NO: 1182 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11702, 11702, 'PU', 'PLYMOUTH', 'LASER', 'SEDAN 2.0L V4 GASOLINA STD.', ' 05 OCUP.', 'LASER', 100, 01, 'S', 04, 4, 5, '', 01, 1212, 0001, 0001, '', 'PLYMOUTH'
);

/* INSERT QUERY NO: 1183 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11703, 11703, 'PU', 'PLYMOUTH', 'NEON', 'HIGHLINE SEDAN 2.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'NEON', 100, 01, 'S', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'PLYMOUTH'
);

/* INSERT QUERY NO: 1184 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11704, 11704, 'PU', 'PLYMOUTH', 'VOYAGER', '3.0L 4X2 GASOLINA STD.', ' 07 OCUP.', 'VOYAGER', 105, 02, 'S', 06, 4, 7, '', 01, 1501, 0003, 0003, '', 'PLYMOUTH'
);

/* INSERT QUERY NO: 1185 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11801, 11801, 'HW', 'HAWTAI', 'E70', 'SEDAN 2.0L GASOLINA STD.', ' 05 OCUP.', 'E70', 100, 01, 'S', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'HAWTAI'
);

/* INSERT QUERY NO: 1186 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 11802, 11802, 'HW', 'HAWTAI', 'BOLIGER', '2.0L 4X4 STD.', ' 05 OCUP.', 'BOLIGER', 105, 22, 'S', 04, 4, 5, '', 01, 1402, 0003, 0003, '', 'HAWTAI'
);

/* INSERT QUERY NO: 1187 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12001, 12001, 'HF', 'HAFEI', 'RIUYI', '1.0L DOBLE CABINA GASOLINA STD.', ' 05 OCUP.', 'RIUYI', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'HAFEI'
);

/* INSERT QUERY NO: 1188 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12002, 12002, 'HF', 'HAFEI', 'LOBO', 'SEDAN 1.1L GASOLINA STD.', ' 05 OCUP.', 'LOBO', 100, 01, 'S', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'HAFEI'
);

/* INSERT QUERY NO: 1189 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12003, 12003, 'HF', 'HAFEI', 'MINYI', '1.3L GASOLINA STD.', ' 07 OCUP.', 'MINYI', 100, 24, 'S', 04, 4, 7, '', 01, 1601, 0001, 0001, '', 'HAFEI'
);

/* INSERT QUERY NO: 1190 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12301, 12301, 'AR', 'ALFA ROMEO', 'MITO', 'HB 1.4L FULL AUT.', ' 05 OCUP.', 'MITO', 100, 27, 'A', 04, 2, 5, '', 01, 1106, 0001, 0001, '', 'ALFA ROMEO'
);

/* INSERT QUERY NO: 1191 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12303, 12303, 'AR', 'ALFA ROMEO', '156', 'SEDAN 2.0L FULL AUT.', ' 05 OCUP.', '156', 100, 01, 'A', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'ALFA ROMEO'
);

/* INSERT QUERY NO: 1192 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12304, 12304, 'AR', 'ALFA ROMEO', '159', 'SEDAN 2.2L GASOLINA FULL AUT.', ' 05 OCUP.', '159', 100, 01, 'A', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'ALFA ROMEO'
);

/* INSERT QUERY NO: 1193 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12305, 12305, 'AR', 'ALFA ROMEO', '147', '147 HB 1.5L 4X2 GASOLINA FULL STD.', ' 05 OCUP.', '147', 100, 27, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'ALFA ROMEO'
);

/* INSERT QUERY NO: 1194 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12306, 12306, 'AR', 'ALFA ROMEO', 'SPIDER', 'COUPE 3.0L 4X2 GASOLINA STD.', ' 02 OCUP.', 'SPIDER', 100, 18, 'S', 06, 2, 2, '', 01, 1112, 0001, 0001, '', 'ALFA ROMEO'
);

/* INSERT QUERY NO: 1195 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12307, 12307, 'AR', 'ALFA ROMEO', 'GIULIETTA', 'HB 1.4L GASOLINA AUT.', ' 05 OCUP.', 'GIULIETTA', 100, 27, 'A', 04, 4, 5, '', 01, 1212, 0001, 0001, '', 'ALFA ROMEO'
);

/* INSERT QUERY NO: 1196 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12401, 12401, 'FI', 'FERRARI', '360 MODENA', 'COUPE 3.6L V8 FULL AUT.', ' 02 OCUP.', '360 MODENA', 100, 08, 'A', 08, 2, 2, '', 01, 1106, 0001, 0001, '', 'FERRARI'
);

/* INSERT QUERY NO: 1197 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12402, 12402, 'FI', 'FERRARI', 'CALIFORNIA', '4.3L V8 GASOLINA STD.', ' 04 OCUP.', 'CALIFORNIA', 100, 04, 'S', 08, 4, 4, '', 01, 1501, 0001, 0001, '', 'FERRARI'
);

/* INSERT QUERY NO: 1198 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12501, 12501, 'JI', 'JINBEI', 'HAISE', 'VAN 2.7L DIESEL STD.', ' 15 OCUP.', 'HAISE', 105, 11, 'S', 04, 3, 15, '', 01, 1112, 0003, 0003, '', 'JINBEI'
);

/* INSERT QUERY NO: 1199 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12502, 12502, 'JI', 'JINBEI', 'HAISE', '2.5L 4X2 DIESEL STD.', ' 16 OCUP.', 'HAISE', 105, 11, 'S', 04, 3, 16, '', 01, 1705, 0003, 0003, '', 'JINBEI'
);

/* INSERT QUERY NO: 1200 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12751, 12751, 'WI', 'WEICHAI', 'ENRANGER', '1.5L GASOLINA STD.', ' 05 OCUP.', 'ENRANGER', 105, 22, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'WEICHAI'
);

/* INSERT QUERY NO: 1201 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12801, 12801, 'MT', 'MASERATI', 'GHIBLI SQ4', '3.0L GASOLINA AUT.', ' 05 OCUP.', 'GHIBLI SQ4', 100, 01, 'A', 06, 4, 5, '', 01, 1501, 0001, 0001, '', 'MASERATI'
);

/* INSERT QUERY NO: 1202 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12802, 12802, 'MT', 'MASERATI', 'QUATTRO PORTE SQ4', '3.0L GASOLINA AUT.', ' 05 OCUP.', 'QUATTRO PORTE SQ4', 100, 01, 'A', 06, 4, 5, '', 01, 1501, 0001, 0001, '', 'MASERATI'
);

/* INSERT QUERY NO: 1203 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12803, 12803, 'MT', 'MASERATI', 'GHIBLI 250', '3.0L DIESEL AUT.', ' 05 OCUP.', 'GHIBLI 250', 100, 01, 'A', 06, 4, 5, '', 01, 1501, 0001, 0001, '', 'MASERATI'
);

/* INSERT QUERY NO: 1204 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12804, 12804, 'MT', 'MASERATI', 'LEVANTE', '3.0L DIESEL AUT.', ' 05 OCUP.', 'LEVANTE', 105, 22, 'A', 06, 4, 5, '', 01, 1501, 0003, 0003, '', 'MASERATI'
);

/* INSERT QUERY NO: 1205 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12805, 12805, 'MT', 'MASERATI', 'LEVANTE', '3.8L GASOLINA AUT.', ' 05 OCUP.', 'LEVANTE', 105, 22, 'A', 08, 4, 5, '', 01, 1501, 0003, 0003, '', 'MASERATI'
);

/* INSERT QUERY NO: 1206 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12806, 12806, 'MT', 'MASERATI', 'LEVANTE', 'MY17 3.0L 4X4 GASOLINA STD.', ' 05 OCUP.', 'LEVANTE', 105, 22, 'S', 06, 4, 5, '', 01, 1601, 0003, 0003, '', 'MASERATI'
);

/* INSERT QUERY NO: 1207 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12901, 12901, 'LX', 'LEXUS', 'ISF', '0 LIMOUSINE 5.0L GASOLINA FULL STD.', ' 05 OCUP.', 'ISF', 100, 01, 'S', 08, 4, 5, '', 01, 1106, 0001, 0001, '', 'LEXUS'
);

/* INSERT QUERY NO: 1208 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12902, 12902, 'LX', 'LEXUS', 'RX', '3.5L 4X4 GASOLINA FULL STD.', ' 05 OCUP.', 'RX', 105, 22, 'S', 06, 4, 5, '', 01, 1106, 0003, 0003, '', 'LEXUS'
);

/* INSERT QUERY NO: 1209 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12903, 12903, 'LX', 'LEXUS', 'GS', '50 SEDAN 3.5L GASOLINA FULL STD.', ' 05 OCUP.', 'GS', 100, 01, 'S', 04, 5, 5, '', 01, 1112, 0001, 0001, '', 'LEXUS'
);

/* INSERT QUERY NO: 1210 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12904, 12904, 'LX', 'LEXUS', 'LX', 'ES300 2.5L GASOLINA FULL AUT.', ' 07 OCUP.', 'LX', 105, 22, 'A', 08, 5, 7, '', 01, 1112, 0003, 0003, '', 'LEXUS'
);

/* INSERT QUERY NO: 1211 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12905, 12905, 'LX', 'LEXUS', 'LX', 'LX 570 4.7L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'LX', 105, 22, 'A', 08, 4, 5, '', 01, 1212, 0003, 0003, '', 'LEXUS'
);

/* INSERT QUERY NO: 1212 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12906, 12906, 'LX', 'LEXUS', 'ES', 'ES 300 4.3L GASOLINA AUT.', ' 05 OCUP.', 'ES', 100, 01, 'A', 04, 4, 5, '', 01, 1212, 0001, 0001, '', 'LEXUS'
);

/* INSERT QUERY NO: 1213 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12907, 12907, 'LX', 'LEXUS', 'RX', 'RX 330 4.7L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'RX', 105, 22, 'A', 06, 4, 5, '', 01, 1212, 0003, 0003, '', 'LEXUS'
);

/* INSERT QUERY NO: 1214 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12908, 12908, 'LX', 'LEXUS', 'CT', 'CT 200 H SEDAN HIBRIDO 5.0L GASOLINA AUT.', ' 05 OCUP.', 'CT', 100, 01, 'A', 04, 4, 5, '', 01, 1212, 0001, 0001, '', 'LEXUS'
);

/* INSERT QUERY NO: 1215 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12909, 12909, 'LX', 'LEXUS', 'IS', 'IS 250 SEDAN 5.1L GASOLINA AUT.', ' 05 OCUP.', 'IS', 100, 01, 'A', 06, 4, 5, '', 01, 1212, 0001, 0001, '', 'LEXUS'
);

/* INSERT QUERY NO: 1216 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12910, 12910, 'LX', 'LEXUS', 'ES', 'ES 350 SEDAN 3.5L GASOLINA AUT.', ' 05 OCUP.', 'ES', 100, 01, 'A', 06, 4, 5, '', 01, 1212, 0001, 0001, '', 'LEXUS'
);

/* INSERT QUERY NO: 1217 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12911, 12911, 'LX', 'LEXUS', 'GS', 'GS 350 SEDAN 2.0L GASOLINA AUT.', ' 05 OCUP.', 'GS', 100, 01, 'A', 06, 4, 5, '', 01, 1212, 0001, 0001, '', 'LEXUS'
);

/* INSERT QUERY NO: 1218 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12912, 12912, 'LX', 'LEXUS', 'LS', 'LS 460 SEDAN 4.6L GASOLINA AUT.', ' 05 OCUP.', 'LS', 100, 01, 'A', 08, 4, 5, '', 01, 1212, 0001, 0001, '', 'LEXUS'
);

/* INSERT QUERY NO: 1219 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12913, 12913, 'LX', 'LEXUS', 'RX', 'RX 250 SEDAN 3.5L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'RX', 105, 22, 'A', 06, 4, 5, '', 01, 1212, 0003, 0003, '', 'LEXUS'
);

/* INSERT QUERY NO: 1220 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12914, 12914, 'LX', 'LEXUS', 'GX', 'GX 460 SEDAN 4.6L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'GX', 105, 22, 'A', 08, 4, 5, '', 01, 1212, 0003, 0003, '', 'LEXUS'
);

/* INSERT QUERY NO: 1221 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12915, 12915, 'LX', 'LEXUS', 'RX', 'RX 350F SPORT 3.5L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'RX', 105, 22, 'A', 06, 4, 5, '', 01, 1212, 0003, 0003, '', 'LEXUS'
);

/* INSERT QUERY NO: 1222 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12916, 12916, 'LX', 'LEXUS', 'RX', 'RX 350 3.5L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'RX', 105, 22, 'A', 06, 5, 5, '', 01, 1402, 0003, 0003, '', 'LEXUS'
);

/* INSERT QUERY NO: 1223 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12917, 12917, 'LX', 'LEXUS', 'RX', 'RX 350F PREMIUM 3.5L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'RX', 105, 22, 'A', 06, 5, 5, '', 01, 1402, 0003, 0003, '', 'LEXUS'
);

/* INSERT QUERY NO: 1224 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12918, 12918, 'LX', 'LEXUS', 'GX', 'GX 460 4.6L 4X4 GASOLINA FULL AUT.', ' 07 OCUP.', 'GX', 105, 22, 'A', 08, 5, 7, '', 01, 1402, 0003, 0003, '', 'LEXUS'
);

/* INSERT QUERY NO: 1225 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12919, 12919, 'LX', 'LEXUS', 'LX', 'LX 570 5.7L 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', 'LX', 105, 22, '', 06, 5, 5, '', 01, 1402, 0003, 0003, '', 'LEXUS'
);

/* INSERT QUERY NO: 1226 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12920, 12920, 'LX', 'LEXUS', 'CT', 'CT 200 SEDAN 1.8L GASOLINA AUT.', ' 05 OCUP.', 'CT', 100, 01, 'A', 04, 5, 5, '', 01, 1402, 0001, 0001, '', 'LEXUS'
);

/* INSERT QUERY NO: 1227 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12921, 12921, 'LX', 'LEXUS', 'IS', 'IS 350 F SPORT 3.5L GASOLINA AUT.', ' 05 OCUP.', 'IS', 100, 01, 'A', 06, 4, 5, '', 01, 1402, 0001, 0001, '', 'LEXUS'
);

/* INSERT QUERY NO: 1228 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12922, 12922, 'LX', 'LEXUS', 'LS', 'LS 460 LIMOUSINE 4.5L GASOLINA AUT.', ' 05 OCUP.', 'LS', 100, 01, 'A', 08, 4, 5, '', 01, 1402, 0001, 0001, '', 'LEXUS'
);

/* INSERT QUERY NO: 1229 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12923, 12923, 'LX', 'LEXUS', 'IS', 'IS 250 2.5L GASOLINA AUT.', ' 05 OCUP.', 'IS', 100, 01, 'A', 06, 4, 5, '', 01, 1402, 0001, 0001, '', 'LEXUS'
);

/* INSERT QUERY NO: 1230 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12924, 12924, 'LX', 'LEXUS', 'IS', 'IS 250 F SPORT 2.5L GASOLINA AUT.', ' 05 OCUP.', 'IS', 100, 01, 'A', 06, 4, 5, '', 01, 1402, 0001, 0001, '', 'LEXUS'
);

/* INSERT QUERY NO: 1231 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12925, 12925, 'LX', 'LEXUS', 'GS', 'GS 350 3.5L GASOLINA AUT.', ' 05 OCUP.', 'GS', 100, 01, 'A', 06, 4, 5, '', 01, 1402, 0001, 0001, '', 'LEXUS'
);

/* INSERT QUERY NO: 1232 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12926, 12926, 'LX', 'LEXUS', 'RX', 'RX 330 3.3L 4X2 GASOLINA STD.', ' 05 OCUP.', 'RX', 105, 22, 'S', 04, 4, 5, '', 01, 1402, 0003, 0003, '', 'LEXUS'
);

/* INSERT QUERY NO: 1233 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12927, 12927, 'LX', 'LEXUS', 'RX', 'RX 300 3.9L 4X4 GASOLINA STD.', ' 05 OCUP.', 'RX', 105, 22, 'S', 06, 4, 5, '', 01, 1402, 0003, 0003, '', 'LEXUS'
);

/* INSERT QUERY NO: 1234 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12928, 12928, 'LX', 'LEXUS', 'NX', 'NX 300 2.5L 4X4 GASOLINA STD.', ' 05 OCUP.', 'NX', 105, 22, 'S', 04, 4, 5, '', 01, 1412, 0003, 0003, '', 'LEXUS'
);

/* INSERT QUERY NO: 1235 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12929, 12929, 'LX', 'LEXUS', 'RC', 'RC F 5L GASOLINA AUT.', ' 04 OCUP.', 'RC', 100, 01, 'A', 08, 2, 4, '', 01, 1501, 0001, 0001, '', 'LEXUS'
);

/* INSERT QUERY NO: 1236 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12930, 12930, 'LX', 'LEXUS', 'RX', '450H 3.5L 4X4 HIBRIDO STD.', ' 05 OCUP.', 'RX', 105, 22, 'S', 06, 4, 5, '', 01, 1601, 0003, 0003, '', 'LEXUS'
);

/* INSERT QUERY NO: 1237 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12931, 12931, 'LX', 'LEXUS', 'IS', '200T 2.0L GASOLINA AUT.', ' 05 OCUP.', 'IS', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'LEXUS'
);

/* INSERT QUERY NO: 1238 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12932, 12932, 'LX', 'LEXUS', 'LS', 'LS 600 HL HIBRIDO 5.0L GASOLINA STD.', ' 05 OCUP.', 'LS', 100, 01, 'S', 08, 4, 5, '', 01, 1601, 0001, 0001, '', 'LEXUS'
);

/* INSERT QUERY NO: 1239 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12933, 12933, 'LX', 'LEXUS', 'NX', 'NX 300H 2.5L 4X4 HIBRIDO STD.', ' 05 OCUP.', 'NX', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'LEXUS'
);

/* INSERT QUERY NO: 1240 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 12934, 12934, 'LX', 'LEXUS', 'NX', 'NX 200T 2.0L 4X4 GASOLINA AUT.', ' 05 OCUP.', 'NX', 105, 22, 'A', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'LEXUS'
);

/* INSERT QUERY NO: 1241 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13001, 13001, 'SA', 'SKODA', 'OCTAVIA', 'SEDAN 2.0L FULL STD.', ' 05 OCUP.', 'OCTAVIA', 100, 01, 'S', 04, 4, 5, '', 01, 1106, 0001, 0001, '', 'SKODA'
);

/* INSERT QUERY NO: 1242 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13002, 13002, 'SA', 'SKODA', 'FABIA', 'HB 1.6L GASOLINA FULL STD.', ' 05 OCUP.', 'FABIA', 100, 01, 'S', 04, 5, 5, '', 01, 1112, 0001, 0001, '', 'SKODA'
);

/* INSERT QUERY NO: 1243 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13101, 13101, 'DA', 'DACIA', 'SUPERNOVA', 'HB 1.4L GASOLINA FULL STD.', ' 05 OCUP.', 'SUPERNOVA', 100, 01, 'S', 04, 5, 5, '', 01, 1112, 0001, 0001, '', 'DACIA'
);

/* INSERT QUERY NO: 1244 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13151, 13151, 'TN', 'TIANMA', 'CROSSLANDER', '2.8L DIESEL STD.', ' 05 OCUP.', 'CROSSLANDER', 105, 22, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'TIANMA'
);

/* INSERT QUERY NO: 1245 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13201, 13201, 'CM', 'CMC', 'Z7', '2.4L C/A AC D/H V/T 4X4 GASOLINA FULL STD.', ' 07 OCUP.', 'Z7', 105, 22, 'S', 04, 5, 7, '', 01, 1212, 0003, 0003, '', 'CMC'
);

/* INSERT QUERY NO: 1246 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13202, 13202, 'CM', 'CMC', 'VARICA', '1.2L 4X2 GASOLINA STD.', ' 06 OCUP.', 'VARICA', 105, 22, 'S', 04, 5, 6, '', 01, 1501, 0003, 0003, '', 'CMC'
);

/* INSERT QUERY NO: 1247 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13203, 13203, 'CM', 'CMC', 'Z7', '2.4L 4X2 GASOLINA AUT.', ' 07 OCUP.', 'Z7', 105, 22, 'A', 04, 4, 7, '', 01, 1501, 0003, 0003, '', 'CMC'
);

/* INSERT QUERY NO: 1248 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13251, 13251, 'ZE', 'ZOTYE', 'V-10', '1.2L 4X2 GASOLINA STD.', ' 02 OCUP.', 'V-10', 105, 02, 'S', 04, 4, 2, '', 01, 1501, 0003, 0003, '', 'ZOTYE'
);

/* INSERT QUERY NO: 1249 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13252, 13252, 'ZE', 'ZOTYE', 'T200', '1.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'T200', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'ZOTYE'
);

/* INSERT QUERY NO: 1250 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13301, 13301, 'BE', 'BRILLIANCE', 'FRV', 'SEDAN 1.3L GASOLINA STD.', ' 05 OCUP.', 'FRV', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'BRILLIANCE'
);

/* INSERT QUERY NO: 1251 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13302, 13302, 'BE', 'BRILLIANCE', 'V5', 'SUV 1.6L V4 DH GASOLINA STD.', ' 05 OCUP.', 'V5', 105, 22, 'S', 04, 5, 5, '', 01, 1212, 0003, 0003, '', 'BRILLIANCE'
);

/* INSERT QUERY NO: 1252 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13501, 13501, 'LF', 'LIFAN', '520', 'SEDAN 1.6L GASOLINA STD.', ' 05 OCUP.', '520', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'LIFAN'
);

/* INSERT QUERY NO: 1253 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13502, 13502, 'LF', 'LIFAN', 'LF', '7130 A EX 1.3L GASOLINA AUT.', ' 05 OCUP.', 'LF', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'LIFAN'
);

/* INSERT QUERY NO: 1254 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13503, 13503, 'LF', 'LIFAN', 'LF', '7160L1 LX 1.6L 4X2 GASOLINA STD.', ' 05 OCUP.', 'LF', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'LIFAN'
);

/* INSERT QUERY NO: 1255 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13504, 13504, 'LF', 'LIFAN', '320', '1.5L 4X2 GASOLINA AUT.', ' 05 OCUP.', '320', 100, 27, 'A', 04, 5, 5, '', 01, 1601, 0001, 0001, '', 'LIFAN'
);

/* INSERT QUERY NO: 1256 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13505, 13505, 'LF', 'LIFAN', 'LF', '7132 1.3L GASOLINA AUT.', ' 05 OCUP.', 'LF', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'LIFAN'
);

/* INSERT QUERY NO: 1257 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13506, 13506, 'LF', 'LIFAN', '620', '1.6L GASOLINA AUT.', ' 05 OCUP.', '620', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'LIFAN'
);

/* INSERT QUERY NO: 1258 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13551, 13551, 'SY', 'SHINERAY', 'X30', 'VAN X30 1.0L GASOLINA STD.', ' 07 OCUP.', 'SHINERAY', 105, 11, 'S', 04, 5, 7, '', 01, 1501, 0003, 0003, '', 'SHINERAY'
);

/* INSERT QUERY NO: 1259 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13552, 13552, 'SY', 'SHINERAY', 'SY 6390', '1.0L 4X2 GASOLINA STD.', ' 04 OCUP.', 'SY 6390', 105, 11, 'S', 04, 4, 4, '', 01, 1501, 0003, 0003, '', 'SHINERAY'
);

/* INSERT QUERY NO: 1260 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13601, 13601, 'BD', 'BYD', 'GLX-I', 'HB 1.0L STD.', ' 05 OCUP.', 'GLX-I', 100, 01, 'S', 03, 2, 5, '', 01, 1106, 0001, 0001, '', 'BYD'
);

/* INSERT QUERY NO: 1261 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13602, 13602, 'BD', 'BYD', 'S6', 'SUV 2.4L ABS 4X4 GASOLINA AUT.', ' 05 OCUP.', 'S6', 105, 22, 'A', 04, 5, 5, '', 01, 1212, 0003, 0003, '', 'BYD'
);

/* INSERT QUERY NO: 1262 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13603, 13603, 'BD', 'BYD', 'G3', '1.5L GASOLINA AUT.', ' 05 OCUP.', 'G3', 100, 01, 'A', 04, 4, 5, '', 01, 1212, 0001, 0001, '', 'BYD'
);

/* INSERT QUERY NO: 1263 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13604, 13604, 'BD', 'BYD', 'F0', 'GLX-I 1.0L GASOLINA STD.', ' 05 OCUP.', 'F0', 100, 01, 'S', 03, 4, 5, '', 01, 1501, 0001, 0001, '', 'BYD'
);

/* INSERT QUERY NO: 1264 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13605, 13605, 'BD', 'BYD', 'F3', '1.5L GASOLINA STD.', ' 05 OCUP.', 'F3', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'BYD'
);

/* INSERT QUERY NO: 1265 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13606, 13606, 'BD', 'BYD', 'F3', '1.5L GASOLINA AUT.', ' 05 OCUP.', 'F3', 100, 01, 'A', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'BYD'
);

/* INSERT QUERY NO: 1266 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13607, 13607, 'BD', 'BYD', 'S5', 'GS I 2.0L GASOLINA AUT.', ' 05 OCUP.', 'S5', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'BYD'
);

/* INSERT QUERY NO: 1267 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13608, 13608, 'BD', 'BYD', 'S1', '1.5L GASOLINA STD.', ' 05 OCUP.', 'S1', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'BYD'
);

/* INSERT QUERY NO: 1268 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13609, 13609, 'BD', 'BYD', 'F5', '1.5L GASOLINA STD.', ' 05 OCUP.', 'F5', 100, 01, 'S', 04, 4, 5, '', 01, 1705, 0001, 0001, '', 'BYD'
);

/* INSERT QUERY NO: 1269 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13610, 13610, 'BD', 'BYD', 'S3', 'GLX 1.5L GASOLINA AUT.', ' 05 OCUP.', 'S3', 105, 22, 'A', 04, 4, 5, '', 01, 1705, 0003, 0003, '', 'BYD'
);

/* INSERT QUERY NO: 1270 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13701, 13701, 'DG', 'DONGFENG', 'OTING', '3.2L V6 4X4 GASOLINA FULL STD.', ' 05 OCUP.', 'OTING', 105, 22, 'S', 06, 5, 5, '', 01, 1106, 0003, 0003, '', 'DONGFENG'
);

/* INSERT QUERY NO: 1271 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13702, 13702, 'DG', 'DONGFENG', 'SUCCE', '1.6L 4X2 GASOLINA FULL STD.', ' 07 OCUP.', 'SUCCE', 100, 02, 'S', 04, 5, 7, '', 01, 1112, 0001, 0001, '', 'DONGFENG'
);

/* INSERT QUERY NO: 1272 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13703, 13703, 'DG', 'DONGFENG', 'S30', '1.6L 4X2 GASOLINA STD.', ' 05 OCUP.', 'S30', 100, 01, 'S', 04, 4, 5, '', 01, 1501, 0001, 0001, '', 'DONGFENG'
);

/* INSERT QUERY NO: 1273 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13704, 13704, 'DG', 'DONGFENG', 'AX7', '2L 4X2 GASOLINA STD.', ' 05 OCUP.', 'AX7', 105, 22, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'DONGFENG'
);

/* INSERT QUERY NO: 1274 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13705, 13705, 'DG', 'DONGFENG', 'VENUCIA', 'R30 1.2L GASOLINA STD.', ' 05 OCUP.', 'VENUCIA', 100, 27, 'S', 03, 5, 5, '', 01, 1601, 0001, 0001, '', 'DONGFENG'
);

/* INSERT QUERY NO: 1275 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13706, 13706, 'DG', 'DONGFENG', 'A30', '1.6L GASOLINA STD.', ' 05 OCUP.', 'A30', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'DONGFENG'
);

/* INSERT QUERY NO: 1276 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13707, 13707, 'DG', 'DONGFENG', 'JOYEAR X3', '1.6L GASOLINA STD.', ' 05 OCUP.', 'JOYEAR X3', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'DONGFENG'
);

/* INSERT QUERY NO: 1277 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13801, 13801, 'MD', 'MAHINDRA', 'BOLERO', '2.5L 4X4 GASOLINA FULL STD.', ' 07 OCUP.', 'BOLERO', 105, 22, 'S', 04, 5, 7, '', 01, 1106, 0003, 0003, '', 'MAHINDRA'
);

/* INSERT QUERY NO: 1278 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13802, 13802, 'MD', 'MAHINDRA', 'SCORPIO', '2.2L 4X4 DIESEL STD.', ' 07 OCUP.', 'SCORPIO', 105, 22, 'S', 04, 5, 7, '', 01, 1112, 0003, 0003, '', 'MAHINDRA'
);

/* INSERT QUERY NO: 1279 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13803, 13803, 'MD', 'MAHINDRA', 'XUV', '500 2.2L DIESEL STD.', ' 05 OCUP.', 'XUV', 105, 22, 'S', 06, 4, 5, '', 01, 1402, 0003, 0003, '', 'MAHINDRA'
);

/* INSERT QUERY NO: 1280 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13901, 13901, 'TH', 'TRIUMPH', 'TR6', 'COUPE V6 2.5L GASOLINA FULL AUT.', ' 02 OCUP.', 'TR6', 100, 04, 'A', 06, 2, 2, '', 01, 1106, 0001, 0001, '', 'TRIUMPH'
);

/* INSERT QUERY NO: 1281 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 13902, 13902, 'TH', 'TRIUMPH', 'SPITFIRE', 'COUPE 1.5L GASOLINA FULL AUT.', ' 04 OCUP.', 'SPITFIRE', 100, 08, 'A', 04, 2, 4, '', 01, 1112, 0001, 0001, '', 'TRIUMPH'
);

/* INSERT QUERY NO: 1282 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14001, 14001, 'GN', 'GOLDEN DRAGON', 'XML6531', '2.7L DIESEL STD.', ' 14 OCUP.', 'XML6531', 105, 11, 'S', 04, 5, 14, '', 01, 1106, 0003, 0003, '', 'GOLDEN DRAGON'
);

/* INSERT QUERY NO: 1283 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14101, 14101, 'WS', 'WILLYS', 'JEEP', '2.2L 4X4 GASOLINA STD.', ' 05 OCUP.', 'JEEP', 105, 11, 'S', 04, 3, 5, '', 01, 1112, 0003, 0003, '', 'WILLYS'
);

/* INSERT QUERY NO: 1284 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14201, 14201, 'CA', 'CVE ESPECIAL', 'CVE ESPECIAL', '', '', '100', 01, 0, '01', 0, 2, 0, '01', 1212, 0001, 0001, 0, '', 'CA CVE ESPECIAL CARRO ELECTRICO DE GOLF'
);

/* INSERT QUERY NO: 1285 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14202, 14202, 'CA', 'CVE ESPECIAL', 'CVE ESPECIAL', '', '', '100', 01, 0, '00', 0, 5, 0, '01', 1705, 0020, 0020, 0, '', 'CA CVE ESPECIAL AUTOMOVIL ELECTRICO PARTICULAR'
);

/* INSERT QUERY NO: 1286 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14301, 14301, 'PN', 'PROTON', '415', 'HB 1.5L GASOLINA FULL STD.', ' 05 OCUP.', '415', 100, 01, 'S', 04, 4, 5, '', 01, 1112, 0001, 0001, '', 'PROTON'
);

/* INSERT QUERY NO: 1287 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14351, 14351, 'DI', 'DADI AUTO', 'CITY CRUISER', 'BDD6490 2.8L GASOLINA STD.', ' 07 OCUP.', 'CITY CRUISER', 105, 22, 'S', 04, 4, 7, '', 01, 1501, 0003, 0003, '', 'DADI AUTO'
);

/* INSERT QUERY NO: 1288 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14396, 14396, 'RR', 'ROVER', 'MINI', 'COOPER GB 1.3L GASOLINA STD.', ' 04 OCUP.', 'MINI', 100, 01, 'S', 04, 2, 4, '', 01, 1601, 0001, 0001, '', 'ROVER'
);

/* INSERT QUERY NO: 1289 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14401, 14401, 'JC', 'JMC', 'N350', 'SUV 2.4L V4 4X4 DIESEL STD.', ' 05 OCUP.', 'N350', 105, 22, 'S', 04, 4, 5, '', 01, 1212, 0003, 0003, '', 'JMC'
);

/* INSERT QUERY NO: 1290 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14402, 14402, 'JC', 'JMC', 'S350', '2.4L 4X4 DIESEL STD.', ' 05 OCUP.', 'S350', 105, 22, 'S', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'JMC'
);

/* INSERT QUERY NO: 1291 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14403, 14403, 'JC', 'JMC', 'LANDWIND', 'X6 2.8L 4X4 DIESEL AUT.', ' 05 OCUP.', 'LANDWIND', 105, 22, 'A', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'JMC'
);

/* INSERT QUERY NO: 1292 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14501, 14501, 'JS', 'JAC MOTORS', 'J2', 'HB 1.0L V4 GASOLINA STD.', ' 05 OCUP.', 'J2', 100, 27, 'S', 04, 4, 5, '', 01, 1212, 0001, 0001, '', 'JAC MOTORS'
);

/* INSERT QUERY NO: 1293 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14502, 14502, 'JS', 'JAC MOTORS', 'J6', 'STATION WAGON 1.8L GASOLINA STD.', ' 07 OCUP.', 'J6', 100, 02, 'S', 04, 5, 7, '', 01, 1212, 0001, 0001, '', 'JAC MOTORS'
);

/* INSERT QUERY NO: 1294 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14503, 14503, 'JS', 'JAC MOTORS', 'J3', 'SPORT SEDAN 1.3L GASOLINA STD.', ' 05 OCUP.', 'J3', 100, 01, 'S', 04, 5, 5, '', 01, 1212, 0001, 0001, '', 'JAC MOTORS'
);

/* INSERT QUERY NO: 1295 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14504, 14504, 'JS', 'JAC MOTORS', 'S5', '2.0L GASOLINA AUT.', ' 05 OCUP.', 'S5', 105, 22, 'A', 04, 4, 5, '', 01, 1402, 0003, 0003, '', 'JAC MOTORS'
);

/* INSERT QUERY NO: 1296 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14505, 14505, 'JS', 'JAC MOTORS', 'J4', 'SEDAN 1.5L GASOLINA AUT.', ' 05 OCUP.', 'J4', 100, 01, 'A', 04, 4, 5, '', 01, 1402, 0001, 0001, '', 'JAC MOTORS'
);

/* INSERT QUERY NO: 1297 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14506, 14506, 'JS', 'JAC MOTORS', 'J5', 'SEDAN 1.8L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'J5', 100, 01, 'A', 04, 4, 5, '', 01, 1410, 0001, 0001, '', 'JAC MOTORS'
);

/* INSERT QUERY NO: 1298 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14507, 14507, 'JS', 'JAC MOTORS', 'REFINE', '2.8L DIESEL STD.', ' 12 OCUP.', 'REFINE', 105, 11, 'S', 04, 4, 12, '', 01, 1501, 0003, 0003, '', 'JAC MOTORS'
);

/* INSERT QUERY NO: 1299 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14508, 14508, 'JS', 'JAC MOTORS', 'J6', '1.8L GASOLINA AUT.', ' 05 OCUP.', 'J6', 105, 22, 'A', 04, 4, 5, '', 01, 1501, 0003, 0003, '', 'JAC MOTORS'
);

/* INSERT QUERY NO: 1300 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14509, 14509, 'JS', 'JAC MOTORS', 'S3', '1.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'S3', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'JAC MOTORS'
);

/* INSERT QUERY NO: 1301 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14510, 14510, 'JS', 'JAC MOTORS', 'S5', '2.0L GASOLINA STD.', ' 05 OCUP.', 'S5', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'JAC MOTORS'
);

/* INSERT QUERY NO: 1302 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14511, 14511, 'JS', 'JAC MOTORS', 'J3', 'TURIN 1.3L 4X2 GASOLINA AUT.', ' 05 OCUP.', 'J3', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'JAC MOTORS'
);

/* INSERT QUERY NO: 1303 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14512, 14512, 'JS', 'JAC MOTORS', 'S2', '1.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'S2', 105, 22, 'S', 04, 4, 5, '', 01, 1601, 0003, 0003, '', 'JAC MOTORS'
);

/* INSERT QUERY NO: 1304 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14513, 14513, 'JS', 'JAC MOTORS', 'J3', 'TURIN 1.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'J3', 100, 01, 'S', 04, 4, 5, '', 01, 1705, 0001, 0001, '', 'JAC MOTORS'
);

/* INSERT QUERY NO: 1305 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14601, 14601, 'JY', 'JONWAY', 'A380', 'SUV 2.0L 4X2 GASOLINA FULL STD.', ' 05 OCUP.', 'A380', 105, 22, 'S', 04, 4, 5, '', 01, 1212, 0003, 0003, '', 'JONWAY'
);

/* INSERT QUERY NO: 1306 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14602, 14602, 'JY', 'JONWAY', 'A380', 'SUV 2.0L 4X2 GASOLINA FULL AUT.', ' 05 OCUP.', 'A380', 105, 22, 'A', 04, 4, 5, '', 01, 1212, 0003, 0003, '', 'JONWAY'
);

/* INSERT QUERY NO: 1307 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14651, 14651, 'SE', 'SOUEAST', 'V6', 'SEDAN 1.5L V4 4X2 GASOLINA STD.', ' 05 OCUP.', 'V6', 100, 01, 'S', 04, 4, 5, '', 01, 1212, 0001, 0001, '', 'SOUEAST'
);

/* INSERT QUERY NO: 1308 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14652, 14652, 'SE', 'SOUEAST', 'V3', 'SEDAN 1.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'V3', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'SOUEAST'
);

/* INSERT QUERY NO: 1309 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14653, 14653, 'SE', 'SOUEAST', 'LIONCEL', 'SEDAN 1.5L V4 GASOLINA STD.', ' 05 OCUP.', 'LIONCEL', 100, 01, 'S', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'SOUEAST'
);

/* INSERT QUERY NO: 1310 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14654, 14654, 'SE', 'SOUEAST', 'LIONCEL', 'SEDAN 1.5L V4 GASOLINA AUT.', ' 05 OCUP.', 'LIONCEL', 100, 01, 'A', 04, 4, 5, '', 01, 1601, 0001, 0001, '', 'SOUEAST'
);

/* INSERT QUERY NO: 1311 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14701, 14701, 'FU', 'FULU', 'FLC3', 'SEDAN 1.0L GASOLINA STD.', ' 04 OCUP.', 'FLC3', 100, 01, 'S', 02, 5, 4, '', 01, 1212, 0001, 0001, '', 'FULU'
);

/* INSERT QUERY NO: 1312 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14702, 14702, 'FU', 'FULU', 'FLC5', 'SEDAN 1.0L GASOLINA STD.', ' 04 OCUP.', 'FLC5', 100, 01, 'S', 01, 5, 4, '', 01, 1212, 0001, 0001, '', 'FULU'
);

/* INSERT QUERY NO: 1313 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14751, 14751, 'EE', 'EAGLE', 'SUMMIT', 'SEDAN 1.5L GASOLINA STD.', ' 05 OCUP.', 'SUMMIT', 100, 01, 'S', 04, 2, 5, '', 01, 1212, 0001, 0001, '', 'EAGLE'
);

/* INSERT QUERY NO: 1314 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14801, 14801, 'GC', 'GMC', 'YUKON DENALI', '6.0L 4X4 GASOLINA AUT.', ' 08 OCUP.', 'YUKON DENALI', 105, 22, 'A', 08, 5, 8, '', 01, 1212, 0003, 0003, '', 'GMC'
);

/* INSERT QUERY NO: 1315 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14851, 14851, 'ZA', 'ZNA', 'JOYEAR', '1.5L 4X2 GASOLINA STD.', ' 05 OCUP.', 'JOYEAR', 105, 22, 'S', 04, 5, 5, '', 01, 1501, 0003, 0003, '', 'ZNA'
);

/* INSERT QUERY NO: 1316 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14901, 14901, 'OE', 'OLDSMOBILE', 'ACHIEVA', '3.1L GASOLINA STD.', ' 04 OCUP.', 'ACHIEVA', 100, 01, 'S', 04, 4, 4, '', 01, 1501, 0001, 0001, '', 'OLDSMOBILE'
);

/* INSERT QUERY NO: 1317 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 14902, 14902, 'OE', 'OLDSMOBILE', 'CUSTOM CRUISER', '5.0L GASOLINA AUT.', ' 08 OCUP.', 'CUSTOM CRUISER', 100, 02, 'A', 08, 4, 8, '', 01, 1601, 0001, 0001, '', 'OLDSMOBILE'
);

/* INSERT QUERY NO: 1318 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 35000, 35000, 'MO', 'SIN MARCA', 'MOTOCICLETA', '', '', '400', 23, 0, '02', 0, 2, 0, '01', 1003, 0010, 0010, 0, '', 'MO MOTOCICLETA < 500 CC'
);

/* INSERT QUERY NO: 1319 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 35001, 35001, 'MO', 'SIN MARCA', 'MOTOCICLETA', '', '', '400', 23, 0, '02', 0, 2, 0, '01', 1112, 0001, 0001, 0, '', 'MO MOTOCICLETA > 500 CC'
);

/* INSERT QUERY NO: 1320 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 35002, 35002, 'MO', 'SIN MARCA', 'MOTOCICLETA', '', '', '400', 23, 0, '02', 0, 2, 0, '01', 1112, 0020, 0020, 0, '', 'MO MOTOCICLETA BMW > 500 CC'
);

/* INSERT QUERY NO: 1321 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 49000, 49000, 'AU', 'AUTOBUS', 'BUS', 'STD.', ' 40 OCUP.', 'ESTUDIANTES O PRIVADO', 214, 26, 'S', 08, 2, 40, '12', 02, 1003, 0001, 0001, '01', ''
);

/* INSERT QUERY NO: 1322 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 49001, 49001, 'AU', 'AUTOBUS', 'BUS', 'STD.', ' 40 OCUP.', 'LOCAL O URBANO', 209, 26, 'S', 08, 2, 40, '12', 02, 1003, 0001, 0001, '01', ''
);

/* INSERT QUERY NO: 1323 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 49002, 49002, 'AU', 'AUTOBUS', 'BUS', 'STD.', ' 40 OCUP.', 'INTERURBANO O TURISMO', 208, 26, 'S', 08, 2, 40, '12', 02, 1003, 0001, 0001, '01', ''
);

/* INSERT QUERY NO: 1324 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 49500, 49500, 'MS', 'MICROBUS', 'MICRO', 'STD.', ' 30 OCUP.', 'ESTUDIANTES O PRIVADO', 216, 19, 'S', 08, 2, 30, '06', 02, 1003, 0001, 0001, '01', ''
);

/* INSERT QUERY NO: 1325 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 49501, 49501, 'MS', 'MICROBUS', 'MICRO', 'STD.', ' 30 OCUP.', 'LOCAL O URBANO', 205, 19, 'S', 08, 2, 30, '06', 02, 1003, 0001, 0001, '01', ''
);

/* INSERT QUERY NO: 1326 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 49502, 49502, 'MS', 'MICROBUS', 'MICRO', 'STD.', ' 30 OCUP.', 'INTERURBANO O TURISMO', 225, 19, 'S', 08, 2, 30, '06', 02, 1402, 0001, 0001, '01', ''
);

/* INSERT QUERY NO: 1327 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50000, 50000, 'NN', 'NISSAN', 'FRONTIER', '2.5L 4X2 DIESEL FULL STD.', ' 03 OCUP.', 'FRONTIER', 210, 09, 'S', 04, 2, 3, '02', 02, 1003, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1328 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50001, 50001, 'NN', 'NISSAN', 'FRONTIER', '2.7L 4X4 GASOLINA FULL STD.', ' 03 OCUP.', 'FRONTIER', 210, 09, 'S', 04, 2, 3, '02', 02, 1003, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1329 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50002, 50002, 'NN', 'NISSAN', 'FRONTIER', '2.5L DOBLE CABINA 4X2 DIESEL FULL STD.', ' 05 OCUP.', 'FRONTIER', 210, 09, 'S', 04, 4, 5, '02', 02, 1003, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1330 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50003, 50003, 'NN', 'NISSAN', 'FRONTIER', '3.0L DOBLE CABINA 4X4 DIESEL FULL STD.', ' 05 OCUP.', 'FRONTIER', 210, 09, 'S', 04, 4, 5, '02', 02, 1003, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1331 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50006, 50006, 'NN', 'NISSAN', 'NAVARA', '2.5L DOBLE CABINA 4X4 DIESEL STD.', ' 05 OCUP.', 'NAVARA', 210, 09, 'S', 04, 4, 5, '01', 02, 1003, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1332 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50007, 50007, 'NN', 'NISSAN', 'NAVARA', '2.5L DOBLE CABINA 4X4 DIESEL AUT.', ' 05 OCUP.', 'NAVARA', 210, 09, 'A', 04, 4, 5, '01', 02, 1003, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1333 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50009, 50009, 'NN', 'NISSAN', 'D21', '2.4L 4X4 GASOLINA STD.', ' 06 OCUP.', 'D21', 210, 09, 'S', 04, 2, 6, '01', 02, 1106, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1334 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50010, 50010, 'NN', 'NISSAN', '1200', '1.2L 4x2 GASOLINA STD.', ' 02 OCUP.', '1200', 210, 09, 'S', 04, 2, 2, '01', 02, 1106, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1335 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50011, 50011, 'NN', 'NISSAN', 'CABSTAR', '3.5L GASOLINA FULL STD.', ' 03 OCUP.', 'CABSTAR', 200, 21, 'S', 04, 2, 3, '03', 02, 1106, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1336 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50012, 50012, 'NN', 'NISSAN', 'FRONTIER', '2.4L DOBLE CAB 4X2 GASOLINA FULL STD.', ' 04 OCUP.', 'FRONTIER', 210, 09, 'S', 04, 2, 4, '02', 02, 1106, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1337 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50013, 50013, 'NN', 'NISSAN', '720', '1.8L 4X2 GASOLINA STD.', ' 02 OCUP.', '720', 210, 09, 'S', 04, 2, 2, '02', 02, 1106, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1338 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50014, 50014, 'NN', 'NISSAN', 'DATSUN 1500', '1.5L GASOLINA STD.', ' 03 OCUP.', 'DATSUN 1500', 210, 09, 'S', 04, 2, 3, '02', 02, 1106, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1339 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50015, 50015, 'NN', 'NISSAN', 'D22', '3.0L DOBLE CABINA 4X4 GASOLINA FULL STD.', ' 05 OCUP.', 'D22', 210, 09, 'S', 04, 4, 5, '02', 02, 1112, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1340 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50016, 50016, 'NN', 'NISSAN', 'TITAN', '5.6L V8 GASOLINA FULL AUT.', ' 05 OCUP.', 'TITAN', 210, 09, 'A', 08, 4, 5, '02', 02, 1112, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1341 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50017, 50017, 'NN', 'NISSAN', 'FRONTIER', '4.0L EXTRA CAB 4X4 GASOLINA FULL STD.', ' 04 OCUP.', 'FRONTIER', 210, 09, 'S', 06, 2, 4, '02', 02, 1112, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1342 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50018, 50018, 'NN', 'NISSAN', 'U41', '4.2L V6 4X2 DIESEL STD.', ' 03 OCUP.', 'U41', 200, 21, 'S', 06, 2, 3, '06', 02, 1112, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1343 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50019, 50019, 'NN', 'NISSAN', 'JUNIOR', '2.5L 4X4 DIESEL STD.', ' 03 OCUP.', 'JUNIOR', 210, 09, 'S', 04, 2, 3, '01', 02, 1112, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1344 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50020, 50020, 'NN', 'NISSAN', 'D21', '2.4L EXTRA CABINA 4X2 GASOLINA STD.', ' 04 OCUP.', 'D21', 210, 09, 'S', 04, 2, 4, '01', 02, 1112, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1345 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50021, 50021, 'NN', 'NISSAN', 'URVAN', 'PANEL 2.5L DIESEL STD.', ' 03 OCUP.', 'URVAN', 210, 20, 'S', 04, 4, 3, '03', 02, 1112, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1346 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50022, 50022, 'NN', 'NISSAN', 'URVAN', 'HI ROOF 2.5L DIESEL FULL STD.', ' 03 OCUP.', 'URVAN', 210, 20, 'S', 04, 3, 3, '02', 02, 1402, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1347 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50023, 50023, 'NN', 'NISSAN', 'D21', '2.4L CAJA ABIERTA 4X2 DISEL STD.', ' 06 OCUP.', 'D21', 210, 09, 'S', 04, 2, 6, '01', 02, 1410, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1348 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50024, 50024, 'NN', 'NISSAN', 'FRONTIER', 'ULTRA LIMITED 2.5L DC 4X2 DIESEL STD.', ' 05 OCUP.', 'FRONTIER', 210, 09, 'S', 04, 4, 5, '02', 02, 1501, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1349 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50025, 50025, 'NN', 'NISSAN', 'FRONTIER', 'SE 2.5L 4X4 DIESEL STD.', ' 05 OCUP.', 'FRONTIER', 210, 09, 'S', 04, 4, 5, '02', 02, 1501, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1350 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50026, 50026, 'NN', 'NISSAN', 'FRONTIER', 'SE 2.5L DOBLE CABINA 4X4 DIESEL STD.', ' 05 OCUP.', 'FRONTIER', 210, 09, 'S', 04, 4, 5, '02', 02, 1501, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1351 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50027, 50027, 'NN', 'NISSAN', 'FRONTIER', 'LIMITED 2.5L DC 4X4 DIESEL STD.', ' 05 OCUP.', 'FRONTIER', 210, 09, 'S', 04, 4, 5, '02', 02, 1501, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1352 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50028, 50028, 'NN', 'NISSAN', 'FRONTIER', 'ULTRA LIMITED 2.5L DC 4X4 DIESEL STD.', ' 05 OCUP.', 'FRONTIER', 210, 09, 'S', 04, 4, 5, '02', 02, 1501, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1353 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50029, 50029, 'NN', 'NISSAN', 'FRONTIER', 'ULTRA LIMITED 2.5L DC 4X4 DIESEL AUT.', ' 05 OCUP.', 'FRONTIER', 210, 09, 'A', 04, 4, 5, '02', 02, 1501, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1354 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50030, 50030, 'NN', 'NISSAN', 'URVAN', 'PANEL DX ROOF 2.5L DIESEL STD.', ' 02 OCUP.', 'URVAN', 210, 20, 'S', 04, 5, 2, '02', 02, 1501, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1355 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50031, 50031, 'NN', 'NISSAN', 'NP300', 'FRONTIER XE 2.5L DIESEL STD.', ' 05 OCUP.', 'NP300', 210, 09, 'S', 04, 4, 5, '02', 02, 1601, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1356 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50032, 50032, 'NN', 'NISSAN', 'SHORT BED', '2.4L 4X2 GASOLINA STD.', ' 02 OCUP.', 'SHORT BED', 210, 09, 'S', 04, 2, 2, '02', 02, 1601, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1357 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50033, 50033, 'NN', 'NISSAN', 'FRONTIER', 'LIMITED 2.5L DC 4X2 DIESEL STD.', ' 05 OCUP.', 'FRONTIER', 210, 09, 'S', 04, 4, 5, '02', 02, 1601, 0001, 0001, '01', 'NISSAN'
);

/* INSERT QUERY NO: 1358 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50301, 50301, 'TY', 'TOYOTA', 'HILUX', '2.5L 4X4 DIESEL STD.', ' 03 OCUP.', 'HILUX', 210, 09, 'S', 04, 2, 3, '02', 02, 1003, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1359 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50302, 50302, 'TY', 'TOYOTA', 'HILUX', '2.5L DOBLE CABINA 4X4 DIESEL STD.', ' 05 OCUP.', 'HILUX', 210, 09, 'S', 04, 4, 5, '02', 02, 1003, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1360 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50303, 50303, 'TY', 'TOYOTA', 'HILUX', '2.5L DOBLE CABINA 4X4 DIESEL AUT.', ' 05 OCUP.', 'HILUX', 210, 09, 'A', 04, 4, 5, '02', 02, 1003, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1361 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50304, 50304, 'TY', 'TOYOTA', 'HILUX', '3.0L DOBLE CABINA 4X4 DIESEL STD.', ' 05 OCUP.', 'HILUX', 210, 09, 'S', 04, 4, 5, '02', 02, 1003, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1362 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50305, 50305, 'TY', 'TOYOTA', 'HILUX', '3.0L DOBLE CABINA 4X4 DIESEL FULL AUT.', ' 05 OCUP.', 'HILUX', 210, 09, 'A', 04, 4, 5, '02', 02, 1003, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1363 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50306, 50306, 'TY', 'TOYOTA', 'HILUX', '2.5L 4X2 DIESEL STD.', ' 03 OCUP.', 'HILUX', 210, 09, 'S', 04, 2, 3, '02', 02, 1003, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1364 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50307, 50307, 'TY', 'TOYOTA', 'HILUX', '2.5L DOBLE CABINA 4X2 DIESEL STD.', ' 05 OCUP.', 'HILUX', 210, 09, 'S', 04, 4, 5, '02', 02, 1003, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1365 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50309, 50309, 'TY', 'TOYOTA', 'HILUX', '2.5L DOBLE CABINA 4X2 DIESEL AUT.', ' 05 OCUP.', 'HILUX', 210, 09, 'A', 04, 4, 5, '02', 02, 1003, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1366 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50311, 50311, 'TY', 'TOYOTA', 'DYNA', '3.0L 4X2 DIESEL STD.', ' 03 OCUP.', 'DYNA', 200, 21, 'S', 04, 2, 3, '03', 02, 1106, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1367 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50312, 50312, 'TY', 'TOYOTA', 'TUNDRA', '5.7L DOBLE CABINA 4X4 GASOLINA AUT.', ' 05 OCUP.', 'TUNDRA', 200, 09, 'A', 08, 4, 5, '03', 02, 1106, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1368 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50313, 50313, 'TY', 'TOYOTA', 'DYNA', '3.6L 4X2 DIESEL STD.', ' 03 OCUP.', 'DYNA', 200, 21, 'S', 04, 2, 3, '03', 02, 1106, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1369 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50314, 50314, 'TY', 'TOYOTA', 'TACOMA', '2.7L 4X2 GASOLINA STD.', ' 04 OCUP.', 'TACOMA', 200, 09, 'S', 04, 2, 4, '03', 02, 1106, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1370 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50315, 50315, 'TY', 'TOYOTA', 'T100', '2.7L 4X2 GASOLINA STD.', ' 03 OCUP.', 'T100', 210, 09, 'S', 04, 2, 3, '02', 02, 1106, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1371 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50316, 50316, 'TY', 'TOYOTA', 'HILUX', '2.8L CABINA SENCILLA 4X2 STD.', ' 04 OCUP.', 'HILUX', 210, 09, 'S', 04, 2, 4, '02', 02, 1112, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1372 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50317, 50317, 'TY', 'TOYOTA', 'TACOMA', '3.4L EXTRA CABINA GASOLINA STD.', ' 04 OCUP.', 'TACOMA', 210, 09, 'S', 04, 2, 4, '02', 02, 1112, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1373 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50318, 50318, 'TY', 'TOYOTA', 'LAND CRUISER', '4.2L 4X4 DIESEL STD.', ' 03 OCUP.', 'LAND CRUISER', 200, 09, 'S', 04, 2, 3, '02', 02, 1112, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1374 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50319, 50319, 'TY', 'TOYOTA', 'TUNDRA', 'DOBLE CABINA 4X2 GASOLINA STD.', ' 05 OCUP.', 'TUNDRA', 210, 09, 'S', 06, 4, 5, '02', 02, 1212, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1375 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50320, 50320, 'TY', 'TOYOTA', 'HIACE', 'PANEL 4X2 STD.', ' 03 OCUP.', 'HIACE', 200, 20, 'S', 04, 3, 3, '03', 02, 1212, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1376 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50321, 50321, 'TY', 'TOYOTA', 'HARD TOP', '4.2L DIESEL 4X2 STD.', ' 02 OCUP.', 'HARD TOP', 210, 09, 'S', 06, 2, 2, '02', 02, 1402, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1377 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50322, 50322, 'TY', 'TOYOTA', 'HARD TOP', '4.2L DIESEL 4X4 STD.', ' 05 OCUP.', 'HARD TOP', 210, 09, 'S', 04, 5, 5, '02', 02, 1402, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1378 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50323, 50323, 'TY', 'TOYOTA', 'HILUX', '2.8L CABINA SENCILLA 4X4 STD.', ' 02 OCUP.', 'HILUX', 210, 09, 'S', 04, 2, 2, '02', 02, 1402, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1379 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50324, 50324, 'TY', 'TOYOTA', 'PRADO', '3.0L DIESEL FULL 4X4 STD.', ' 05 OCUP.', 'PRADO', 210, 09, 'S', 04, 5, 5, '02', 02, 1402, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1380 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50325, 50325, 'TY', 'TOYOTA', 'LAND CRUISER', '4.2L CABINA SENCILLA DIESEL STD.', ' 03 OCUP.', 'LAND CRUISER', 210, 09, 'S', 06, 2, 3, '02', 02, 1501, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1381 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50326, 50326, 'TY', 'TOYOTA', 'LAND CRUISER', '4.2L CS SPORT DIESEL STD.', ' 03 OCUP.', 'LAND CRUISER', 210, 09, 'S', 06, 2, 3, '02', 02, 1501, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1382 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50327, 50327, 'TY', 'TOYOTA', 'LAND CRUISER', '4.2L DOBLE CABINA DIESEL STD.', ' 05 OCUP.', 'LAND CRUISER', 210, 09, 'S', 06, 4, 5, '01', 02, 1501, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1383 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50328, 50328, 'TY', 'TOYOTA', 'HILUX', 'SRV 3.0L DOBLE CABINA 4X4 DIESEL STD.', ' 05 OCUP.', 'HILUX', 210, 09, 'S', 04, 4, 5, '01', 02, 1501, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1384 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50329, 50329, 'TY', 'TOYOTA', 'HILUX', 'SRV LIMITED 3.0L DC 4X4 DIESEL STD.', ' 05 OCUP.', 'HILUX', 210, 09, 'S', 04, 4, 5, '01', 02, 1501, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1385 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50330, 50330, 'TY', 'TOYOTA', 'HILUX', 'SRV 3.0L DOBLE CABINA 4X4 DIESEL AUT.', ' 05 OCUP.', 'HILUX', 210, 09, 'A', 04, 4, 5, '01', 02, 1501, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1386 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50331, 50331, 'TY', 'TOYOTA', 'HILUX', 'SRV LIMITED 3.0L DC 4X4 DIESEL AUT.', ' 05 OCUP.', 'HILUX', 210, 09, 'A', 04, 4, 5, '01', 02, 1501, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1387 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50332, 50332, 'TY', 'TOYOTA', 'HILUX', '2.5L EXTRA CABINA 4X4 DIESEL STD.', ' 04 OCUP.', 'HILUX', 210, 09, 'S', 04, 2, 4, '01', 02, 1501, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1388 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50333, 50333, 'TY', 'TOYOTA', 'TUNDRA', 'TRD 5.7L CREW MAX 4X4 GASOLINA AUT.', ' 05 OCUP.', 'TUNDRA', 210, 09, 'A', 08, 4, 5, '01', 02, 1501, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1389 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50334, 50334, 'TY', 'TOYOTA', 'TUNDRA', 'LIMITED 5.7L CREW MAX GASOLINA AUT.', ' 05 OCUP.', 'TUNDRA', 210, 09, 'A', 08, 4, 5, '01', 02, 1501, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1390 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50335, 50335, 'TY', 'TOYOTA', 'HILUX', 'LOW BED 2.5L CS 4X2 DIESEL STD.', ' 03 OCUP.', 'HILUX', 210, 09, 'S', 04, 2, 3, '02', 02, 1501, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1391 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50336, 50336, 'TY', 'TOYOTA', 'HILUX', 'LOW BED 2.5L DC 4X2 DIESEL STD.', ' 05 OCUP.', 'HILUX', 210, 09, 'S', 04, 4, 5, '02', 02, 1501, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1392 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50337, 50337, 'TY', 'TOYOTA', 'HILUX', 'PAQ ELECTRICO 2.5L CS 4X4 DIESEL STD.', ' 03 OCUP.', 'HILUX', 210, 09, 'S', 04, 2, 3, '02', 02, 1501, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1393 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50338, 50338, 'TY', 'TOYOTA', 'HILUX', 'SRV LIMITED VSC 3.0L DC 4X4 DIESEL AUT.', ' 05 OCUP.', 'HILUX', 210, 09, 'A', 04, 4, 5, '01', 02, 1501, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1394 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50339, 50339, 'TY', 'TOYOTA', 'HIACE', 'PANEL LH20 3.0L DIESEL STD.', ' 03 OCUP.', 'HIACE', 200, 20, 'S', 04, 4, 3, '06', 02, 1501, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1395 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50340, 50340, 'TY', 'TOYOTA', 'HIACE', 'PANEL LH21 2.5L TURBO DIESEL STD.', ' 03 OCUP.', 'HIACE', 200, 20, 'S', 04, 4, 3, '06', 02, 1501, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1396 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50341, 50341, 'TY', 'TOYOTA', 'HIACE', 'PANEL TECHO ALTO 2.5L TURBO DIESEL STD.', ' 03 OCUP.', 'HIACE', 200, 20, 'S', 04, 4, 3, '06', 02, 1501, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1397 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50342, 50342, 'TY', 'TOYOTA', 'TACOMA', '2.7L 4X2 GASOLINA STD.', ' 04 OCUP.', 'TACOMA', 210, 09, 'S', 04, 2, 4, '02', 02, 1106, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1398 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50343, 50343, 'TY', 'TOYOTA', 'TACOMA', '2.7L 4X4 GASOLINA STD.', ' 04 OCUP.', 'TACOMA', 210, 09, 'S', 04, 4, 4, '02', 02, 1601, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1399 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50344, 50344, 'TY', 'TOYOTA', 'TACOMA', '4.0L EXTRA CABINA 4X4 GASOLINA STD.', ' 04 OCUP.', 'TACOMA', 210, 09, 'S', 06, 4, 4, '01', 02, 1601, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1400 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50345, 50345, 'TY', 'TOYOTA', 'TUNDRA', '5.7L DOBLE CABINA 4X4 GASOLINA AUT.', ' 05 OCUP.', 'TUNDRA', 210, 09, 'A', 08, 4, 5, '02', 02, 1601, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1401 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50346, 50346, 'TY', 'TOYOTA', 'DLX', '2.4L 4X2 GASOLINA STD.', ' 04 OCUP.', 'DLX', 210, 09, 'S', 04, 4, 4, '02', 02, 1601, 0001, 0001, '01', 'TOYOTA'
);

/* INSERT QUERY NO: 1402 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50601, 50601, 'HI', 'HYUNDAI', 'HD65', '3.3L CORTO DIESEL STD.', ' 02 OCUP.', 'HD65', 200, 21, 'S', 04, 2, 2, '05', 02, 1003, 0001, 0001, '01', 'HYUNDAI'
);

/* INSERT QUERY NO: 1403 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50603, 50603, 'HI', 'HYUNDAI', 'HD65', '3.3L LARGO DIESEL STD.', ' 02 OCUP.', 'HD65', 200, 21, 'S', 04, 2, 2, '05', 02, 1003, 0001, 0001, '01', 'HYUNDAI'
);

/* INSERT QUERY NO: 1404 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50604, 50604, 'HI', 'HYUNDAI', 'PORTER', '2.5L STD.', ' 02 OCUP.', 'PORTER', 200, 21, 'S', 04, 2, 2, '05', 02, 1106, 0001, 0001, '01', 'HYUNDAI'
);

/* INSERT QUERY NO: 1405 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50605, 50605, 'HI', 'HYUNDAI', 'H100', '2.5L STD.', ' 02 OCUP.', 'H100', 210, 20, 'S', 04, 2, 2, '02', 02, 1106, 0001, 0001, '01', 'HYUNDAI'
);

/* INSERT QUERY NO: 1406 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50606, 50606, 'HI', 'HYUNDAI', 'MIGHTY', '3.6L STD.', ' 03 OCUP.', 'MIGHTY', 200, 21, 'S', 04, 2, 3, '04', 02, 1106, 0001, 0001, '01', 'HYUNDAI'
);

/* INSERT QUERY NO: 1407 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50607, 50607, 'HI', 'HYUNDAI', 'LIBERO', '2.5L STD.', ' 03 OCUP.', 'LIBERO', 200, 09, 'S', 04, 2, 3, '03', 02, 1112, 0001, 0001, '01', 'HYUNDAI'
);

/* INSERT QUERY NO: 1408 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50608, 50608, 'HI', 'HYUNDAI', 'HD65', 'DOBLE CABINA 3.9L DIESEL STD.', ' 05 OCUP.', 'HD65', 200, 21, 'S', 04, 4, 5, '03', 02, 1112, 0001, 0001, '01', 'HYUNDAI'
);

/* INSERT QUERY NO: 1409 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50609, 50609, 'HI', 'HYUNDAI', 'PONY', '1.2L GASOLINA STD.', ' 03 OCUP.', 'PONY', 210, 09, 'S', 04, 2, 3, '01', 02, 1112, 0001, 0001, '01', 'HYUNDAI'
);

/* INSERT QUERY NO: 1410 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50610, 50610, 'HI', 'HYUNDAI', 'H1', 'GLS 2.5L DIESEL STD.', ' 03 OCUP.', 'H1', 210, 20, 'S', 04, 4, 3, '02', 02, 1601, 0001, 0001, '01', 'HYUNDAI'
);

/* INSERT QUERY NO: 1411 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50611, 50611, 'HI', 'HYUNDAI', 'HD50', 'S 3.9L DIESEL STD.', ' 03 OCUP.', 'HD50', 200, 21, 'S', 04, 2, 3, '04', 02, 1601, 0001, 0001, '01', 'HYUNDAI'
);

/* INSERT QUERY NO: 1412 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50612, 50612, 'HI', 'HYUNDAI', 'H350', '2.5L DIESEL STD.', ' 02 OCUP.', 'H350', 210, 20, 'S', 04, 4, 2, '02', 02, 1705, 0001, 0001, '01', 'HYUNDAI'
);

/* INSERT QUERY NO: 1413 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 50901, 50901, 'HA', 'HONDA', 'RIDGELINE', '3.5L V6 GASOLINA AUT.', ' 05 OCUP.', 'RIDGELINE', 200, 09, 'A', 06, 4, 5, '03', 02, 1112, 0001, 0001, '01', 'HONDA'
);

/* INSERT QUERY NO: 1414 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51201, 51201, 'MI', 'MITSUBISHI', 'L200', '2.5L DOBLE CABINA 4X4 DIESEL STD.', ' 05 OCUP.', 'L200', 210, 09, 'S', 04, 4, 5, '02', 02, 1106, 0001, 0001, '01', 'MITSUBISHI'
);

/* INSERT QUERY NO: 1415 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51202, 51202, 'MI', 'MITSUBISHI', 'CANTER', '3.9L 4X2 DIESEL STD.', ' 03 OCUP.', 'CANTER', 200, 09, 'S', 04, 2, 3, '06', 02, 1106, 0001, 0001, '01', 'MITSUBISHI'
);

/* INSERT QUERY NO: 1416 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51203, 51203, 'MI', 'MITSUBISHI', 'MIGHTY MAX', '2.6L DOBLE CAB 4X2 GASOLINA STD.', ' 05 OCUP.', 'MIGHTY MAX', 210, 09, 'S', 04, 4, 5, '02', 02, 1106, 0001, 0001, '01', 'MITSUBISHI'
);

/* INSERT QUERY NO: 1417 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51204, 51204, 'MI', 'MITSUBISHI', 'L300', '2.5L GASOLINA STD.', ' 12 OCUP.', 'L300', 210, 20, 'S', 04, 4, 12, '02', 02, 1106, 0001, 0001, '01', 'MITSUBISHI'
);

/* INSERT QUERY NO: 1418 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51205, 51205, 'MI', 'MITSUBISHI', 'VARICA', '1.2L GASOLINA STD.', ' 02 OCUP.', 'VARICA', 210, 20, 'S', 04, 4, 2, '02', 02, 1106, 0001, 0001, '01', 'MITSUBISHI'
);

/* INSERT QUERY NO: 1419 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51207, 51207, 'MI', 'MITSUBISHI', 'L300', 'VAN 2.5L DIESEL STD.', ' 12 OCUP.', 'L300', 210, 20, 'S', 04, 4, 12, '02', 02, 1212, 0001, 0001, '01', 'MITSUBISHI'
);

/* INSERT QUERY NO: 1420 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51208, 51208, 'MI', 'MITSUBISHI', 'L300', 'PICK UP 2.5L DIESEL STD.', ' 03 OCUP.', 'L300', 200, 20, 'S', 04, 4, 3, '04', 02, 1212, 0001, 0001, '01', 'MITSUBISHI'
);

/* INSERT QUERY NO: 1421 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51209, 51209, 'MI', 'MITSUBISHI', 'L200', '2.5L CABINA SENCILLA 4X2 DIESEL STD.', ' 03 OCUP.', 'L200', 210, 09, 'S', 04, 2, 3, '02', 02, 1601, 0001, 0001, '01', 'MITSUBISHI'
);

/* INSERT QUERY NO: 1422 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51210, 51210, 'MI', 'MITSUBISHI', 'L200', '2.5L CABINA SENCILLA 4X4 DIESEL STD.', ' 03 OCUP.', 'L200', 210, 09, 'S', 04, 2, 3, '02', 02, 1601, 0001, 0001, '01', 'MITSUBISHI'
);

/* INSERT QUERY NO: 1423 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51211, 51211, 'MI', 'MITSUBISHI', 'FUSO', '7.5L 4X2 DIESEL STD.', ' 03 OCUP.', 'FUSO', 200, 21, 'S', 06, 2, 3, '06', 02, 1601, 0001, 0001, '01', 'MITSUBISHI'
);

/* INSERT QUERY NO: 1424 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51501, 51501, 'SI', 'SUZUKI', 'APV', 'PANEL 1.6L GASOLINA STD.', ' 02 OCUP.', 'APV', 210, 20, 'S', 04, 5, 2, '02', 02, 1003, 0001, 0001, '01', 'SUZUKI'
);

/* INSERT QUERY NO: 1425 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51502, 51502, 'SI', 'SUZUKI', 'APV', 'PANEL 1.6L GASOLINA AUT.', ' 02 OCUP.', 'APV', 210, 20, 'A', 04, 5, 2, '02', 02, 1003, 0001, 0001, '01', 'SUZUKI'
);

/* INSERT QUERY NO: 1426 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51503, 51503, 'SI', 'SUZUKI', 'APV', '1.6L 4X2 GASOLINA STD.', ' 02 OCUP.', 'APV', 210, 09, 'S', 04, 2, 2, '02', 02, 1003, 0001, 0001, '01', 'SUZUKI'
);

/* INSERT QUERY NO: 1427 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51504, 51504, 'SI', 'SUZUKI', 'APV', '1.6L 4X2 GASOLINA AUT.', ' 02 OCUP.', 'APV', 210, 09, 'A', 04, 2, 2, '02', 02, 1003, 0001, 0001, '01', 'SUZUKI'
);

/* INSERT QUERY NO: 1428 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51505, 51505, 'SI', 'SUZUKI', 'CARRY', 'PANEL 1.3L GASOLINA STD.', ' 02 OCUP.', 'CARRY', 210, 20, 'S', 04, 2, 2, '02', 02, 1106, 0001, 0001, '01', 'SUZUKI'
);

/* INSERT QUERY NO: 1429 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51801, 51801, 'MA', 'MAZDA', 'BT-50', '2.2L DOBLE CABINA 4X4 DIESEL STD.', ' 05 OCUP.', 'BT-50', 210, 09, 'S', 04, 4, 5, '01', 02, 1106, 0001, 0001, '01', 'MAZDA'
);

/* INSERT QUERY NO: 1430 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51802, 51802, 'MA', 'MAZDA', 'BT-50', '3.2L DOBLE CABINA 4X4 DIESEL STD.', ' 05 OCUP.', 'BT-50', 210, 09, 'S', 04, 4, 5, '01', 02, 1106, 0001, 0001, '01', 'MAZDA'
);

/* INSERT QUERY NO: 1431 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51803, 51803, 'MA', 'MAZDA', 'E2200', '4X2 STD.', ' 03 OCUP.', 'E2200', 210, 20, 'S', 04, 2, 3, '02', 02, 1106, 0001, 0001, '01', 'MAZDA'
);

/* INSERT QUERY NO: 1432 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51804, 51804, 'MA', 'MAZDA', 'B2500', '2.5L DOBLE CABINA 4X4 DIESEL STD.', ' 05 OCUP.', 'B2500', 210, 09, 'S', 04, 4, 5, '02', 02, 1106, 0001, 0001, '01', 'MAZDA'
);

/* INSERT QUERY NO: 1433 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51805, 51805, 'MA', 'MAZDA', 'T4000', '4.0L GASOLINA STD.', ' 03 OCUP.', 'T4000', 200, 21, 'S', 04, 2, 3, '04', 02, 1106, 0001, 0001, '01', 'MAZDA'
);

/* INSERT QUERY NO: 1434 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51806, 51806, 'MA', 'MAZDA', 'B2500', '2.5L 4X4 DIESEL STD.', ' 03 OCUP.', 'B2500', 210, 09, 'S', 04, 2, 3, '02', 02, 1106, 0001, 0001, '01', 'MAZDA'
);

/* INSERT QUERY NO: 1435 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51807, 51807, 'MA', 'MAZDA', 'B2900', '1.9L DOBLE CABINA 4X4 DIESEL STD.', ' 05 OCUP.', 'B2900', 210, 09, 'S', 04, 4, 5, '02', 02, 1106, 0001, 0001, '01', 'MAZDA'
);

/* INSERT QUERY NO: 1436 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51808, 51808, 'MA', 'MAZDA', 'B2200', '2.2L 4X2 DIESEL STD.', ' 02 OCUP.', 'B2200', 210, 09, 'S', 04, 2, 2, '02', 02, 1112, 0001, 0001, '01', 'MAZDA'
);

/* INSERT QUERY NO: 1437 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51809, 51809, 'MA', 'MAZDA', 'BT-50', '2.2L EXTRA CAB 4X4 DIESEL AUT.', ' 05 OCUP.', 'BT-50', 210, 09, 'A', 04, 4, 5, '01', 02, 1106, 0001, 0001, '01', 'MAZDA'
);

/* INSERT QUERY NO: 1438 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51810, 51810, 'MA', 'MAZDA', 'B2600', 'LE-5 2.6L 4X2 GASOLINA STD.', ' 04 OCUP.', 'B2600', 210, 09, 'S', 04, 4, 4, '02', 02, 1601, 0001, 0001, '01', 'MAZDA'
);

/* INSERT QUERY NO: 1439 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 51811, 51811, 'MA', 'MAZDA', 'PUP', '2.0L 4X2 GASOLINA STD.', ' 02 OCUP.', 'PUP', 210, 09, 'S', 04, 2, 2, '02', 02, 1705, 0001, 0001, '01', 'MAZDA'
);

/* INSERT QUERY NO: 1440 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52101, 52101, 'FD', 'FORD', 'F-250', '7.3L CREW CAB 4X4 GASOLINA AUT.', ' 05 OCUP.', 'F-250', 210, 09, 'A', 08, 4, 5, '02', 02, 1106, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1441 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52102, 52102, 'FD', 'FORD', 'F-350', '7.3L 4X2 DIESEL STD.', ' 03 OCUP.', 'F-350', 200, 09, 'S', 06, 2, 3, '05', 02, 1106, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1442 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52103, 52103, 'FD', 'FORD', 'RANGER', 'WILDTRAK 3.2L 4X4 DIESEL STD.', ' 05 OCUP.', 'RANGER', 210, 09, 'S', 04, 4, 5, '02', 02, 1106, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1443 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52104, 52104, 'FD', 'FORD', 'F-450', '7.3L 4X2 DIESEL STD.', ' 03 OCUP.', 'F-450', 200, 09, 'S', 08, 4, 3, '03', 02, 1106, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1444 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52105, 52105, 'FD', 'FORD', 'F-350', '6.0L CREW CAB DIESEL AUT.', ' 06 OCUP.', 'F-350', 200, 09, 'A', 08, 4, 6, '03', 02, 1106, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1445 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52106, 52106, 'FD', 'FORD', 'F-150', '5.4L 4X4 AUT.', ' 05 OCUP.', 'F-150', 200, 09, 'A', 06, 2, 5, '05', 02, 1106, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1446 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52107, 52107, 'FD', 'FORD', 'E-250', 'VAN 4.9L V6 STD.', ' 02 OCUP.', 'E-250', 210, 20, 'S', 06, 5, 2, '02', 02, 1112, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1447 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52108, 52108, 'FD', 'FORD', 'APACHE', '4.8L V8 STD.', ' 03 OCUP.', 'APACHE', 210, 09, 'S', 08, 2, 3, '02', 02, 1112, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1448 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52109, 52109, 'FD', 'FORD', 'ECOLINE', 'VAN 5.4L V8 GASOLINA STD.', ' 02 OCUP.', 'ECOLINE', 210, 20, 'S', 08, 4, 2, '02', 02, 1112, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1449 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52110, 52110, 'FD', 'FORD', 'RANGER', 'XLT 3.2L 4X4 DIESEL STD.', ' 05 OCUP.', 'RANGER', 210, 09, 'S', 04, 4, 5, '02', 02, 1112, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1450 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52111, 52111, 'FD', 'FORD', 'ECONOLINE', 'VAN 5.4L V8 GASOLINA STD.', ' 03 OCUP.', 'ECONOLINE', 210, 20, 'S', 08, 4, 3, '02', 02, 1112, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1451 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52112, 52112, 'FD', 'FORD', 'CARGO VAN', '5.4L V8 GASOLINA STD.', ' 03 OCUP.', 'CARGO VAN', 210, 20, 'S', 08, 4, 3, '02', 02, 1112, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1452 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52113, 52113, 'FD', 'FORD', 'ECONOLINE', 'VAN 7.3L V8 DIESEL STD.', ' 04 OCUP.', 'ECONOLINE', 210, 20, 'S', 08, 3, 4, '02', 02, 1112, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1453 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52114, 52114, 'FD', 'FORD', 'F-150', 'RAPTOR 6.2L V8 4X4 GASOLINA FULL AUT.', ' 05 OCUP.', 'F-150', 210, 09, 'A', 08, 4, 5, '02', 02, 1112, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1454 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52115, 52115, 'FD', 'FORD', 'F-750 ', 'XLT 6.7L V6 GASOLINA STD.', ' 03 OCUP.', 'F-750', 200, 09, 'S', 06, 2, 3, '06', 02, 1212, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1455 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52116, 52116, 'FD', 'FORD', 'F-650 ', 'XL 6.7L V8 GASOLINA STD.', ' 02 OCUP.', 'F-650', 200, 09, 'S', 08, 2, 2, '06', 02, 1212, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1456 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52117, 52117, 'FD', 'FORD', 'COURIER ', '4x2 1.6L GASOLINA STD.', ' 02 OCUP.', 'COURIER', 210, 09, 'S', 04, 2, 2, '02', 02, 1402, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1457 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52118, 52118, 'FD', 'FORD', 'SUPER DUTY', 'CARGA LIVIANA 4x2 7.3L DISEL STD.', ' 03 OCUP.', 'SUPER DUTY', 210, 09, 'S', 04, 2, 3, '02', 02, 1402, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1458 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52119, 52119, 'FD', 'FORD', 'RANGER', 'CABINA SENCILLA 2.8L 4X4 GASOLINA STD.', ' 02 OCUP.', 'RANGER', 210, 09, 'S', 04, 4, 2, '02', 02, 1501, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1459 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52120, 52120, 'FD', 'FORD', 'RANGER', '3.0L CARGA LIVIANA 4X2 GASOLINA AUT.', ' 03 OCUP.', 'RANGER', 210, 09, 'A', 06, 2, 3, '02', 02, 1601, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1460 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52121, 52121, 'FD', 'FORD', 'E-450', 'VAN 6.0L V8 DIESEL STD.', ' 02 OCUP.', 'E-450', 200, 20, 'S', 08, 5, 2, '06', 02, 1601, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1461 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52122, 52122, 'FD', 'FORD', 'F-250', 'SUPER DUTY 6.4L 4X4 DIESEL STD.', ' 05 OCUP.', 'F-250', 210, 09, 'S', 08, 4, 5, '02', 02, 1601, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1462 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52123, 52123, 'FD', 'FORD', 'F-150', '3.5L 4X4 TURBO DIESEL AUT.', ' 05 OCUP.', 'F-150', 200, 09, 'A', 06, 4, 5, '04', 02, 1601, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1463 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52124, 52124, 'FD', 'FORD', 'E-150', '4.2L GASOLINA AUT.', ' 02 OCUP.', 'E-150', 200, 20, 'A', 06, 4, 2, '06', 02, 1705, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1464 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52125, 52125, 'FD', 'FORD', 'E-350', '5.4L GASOLINA AUT.', ' 04 OCUP.', 'E-350', 200, 20, 'A', 08, 4, 4, '06', 02, 1705, 0001, 0001, '01', 'FORD'
);

/* INSERT QUERY NO: 1465 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52301, 52301, 'CT', 'CHEVROLET', 'S10', '2.2L 4X2 GASOLINA STD.', ' 03 OCUP.', 'S10', 210, 09, 'S', 04, 2, 3, '02', 02, 1106, 0001, 0001, '01', 'CHEVROLET'
);

/* INSERT QUERY NO: 1466 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52302, 52302, 'CT', 'CHEVROLET', 'CMV', 'VAN 1.0L 4X2 GASOLINA STD.', ' 02 OCUP.', 'CMV', 210, 20, 'S', 03, 2, 2, '02', 02, 1106, 0001, 0001, '01', 'CHEVROLET'
);

/* INSERT QUERY NO: 1467 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52303, 52303, 'CT', 'CHEVROLET', 'SILVERADO', '5.3L 4X4 GASOLINA STD.', ' 03 OCUP.', 'SILVERADO', 200, 09, 'S', 08, 2, 3, '04', 02, 1106, 0001, 0001, '01', 'CHEVROLET'
);

/* INSERT QUERY NO: 1468 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52304, 52304, 'CT', 'CHEVROLET', 'AVALANCHE', '5.3L V8 4X2 GASOLINA AUT.', ' 05 OCUP.', 'AVALANCHE', 210, 09, 'A', 08, 4, 5, '01', 02, 1106, 0001, 0001, '01', 'CHEVROLET'
);

/* INSERT QUERY NO: 1469 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52305, 52305, 'CT', 'CHEVROLET', 'C-30', '5.7L V8 4X2 GASOLINA STD.', ' 03 OCUP.', 'C-30', 200, 09, 'S', 08, 2, 3, '06', 02, 1106, 0001, 0001, '01', 'CHEVROLET'
);

/* INSERT QUERY NO: 1470 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52306, 52306, 'CT', 'CHEVROLET', 'C-10', '4.3L V6 GASOLINA STD.', ' 03 OCUP.', 'C-10', 200, 09, 'S', 06, 2, 3, '04', 02, 1106, 0001, 0001, '01', 'CHEVROLET'
);

/* INSERT QUERY NO: 1471 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52307, 52307, 'CT', 'CHEVROLET', 'CARGO VAN', '5.7L V8 GASOLINA STD.', ' 03 OCUP.', 'CARGO VAN', 210, 20, 'S', 08, 5, 3, '02', 02, 1112, 0001, 0001, '01', 'CHEVROLET'
);

/* INSERT QUERY NO: 1472 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52308, 52308, 'CT', 'CHEVROLET', 'CHEVY', '1.6L GASOLINA FULL STD.', ' 02 OCUP.', 'CHEVY', 210, 09, 'S', 04, 2, 2, '01', 02, 1112, 0001, 0001, '01', 'CHEVROLET'
);

/* INSERT QUERY NO: 1473 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52309, 52309, 'CT', 'CHEVROLET', 'CHEYENNE', '5.3L V8 DOBLE CAB GASOLINA FULL AUT.', ' 05 OCUP.', 'CHEYENNE', 210, 09, 'A', 08, 4, 5, '02', 02, 1112, 0001, 0001, '01', 'CHEVROLET'
);

/* INSERT QUERY NO: 1474 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52310, 52310, 'CT', 'CHEVROLET', 'W4S042', '4.8L V6 DIESEL STD.', ' 03 OCUP.', 'W4S042', 200, 09, 'S', 06, 2, 3, '03', 02, 1112, 0001, 0001, '01', 'CHEVROLET'
);

/* INSERT QUERY NO: 1475 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52311, 52311, 'CT', 'CHEVROLET', 'CMP', '1.0L GASOLINA STD.', ' 02 OCUP.', 'CMP', 210, 09, 'S', 03, 2, 2, '01', 02, 1112, 0001, 0001, '01', 'CHEVROLET'
);

/* INSERT QUERY NO: 1476 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52312, 52312, 'CT', 'CHEVROLET', 'EXPRESS VAN', '5.7L V8 GASOLINA STD.', ' 02 OCUP.', 'EXPRESS VAN', 210, 20, 'S', 08, 4, 2, '02', 02, 1112, 0001, 0001, '01', 'CHEVROLET'
);

/* INSERT QUERY NO: 1477 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52313, 52313, 'CT', 'CHEVROLET', 'COLORADO', 'LT PICKUP 3.7L 4X4 GASOLINA STD.', ' 05 OCUP.', 'COLORADO', 210, 09, 'S', 05, 4, 5, '02', 02, 1212, 0001, 0001, '01', 'CHEVROLET'
);

/* INSERT QUERY NO: 1478 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52314, 52314, 'CT', 'CHEVROLET', 'AVALANCHE', 'LT DOBLE CABINA 5.3L GASOLINA AUT.', ' 05 OCUP.', 'AVALANCHE', 210, 09, 'A', 08, 4, 5, '02', 02, 1212, 0001, 0001, '01', 'CHEVROLET'
);

/* INSERT QUERY NO: 1479 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52315, 52315, 'CT', 'CHEVROLET', 'VANETTE', '7L DISEL STD.', ' 02 OCUP.', 'AVALANCHE', 200, 11, 'S', 08, 2, 2, '06', 02, 1212, 0001, 0001, '01', 'CHEVROLET'
);

/* INSERT QUERY NO: 1480 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52316, 52316, 'CT', 'CHEVROLET', 'N300', 'CARGA LIVIANA 1.2L GASOLINA STD.', ' 05 OCUP.', 'N300', 210, 11, 'S', 04, 2, 5, '02', 02, 1402, 0001, 0001, '01', 'CHEVROLET'
);

/* INSERT QUERY NO: 1481 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52317, 52317, 'CT', 'CHEVROLET', 'COLORADO', 'LT 2.5L 4X4 DIESEL STD.', ' 05 OCUP.', 'COLORADO', 210, 11, 'S', 04, 2, 5, '02', 02, 1402, 0001, 0001, '01', 'CHEVROLET'
);

/* INSERT QUERY NO: 1482 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52318, 52318, 'CT', 'CHEVROLET', 'SILVERADO', 'LTZ 5.3L GASOLINA STD.', ' 05 OCUP.', 'SILVERADO', 210, 09, 'S', 08, 4, 5, '01', 02, 1501, 0001, 0001, '01', 'CHEVROLET'
);

/* INSERT QUERY NO: 1483 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52319, 52319, 'CT', 'CHEVROLET', 'AVALANCHE', '5.3L V8 4X4 GASOLINA STD.', ' 05 OCUP.', 'AVALANCHE', 210, 09, 'S', 08, 4, 5, '01', 02, 1501, 0001, 0001, '01', 'CHEVROLET'
);

/* INSERT QUERY NO: 1484 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52320, 52320, 'CT', 'CHEVROLET', 'W4500', '4.8L 4X2 DIESEL STD.', ' 03 OCUP.', 'W4500', 200, 21, 'S', 04, 2, 3, '06', 02, 1601, 0001, 0001, '01', 'CHEVROLET'
);

/* INSERT QUERY NO: 1485 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52501, 52501, 'KA', 'KIA', 'K2700', '2.7L 4X2 DIESEL STD.', ' 03 OCUP.', 'K2700', 200, 21, 'S', 04, 2, 3, '03', 02, 1106, 0001, 0001, '01', 'KIA'
);

/* INSERT QUERY NO: 1486 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52502, 52502, 'KA', 'KIA', 'BONGO III', '3.0L DIESEL STD.', ' 03 OCUP.', 'BONGO III', 200, 21, 'S', 06, 2, 3, '03', 02, 1106, 0001, 0001, '01', 'KIA'
);

/* INSERT QUERY NO: 1487 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52503, 52503, 'KA', 'KIA', 'K3000S', '3.0L DIESEL STD.', ' 03 OCUP.', 'K3000S', 200, 21, 'S', 04, 2, 3, '04', 02, 1106, 0001, 0001, '01', 'KIA'
);

/* INSERT QUERY NO: 1488 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52504, 52504, 'KA', 'KIA', 'TOWNER', '1.0L STD.', ' 02 OCUP.', 'TOWNER', 210, 20, 'S', 03, 3, 2, '01', 02, 1106, 0001, 0001, '01', 'KIA'
);

/* INSERT QUERY NO: 1489 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52505, 52505, 'KA', 'KIA', 'K3600 II', '3.6L V8 DIESEL STD.', ' 03 OCUP.', 'K3600 II', 200, 21, 'S', 08, 2, 3, '04', 02, 1106, 0001, 0001, '01', 'KIA'
);

/* INSERT QUERY NO: 1490 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52506, 52506, 'KA', 'KIA', 'BONGO', '2.7L DOBLE CABINA DIESEL STD.', ' 05 OCUP.', 'BONGO', 200, 09, 'S', 04, 4, 5, '03', 02, 1112, 0001, 0001, '01', 'KIA'
);

/* INSERT QUERY NO: 1491 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52507, 52507, 'KA', 'KIA', 'BONGO III', '2.9L DIESEL STD.', ' 15 OCUP.', 'BONGO III', 210, 11, 'S', 04, 5, 15, '02', 02, 1112, 0001, 0001, '01', 'KIA'
);

/* INSERT QUERY NO: 1492 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52508, 52508, 'KA', 'KIA', 'CERES', 'CERES 2.4L C/A AC D/H V/P DIESEL STD.', ' 03 OCUP.', 'CERES', 210, 21, 'S', 04, 2, 3, '02', 02, 1112, 0001, 0001, '01', 'KIA'
);

/* INSERT QUERY NO: 1493 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52509, 52509, 'KA', 'KIA', 'FRONTIER', '2.7L C/A AC V/P DIESEL STD.', ' 05 OCUP.', 'FRONTIER', 210, 09, 'S', 04, 4, 5, '02', 02, 1112, 0001, 0001, '01', 'KIA'
);

/* INSERT QUERY NO: 1494 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52510, 52510, 'KA', 'KIA', 'TRADE', '3.6L DH DIESEL STD.', ' 03 OCUP.', 'TRADE', 200, 21, 'S', 04, 2, 3, '03', 02, 1212, 0001, 0001, '01', 'KIA'
);

/* INSERT QUERY NO: 1495 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52511, 52511, 'KA', 'KIA', 'K2700', '2.7L DOBLE CABINA 4X4 DIESEL STD.', ' 06 OCUP.', 'K2700', 200, 21, 'S', 04, 4, 6, '03', 02, 1212, 0001, 0001, '01', 'KIA'
);

/* INSERT QUERY NO: 1496 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52512, 52512, 'KA', 'KIA', 'BONGO III', '2.9L 4X2 DIESEL STD.', ' 15 OCUP.', 'BONGO III', 210, 11, 'S', 04, 4, 15, '02', 02, 1402, 0001, 0001, '01', 'KIA'
);

/* INSERT QUERY NO: 1497 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 52513, 52513, 'KA', 'KIA', 'K2500', '2.5L DOBLE CABINA DIESEL STD.', ' 05 OCUP.', 'K2500', 210, 21, 'S', 04, 4, 5, '02', 02, 1601, 0001, 0001, '01', 'KIA'
);

/* INSERT QUERY NO: 1498 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 53101, 53101, 'VW', 'VOLKSWAGEN', 'AMAROK', '2.0L 4X4 DIESEL STD.', ' 05 OCUP.', 'AMAROK', 210, 09, 'S', 04, 4, 5, '01', 02, 1003, 0001, 0001, '01', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 1499 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 53102, 53102, 'VW', 'VOLKSWAGEN', 'CADDY', 'VAN 1.9L GASOLINA STD.', ' 02 OCUP.', 'CADDY', 210, 20, 'S', 04, 3, 2, '01', 02, 1112, 0001, 0001, '01', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 1500 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 53103, 53103, 'VW', 'VOLKSWAGEN', 'SAVEIRO', '1.6L GASOLINA STD.', ' 02 OCUP.', 'SAVEIRO', 210, 20, 'S', 04, 2, 2, '01', 02, 1212, 0001, 0001, '01', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 1501 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 53104, 53104, 'VW', 'VOLKSWAGEN', 'AMAROK', '3.0L DIESEL AUT.', ' 05 OCUP.', 'AMAROK', 210, 09, 'A', 06, 4, 5, '01', 02, 1705, 0001, 0001, '01', 'VOLKSWAGEN'
);

/* INSERT QUERY NO: 1502 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 53301, 53301, 'SU', 'SUBARU', 'E10', 'PANEL 1.0L GASOLINA STD.', ' 02 OCUP.', 'E10', 210, 20, 'S', 03, 2, 2, '01', 02, 1112, 0001, 0001, '01', 'SUBARU'
);

/* INSERT QUERY NO: 1503 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 53302, 53302, 'SU', 'SUBARU', '1800', '1.8L GASOLINA STD.', ' 02 OCUP.', '1800', 210, 09, 'S', 04, 2, 2, '01', 02, 1112, 0001, 0001, '01', 'SUBARU'
);

/* INSERT QUERY NO: 1504 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 53501, 53501, 'DE', 'DODGE', 'DAKOTA', '3.9L V6 EXTRA CABINA GASOLINA STD.', ' 04 OCUP.', 'DAKOTA', 210, 09, 'S', 06, 3, 4, '02', 02, 1112, 0001, 0001, '01', 'DODGE'
);

/* INSERT QUERY NO: 1505 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 53502, 53502, 'DE', 'DODGE', 'DAKOTA', '3.9L V6 DOBLE CABINA GASOLINA AUT.', ' 04 OCUP.', 'DAKOTA', 210, 09, 'A', 06, 4, 4, '02', 02, 1112, 0001, 0001, '01', 'DODGE'
);

/* INSERT QUERY NO: 1506 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 53503, 53503, 'DE', 'DODGE', 'QUAD CAB', '4.7L V8 GASOLINA AUT.', ' 05 OCUP.', 'QUAD CAB', 210, 09, 'A', 08, 4, 5, '02', 02, 1112, 0001, 0001, '01', 'DODGE'
);

/* INSERT QUERY NO: 1507 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 53504, 53504, 'DE', 'DODGE', 'RAM 50', 'L EXTRA CAB DIESEL STD.', ' 04 OCUP.', 'RAM 50', 210, 09, 'S', 04, 4, 4, '02', 02, 1112, 0001, 0001, '01', 'DODGE'
);

/* INSERT QUERY NO: 1508 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 53505, 53505, 'DE', 'DODGE', 'SPRINTER', '2500 HC PANEL 2.7L V4 4X2 DIESEL STD.', ' 02 OCUP.', 'SPRINTER', 210, 11, 'S', 04, 3, 2, '02', 02, 1212, 0001, 0001, '01', 'DODGE'
);

/* INSERT QUERY NO: 1509 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 53506, 53506, 'DE', 'DODGE', 'RAM', '5.9L V4 DIESEL STD.', ' 06 OCUP.', 'RAM', 210, 09, 'S', 04, 2, 6, '02', 02, 1412, 0001, 0001, '01', 'DODGE'
);

/* INSERT QUERY NO: 1510 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 53701, 53701, 'DO', 'DAEWOO', 'LABO', 'PANEL 2.0L DIESEL STD.', ' 02 OCUP.', 'LABO', 210, 09, 'S', 04, 2, 2, '01', 02, 1106, 0001, 0001, '01', 'DAEWOO'
);

/* INSERT QUERY NO: 1511 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 53702, 53702, 'DO', 'DAEWOO', 'DAMAS', 'PANEL 1.0L GASOLINA STD.', ' 02 OCUP.', 'DAMAS', 210, 20, 'S', 03, 3, 2, '01', 02, 1112, 0001, 0001, '01', 'DAEWOO'
);

/* INSERT QUERY NO: 1512 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 53901, 53901, 'PT', 'PEUGEOT', 'PARTNER', '1.9L DIESEL STD.', ' 02 OCUP.', 'PARTNER', 210, 09, 'S', 04, 2, 2, '01', 02, 1106, 0001, 0001, '01', 'DAEWOO'
);

/* INSERT QUERY NO: 1513 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 53904, 53904, 'PT', 'PEUGEOT', 'PARTNER', 'ER TEPPE HDI A/C 1.6L DIESEL STD.', ' 09 OCUP.', 'PARTNER', 210, 20, 'S', 04, 4, 9, '01', 02, 1402, 0001, 0001, '01', 'PEUGEOT'
);

/* INSERT QUERY NO: 1514 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 53905, 53905, 'PT', 'PEUGEOT', 'BOXER', '2.2L DIESEL STD.', ' 03 OCUP.', 'BOXER', 210, 20, 'S', 04, 4, 3, '02', 02, 1601, 0001, 0001, '01', 'PEUGEOT'
);

/* INSERT QUERY NO: 1515 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54301, 54301, 'IS', 'ISUZU', 'D-MAX', '3.0L DOBLE CABINA 4X4 DIESEL FULL STD.', ' 05 OCUP.', 'D-MAX', 210, 09, 'S', 04, 4, 5, '01', 02, 1003, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1516 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54302, 54302, 'IS', 'ISUZU', 'D-MAX', 'LS 2.5L DOBLE CABINA 4X4 DIESEL STD.', ' 05 OCUP.', 'D-MAX', 210, 09, 'S', 04, 4, 5, '01', 02, 1003, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1517 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54303, 54303, 'IS', 'ISUZU', 'D-MAX', '2.5L 4X2 DIESEL STD.', ' 02 OCUP.', 'D-MAX', 210, 09, 'S', 04, 2, 2, '01', 02, 1003, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1518 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54304, 54304, 'IS', 'ISUZU', 'D-MAX', '2.5L EXTRA CAB 4X4 DIESEL STD.', ' 05 OCUP.', 'D-MAX', 210, 09, 'S', 04, 2, 5, '01', 02, 1003, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1519 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54305, 54305, 'IS', 'ISUZU', 'D-MAX', '2.5L DOBLE CABINA 4X2 DIESEL STD.', ' 05 OCUP.', 'D-MAX', 210, 09, 'S', 04, 4, 5, '01', 02, 1003, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1520 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54306, 54306, 'IS', 'ISUZU', 'D-MAX', '2.5L 4X4 DIESEL STD.', ' 02 OCUP.', 'D-MAX', 210, 09, 'S', 04, 2, 2, '01', 02, 1003, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1521 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54307, 54307, 'IS', 'ISUZU', 'D-MAX', '2.5L DOBLE CABINA 4X4 DIESEL STD.', ' 05 OCUP.', 'D-MAX', 210, 09, 'S', 04, 4, 5, '01', 02, 1003, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1522 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54308, 54308, 'IS', 'ISUZU', 'SERIE N', '2.8L CORTO STD.', ' 03 OCUP.', 'NMR', 200, 21, 'S', 04, 2, 3, '05', 02, 1003, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1523 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54309, 54309, 'IS', 'ISUZU', 'SERIE N', '2.8L VAGONETA STD.', ' 03 OCUP.', 'NMR', 200, 21, 'S', 04, 2, 3, '04', 02, 1003, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1524 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54310, 54310, 'IS', 'ISUZU', 'SERIE N', '2.8L LARGO STD.', ' 03 OCUP.', 'NMR', 200, 21, 'S', 04, 2, 3, '05', 02, 1003, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1525 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54311, 54311, 'IS', 'ISUZU', 'SERIE N', '3.0L DOBLE CABINA STD.', ' 05 OCUP.', 'NMR', 200, 21, 'S', 04, 2, 5, '05', 02, 1003, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1526 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54312, 54312, 'IS', 'ISUZU', 'SERIE N', '4.6L STD.', ' 03 OCUP.', 'NPR', 200, 21, 'S', 04, 2, 3, '04', 02, 1003, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1527 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54313, 54313, 'IS', 'ISUZU', 'SERIE N', '4.6L STD.', ' 03 OCUP.', 'NQR', 200, 21, 'S', 04, 2, 3, '04', 02, 1003, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1528 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54316, 54316, 'IS', 'ISUZU', 'SERIE N', '2.8L STD.', ' 03 OCUP.', 'NRL', 200, 21, 'S', 04, 2, 3, '04', 02, 1003, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1529 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54317, 54317, 'IS', 'ISUZU', 'KB 2300', '2.3L 4X2 GASOLINA STD.', ' 04 OCUP.', 'KB2300', 200, 09, 'S', 04, 2, 4, '04', 02, 1106, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1530 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54318, 54318, 'IS', 'ISUZU', 'SERIE N', '4.6L DIESEL STD.', ' 03 OCUP.', 'NQR', 200, 21, 'S', 04, 2, 3, '04', 02, 1106, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1531 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54319, 54319, 'IS', 'ISUZU', 'SERIE N', '2.8L DIESEL STD.', ' 03 OCUP.', 'NKR', 200, 21, 'S', 04, 2, 3, '06', 02, 1106, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1532 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54320, 54320, 'IS', 'ISUZU', 'KB', '2.3L 4X2 GASOLINA STD.', ' 03 OCUP.', 'KB', 210, 09, 'S', 04, 2, 3, '02', 02, 1106, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1533 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54321, 54321, 'IS', 'ISUZU', 'SERIE N', '4.8L 4X2 DIESEL STD.', ' 03 OCUP.', 'NPR', 200, 21, 'S', 04, 2, 3, '04', 02, 1106, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1534 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54322, 54322, 'IS', 'ISUZU', 'SERIE N', '2.8L DIESEL STD.', ' 03 OCUP.', 'NLR', 210, 21, 'S', 04, 2, 3, '02', 02, 1106, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1535 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54323, 54323, 'IS', 'ISUZU', 'HOMBRE', '2.2L 4X4 GASOLINA STD.', ' 02 OCUP.', 'HOMBRE', 210, 09, 'S', 04, 2, 2, '02', 02, 1112, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1536 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54324, 54324, 'IS', 'ISUZU', 'KB 2500', '2.5L 4X2 GASOLINA STD.', ' 03 OCUP.', 'KB2500', 210, 09, 'S', 04, 2, 3, '02', 02, 1112, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1537 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54325, 54325, 'IS', 'ISUZU', 'KB', '2.8L SPACE CAB DIESEL STD.', ' 04 OCUP.', 'KB', 210, 09, 'S', 04, 2, 4, '02', 02, 1112, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1538 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54326, 54326, 'IS', 'ISUZU', 'SERIE Q', '2.8L 4X2 CARGA LIVIANA DIESEL STD.', ' 03 OCUP.', 'QKR55L', 210, 21, 'S', 04, 2, 3, '02', 02, 1402, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1539 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54327, 54327, 'IS', 'ISUZU', 'SHORT BED', 'S 2.3L CAJA ABIERTA 4X2 GASOLINA AUT.', ' 03 OCUP.', 'SHORT BED', 210, 09, 'A', 04, 4, 3, '02', 02, 1501, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1540 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54328, 54328, 'IS', 'ISUZU', 'SERIE N', '4.6L 4X4 DIESEL STD.', ' 03 OCUP.', 'NPS', 200, 21, 'S', 04, 2, 3, '06', 02, 1601, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1541 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54329, 54329, 'IS', 'ISUZU', 'SERIE N', '4.6L 4X2 DIESEL STD.', ' 03 OCUP.', 'NPS', 200, 21, 'S', 04, 2, 3, '06', 02, 1601, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1542 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54330, 54330, 'IS', 'ISUZU', 'KB', '2.8L DOBLE CABINA 4X4 DIESEL STD.', ' 05 OCUP.', 'KB', 210, 09, 'S', 04, 4, 5, '02', 02, 1705, 0001, 0001, '01', 'ISUZU'
);

/* INSERT QUERY NO: 1543 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54501, 54501, 'MZ', 'MBENZ', 'SPRINTER', '2.2L DIESEL STD.', ' 16 OCUP.', 'SPRINTER', 210, 11, 'S', 04, 3, 16, '02', 02, 1106, 0001, 0001, '01', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 1544 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54502, 54502, 'MZ', 'MBENZ', 'MB 140', '2.9L DIESEL STD.', ' 16 OCUP.', 'MB 140', 210, 11, 'S', 04, 4, 16, '02', 02, 1112, 0001, 0001, '01', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 1545 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54503, 54503, 'MZ', 'MBENZ', 'SPRINTER', '2.2L DIESEL STD.', ' 02 OCUP.', 'SPRINTER', 210, 11, 'S', 04, 2, 2, '02', 02, 1112, 0001, 0001, '01', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 1546 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54504, 54504, 'MZ', 'MBENZ', '708E', '7.2L V6 DIESEL STD.', ' 05 OCUP.', '708E', 200, 11, 'S', 04, 4, 5, '04', 02, 1112, 0001, 0001, '01', 'MERCEDES BENZ'
);

/* INSERT QUERY NO: 1547 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54701, 54701, 'DU', 'DAIHATSU', 'DELTA', '3.6L 4X2 DIESEL STD.', ' 03 OCUP.', 'DELTA', 200, 21, 'S', 04, 2, 3, '04', 02, 1106, 0001, 0001, '01', 'DAIHATSU'
);

/* INSERT QUERY NO: 1548 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54702, 54702, 'DU', 'DAIHATSU', 'DELTA V118L-HS', '3.6L 4X2 DIESEL STD.', ' 03 OCUP.', 'DELTA V118L-HS', 200, 21, 'S', 04, 2, 3, '04', 02, 1106, 0001, 0001, '01', 'DAIHATSU'
);

/* INSERT QUERY NO: 1549 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54703, 54703, 'DU', 'DAIHATSU', 'HIJET', '1.0L 4X2 STD.', ' 02 OCUP.', 'HIJET', 210, 09, 'S', 03, 2, 2, '01', 02, 1106, 0001, 0001, '01', 'DAIHATSU'
);

/* INSERT QUERY NO: 1550 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 54704, 54704, 'DU', 'DAIHATSU', 'V10LTC', '3.0L V6 STD.', ' 03 OCUP.', 'V10LTC', 210, 09, 'S', 06, 2, 3, '01', 02, 1106, 0001, 0001, '01', 'DAIHATSU'
);

/* INSERT QUERY NO: 1551 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 55401, 55401, 'OL', 'OPEL', 'COMBO', '1.4L GASOLINA STD.', ' 02 OCUP.', 'COMBO', 210, 09, 'S', 04, 3, 2, '02', 02, 1112, 0001, 0001, '01', 'OPEL'
);

/* INSERT QUERY NO: 1552 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 55601, 55601, 'DM', 'DFM', 'MINI TRUCK', '1.1L STD.', ' 02 OCUP.', 'MINI TRUCK', 210, 09, 'S', 04, 2, 2, '01', 02, 1003, 0001, 0001, '01', 'DFM'
);

/* INSERT QUERY NO: 1553 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 55602, 55602, 'DM', 'DFM', 'MINI VAN', '1.0L STD.', ' 02 OCUP.', 'MINI VAN', 210, 20, 'S', 04, 3, 2, '01', 02, 1003, 0001, 0001, '01', 'DFM'
);

/* INSERT QUERY NO: 1554 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 55701, 55701, 'GW', 'GREAT WALL', 'DEER', '5L GASOLINA STD.', ' 04 OCUP.', 'DEER', 200, 09, 'S', 06, 2, 4, '03', 02, 1106, 0001, 0001, '01', 'GREAT WALL'
);

/* INSERT QUERY NO: 1555 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 55702, 55702, 'GW', 'GREAT WALL', 'SOCOOL', '2.8L GASOLINA STD.', ' 05 OCUP.', 'SOCOOL', 210, 09, 'S', 04, 4, 5, '02', 02, 1112, 0001, 0001, '01', 'GREAT WALL'
);

/* INSERT QUERY NO: 1556 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 55703, 55703, 'GW', 'GREAT WALL', 'WINGLE', '2.5L CAB SENCILLA 4X4 DIESEL STD.', ' 05 OCUP.', 'WINGLE', 210, 09, 'S', 04, 4, 5, '02', 02, 1112, 0001, 0001, '01', 'GREAT WALL'
);

/* INSERT QUERY NO: 1557 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 55704, 55704, 'GW', 'GREAT WALL', 'WINGLE', '2.5L DOBLE CAB 4X4 DIESEL STD.', ' 05 OCUP.', 'WINGLE', 210, 09, 'S', 04, 4, 5, '02', 02, 1112, 0001, 0001, '01', 'GREAT WALL'
);

/* INSERT QUERY NO: 1558 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 55706, 55706, 'GW', 'GREAT WALL', 'WINGLE', '2 2.2L 4X2 DIESEL STD.', ' 05 OCUP.', 'WINGLE', 210, 09, 'S', 04, 4, 5, '02', 02, 1112, 0001, 0001, '01', 'GREAT WALL'
);

/* INSERT QUERY NO: 1559 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 55707, 55707, 'GW', 'GREAT WALL', 'SAILOR', '2.8L DOBLE CABINA 4X2 DIESEL AUT.', ' 05 OCUP.', 'SAILOR', 210, 09, 'A', 04, 4, 5, '01', 02, 1501, 0001, 0001, '01', 'GREAT WALL'
);

/* INSERT QUERY NO: 1560 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 55708, 55708, 'GW', 'GREAT WALL', 'WINGLE', '5 2.2L DOBLE CAB 4X2 GASOLINA STD.', ' 05 OCUP.', 'WINGLE', 210, 09, 'S', 04, 4, 5, '02', 02, 1705, 0001, 0001, '01', 'GREAT WALL'
);

/* INSERT QUERY NO: 1561 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56001, 56001, 'SG', 'SSANGYONG', 'ACTION', 'SPORT 2.0L DOBLE CAB DIESEL STD.', ' 05 OCUP.', 'ACTION', 210, 09, 'S', 04, 4, 5, '02', 02, 1212, 0001, 0001, '01', 'SSANGYONG'
);

/* INSERT QUERY NO: 1562 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56201, 56201, 'GS', 'GMOTORS', 'SIERRA', '5.0L 4X2 GASOLINA STD.', ' 05 OCUP.', 'SIERRA', 200, 09, 'S', 08, 4, 5, '03', 02, 1106, 0001, 0001, '01', 'GENERAL MOTORS'
);

/* INSERT QUERY NO: 1563 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56202, 56202, 'GS', 'GMOTORS', 'C-20', '4.1L V6 GASOLINA STD.', ' 03 OCUP.', 'C-20', 210, 09, 'S', 06, 2, 3, '02', 02, 1112, 0001, 0001, '01', 'GENERAL MOTORS'
);

/* INSERT QUERY NO: 1564 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56203, 56203, 'GS', 'GMOTORS', 'SIERRA', 'DOBLE CABINA 6.6L 4X4 DIESEL AUT.', ' 05 OCUP.', 'SIERRA', 210, 09, 'A', 08, 4, 5, '02', 02, 1212, 0001, 0001, '01', 'GENERAL MOTORS'
);

/* INSERT QUERY NO: 1565 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56204, 56204, 'GS', 'GMOTORS', 'SONOMA', '4.3L STD.', ' 05 OCUP.', 'SONOMA', 210, 09, 'S', 06, 4, 5, '02', 02, 1212, 0001, 0001, '01', 'GENERAL MOTORS'
);

/* INSERT QUERY NO: 1566 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56401, 56401, 'LN', 'LINCOLN', 'MARK', 'LT DOBLE CABINA 5.4L 4X4 GASOLINA STD.', ' 05 OCUP.', 'MARK', 210, 09, 'S', 08, 4, 5, '01', 02, 1601, 0001, 0001, '01', 'LINCOLN'
);

/* INSERT QUERY NO: 1567 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56601, 56601, 'ZA', 'ZNA', 'RICH', '2.5L CAJA ABIERTA 4X4 STD.', ' 05 OCUP.', 'RICH', 210, 09, 'S', 04, 4, 5, '02', 02, 1402, 0001, 0001, '01', 'ZNA'
);

/* INSERT QUERY NO: 1568 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56602, 56602, 'ZA', 'ZNA', 'RICH', 'QD80 DOBLE CABINA 2.5L 4X4 DIESEL STD.', ' 05 OCUP.', 'RICH', 210, 09, 'S', 04, 4, 5, '01', 02, 1501, 0001, 0001, '01', 'ZNA'
);

/* INSERT QUERY NO: 1569 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56603, 56603, 'ZA', 'ZNA', 'RICH', 'ZG24 DOBLE CABINA 2.5L 4X4 GASOLINA STD.', ' 05 OCUP.', 'RICH', 210, 09, 'S', 04, 4, 5, '01', 02, 1501, 0001, 0001, '01', 'ZNA'
);

/* INSERT QUERY NO: 1570 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56604, 56604, 'ZA', 'ZNA', 'NEW RICH', 'ZD25 DOBLE CABINA 2.5L 4X4 DIESEL STD.', ' 05 OCUP.', 'NEW RICH', 210, 09, 'S', 04, 4, 5, '01', 02, 1501, 0001, 0001, '01', 'ZNA'
);

/* INSERT QUERY NO: 1571 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56605, 56605, 'ZA', 'ZNA', 'NEW RICH', 'ZD24 DOBLE CABINA 2.5L 4X4 GASOLINA STD.', ' 05 OCUP.', 'NEW RICH', 210, 09, 'S', 04, 4, 5, '01', 02, 1501, 0001, 0001, '01', 'ZNA'
);

/* INSERT QUERY NO: 1572 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56701, 56701, 'CN', 'CITROEN', 'BERLINGO', '1.6L DIESEL STD.', ' 02 OCUP.', 'BERLINGO', 210, 20, 'S', 04, 4, 2, '01', 02, 1003, 0001, 0001, '01', 'CITROEN'
);

/* INSERT QUERY NO: 1573 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56702, 56702, 'CN', 'CITROEN', 'JUMPER', '2.2L DIESEL STD.', ' 03 OCUP.', 'JUMPER', 210, 20, 'S', 04, 4, 3, '02', 02, 1003, 0001, 0001, '01', 'CITROEN'
);

/* INSERT QUERY NO: 1574 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56703, 56703, 'CN', 'CITROEN', 'JUMPER', '2.2L C/A AC DIESEL STD.', ' 03 OCUP.', 'JUMPER', 210, 20, 'S', 04, 4, 3, '02', 02, 1003, 0001, 0001, '01', 'CITROEN'
);

/* INSERT QUERY NO: 1575 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56704, 56704, 'CN', 'CITROEN', 'JUMPY', '1.6L DIESEL STD.', ' 03 OCUP.', 'JUMPY', 210, 20, 'S', 04, 4, 3, '01', 02, 1003, 0001, 0001, '01', 'CITROEN'
);

/* INSERT QUERY NO: 1576 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56705, 56705, 'CN', 'CITROEN', 'JUMPY', '1.6L C/A AC DIESEL STD.', ' 03 OCUP.', 'JUMPY', 210, 20, 'S', 04, 4, 3, '01', 02, 1003, 0001, 0001, '01', 'CITROEN'
);

/* INSERT QUERY NO: 1577 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56706, 56706, 'CN', 'CITROEN', 'C15', '1.9L GASOLINA STD.', ' 02 OCUP.', 'C15', 210, 20, 'S', 04, 5, 2, '01', 02, 1106, 0001, 0001, '01', 'CITROEN'
);

/* INSERT QUERY NO: 1578 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56707, 56707, 'CN', 'CITROEN', 'JUMPER', '2.5L DIESEL STD.', ' 02 OCUP.', 'JUMPER', 210, 20, 'S', 04, 5, 2, '01', 02, 1106, 0001, 0001, '01', 'CITROEN'
);

/* INSERT QUERY NO: 1579 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56708, 56708, 'CN', 'CITROEN', 'BERLINGO', '1.4L DIESEL STD.', ' 03 OCUP.', 'BERLINGO', 210, 20, 'S', 04, 4, 3, '01', 02, 1106, 0001, 0001, '01', 'CITROEN'
);

/* INSERT QUERY NO: 1580 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56801, 56801, 'RT', 'RENAULT', 'KANGOO EXPRESS', '1.9L DIESEL STD.', ' 02 OCUP.', 'KANGOO EXPRESS', 210, 20, 'S', 04, 2, 2, '01', 02, 1106, 0001, 0001, '01', 'RENAULT'
);

/* INSERT QUERY NO: 1581 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56802, 56802, 'RT', 'RENAULT', 'DOKKER', 'VAN 1.5L 4X2 GASOLINA STD.', ' 02 OCUP.', 'DOKKER', 210, 20, 'S', 04, 2, 2, '01', 02, 1412, 0001, 0001, '01', 'RENAULT'
);

/* INSERT QUERY NO: 1582 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 56803, 56803, 'RT', 'RENAULT', 'MASTER', 'MTR 2.8L 4X2 DIESEL STD.', ' 03 OCUP.', 'MASTER', 210, 20, 'S', 04, 4, 3, '02', 02, 1705, 0001, 0001, '01', 'RENAULT'
);

/* INSERT QUERY NO: 1583 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 57201, 57201, 'FT', 'FIAT', 'STRADA', '1.6L GASOLINA STD.', ' 02 OCUP.', 'STRADA', 210, 09, 'S', 04, 2, 2, '02', 02, 1106, 0001, 0001, '01', 'FIAT'
);

/* INSERT QUERY NO: 1584 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 57202, 57202, 'FT', 'FIAT', 'FIORINO', '1.5L GASOLINA STD.', ' 02 OCUP.', 'FIORINO', 210, 20, 'S', 04, 2, 2, '02', 02, 1106, 0001, 0001, '01', 'FIAT'
);

/* INSERT QUERY NO: 1585 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 57203, 57203, 'FT', 'FIAT', 'FIORINO', '1.3L GASOLINA STD.', ' 02 OCUP.', 'FIORINO', 210, 20, 'S', 04, 2, 2, '02', 02, 1106, 0001, 0001, '01', 'FIAT'
);

/* INSERT QUERY NO: 1586 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 57204, 57204, 'FT', 'FIAT', 'FIORINO', '1.7L DIESEL 4X2 STD.', ' 02 OCUP.', 'FIORINO', 210, 20, 'S', 04, 2, 2, '02', 02, 1112, 0001, 0001, '01', 'FIAT'
);

/* INSERT QUERY NO: 1587 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 57205, 57205, 'FT', 'FIAT', 'DUCATO', '2.3L DIESEL 4X2 STD.', ' 03 OCUP.', 'DUCATO', 200, 09, 'S', 04, 2, 3, '03', 02, 1212, 0001, 0001, '01', 'FIAT'
);

/* INSERT QUERY NO: 1588 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 57206, 57206, 'FT', 'FIAT', 'DOBLO', '1.3L DIESEL STD.', ' 02 OCUP.', 'DOBLO', 210, 20, 'S', 04, 4, 2, '02', 02, 1112, 0001, 0001, '01', 'FIAT'
);

/* INSERT QUERY NO: 1589 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 57207, 57207, 'FT', 'FIAT', 'FULLBACK GLS', '2.5L 4X4 DIESEL STD.', ' 05 OCUP.', 'FULLBACK GLS', 210, 09, 'S', 04, 4, 5, '02', 02, 1601, 0001, 0001, '01', 'FIAT'
);

/* INSERT QUERY NO: 1590 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 57208, 57208, 'FT', 'FIAT', 'FULLBACK LX', '2.5L 4X4 DIESEL AUT.', ' 05 OCUP.', 'FULLBACK LX', 210, 09, 'A', 04, 4, 5, '02', 02, 1601, 0001, 0001, '01', 'FIAT'
);

/* INSERT QUERY NO: 1591 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 57209, 57209, 'FT', 'FIAT', 'STRADA', 'WORKING 1.4L GASOLINA STD.', ' 02 OCUP.', 'STRADA', 210, 09, 'S', 04, 2, 2, '01', 02, 1601, 0001, 0001, '01', 'FIAT'
);

/* INSERT QUERY NO: 1592 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 57801, 57801, 'CF', 'CHANGFENG', 'YANGZI', 'FINE 2.8L 4X4 DOBLE CABINA AUT.', ' 05 OCUP.', 'YANGZI', 210, 09, 'A', 04, 4, 5, '02', 02, 1402, 0001, 0001, '01', 'CHANGFENG'
);

/* INSERT QUERY NO: 1593 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 57901, 57901, 'AS', 'ASIA', 'TOWNER', '1.0L GASOLINA STD.', ' 02 OCUP.', 'TOWNER', 210, 20, 'S', 03, 4, 2, '02', 02, 1112, 0001, 0001, '01', 'ASIA'
);

/* INSERT QUERY NO: 1594 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58102, 58102, 'RA', 'RAM', 'RAM 2500', '6.7L DOBLE CABINA 4X4 DIESEL AUT.', ' 05 OCUP.', 'RAM 2500', 210, 09, 'A', 06, 4, 5, '01', 02, 1106, 0001, 0001, '01', 'RAM'
);

/* INSERT QUERY NO: 1595 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58103, 58103, 'RA', 'RAM', 'RAM 1500', 'DOBLE CABINA 4X4 GASOLINA AUT.', ' 05 OCUP.', 'RAM 1500', 210, 09, 'A', 06, 4, 5, '01', 02, 1106, 0001, 0001, '01', 'RAM'
);

/* INSERT QUERY NO: 1596 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58201, 58201, 'HO', 'HINO', 'DUTRO', '6.5T CHASIS CABINA DIESEL STD.', ' 03 OCUP.', 'DUTRO', 200, 21, 'S', 04, 2, 3, '04', 02, 1106, 0001, 0001, '01', 'HINO'
);

/* INSERT QUERY NO: 1597 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58202, 58202, 'HO', 'HINO', 'FD174SA ', '.2T CHASIS CABINA STD.', ' 03 OCUP.', 'FD174SA ', 200, 21, 'S', 04, 2, 3, '05', 02, 1106, 0001, 0001, '01', 'HINO'
);

/* INSERT QUERY NO: 1598 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58203, 58203, 'HO', 'HINO', 'DUTRO', '7.5T CHASIS CABINA DIESEL STD.', ' 03 OCUP.', 'DUTRO', 200, 21, 'S', 04, 2, 3, '05', 02, 1106, 0001, 0001, '01', 'HINO'
);

/* INSERT QUERY NO: 1599 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58204, 58204, 'HO', 'HINO', 'FB2W', '7.5T CHASIS CABINA DIESEL STD.', ' 03 OCUP.', 'FB2W', 200, 21, 'S', 04, 2, 3, '05', 02, 1106, 0001, 0001, '01', 'HINO'
);

/* INSERT QUERY NO: 1600 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58205, 58205, 'HO', 'HINO', 'DUTRO', '8.5T CHASIS CABINA DIESEL STD.', ' 03 OCUP.', 'DUTRO', 200, 21, 'S', 04, 2, 3, '05', 02, 1106, 0001, 0001, '01', 'HINO'
);

/* INSERT QUERY NO: 1601 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58206, 58206, 'HO', 'HINO', 'GH', '7.9T CHASIS CABINA DIESEL STD.', ' 03 OCUP.', 'GH', 200, 21, 'S', 04, 2, 3, '05', 02, 1106, 0001, 0001, '01', 'HINO'
);

/* INSERT QUERY NO: 1602 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58207, 58207, 'HO', 'HINO', 'FB4JGSA', '3.5T CHASIS CABINA DIESEL STD.', ' 03 OCUP.', 'FB4JGSA', 200, 21, 'S', 04, 2, 3, '06', 02, 1106, 0001, 0001, '01', 'HINO'
);

/* INSERT QUERY NO: 1603 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58208, 58208, 'HO', 'HINO', 'XZU413', '4.0L DIESEL STD.', ' 03 OCUP.', 'XZU413', 200, 21, 'S', 04, 2, 3, '06', 02, 1112, 0001, 0001, '01', 'HINO'
);

/* INSERT QUERY NO: 1604 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58209, 58209, 'HO', 'HINO', 'SERIE 300', 'HC25 2.5T CHASIS 4.0L DIESEL STD.', ' 03 OCUP.', 'SERIE 300', 200, 21, 'S', 04, 2, 3, '04', 02, 1501, 0001, 0001, '01', 'HINO'
);

/* INSERT QUERY NO: 1605 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58210, 58210, 'HO', 'HINO', 'SERIE 300', 'HC35 3.5T CHASIS CORTO 4.0L DIESEL STD.', ' 03 OCUP.', 'SERIE 300', 200, 21, 'S', 04, 2, 3, '06', 02, 1501, 0001, 0001, '01', 'HINO'
);

/* INSERT QUERY NO: 1606 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58211, 58211, 'HO', 'HINO', 'SERIE 300', 'HL35 3.5T CHASIS LARGO 4.0L DIESEL STD.', ' 03 OCUP.', 'SERIE 300', 200, 21, 'S', 04, 2, 3, '06', 02, 1501, 0001, 0001, '01', 'HINO'
);

/* INSERT QUERY NO: 1607 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58212, 58212, 'HO', 'HINO', 'SERIE 300', 'HC40 4.0T CHASIS CORTO 4.0L DIESEL STD.', ' 03 OCUP.', 'SERIE 300', 200, 21, 'S', 04, 2, 3, '06', 02, 1501, 0001, 0001, '01', 'HINO'
);

/* INSERT QUERY NO: 1608 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58216, 58216, 'HO', 'HINO', 'SERIE 300', 'HDC3 3.0T DOBLE CABINA 4.0L DIESEL STD.', ' 07 OCUP.', 'SERIE 300', 200, 21, 'S', 04, 4, 7, '05', 02, 1501, 0001, 0001, '01', 'HINO'
);

/* INSERT QUERY NO: 1609 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58217, 58217, 'HO', 'HINO', 'SERIE 300', 'HV25 VAGONETA 2.5T 4.0L DIESEL STD.', ' 03 OCUP.', 'SERIE 300', 200, 21, 'S', 04, 2, 3, '04', 02, 1501, 0001, 0001, '01', 'HINO'
);

/* INSERT QUERY NO: 1610 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58218, 58218, 'HO', 'HINO', 'SERIE 300', 'HH40 4.0T HIBRIDO 4.0L DIESEL STD.', ' 03 OCUP.', 'SERIE 300', 200, 21, 'S', 04, 2, 3, '06', 02, 1501, 0001, 0001, '01', 'HINO'
);

/* INSERT QUERY NO: 1611 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58219, 58219, 'HO', 'HINO', 'DUTRO', '2.5T CHASIS 4.0L DIESEL STD.', ' 03 OCUP.', 'DUTRO', 200, 21, 'S', 04, 2, 3, '04', 02, 1601, 0001, 0001, '01', 'HINO'
);

/* INSERT QUERY NO: 1612 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58220, 58220, 'HO', 'HINO', 'SERIE 300', 'WU600L 4.0L CAJA CERRADA DIESEL STD.', ' 03 OCUP.', 'SERIE 300', 200, 21, 'S', 04, 2, 3, '04', 02, 1601, 0001, 0001, '01', 'HINO'
);

/* INSERT QUERY NO: 1613 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58221, 58221, 'HO', 'HINO', 'DUTRO', '3.5T CHASIS CABINA 4.0L DIESEL STD.', ' 03 OCUP.', 'DUTRO', 200, 21, 'S', 04, 2, 3, '06', 02, 1601, 0001, 0001, '01', 'HINO'
);

/* INSERT QUERY NO: 1614 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58301, 58301, 'CM', 'CMC', 'VARICA', '2.0L GASOLINA STD.', ' 02 OCUP.', 'VARICA', 210, 20, 'S', 04, 4, 2, '01', 02, 1112, 0001, 0001, '01', 'CMC'
);

/* INSERT QUERY NO: 1615 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58302, 58302, 'CM', 'CMC', 'VARICA', 'PANEL 1.5L GASOLINA STD.', ' 02 OCUP.', 'VARICA', 210, 20, 'S', 04, 4, 2, '01', 02, 1412, 0001, 0001, '01', 'CMC'
);

/* INSERT QUERY NO: 1616 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58401, 58401, 'JC', 'JMC', 'FURGON', '2.7L 4X2 DIESEL STD.', ' 03 OCUP.', 'FURGON', 200, 09, 'S', 04, 2, 3, '05', 02, 1106, 0001, 0001, '01', 'JMC'
);

/* INSERT QUERY NO: 1617 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58402, 58402, 'JC', 'JMC', 'N601', '2.7L DIESEL STD.', ' 03 OCUP.', 'N601', 200, 21, 'S', 04, 2, 3, '05', 02, 1106, 0001, 0001, '01', 'JMC'
);

/* INSERT QUERY NO: 1618 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58403, 58403, 'JC', 'JMC', 'NKR', '2.7L DIESEL STD.', ' 06 OCUP.', 'NKR', 200, 21, 'S', 04, 2, 6, '05', 02, 1106, 0001, 0001, '01', 'JMC'
);

/* INSERT QUERY NO: 1619 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58404, 58404, 'JC', 'JMC', 'NHR', '2.8L DIESEL STD.', ' 03 OCUP.', 'NHR', 210, 09, 'S', 04, 2, 3, '02', 02, 1112, 0001, 0001, '01', 'JMC'
);

/* INSERT QUERY NO: 1620 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58405, 58405, 'JC', 'JMC', 'BD-LDX', 'PICK UP DOBLE CABINA 2.3L V4 DIESEL STD.', ' 05 OCUP.', 'BD-LDX', 210, 09, 'S', 04, 4, 5, '02', 02, 1212, 0001, 0001, '01', 'JMC'
);

/* INSERT QUERY NO: 1621 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58406, 58406, 'JC', 'JMC', 'N900', '2.8L DIESEL STD.', ' 03 OCUP.', 'N900', 200, 21, 'S', 04, 2, 3, '03', 02, 1410, 0001, 0001, '01', 'JMC'
);

/* INSERT QUERY NO: 1622 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58407, 58407, 'JC', 'JMC', 'N800', 'CARGA LIVIANA 2.4L 4X2 DIESEL STD.', ' 03 OCUP.', 'N800', 200, 21, 'S', 04, 2, 3, '03', 02, 1410, 0001, 0001, '01', 'JMC'
);

/* INSERT QUERY NO: 1623 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58501, 58501, 'JA', 'JAC', 'HFC', '2.8L DIESEL STD.', ' 03 OCUP.', 'HFC', 200, 21, 'S', 04, 2, 3, '03', 02, 1112, 0001, 0001, '01', 'JAC'
);

/* INSERT QUERY NO: 1624 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58502, 58502, 'JA', 'JAC', 'HFC', '1050 CABINA SENCILLA 4.0L DIESEL STD.', ' 03 OCUP.', 'HFC', 200, 21, 'S', 04, 2, 2, '03', 02, 1402, 0001, 0001, '01', 'JAC'
);

/* INSERT QUERY NO: 1625 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58503, 58503, 'JA', 'JAC', 'HFC', '1035 KRD DOBLE CABINA 4.0L DIESEL STD.', ' 03 OCUP.', 'HFC', 200, 21, 'S', 04, 2, 3, '04', 02, 1402, 0001, 0001, '01', 'JAC'
);

/* INSERT QUERY NO: 1626 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58504, 58504, 'JA', 'JAC', 'T6', '2.0L 4X4 DIESEL STD.', ' 05 OCUP.', 'T6', 210, 09, 'S', 04, 4, 5, '01', 02, 1601, 0001, 0001, '01', 'JAC'
);

/* INSERT QUERY NO: 1627 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58601, 58601, 'HF', 'HAFEI', 'RUIYI', '1.0L 4X2 GASOLINA STD.', ' 02 OCUP.', 'RUIYI', 200, 21, 'S', 04, 2, 2, '03', 02, 1106, 0001, 0001, '01', 'HAFEI'
);

/* INSERT QUERY NO: 1628 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58602, 58602, 'HF', 'HAFEI', 'MINYI', '1.1L GASOLINA STD.', ' 02 OCUP.', 'MINYI', 210, 09, 'S', 04, 2, 2, '01', 02, 1106, 0001, 0001, '01', 'HAFEI'
);

/* INSERT QUERY NO: 1629 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58603, 58603, 'HF', 'HAFEI', 'ZHONGYI', '1.1L GASOLINA STD.', ' 02 OCUP.', 'ZHONGYI', 210, 20, 'S', 04, 5, 2, '01', 02, 1601, 0001, 0001, '01', 'HAFEI'
);

/* INSERT QUERY NO: 1630 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58701, 58701, 'FW', 'FAW', 'ND', '3.2L DIESEL STD.', ' 03 OCUP.', 'ND', 210, 09, 'S', 04, 2, 3, '02', 02, 1112, 0001, 0001, '01', 'FAW'
);

/* INSERT QUERY NO: 1631 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58702, 58702, 'FW', 'FAW', 'CA 6371', 'STATION WAGON 1.0L 4X2 GASOLINA STD.', ' 08 OCUP.', 'CA 6371', 210, 24, 'S', 04, 5, 8, '02', 02, 1112, 0001, 0001, '01', 'FAW'
);

/* INSERT QUERY NO: 1632 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58703, 58703, 'FW', 'FAW', 'CA 1021 KU2', 'CA 1021 KU2 2.8L 4X4 DIESEL STD.', ' 05 OCUP.', 'CA 1021 KU2', 210, 09, 'S', 04, 4, 5, '02', 02, 1112, 0001, 0001, '01', 'FAW'
);

/* INSERT QUERY NO: 1633 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58704, 58704, 'FW', 'FAW', 'TIGER', '2.8L 4X2 DIESEL STD.', ' 05 OCUP.', 'TIGER', 200, 21, 'S', 04, 4, 5, '02', 02, 1501, 0001, 0001, '01', 'FAW'
);

/* INSERT QUERY NO: 1634 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58801, 58801, 'DG', 'DONGFENG', 'RICH', '3.2L DOBLE CABINA DIESEL STD.', ' 05 OCUP.', 'RICH', 210, 09, 'S', 04, 4, 5, '02', 02, 1106, 0001, 0001, '01', 'DONGFENG'
);

/* INSERT QUERY NO: 1635 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58802, 58802, 'DG', 'DONGFENG', 'MINIVAN', '1.1L GASOLINA STD.', ' 02 OCUP.', 'MINIVAN', 210, 20, 'S', 04, 4, 2, '02', 02, 1112, 0001, 0001, '01', 'DONGFENG'
);

/* INSERT QUERY NO: 1636 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 58901, 58901, 'HR', 'HUMMER', 'H2', '6.0L V8 GASOLINA AUT.', ' 05 OCUP.', 'H2', 210, 09, 'A', 08, 4, 5, '02', 02, 1112, 0001, 0001, '01', 'HUMMER'
);

/* INSERT QUERY NO: 1637 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59001, 59001, 'SE', 'SOUEAST', 'C1', '1.3L GASOLINA STD.', ' 02 OCUP.', 'C1', 210, 20, 'S', 04, 5, 2, '02', 02, 1112, 0001, 0001, '01', 'SOUEAST'
);

/* INSERT QUERY NO: 1638 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59101, 59101, 'IL', 'INTERNATIONAL', 'MXT', '6.0L 4X4 DIESEL AUT.', ' 05 OCUP.', 'MXT', 200, 09, 'A', 06, 5, 5, '06', 02, 1211, 0001, 0001, '01', 'INTERNATIONAL'
);

/* INSERT QUERY NO: 1639 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59201, 59201, 'CG', 'CHANGAN', 'TIGER', '1.0L GASOLINA STD.', ' 02 OCUP.', 'TIGER', 210, 09, 'S', 04, 2, 2, '01', 02, 1112, 0001, 0001, '01', 'CHANGAN'
);

/* INSERT QUERY NO: 1640 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59202, 59202, 'CG', 'CHANGAN', 'STAR II', '1.3L GASOLINA STD.', ' 02 OCUP.', 'STAR II', 210, 20, 'S', 04, 3, 2, '01', 02, 1112, 0001, 0001, '01', 'CHANGAN'
);

/* INSERT QUERY NO: 1641 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59203, 59203, 'CG', 'CHANGAN', 'Q20', 'CABINA SENCILLA 1.3L GASOLINA STD.', ' 02 OCUP.', 'Q20', 200, 21, 'S', 04, 2, 2, '03', 02, 1402, 0001, 0001, '01', 'CHANGAN'
);

/* INSERT QUERY NO: 1642 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59204, 59204, 'CG', 'CHANGAN', 'Q20', 'CABINA DOBLE 1.3L GASOLINA STD.', ' 02 OCUP.', 'Q20', 200, 21, 'S', 04, 2, 2, '03', 02, 1402, 0001, 0001, '01', 'CHANGAN'
);

/* INSERT QUERY NO: 1643 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59205, 59205, 'CG', 'CHANGAN', 'STAR LIGHT', '1.3L GASOLINA STD.', ' 02 OCUP.', 'STAR LIGHT', 210, 20, 'S', 04, 4, 2, '01', 02, 1501, 0001, 0001, '01', 'CHANGAN'
);

/* INSERT QUERY NO: 1644 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59206, 59206, 'CG', 'CHANGAN', 'STAR 9', '1.3L GASOLINA STD.', ' 02 OCUP.', 'STAR 9', 210, 20, 'S', 04, 4, 2, '01', 02, 1601, 0001, 0001, '01', 'CHANGAN'
);

/* INSERT QUERY NO: 1645 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59301, 59301, 'FN', 'FOTON', 'AUMARK', '2.8L DIESEL STD.', ' 03 OCUP.', 'AUMARK', 210, 09, 'S', 04, 2, 3, '02', 02, 1212, 0001, 0001, '01', 'FOTON'
);

/* INSERT QUERY NO: 1646 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59302, 59302, 'FN', 'FOTON', 'TUNLAND', '2.8L DIESEL STD.', ' 05 OCUP.', 'TUNLAND', 210, 09, 'S', 04, 4, 5, '02', 02, 1402, 0001, 0001, '01', 'FOTON'
);

/* INSERT QUERY NO: 1647 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59401, 59401, 'IO', 'IVECO', '4912V', '2.8L GASOLINA STD.', ' 03 OCUP.', '4912V', 210, 20, 'S', 04, 4, 3, '02', 02, 1112, 0001, 0001, '01', 'IVECO'
);

/* INSERT QUERY NO: 1648 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59402, 59402, 'IO', 'IVECO', 'TURBODAILY', '2.8L DIESEL STD.', ' 03 OCUP.', 'TURBODAILY', 200, 11, 'S', 04, 2, 3, '04', 02, 1112, 0001, 0001, '01', 'IVECO'
);

/* INSERT QUERY NO: 1649 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59501, 59501, 'HG', 'HIGER', 'KLQ1020B0', '2.8L DIESEL STD.', ' 05 OCUP.', 'KLQ1020B0', 210, 09, 'S', 04, 4, 5, '02', 02, 1212, 0001, 0001, '01', 'HIGER'
);

/* INSERT QUERY NO: 1650 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59502, 59502, 'HG', 'HIGER', 'DK4B', 'EURO III 2.4L DIESEL STD.', ' 03 OCUP.', 'DK4B', 200, 20, 'S', 04, 3, 3, '03', 02, 1402, 0001, 0001, '01', 'HIGER'
);

/* INSERT QUERY NO: 1651 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59503, 59503, 'HG', 'HIGER', 'KLQ1030E30', 'EURO III 2.4L DIESEL STD.', ' 05 OCUP.', 'KLQ1030E30', 200, 09, 'S', 08, 4, 5, '03', 02, 1402, 0001, 0001, '01', 'HIGER'
);

/* INSERT QUERY NO: 1652 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59504, 59504, 'HG', 'HIGER', 'KLQ1032Q40S', 'EURO IV 2.4L GASOLINA STD.', ' 05 OCUP.', 'KLQ1032Q40S', 200, 09, 'S', 04, 4, 5, '04', 02, 1402, 0001, 0001, '01', 'HIGER'
);

/* INSERT QUERY NO: 1653 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59551, 59551, 'SY', 'SHINERAY', 'T20N', 'K T20N 1.3L DIESEL STD.', ' 02 OCUP.', 'SHINERAY', 200, 09, 'S', 04, 2, 2, '03', 02, 1501, 0001, 0001, '01', 'SHINERAY'
);

/* INSERT QUERY NO: 1654 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59601, 59601, 'GY', 'GEELY', 'PU', '1.3L GASOLINA STD.', ' 02 OCUP.', 'PU', 210, 09, 'S', 04, 2, 2, '01', 02, 1112, 0001, 0001, '01', 'GEELY'
);

/* INSERT QUERY NO: 1655 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59701, 59701, 'MD', 'MAHINDRA', 'SCORPIO', 'DOBLE CABINA 2.2L DIESEL STD.', ' 05 OCUP.', 'SCORPIO', 210, 09, 'S', 04, 4, 5, '02', 02, 1402, 0001, 0001, '01', 'MAHINDRA'
);

/* INSERT QUERY NO: 1656 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59702, 59702, 'MD', 'MAHINDRA', 'SCORPIO', 'CABINA SENCILLA 2.2L DIESEL STD.', ' 02 OCUP.', 'SCORPIO', 210, 09, 'S', 04, 2, 2, '02', 02, 1402, 0001, 0001, '01', 'MAHINDRA'
);

/* INSERT QUERY NO: 1657 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59703, 59703, 'MD', 'MAHINDRA', 'GENIO', 'DOBLE CABINA 2.2L DIESEL STD.', ' 05 OCUP.', 'GENIO', 210, 09, 'S', 04, 2, 5, '02', 02, 1402, 0001, 0001, '01', 'MAHINDRA'
);

/* INSERT QUERY NO: 1658 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59801, 59801, 'BA', 'BAW', 'BJ1021PHD41', '2.8L GASOLINA STD.', ' 02 OCUP.', 'BJ1021PHD41', 210, 09, 'S', 04, 2, 2, '02', 02, 1112, 0001, 0001, '01', 'BAW'
);

/* INSERT QUERY NO: 1659 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59851, 59851, 'BC', 'BAIC', 'MZ40', '1.2L GASOLINA STD.', ' 02 OCUP.', 'MZ40', 210, 20, 'S', 04, 5, 2, '02', 02, 1501, 0001, 0001, '01', 'BAIC'
);

/* INSERT QUERY NO: 1660 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59852, 59852, 'BC', 'BAIC', 'MZ40', 'LJ474 1.3L GASOLINA STD.', ' 02 OCUP.', 'MZ40', 210, 20, 'S', 04, 5, 2, '02', 02, 1601, 0001, 0001, '01', 'BAIC'
);

/* INSERT QUERY NO: 1661 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59901, 59901, 'TN', 'TIANMA', 'POWE LANDER', '2.8L GASOLINA STD.', ' 03 OCUP.', 'POWE LANDER', 210, 09, 'S', 04, 2, 3, '02', 02, 1212, 0001, 0001, '01', 'TIANMA'
);

/* INSERT QUERY NO: 1662 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 59981, 59981, 'PS', 'POLARSUN', 'PANEL', '2.6L DIESEL STD.', ' 02 OCUP.', 'PANEL', 210, 20, 'S', 04, 4, 2, '02', 02, 1601, 0001, 0001, '01', 'POLARSUN'
);

/* INSERT QUERY NO: 1663 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 74005, 74005, 'EQ', '', 'EQUIPO PESADO', 'BINA P/ADAPT ESPECIAL USO 99 STD.', ' 02 OCUP.', 'EQUIPO PESADO', 250, 21, 'S', 08, 2, 2, '07', 02, 1212, 0001, 0001, '01', 'SIN MARCA'
);

/* INSERT QUERY NO: 1664 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 75000, 75000, 'EQ', 'RABON', 'RABON', '', '', '250', 21, 0, '06', 2, 2, 07, '02', 1401, 0001, 0001, 01, '', 'EQ CAMION RABON MAS 3.5T HASTA 7.5T'
);

/* INSERT QUERY NO: 1665 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 75001, 75001, 'EQ', 'TANDEM', 'TANDEM', '', '', '251', 21, 0, '06', 2, 2, 08, '02', 1402, 0001, 0001, 01, '', 'EQ CAMION TANDEM MAS 7.5T HASTA 14T'
);

/* INSERT QUERY NO: 1666 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 75002, 75002, 'EQ', 'THORTON', 'THORTON', '', '', '252', 21, 0, '06', 2, 2, 09, '02', 1402, 0001, 0001, 01, '', 'EQ CAMION THORTON MAS 14T'
);

/* INSERT QUERY NO: 1667 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 80000, 80000, 'TR', 'MONTACARGA', 'MONTACARGA', '', '', '206', 06, 0, '04', 0, 1, 10, '02', 1112, 0001, 0001, 01, '', 'TR MONTACARGA'
);

/* INSERT QUERY NO: 1668 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 80001, 80001, 'TR', 'TRACTOCAMION', 'TRACTOCAMION', '', '', '206', 06, 0, '08', 2, 2, 10, '02', 1112, 0001, 0001, 01, '', 'TR CAMION ARMADO TIPO TRACTOCAMION'
);

/* INSERT QUERY NO: 1669 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 80002, 80002, 'TR', 'TRACTOCAMION', 'TRACTOCAMION', '', '', '206', 06, 0, '08', 2, 2, 10, '02', 1410, 0001, 0001, 01, '', 'TR TRACTOCAMION'
);

/* INSERT QUERY NO: 1670 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 80003, 80003, 'TR', 'TRACTOCAMION', 'TRACTOCAMION', '', '', '206', 06, 0, '08', 0, 2, 10, '02', 1412, 0000, 0000, 01, '', 'TR TRACTOR AGRICOLA'
);

/* INSERT QUERY NO: 1671 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 80004, 80004, 'TR', 'TRACTOCAMION', 'EQUIPO CONTRATISTA', '', '', '206', 06, 0, '08', 2, 2, 10, '02', 1801, 0010, 0010, 01, '', 'TR EQUIPO CONTRATISTA'
);

/* INSERT QUERY NO: 1672 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 90000, 90000, 'RM', 'REMOLQUE', 'REMOLQUE', '', '', '207', 07, 0, '00', 0, 0, 11, '02', 1003, 0001, 0001, 00, '', 'RM REMOLQUE CAJA CERRADA 2 EJES 40\''
);

/* INSERT QUERY NO: 1673 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 90001, 90001, 'RM', 'REMOLQUE', 'REMOLQUE', '', '', '207', 07, 0, '00', 0, 0, 11, '02', 1106, 0001, 0001, 00, '', 'RM REMOLQUE CAJA REFRIGERADA 2 EJES 40\' C/EQREF'
);

/* INSERT QUERY NO: 1674 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 90002, 90002, 'RM', 'REMOLQUE', 'REMOLQUE', '', '', '207', 07, 0, '00', 0, 0, 11, '02', 1106, 0001, 0001, 00, '', 'RM REMOLQUE PLATAFORMA 2 EJES 40\''
);

/* INSERT QUERY NO: 1675 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 90003, 90003, 'RM', 'REMOLQUE', 'REMOLQUE', '', '', '207', 07, 0, '00', 0, 0, 11, '02', 1106, 0001, 0001, 00, '', 'RM REMOLQUE PLATAFORMA ENCORTINADA 2 EJES 40\''
);

/* INSERT QUERY NO: 1676 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 90004, 90004, 'RM', 'REMOLQUE', 'REMOLQUE', '', '', '207', 07, 0, '00', 0, 0, 11, '02', 1106, 0001, 0001, 00, '', 'RM REMOLQUE TANQUE A.INOX ANILL 2 EJES 31'
);

/* INSERT QUERY NO: 1677 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 90005, 90005, 'RM', 'REMOLQUE', 'REMOLQUE', '', '', '207', 07, 0, '00', 0, 0, 11, '02', 1106, 0001, 0001, 00, '', 'RM REMOLQUE TANQUE A. AL CARBON 2 EJES 31'
);

/* INSERT QUERY NO: 1678 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 90006, 90006, 'RM', 'REMOLQUE', 'REMOLQUE', '', '', '207', 07, 0, '00', 0, 0, 11, '02', 1106, 0001, 0001, 00, '', 'RM REMOLQUE TANQUE PARA GAS 42'
);

/* INSERT QUERY NO: 1679 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 90007, 90007, 'RM', 'REMOLQUE', 'REMOLQUE', '', '', '207', 07, 0, '00', 0, 0, 11, '02', 1106, 0001, 0001, 00, '', 'RM REMOLQUE TANQUE LECHERO 2 EJES 25'
);

/* INSERT QUERY NO: 1680 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 90008, 90008, 'RM', 'REMOLQUE', 'REMOLQUE', '', '', '207', 07, 0, '00', 0, 0, 11, '02', 1106, 0001, 0001, 00, '', 'RM REMOLQUE TANQUE PARA ACIDO 2 EJES 22'
);

/* INSERT QUERY NO: 1681 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 90009, 90009, 'RM', 'REMOLQUE', 'REMOLQUE', '', '', '207', 07, 0, '00', 0, 0, 11, '02', 1106, 0001, 0001, 00, '', 'RM REMOLQUE TOLVA 2 EJES 36 MTS^3'
);

/* INSERT QUERY NO: 1682 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 90010, 90010, 'RM', 'REMOLQUE', 'REMOLQUE', '', '', '207', 07, 0, '00', 0, 0, 11, '02', 1106, 0001, 0001, 00, '', 'RM REMOLQUE VOLTEO 2 EJES 30 MTS^3'
);

/* INSERT QUERY NO: 1683 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 90011, 90011, 'RM', 'REMOLQUE', 'REMOLQUE', '', '', '207', 07, 0, '00', 0, 0, 11, '02', 1106, 0001, 0001, 00, '', 'RM REMOLQUE JAULA GANADERA 2 EJES 48\''
);

/* INSERT QUERY NO: 1684 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 90012, 90012, 'RM', 'REMOLQUE', 'REMOLQUE', '', '', '207', 07, 0, '00', 0, 0, 11, '02', 1106, 0001, 0001, 00, '', 'RM REMOLQUE JAULA 2 EJES 40\''
);

/* INSERT QUERY NO: 1685 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 90013, 90013, 'RM', 'REMOLQUE', 'REMOLQUE', '', '', '207', 07, 0, '00', 0, 0, 11, '02', 1106, 0001, 0001, 00, '', 'RM REMOLQUE CAMA BAJA 2 EJES 32 TON'
);

/* INSERT QUERY NO: 1686 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 90014, 90014, 'RM', 'REMOLQUE', 'REMOLQUE', '', '', '207', 07, 0, '00', 0, 0, 11, '02', 1106, 0001, 0001, 00, '', 'RM REMOLQUE PORTACONTENEDOR 2 EJES 40\''
);

/* INSERT QUERY NO: 1687 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 90015, 90015, 'RM', 'REMOLQUE', 'REMOLQUE', '', '', '207', 07, 0, '00', 0, 0, 11, '02', 1112, 0001, 0001, 00, '', 'RM REMOLQUE SISTEMA NODRIZA C/EQUIPO P/10 AUTOS'
);

/* INSERT QUERY NO: 1688 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 90016, 90016, 'RM', 'REMOLQUE', 'REMOLQUE', '', '', '207', 07, 0, '00', 0, 0, 11, '02', 1212, 0001, 0001, 01, '', 'RM REMOLQUE TANQUE + RC'
);

/* INSERT QUERY NO: 1689 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 90017, 90017, 'RM', 'REMOLQUE', 'REMOLQUE', '', '', '207', 07, 0, '00', 0, 0, 11, '02', 1212, 0001, 0001, 00, '', 'RM REMOLQUE ACOPLE 2 EJES'
);

/* INSERT QUERY NO: 1690 */
INSERT INTO insurance_qualitascatalogmodel(tarifa, amis, clave, marca1, marca2, subtipo, vers, tipo, cate, car, tras, cil, ptas, ocup, ton, subr, tarvig, gdm, grt, grc, marca, descripcionmarca)
VALUES
(
1801, 90018, 90018, 'RM', 'REMOLQUE', 'REMOLQUE', '', '', '207', 07, 0, '00', 0, 0, 11, '02', 1601, 0001, 0001, 00, '', 'RM REMOLQUE PKP'
);

