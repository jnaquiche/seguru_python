# select * from insurance_vehiclemodel


SET FOREIGN_KEY_CHECKS = 0; 
TRUNCATE table insurance_vehiclemodel; 
SET FOREIGN_KEY_CHECKS = 1;


INSERT INTO insurance_vehiclemodel(
name,
created_at,
updated_at,
vehicle_brand_id,
amis,
years,
ins_model_code,
oceanica_model_code
)
SELECT 

    brandModelDescription,
	now(),
	now(),
	id,
	amis,
    2019,
    ins_model_code,
	oceanica_model_code
    
FROM 
   
(    select * from insurance_vehiclebrand as vb
    inner join
	(select brandcode,brandDescription, min(brandModelCode) as oceanica_model_code, 
			brandModelDescription 
		from insurance_oceanicacatalogmodel
        group by brandcode, brandDescription, brandModelDescription) as oc
    
    on (vb.oceanica_brand_code = oc.brandcode)
    
	left join
	(select descripcion, min(codigo) as ins_model_code,
					valorref1 
		from insurance_inscatalogmodel 
		where tagxml = 'MODELORSGO'
        group by descripcion, valorref1) as ins

	on (vb.ins_brand_code = ins.valorref1 and oc.brandModelDescription = ins.descripcion)

	  left join
	(select marca2, subtipo, min(amis) amis
		from insurance_qualitascatalogmodel
        group by marca2, subtipo
	) as qa

	on (oc.brandModelDescription = qa.subtipo AND oc.brandDescription = qa.marca2)
) as result

#WHERE brandModelDescription = 'S3'