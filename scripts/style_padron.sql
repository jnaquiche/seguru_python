SET SQL_SAFE_UPDATES = 0;
UPDATE seguru.insurance_padron SET nombre = CONCAT(UCASE(LEFT(nombre, 1)), LCASE(SUBSTRING(nombre, 2)));
UPDATE seguru.insurance_padron SET apellido1 = CONCAT(UCASE(LEFT(apellido1, 1)), LCASE(SUBSTRING(apellido1, 2)));
UPDATE seguru.insurance_padron SET apellido2 = CONCAT(UCASE(LEFT(apellido2, 1)), LCASE(SUBSTRING(apellido2, 2)));
SET SQL_SAFE_UPDATES = 1;