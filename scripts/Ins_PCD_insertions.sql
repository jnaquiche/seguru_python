
SET FOREIGN_KEY_CHECKS = 0; 
TRUNCATE table insurance_insdistrict;
TRUNCATE table insurance_inscounty;
TRUNCATE table insurance_insstate;
 
SET FOREIGN_KEY_CHECKS = 1;



INSERT INTO `seguru`.`insurance_insstate`
(`name`, `code`)
SELECT
descripcion,
codigo

FROM seguru.insurance_inscatalogmodel
where tagxml = 'PROVCLI'
order by codigo;




INSERT INTO `seguru`.`insurance_inscounty`
(
`name`,
`code`,
`state_id`)

SELECT
descripcion,
codigo,
SUBSTRING(valorref1, 2, 1)

FROM seguru.insurance_inscatalogmodel
where tagxml = 'CANTCLI'
order by codigo, valorref1;


INSERT INTO `seguru`.`insurance_insdistrict`
(
`name`,
`code`,
`county_id`)

SELECT
descripcion,
codigo,
(select id from `seguru`.`insurance_inscounty` 
	where `code` = valorref1 and `state_id` = SUBSTRING(valorref2, 2, 1)
)

FROM seguru.insurance_inscatalogmodel
where tagxml = 'DISTCLI'
order by codigo, valorref1, valorref2;
