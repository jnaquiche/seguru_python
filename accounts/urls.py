from django.urls import path
from django.contrib.auth import views as auth_views

urlpatterns = [
	path('login/', auth_views.login, {'template_name': 'accounts/login.html'}, name='accounts.login'),
	path('logout/', auth_views.logout, name='accounts.logout'),
]
