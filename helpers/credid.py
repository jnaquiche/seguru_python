import requests
import json

class Credid():
	
	def __init__(self):
		pass

	def getData(self,plate):
		plate = str(plate)
		url="https://credid.net/wstest/api/Reporte/vehiculo?placa="
		token="6F9B43BA-91B1-4684-AC60-CA0D587836A8" 
		r = requests.get(url+plate, headers={'Authorization': token})
		status = r.status_code
		data ={}

		if status == 200:
			##
			print (r.text)
			parsed = json.loads(r.text)
			if(parsed["Status"] == "OK"):
				data["brand"]= parsed["Dato"]["Marca"]
				data["model"]= parsed["Dato"]["Modelo"]
				data["year"]= parsed["Dato"]["Anio"]
				data["color"]= parsed["Dato"]["Color"]
				data["weight"]= parsed["Dato"]["PesoBruto"]
				data["seats"]= parsed["Dato"]["Capacidad"]
				data["type"]= parsed["Dato"]["Carroceria"]
				data["vin"]= parsed["Dato"]["NumeroVinCarroceria"]
				data["motor"]= parsed["Dato"]["Motor"]["Numero"]
				data["combustible"]= parsed["Dato"]["Motor"]["Combustible"]
				data["cilinders"]= parsed["Dato"]["Motor"]["Cilindros"]
				data["cc"]= parsed["Dato"]["Motor"]["Cilindrada"]
				data["propietario"]=parsed["Dato"]["InformacionPropietario"][0]["Nombre"]
				#for k, v in data.items():
				#	print (k, v)
				return data

			else:
				#return empty dictionary
				print("Provided plate does not return info")
				return data

		else:
			print("Error in request")
			return data

	def getIdInfo(self,pid):
		num= str(pid)

		url="https://credid.net/wstest/api/Reporte?cedula="
		token="6F9B43BA-91B1-4684-AC60-CA0D587836A8" 
		r = requests.get(url+num, headers={'Authorization': token})
		status = r.status_code
		data ={}

		if status == 200:
			##
			print("CREDID getIdInfo Data")
			print(r.text)
			parsed = json.loads(r.text)
			
			if parsed["Tipo"] == "Nacional":
				data["type"] ="Nacional"
				data["name"] = parsed["FiliacionFisica"]["Nombre"]
				data["lastname1"] = parsed["FiliacionFisica"]["Apellido1"]
				data["lastname2"] = parsed["FiliacionFisica"]["Apellido2"]
				data["birthDate"] = parsed["FiliacionFisica"]["FechaNacimiento"]
				data["gender"] = parsed["FiliacionFisica"]["Genero"]
				data["relation"] = parsed["Matrimonios"][0]["Relacion"]
				data["dueDate"] = parsed["FiliacionFisica"]["VencimientoCedula"]
				#print(data["relation"])
				#for k, v in data.items():
				#	print (k, v)
				return data

			elif parsed["Tipo"] == "Juridica":
				data["type"] ="Juridica"
				data["name"] = parsed["FiliacionJuridica"]["Nombre"]
				#return empty dictionary
				#print("Provided plate does not return info")
				return data

		else:
			print("Error in request")
			return data


		

	def test(self):
		self.getData('fzj008');


#credid = Credid()
#mydict=credid.test()
	

