
from helpers.ApiAdapter import ApiAdapter
from helpers.NumberFormatter import NumberFormatter 
from xml.etree import ElementTree
import xml.etree.ElementTree as ET
import string,re, requests,sys
import traceback
from datetime import *; 
from dateutil.relativedelta import *

class InsAdaptee(ApiAdapter,NumberFormatter):
	def __init__(self):
		pass

	def getInsRequest(self, cotPack, year, value, brandCode, brandModel):	
		requestTemplate ='''
		<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
		xmlns:cot="http://www.ins-cr.com/servicios/transacciones/B2B/cotizacion/cotins" 
		xmlns:tran="http://www.ins-cr.com/servicios/transacciones/B2B/cotizacion/cotins">
		<soapenv:Header>
		<cot:SeguridadHeaderElement>
		<Empresa>4ECA61B312E931814E1721992F556554</Empresa>
		<Sistema>547AAE7CA9B020912F34D82775AF6F6B88628AE61B02639C298D54B3FCCD0F0F846C649C5C39111681789F8CE41DACEA</Sistema>
		<Direcciones>FCAB0BAF7C9207A2B7E45374CD77EFEF</Direcciones>
		</cot:SeguridadHeaderElement>
		</soapenv:Header>
		<soapenv:Body>
		<cot:CotizarPolizaRequest>
		<InfoSolicitud>
		<InfoTransaccion>
		<FechaHora>2019-04-11T12:46:55</FechaHora>
		</InfoTransaccion>
		<Cotizacion>
		<ConsecutivoConfiguracion>%s</ConsecutivoConfiguracion>
		<Solicitante>UsrDummy</Solicitante>
		<Parametros><![CDATA[<PARAMETROS><VIGDESDE>09/04/2019</VIGDESDE>
		<TIPOIDCLI>0</TIPOIDCLI><IDENTCLI>801170740</IDENTCLI>
		<FECNACICLI></FECNACICLI><PHOSINBUC></PHOSINBUC>
		<SERIERSGO>001</SERIERSGO><TIPOPLACARSGO>N</TIPOPLACARSGO><CLASEPLACA2RSGO>00</CLASEPLACA2RSGO>
		<NUMPLACARSGO></NUMPLACARSGO>
		<ANIORSGO>%s</ANIORSGO>
		<DIRCOMPCLI></DIRCOMPCLI>
		<VALDECRSGO>%s</VALDECRSGO>
		<CLASEPLACARSGO>PAR</CLASEPLACARSGO><MONTO1>0</MONTO1>
		<TIPDEDCOBK>B</TIPDEDCOBK><VIGHASTA></VIGHASTA><NOMCOMPLETOCLI>FUENTES MUÑIZ DAYSI MARGARITA</NOMCOMPLETOCLI>
		<PROVCLI>1</PROVCLI><CANTCLI>01</CANTCLI><DISTCLI>01</DISTCLI><TIPOTARIFRSGO>15</TIPOTARIFRSGO>
		<MARCARSGO>%s</MARCARSGO>
		<MODELORSGO>%s</MODELORSGO>
		<GENEROCLI>F</GENEROCLI><FACEXP>-45</FACEXP><VALORMERC>P</VALORMERC>
		<PORCDESCCOB>00</PORCDESCCOB><COB-A>S</COB-A><COB-C>S</COB-C><COB-D>S</COB-D><COB-F>S</COB-F><COB-H>S</COB-H><COB-N>S</COB-N><COB-G>S</COB-G><COB-L>N</COB-L><COB-M>S</COB-M><COB-J>N</COB-J>
		<CORREOE>mfuentes@insservicios.com</CORREOE>
		<COB-K>N</COB-K><COB-B>S</COB-B><LIMITECOBB>0</LIMITECOBB>
		<DECPERDTOTAL>NO</DECPERDTOTAL><TIPOLICENCIARSGO>0</TIPOLICENCIARSGO>
		<COB-E>N</COB-E><DUMMYDEDC>150000</DUMMYDEDC><DUMMYDEDDFH>150000</DUMMYDEDDFH><FECVRTV>01/01/2020</FECVRTV>
		<LIMITEMAXIMOA>300000000</LIMITEMAXIMOA><LIMITEMINIMOA>200000000</LIMITEMINIMOA><LIMITEMINIMOC>0</LIMITEMINIMOC>
		<TIPODEDUCIBLECOBN>A</TIPODEDUCIBLECOBN><TIPOASEGRSGO>01</TIPOASEGRSGO><COB-X>N</COB-X>
		<SUCEMI>09</SUCEMI><CALIFICACION>100</CALIFICACION><CAPACIDADRSGO>05</CAPACIDADRSGO>
		<CILINDRAJERSGO>04</CILINDRAJERSGO><COLORVEHICRSGO>02</COLORVEHICRSGO>
		<CONDICIONRSGO>B</CONDICIONRSGO><CUBICAJERSGO>0001</CUBICAJERSGO><MONTO8>04</MONTO8>
		<ESTILORSGO>01</ESTILORSGO><PESORSGO>1500</PESORSGO><NUMVINRSGO></NUMVINRSGO>
		<SERIEMOTORRSGO></SERIEMOTORRSGO><TIPOCARGRSGO>05</TIPOCARGRSGO>
		<TIPOCOMBUSTRSGO>01</TIPOCOMBUSTRSGO><TIPOPINTURARSGO>2</TIPOPINTURARSGO>
		<TIPOREMOLQUERSGO>0</TIPOREMOLQUERSGO><TIPOVEHICRSGO>03</TIPOVEHICRSGO>
		<VIACIRCRSGO>0001</VIACIRCRSGO>
		<SEGAPELLIDOCLI>MUÑIZ</SEGAPELLIDOCLI><SEGNOMBRECLI>MARGARITA</SEGNOMBRECLI>
		<PRIAPELLIDOCLI>FUENTES</PRIAPELLIDOCLI><PRINOMBRECLI>DAYSI</PRINOMBRECLI>
		<COB-I></COB-I><AGENTE>6602300</AGENTE>
		<CANTEQP>0</CANTEQP><DESCEQP></DESCEQP><REFEQP></REFEQP><VALOREQP>0</VALOREQP>
		</PARAMETROS>]]>
		</Parametros>
		</Cotizacion>
		</InfoSolicitud>
		</cot:CotizarPolizaRequest>
		</soapenv:Body>
		</soapenv:Envelope>
		''' % (cotPack, year, value, brandCode, brandModel)

		headers = {'content-type': 'application/soap+xml'}
		response = requests.post('http://serviciosweb-externos-desa.ins-cr.com/ins/servicios/externos/integracion/transacciones/cotizacion/cotins',headers=headers,data=requestTemplate.encode('utf-8'))
		
		# print('=================================================')
		# print(requestTemplate)
		# print('=================================================')

		# print('=================================================')
		# print(response.content)
		# print('=================================================')

		return (response.content)

	# emitInsPolicy(self, 'UsrDummy', '15/06/2019', '0', '110250602','23/01/1979', '', '001','N', '00','LBM179','2018', '50n de la plaza','20000000','PAR',0,'B','15/06/2020','David Lerroy Bernard Mora','1','01','01','15','005','040','M','-45','P','00', 'leroy.bernard@gmail.com','05', '04', '02','2000','04','1500','1234','5678','05','01','02','David','Lerroy','Bernard','Mora')
	def emitInsPolicy(self, cotPack, solicitante, fromDate, idType, clientId,birdDate, PHOSINBUC, SERIERSGO, plateType, CLASEPLACA2RSGO,plateNumber,year, address, amount,CLASEPLACARSGO,MONTO1,TIPDEDCOBK,toDate, fullName,stateId,countyId, districtId, TIPOTARIFRSGO, brandId, ModelId, Gender,FACEXP,VALORMERC,PORCDESCCOB, email,capacity, cilinders, colorId, cc, MONTO8,weigth, vinNumber, motorSerial,loadType, combustibleId,vehicleTypeId,firstName,middleName,lastName, secondLastName):	
		
		requestTemplate ='''
		<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cot="http://www.ins-cr.com/servicios/transacciones/B2B/cotizacion/cotins" xmlns:tran="http://www.ins-cr.com/servicios/transacciones/B2B/cotizacion/cotins">
	   <soapenv:Header>
	      <cot:SeguridadHeaderElement>
	         <Empresa>4ECA61B312E931814E1721992F556554</Empresa>
	         <Sistema>547AAE7CA9B020912F34D82775AF6F6B88628AE61B02639C298D54B3FCCD0F0F846C649C5C39111681789F8CE41DACEA</Sistema>
	         <Direcciones>E471012293DD321711723F717111ADF3</Direcciones>
	      </cot:SeguridadHeaderElement>
		   </soapenv:Header>
		   <soapenv:Body>
		      <cot:EmisionClienteRequest>
		         <InfoSolicitud>
		            <InfoTransaccion>
		               <FechaHora>2019-06-16T13:33:09</FechaHora>
		            </InfoTransaccion>
		            <Emision>
		               <ConConfiguracion>{44}</ConConfiguracion>
		               <Solicitante>{0}</Solicitante>
							<Parametros><![CDATA[<PARAMETROS>
							<VIGDESDE>{1}</VIGDESDE>
							<TIPOIDCLI>{2}</TIPOIDCLI>
							<IDENTCLI>{3}</IDENTCLI>
							<FECNACICLI>{4}</FECNACICLI>
							<PHOSINBUC>{5}</PHOSINBUC>
							<SERIERSGO>{6}</SERIERSGO>
							<TIPOPLACARSGO>{7}</TIPOPLACARSGO>
							<CLASEPLACA2RSGO>{8}</CLASEPLACA2RSGO>
							<NUMPLACARSGO>{9}</NUMPLACARSGO>
							<ANIORSGO>{10}</ANIORSGO>
							<DIRCOMPCLI>{11}</DIRCOMPCLI>
							<VALDECRSGO>{12}</VALDECRSGO>
							<CLASEPLACARSGO>{13}</CLASEPLACARSGO>
							<MONTO1>{14}</MONTO1>
							<TIPDEDCOBK>{15}</TIPDEDCOBK>
							<VIGHASTA>{16}</VIGHASTA>
							<NOMCOMPLETOCLI>{17}</NOMCOMPLETOCLI>
							<PROVCLI>{18}</PROVCLI>
							<CANTCLI>{19}</CANTCLI>
							<DISTCLI>{20}</DISTCLI>
							<TIPOTARIFRSGO>{21}</TIPOTARIFRSGO>
							<MARCARSGO>{22}</MARCARSGO>
							<MODELORSGO>{23}</MODELORSGO>
							<GENEROCLI>{24}</GENEROCLI>
							<FACEXP>{25}</FACEXP>
							<VALORMERC>{26}</VALORMERC>
							<PORCDESCCOB>{27}</PORCDESCCOB>
							<COB-A>S</COB-A>
							<COB-C>S</COB-C>
							<COB-D>S</COB-D>
							<COB-F>S</COB-F>
							<COB-H>S</COB-H>
							<COB-N>S</COB-N>
							<COB-G>S</COB-G>
							<COB-L>N</COB-L>
							<COB-M>S</COB-M>
							<COB-J>N</COB-J>
							<COB-K>N</COB-K>
							<COB-B>S</COB-B>
							<LIMITECOBB>0</LIMITECOBB>
							<DECPERDTOTAL>NO</DECPERDTOTAL>
							<TIPOLICENCIARSGO>0</TIPOLICENCIARSGO>
							<COB-E>N</COB-E>
							<DUMMYDEDC>150000</DUMMYDEDC>
							<DUMMYDEDDFH>150000</DUMMYDEDDFH>
							<FECVRTV>01/01/2020</FECVRTV>
							<LIMITEMAXIMOA>300000000</LIMITEMAXIMOA>
							<LIMITEMINIMOA>200000000</LIMITEMINIMOA>
							<LIMITEMINIMOC>0</LIMITEMINIMOC>
							<TIPODEDUCIBLECOBN>A</TIPODEDUCIBLECOBN>
							<TIPOASEGRSGO>01</TIPOASEGRSGO>
							<COB-X>N</COB-X>
							<SUCEMI>09</SUCEMI>
							<CALIFICACION>100</CALIFICACION>
							<CAPACIDADRSGO>{28}</CAPACIDADRSGO>
							<CILINDRAJERSGO>{29}</CILINDRAJERSGO>
							<COLORVEHICRSGO>{30}</COLORVEHICRSGO>
							<CONDICIONRSGO>B</CONDICIONRSGO>
							<CUBICAJERSGO>{31}</CUBICAJERSGO>
							<MONTO8>{32}</MONTO8>
							<ESTILORSGO>01</ESTILORSGO>
							<PESORSGO>{33}</PESORSGO>
							<NUMVINRSGO>{34}</NUMVINRSGO>
							<SERIEMOTORRSGO>{35}</SERIEMOTORRSGO>
							<TIPOCARGRSGO>{36}</TIPOCARGRSGO>
							<TIPOCOMBUSTRSGO>{37}</TIPOCOMBUSTRSGO>
							<TIPOPINTURARSGO>2</TIPOPINTURARSGO>
							<TIPOREMOLQUERSGO>0</TIPOREMOLQUERSGO>
							<TIPOVEHICRSGO>{38}</TIPOVEHICRSGO>
							<VIACIRCRSGO>0001</VIACIRCRSGO>
							<PRINOMBRECLI>{39}</PRINOMBRECLI>
							<SEGNOMBRECLI>{40}</SEGNOMBRECLI>
							<PRIAPELLIDOCLI>{41}</PRIAPELLIDOCLI>
							<SEGAPELLIDOCLI>{42}</SEGAPELLIDOCLI>

							<CORREOE>{43}</CORREOE>

							<COB-I></COB-I>
							<AGENTE>6602300</AGENTE>
							<CANTEQP>0</CANTEQP><DESCEQP></DESCEQP><REFEQP></REFEQP><VALOREQP>0</VALOREQP>
		
							</PARAMETROS>]]>
					</Parametros>
		            </Emision>
		         </InfoSolicitud>
		      </cot:EmisionClienteRequest>
		   </soapenv:Body>
		</soapenv:Envelope>

		'''.format(solicitante, fromDate, idType, clientId,birdDate, PHOSINBUC, SERIERSGO, plateType, CLASEPLACA2RSGO,plateNumber,year, address, amount,CLASEPLACARSGO,MONTO1,TIPDEDCOBK,toDate, fullName,stateId,countyId, districtId, TIPOTARIFRSGO, brandId, ModelId, Gender,FACEXP,VALORMERC,PORCDESCCOB,capacity, cilinders, colorId, cc, MONTO8,weigth, vinNumber, motorSerial,loadType, combustibleId,vehicleTypeId,firstName,middleName,lastName, secondLastName, email,cotPack)
		headers = {'content-type': 'application/soap+xml'}
		response = requests.post('http://serviciosweb-externos-desa.ins-cr.com/ins/servicios/externos/integracion/transacciones/cotizacion/cotins',headers=headers,data=requestTemplate.encode('utf-8'))
		print('=================================================')
		print(requestTemplate)
		print('=================================================')

		print('=================================================')
		print(response.content)
		print('=================================================')

		return (response.content)

	def getMockResponse(self):
		return '''<?xml version="1.0" encoding="utf-8"?>
				<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
				<soapenv:Body>
				  <NS1:CotizarPolizaResponse xmlns:NS1="http://www.ins-cr.com/servicios/transacciones/B2B/cotizacion/cotins">
				    <InfoRespuesta>
				      <CodEstado>00</CodEstado>
				      <NumCotizacion>CT-TMP-2019-00005667</NumCotizacion>
				      <Moneda>Colones</Moneda>
				      <Observaciones/>
				      <Primas>
				        <Prima>
				          <ProductoCalculo>170</ProductoCalculo>
				          <FrecuenciaPago>Costo Semestral (I.V.I) :</FrecuenciaPago>
				          <PrimaPago>129377</PrimaPago>
				        </Prima>
				      </Primas>
				      <DetalleCotizacion>&lt;REPORTE&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;Tipo tarifa&lt;/CAMPO&gt;&lt;VALOR&gt;15&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;Tipo de vehiculo&lt;/CAMPO&gt;&lt;VALOR&gt;03&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;A\xc3\xb1o&lt;/CAMPO&gt;&lt;VALOR&gt;2019&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;Peso&lt;/CAMPO&gt;&lt;VALOR&gt;1500&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;Tipo de combustible&lt;/CAMPO&gt;&lt;VALOR&gt;01&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;Cantidad de pasajeros&lt;/CAMPO&gt;&lt;VALOR&gt;05&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;\xc2\xbfEs veh\xc3\xadculo usado o nuevo 0 kilometros?&lt;/CAMPO&gt;&lt;VALOR&gt;P&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;\xc2\xbfFue P\xc3\xa9rdida Total anteriormente?&lt;/CAMPO&gt;&lt;VALOR&gt;NO&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;\xc2\xbfDe uso personal y comercial?&lt;/CAMPO&gt;&lt;VALOR&gt;04&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;\xc2\xbfTipo de dispositivo de seguridad?&lt;/CAMPO&gt;&lt;VALOR&gt;00&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;Tipo de aseguramiento&lt;/CAMPO&gt;&lt;VALOR&gt;01&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;\xc2\xbfDesea incluir beneficio por alcohol?&lt;/CAMPO&gt;&lt;VALOR&gt;0&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;Opciones Cobertura "N"&lt;/CAMPO&gt;&lt;VALOR&gt;A&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;/REPORTE&gt;&lt;COBERTURAS&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;R. C. Lesion o Muerte Terceros&lt;/NOMBRE&gt;&lt;CODIGO&gt;17A&lt;/CODIGO&gt;&lt;MONTO&gt;300000000.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;10825.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;Serv.Medic.B\xc3\xa1sica&lt;/NOMBRE&gt;&lt;CODIGO&gt;17B&lt;/CODIGO&gt;&lt;MONTO&gt;10000000.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;619.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;R. C. Da/os Propiedad Terceros&lt;/NOMBRE&gt;&lt;CODIGO&gt;17C&lt;/CODIGO&gt;&lt;MONTO&gt;100000000.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;30303.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;Colisi\xc3\xb3n y/o vuelco&lt;/NOMBRE&gt;&lt;CODIGO&gt;17D&lt;/CODIGO&gt;&lt;MONTO&gt;6000000.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;49731.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;Robo y Hurto&lt;/NOMBRE&gt;&lt;CODIGO&gt;17F&lt;/CODIGO&gt;&lt;MONTO&gt;6000000.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;6567.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;Asistencia en Viaje&lt;/NOMBRE&gt;&lt;CODIGO&gt;17G&lt;/CODIGO&gt;&lt;MONTO&gt;0.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;0.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;Riesgos Adicionales&lt;/NOMBRE&gt;&lt;CODIGO&gt;17H&lt;/CODIGO&gt;&lt;MONTO&gt;6000000.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;2271.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;Multiasistencia Automoviles&lt;/NOMBRE&gt;&lt;CODIGO&gt;17M&lt;/CODIGO&gt;&lt;MONTO&gt;0.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;6820.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;Exenci\xc3\xb3n de Deducible&lt;/NOMBRE&gt;&lt;CODIGO&gt;17N&lt;/CODIGO&gt;&lt;MONTO&gt;0.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;7428.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;Recargo Fraccionamiento Prima&lt;/NOMBRE&gt;&lt;CODIGO&gt;996&lt;/CODIGO&gt;&lt;MONTO&gt;0.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;0.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;Impuesto a las Ventas&lt;/NOMBRE&gt;&lt;CODIGO&gt;998&lt;/CODIGO&gt;&lt;MONTO&gt;0.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;14813.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;/COBERTURAS&gt;</DetalleCotizacion>
				    </InfoRespuesta>
				  </NS1:CotizarPolizaResponse>
				</soapenv:Body>
				</soapenv:Envelope>
				'''

	# emitInsPolicyEnCarretera(self, 'UsrDummy',0,'110250602', 'Bernard', 'Mora', 'David', 'Lerroy', 'M', '23/01/1979','02',88941188,'4','07','01', 'leroy.bernard@gmail.com', '29/06/2019','29/06/2020','','09','A',0, 0, 40)
	def emitInsPolicyEnCarretera(self, petitioner,typeId,idNumber, lastName, secondLastName, name, middleName, gender, birtDate,
		clientType,phoneNumber,stateId, countyId, districtId, email,policyStartDate, policyDueDate,policyNumber,
		sucemi,paymentType,monto2, monto1,age):	
		
		now = datetime.now()
		currentDate=str(now.strftime('%d/%m/%Y'))
		

		requestTemplate ='''
		<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cot="http://www.ins-cr.com/servicios/transacciones/B2B/cotizacion/cotins" xmlns:tran="http://www.ins-cr.com/servicios/transacciones/B2B/cotizacion/cotins">
		   <soapenv:Header>
		      <cot:SeguridadHeaderElement>
		         <Empresa>4ECA61B312E931814E1721992F556554</Empresa>
		         <Sistema>547AAE7CA9B020912F34D82775AF6F6B88628AE61B02639C298D54B3FCCD0F0F846C649C5C39111681789F8CE41DACEA</Sistema>
		         <Direcciones>FCAB0BAF7C9207A2B7E45374CD77EFEF</Direcciones>
		      </cot:SeguridadHeaderElement>
		   </soapenv:Header>
		   <soapenv:Body>
		      <cot:EmisionClienteRequest>
		         <InfoSolicitud>
		            <InfoTransaccion>
		               <FechaHora>{0}</FechaHora>
		            </InfoTransaccion>
		            <Emision>
		               <ConConfiguracion>906</ConConfiguracion>
		               <Solicitante>{1}</Solicitante>
		               <Parametros><![CDATA[
		               <PARAMETROS>
							<TIPOIDCLI>{2}</TIPOIDCLI>
							<IDENTCLI>{3}</IDENTCLI>
							<PRIAPELLIDOCLI>{4}</PRIAPELLIDOCLI>
							<SEGAPELLIDOCLI>{5}</SEGAPELLIDOCLI>
							<PRINOMBRECLI>{6}</PRINOMBRECLI>
							<SEGNOMBRECLI>{7}</SEGNOMBRECLI>
							<NOMCOMPLETOCLI>'NOMBRE'</NOMCOMPLETOCLI>
							<GENEROCLI>{8}</GENEROCLI>
							<FECNACICLI>{9}</FECNACICLI>
							<TIPOTELCLI>{10}</TIPOTELCLI>
							<NUMTELCLI>{11}</NUMTELCLI>
							<PROVCLI>{12}</PROVCLI>
							<CANTCLI>{13}</CANTCLI>
							<DISTCLI>{14}</DISTCLI>
							<CORREOE>{15}</CORREOE>
							<VIGDESDE>{16}</VIGDESDE>
							<VIGHASTA>{17}</VIGHASTA>
							<NUMPOLIZA>{18}</NUMPOLIZA>
							<SUCEMI>{19}</SUCEMI>
							<AGENTE>6602300</AGENTE>
							<FORMAPAGO>{20}</FORMAPAGO>
							<DIRECRSGO>Casa</DIRECRSGO>
							<MONTO2>{21}</MONTO2>
							<NUMOPERACION>0002</NUMOPERACION>
							<NUMOPERACION>0001</NUMOPERACION>
							<NUMOPERACION>0003</NUMOPERACION>
							<OBSERVACIONES>EU</OBSERVACIONES>
							<MONTO1>{22}</MONTO1>
							<CBEDAD>{23}</CBEDAD>
							<TIPOACTRSGO>0035</TIPOACTRSGO>
							<MOASR>NO</MOASR>
							<TIPORIESGOCOB>01</TIPORIESGOCOB>
							<TIPOTARIFACOB>0001</TIPOTARIFACOB>
							<TIPOTARIFACOB>0003</TIPOTARIFACOB>
							<TIPOTARIFACOB>0002</TIPOTARIFACOB>
							<SUMASEGRSGO>1</SUMASEGRSGO>
							<PHOSINBUC>22222222</PHOSINBUC>
							<DIRECSINBUC/>
							<PROVSINBUC/>
							<TIPOIDBENEF>0</TIPOIDBENEF>
							<IDBENEF/>
							<NOMCOMPBENEF/>
							<PARENTESCOBENEF/>
							<PORCENTAJEBENEF>0</PORCENTAJEBENEF>
							<NOMBENEF/>
							<PRIAPEBENEF/>
							<SEGAPEBENEF/>
							<TIPOIDBENEF>0</TIPOIDBENEF>
							<IDBENEF/>
							<NOMCOMPBENEF/>
							<PARENTESCOBENEF/>
							<PORCENTAJEBENEF>0</PORCENTAJEBENEF>
							<NOMBENEF/>
							<PRIAPEBENEF/>
							<SEGAPEBENEF/>
							<TIPOIDBENEF>0</TIPOIDBENEF>
							<IDBENEF/>
							<NOMCOMPBENEF/>
							<PARENTESCOBENEF/>
							<PORCENTAJEBENEF>0</PORCENTAJEBENEF>
							<NOMBENEF/>
							<PRIAPEBENEF/>
							<SEGAPEBENEF/>
							<TIPOIDBENEF>0</TIPOIDBENEF>
							<IDBENEF/>
							<NOMCOMPBENEF/>
							<PARENTESCOBENEF/>
							<PORCENTAJEBENEF>0</PORCENTAJEBENEF>
							<NOMBENEF/>
							<PRIAPEBENEF/>
							<SEGAPEBENEF/>
						</PARAMETROS>
		               
		               ]]>
					</Parametros>
		            </Emision>
		         </InfoSolicitud>
		      </cot:EmisionClienteRequest>
		   </soapenv:Body>
		</soapenv:Envelope>

		'''.format(now, petitioner,typeId,idNumber, lastName, secondLastName, name, middleName, gender, birtDate,clientType,phoneNumber,stateId, countyId, districtId, email,policyStartDate, policyDueDate,policyNumber,sucemi,paymentType,monto2, monto1,age)
		headers = {'content-type': 'application/soap+xml'}
		response = requests.post('http://serviciosweb-externos-desa.ins-cr.com/ins/servicios/externos/integracion/transacciones/cotizacion/cotins',headers=headers,data=requestTemplate.encode('utf-8'))
		
		print(requestTemplate)
		return (response.content)

	def getMockResponse(self):
		return '''<?xml version="1.0" encoding="utf-8"?>
				<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
				<soapenv:Body>
				  <NS1:CotizarPolizaResponse xmlns:NS1="http://www.ins-cr.com/servicios/transacciones/B2B/cotizacion/cotins">
				    <InfoRespuesta>
				      <CodEstado>00</CodEstado>
				      <NumCotizacion>CT-TMP-2019-00005667</NumCotizacion>
				      <Moneda>Colones</Moneda>
				      <Observaciones/>
				      <Primas>
				        <Prima>
				          <ProductoCalculo>170</ProductoCalculo>
				          <FrecuenciaPago>Costo Semestral (I.V.I) :</FrecuenciaPago>
				          <PrimaPago>129377</PrimaPago>
				        </Prima>
				      </Primas>
				      <DetalleCotizacion>&lt;REPORTE&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;Tipo tarifa&lt;/CAMPO&gt;&lt;VALOR&gt;15&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;Tipo de vehiculo&lt;/CAMPO&gt;&lt;VALOR&gt;03&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;A\xc3\xb1o&lt;/CAMPO&gt;&lt;VALOR&gt;2019&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;Peso&lt;/CAMPO&gt;&lt;VALOR&gt;1500&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;Tipo de combustible&lt;/CAMPO&gt;&lt;VALOR&gt;01&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;Cantidad de pasajeros&lt;/CAMPO&gt;&lt;VALOR&gt;05&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;\xc2\xbfEs veh\xc3\xadculo usado o nuevo 0 kilometros?&lt;/CAMPO&gt;&lt;VALOR&gt;P&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;\xc2\xbfFue P\xc3\xa9rdida Total anteriormente?&lt;/CAMPO&gt;&lt;VALOR&gt;NO&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;\xc2\xbfDe uso personal y comercial?&lt;/CAMPO&gt;&lt;VALOR&gt;04&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;\xc2\xbfTipo de dispositivo de seguridad?&lt;/CAMPO&gt;&lt;VALOR&gt;00&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;Tipo de aseguramiento&lt;/CAMPO&gt;&lt;VALOR&gt;01&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;\xc2\xbfDesea incluir beneficio por alcohol?&lt;/CAMPO&gt;&lt;VALOR&gt;0&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;COTIZACIONDATOS&gt;&lt;CAMPO&gt;Opciones Cobertura "N"&lt;/CAMPO&gt;&lt;VALOR&gt;A&lt;/VALOR&gt;&lt;/COTIZACIONDATOS&gt;&lt;/REPORTE&gt;&lt;COBERTURAS&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;R. C. Lesion o Muerte Terceros&lt;/NOMBRE&gt;&lt;CODIGO&gt;17A&lt;/CODIGO&gt;&lt;MONTO&gt;300000000.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;10825.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;Serv.Medic.B\xc3\xa1sica&lt;/NOMBRE&gt;&lt;CODIGO&gt;17B&lt;/CODIGO&gt;&lt;MONTO&gt;10000000.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;619.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;R. C. Da/os Propiedad Terceros&lt;/NOMBRE&gt;&lt;CODIGO&gt;17C&lt;/CODIGO&gt;&lt;MONTO&gt;100000000.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;30303.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;Colisi\xc3\xb3n y/o vuelco&lt;/NOMBRE&gt;&lt;CODIGO&gt;17D&lt;/CODIGO&gt;&lt;MONTO&gt;6000000.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;49731.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;Robo y Hurto&lt;/NOMBRE&gt;&lt;CODIGO&gt;17F&lt;/CODIGO&gt;&lt;MONTO&gt;6000000.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;6567.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;Asistencia en Viaje&lt;/NOMBRE&gt;&lt;CODIGO&gt;17G&lt;/CODIGO&gt;&lt;MONTO&gt;0.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;0.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;Riesgos Adicionales&lt;/NOMBRE&gt;&lt;CODIGO&gt;17H&lt;/CODIGO&gt;&lt;MONTO&gt;6000000.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;2271.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;Multiasistencia Automoviles&lt;/NOMBRE&gt;&lt;CODIGO&gt;17M&lt;/CODIGO&gt;&lt;MONTO&gt;0.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;6820.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;Exenci\xc3\xb3n de Deducible&lt;/NOMBRE&gt;&lt;CODIGO&gt;17N&lt;/CODIGO&gt;&lt;MONTO&gt;0.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;7428.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;Recargo Fraccionamiento Prima&lt;/NOMBRE&gt;&lt;CODIGO&gt;996&lt;/CODIGO&gt;&lt;MONTO&gt;0.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;0.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;COBERTURA&gt;&lt;NOMBRE&gt;Impuesto a las Ventas&lt;/NOMBRE&gt;&lt;CODIGO&gt;998&lt;/CODIGO&gt;&lt;MONTO&gt;0.00&lt;/MONTO&gt;&lt;MONTOPRIMA&gt;14813.00&lt;/MONTOPRIMA&gt;&lt;/COBERTURA&gt;&lt;/COBERTURAS&gt;</DetalleCotizacion>
				    </InfoRespuesta>
				  </NS1:CotizarPolizaResponse>
				</soapenv:Body>
				</soapenv:Envelope>
				'''

	def getInsDataAutoPercent(self, year, value, brandCode, brandModel):
		return self.getInsData( 914, year, value, brandCode, brandModel)
	
	def getInsDataAutoFixed(self, year, value, brandCode, brandModel):
		return self.getInsData( 915, year, value, brandCode, brandModel)
	
	def getInsData(self, cotPack, year, value, brandCode, brandModel):
		#print("getInsData Params:", value, brandCode, brandModel)
		result = {}
		try:
			#responseIns = self.getMockResponse()#self.getInsRequest(self, year, value, brandCode, brandModel)
			responseIns = self.getInsRequest(cotPack, year, value, brandCode, brandModel)
			#print("getInsData response:", responseIns)

			dom = ElementTree.fromstring(responseIns)

			# define namespace mappings to use as shorthand below
			namespaces = {
			 'soapenv': 'http://schemas.xmlsoap.org/soap/envelope/',
			 'NS1': 'http://www.ins-cr.com/servicios/transacciones/B2B/cotizacion/cotins'
			}
			
			# reference the namespace mappings here by `<name>:`
			cdata = dom.find(
			 './soapenv:Body'
			 '/NS1:CotizarPolizaResponse'
			 '/InfoRespuesta/DetalleCotizacion',
			 namespaces,
			)

			cdata1 = dom.find(
			 './soapenv:Body'
			 '/NS1:CotizarPolizaResponse'
			 '/InfoRespuesta/Primas',
			 namespaces,
			)

			domIns = ElementTree.fromstring('<ROOT>' + cdata.text + '</ROOT>')

			#print(cdata.text)
			#get values
			coberturas = self.getCoberturas(domIns)
			#print(coberturas)
			primas = self.getPrima(dom)
			#print(primas['PrimaPago'])

			cobertura17A = coberturas['17A']
			cobertura17B = coberturas['17B']
			cobertura17C = coberturas['17C']
			cobertura17D = coberturas['17D']
			cobertura17F = coberturas['17F']
			cobertura17G = coberturas['17G']
			cobertura17H = coberturas['17H']
			cobertura17N = coberturas['17N']
			cobertura17M = coberturas['17M']
			cobertura996 = coberturas['996']
			cobertura998 = coberturas['998']
			
			#Store result
			result['Pago Anual'] = self.formatValue(str(float(primas['PrimaPago'])*2))
			result['Pago Semestral'] = self.formatValue(primas['PrimaPago'])
			result['Pago Trimestral'] = self.formatValue(str(float(primas['PrimaPago'])/2))
			result['Pago Mensual'] = self.formatValue(str(float(primas['PrimaPago'])/6))

			result['Responsabilidad Civil Personas'] = self.formatValue(cobertura17A['MONTO'])
			result['Responsabilidad Civil Bienes'] = self.formatValue(cobertura17C['MONTO']) 
			result['Responsabilidad Civil Alcohol'] = 'Amparada' 
			result['Responsabilidad Civil Daño Moral'] = 'Hasta 25% del monto asegurado en conciliaciones, hasta 100% del monto asegurado contra Fallo en Firme.'

			result['Colisión y vuelco'] = self.formatValue(cobertura17D['MONTO'])
			result['Hurto y Robo'] = self.formatValue(cobertura17F['MONTO'])
			result['Riesgos Adicionales y Rotura de Cristales'] = self.formatValue(cobertura17H['MONTO'])
			result['Servicios médicos'] = self.formatValue(cobertura17B['MONTO'])
			result['Gastos funerarios'] =  'No Amparada'
			result['Gastos legales'] = 'No Amparada' 
			
		except Exception:
			result=self.getDefaultQuotation()
			print (traceback.print_exc(file=sys.stdout))
			#print("Exception ocurred with getInsRequest. Params:", year, value, brandCode, brandModel)
			
		return result


	def insExample(self):
		
		#Marca  VOLVO, Codigo Marca 079, Modelo  XC60, CodogoModelo 028	
		#test=myAdaptee.getInsData("2019","60000000","079","028")
		#print(test)
		#myAdaptee.parseInsResponse(test)
		result = self.getInsData( "2019","60000000","079","028")
		print(result)


	def getChild(dom, path):
		result = {}
		elements = dom.find(path)
		for child in elements:
			# print ("getChild: ", child.tag, child.text)
			result[child.tag] = child.text
		return result

	#example getPrima
	#primas = getPrima(dom)
	#print(primas['PrimaPago'])
	def getPrima(self, dom):
		namespaces = {
		 'soapenv': 'http://schemas.xmlsoap.org/soap/envelope/',
		 'NS1': 'http://www.ins-cr.com/servicios/transacciones/B2B/cotizacion/cotins'
		}

		__cdata = dom.find(
		'./soapenv:Body'
		'/NS1:CotizarPolizaResponse'
		'/InfoRespuesta/Primas/',
		namespaces,
		)

		__result = {}
		for child in __cdata:
			# print ("getChild: ", child.tag, child.text)
			__result[child.tag] = child.text
		return __result

	
	def getChild(self, dom, path):
		result = {}
		elements = dom.find(path)
		for child in elements:
			result[child.tag] = child.text
		return result

	#coberturas = getcoberturas(domIns )
	#cover17a = coberturas['17A']
	#print(cover17a['MONTO'])
	def getCoberturas(self, dom):
		result = {}
		elements = dom.findall('COBERTURAS/COBERTURA')
		for item in elements:

			currentCobertura = {}
			for child in item:
				currentCobertura[child.tag] = child.text
			result[currentCobertura["CODIGO"]] = currentCobertura 
			currentCobertura = {}
		return result

# myAdaptee = InsAdaptee()
# res = myAdaptee.emitInsPolicy( 914,'UsrDummy', '15/06/2019', '0', '110250602','23/01/1979', '', '001','N', '00','LBM179','2018', '50n de la plaza','20000000','PAR',0,'B','15/06/2020','David Lerroy Bernard Mora','1','01','01','15','005','040','M','-45','P','00', 'leroy.bernard@gmail.com','05', '04', '02','2000','04','1500','1234','5678','05','01','02','David','Lerroy','Bernard','Mora')
#print(res)
#policy = myAdaptee.emitInsPolicy('UsrDummy', '15/06/2019', '0', '110250602','23/01/1979', '', '001','N', '00','LBM179','2018', '50n de la plaza','20000000','PAR',0,'B','15/06/2020','David Lerroy Bernard Mora','1','01','01','15','005','040','M','-45','P','00', 'leroy.bernard@gmail.com','05', '04', '02','2000','04','1500','1234','5678','05','01','02','David','Lerroy','Bernard','Mora')
#result = myAdaptee.getInsDataAutoPercent(2019, 20000000, '005','040')	
