from ApiAdapter import ApiAdapter
from xml.etree import ElementTree
import xml.etree.ElementTree as ET
import string,re


class DumpOceanicaCatalog(ApiAdapter):
	def __init__(self):
		pass
		
	def getVehicleBrands(self, brandDesciption):
		method = 'POST'
		auth=('wservice', 'h3r3d14')
		url='http://portal.oceanica-cr.com:9003/orawsv/WSERVICE/PR_WEBSERVICE/DEV_MARCA_VEH'
		headers = {'content-type': 'application/soap+xml'} 
		requestTemplate = '''
		<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dev="http://xmlns.oracle.com/orawsv/WSERVICE/PR_WEBSERVICE/DEV_MARCA_VEH">
		   <soapenv:Header/>
		   <soapenv:Body>
		      <dev:DEV_MARCA_VEHInput>
		         <dev:PCODMARCA-VARCHAR2-OUT/>
		         <dev:PDESCMARCA-VARCHAR2-INOUT>%s</dev:PDESCMARCA-VARCHAR2-INOUT>
		      </dev:DEV_MARCA_VEHInput>
		   </soapenv:Body>
		</soapenv:Envelope>
		''' % (brandDesciption)
		#print(requestTemplate)
		result = self.apiRequest(url, headers, method, requestTemplate.encode('utf-8'), auth)
		return result


	def getVehicleBrandModels(self, brandCode):
		method = 'POST'
		auth=('wservice', 'h3r3d14')
		url='http://portal.oceanica-cr.com:9003/orawsv/WSERVICE/PR_WEBSERVICE/DEV_MODELO_VEH'
		headers = {'content-type': 'application/soap+xml'} 
		requestTemplate='''
		<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dev="http://xmlns.oracle.com/orawsv/WSERVICE/PR_WEBSERVICE/DEV_MODELO_VEH">
			<soapenv:Header/>
			<soapenv:Body>
			<dev:DEV_MODELO_VEHInput>
			<dev:PCODMARCA-VARCHAR2-IN>%s</dev:PCODMARCA-VARCHAR2-IN>
			<dev:PCODMODELO-VARCHAR2-INOUT></dev:PCODMODELO-VARCHAR2-INOUT>
			<dev:PDESCMODELO-VARCHAR2-INOUT></dev:PDESCMODELO-VARCHAR2-INOUT>
			</dev:DEV_MODELO_VEHInput>
			</soapenv:Body>
		</soapenv:Envelope>
		''' % (brandCode)
		result = self.apiRequest(url, headers, method, requestTemplate.encode('utf-8'), auth)
		return result

	def fillVehicleBrandsDictionary(self,response):
		
		myDict ={}
		namespaces = {
  		'soap': 'http://schemas.xmlsoap.org/soap/envelope/',
  		'oceanica':'http://xmlns.oracle.com/orawsv/WSERVICE/PR_WEBSERVICE/DEV_MARCA_VEH' 
		}
		
		dom = ElementTree.fromstring(response)
		
		# reference the namespace mappings here by `<name>:`
		names = dom.findall(
		  './soap:Body'
		  '/oceanica:DEV_MARCA_VEHOutput'
		  '/oceanica:PDESCMARCA',
		  namespaces,
		)
		codes = dom.findall(
		  './soap:Body'
		  '/oceanica:DEV_MARCA_VEHOutput'
		  '/oceanica:PCODMARCA',
		  namespaces,
		)

		# for name in names:
		# 	print(name.text)
		
		# for code in codes:
		# 	print(code.text)

		namesList = str(names[0].text).split(",")
		#namesList.pop(0)
		codesList = str(codes[0].text).split(",")
		length = len(namesList)

		for i in range(length): 
		    myDict[namesList[i]] = codesList[i]

		return myDict

	def fillVehicleModelsDictionary(self,response):
		
		myDict ={}
		namespaces = {
  		'soap': 'http://schemas.xmlsoap.org/soap/envelope/',
  		'oceanica':'http://xmlns.oracle.com/orawsv/WSERVICE/PR_WEBSERVICE/DEV_MODELO_VEH' 
		}
		dom = ElementTree.fromstring(response)
		
		# reference the namespace mappings here by `<name>:`
		names = dom.findall(
		  './soap:Body'
		  '/oceanica:DEV_MODELO_VEHOutput'
		  '/oceanica:PDESCMODELO',
		  namespaces,
		)
		codes = dom.findall(
		  './soap:Body'
		  '/oceanica:DEV_MODELO_VEHOutput'
		  '/oceanica:PCODMODELO',
		  namespaces,
		)

		namesList = str(names[0].text).split(";")
		codesList = str(codes[0].text).split(";")
		length = len(namesList)

		for i in range(length): 
		    myDict[namesList[i]] = codesList[i]
		
		return myDict

		
myOcean=DumpOceanicaCatalog()

#vehicleBrandModels=myOcean.getVehicleBrandModels("008")
#myModelsDict=myOcean.fillVehicleModelsDictionary(vehicleBrandModels)

#Dictionary containing Brands and Codes for current alphabet letter
#vehicleInfo=myOcean.fillVehicleDictionary(vehicleBrands)

f = open("/home/tomodashi/Desktop/test.sql", "w")
counter=0

#for each letter in alphabet
for x in list(string.ascii_lowercase):
	
	#get brands for leter 'x'
	brandsForLetter=myOcean.getVehicleBrands(x)
	brandsDictionary={}

	#get a dictionary 'brand':'code' for letter x
	try:
		brandsDictionary=myOcean.fillVehicleBrandsDictionary(brandsForLetter)
	except:
		print('Response {res}:'.format(res=brandsForLetter))
	#print(brandsDictionary)

	#iterate over dicionary to get models for brand x
	for key, value in brandsDictionary.items():
		#print('key: {key}, value: {value}'.format(key=key, value=value))
	
	 	#getting models
	 	brandCode=str(value).replace(" ", "")
	 	
	 	#print('brand code:{code}'.format(code=brandCode))
	 	brandModels= myOcean.getVehicleBrandModels(str(brandCode))
	 	#print(brandModels)
	 	brandModelsDicionary={}
	 	regex = re.compile('[%s]' % re.escape(string.punctuation))

	 	#dictionary for models given a brand
	 	try:
	 		brandModelsDicionary=myOcean.fillVehicleModelsDictionary(brandModels)
	 	except:
	 		print('Response: {response}:'.format(response=brandModels))
	 	
	 	#print('descripcion de modelo: {dict}'.format(dict=brandModelsDicionary))
	 	for key1, value1 in brandModelsDicionary.items():

	 		key = "".join(key.split())
	 		key1 = "".join(key1.split())
	 		value = "".join(value.split())
	 		value1 = "".join(value1.split())

	 		keyM =regex.sub('', key)
	 		key1M  = regex.sub('', key1)
	 		valueM = regex.sub('', value)
	 		value1M = regex.sub('', value1) 
	 		
	 		print('Marca {brand}, Codigo Marca {brandCode}, Modelo {model}, CodogoModelo {modelCode}'.format(brand=key,brandCode=value,model=key1,modelCode=value1))
	 		f.write("INSERT INTO insurance_OceanicaCatalogModel(brandcode, brandDescription,brandModelCode,brandModelDescription)VALUES('{bc}','{bd}','{bmc}','{bmd}');\n".format(bd=key,bc=value,bmd=key1,bmc=value1))
	 		counter += 1
			#print('Current insertion number: {count}'.format(count=counter))
print("done!")