import requests
from io import StringIO
#from xml.dom import minidom
#import untangle

from requests.exceptions import HTTPError
from django.conf import settings

import xmltodict,json
from zeep import Client
from zeep.wsse.username import UsernameToken
from xml.etree import ElementTree as ET



class ApiAdapter:
	def __init__(self):
		pass
	def exampleRequest(self):
		request = '''
		<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dev="http://xmlns.oracle.com/orawsv/WSERVICE/PR_WEBSERVICE/DEV_MARCA_VEH">
		   <soapenv:Header/>
		   <soapenv:Body>
		      <dev:DEV_MARCA_VEHInput>
		         <dev:PCODMARCA-VARCHAR2-OUT/>
		         <dev:PDESCMARCA-VARCHAR2-INOUT>A</dev:PDESCMARCA-VARCHAR2-INOUT>
		      </dev:DEV_MARCA_VEHInput>
		   </soapenv:Body>
		</soapenv:Envelope>
		'''.encode('utf-8')
		headers = {'content-type': 'application/soap+xml'}
		#req = requests.Request('POST', 'http://portal.oceanica-cr.com:9003/orawsv/WSERVICE/PR_WEBSERVICE/DEV_MARCA_VEH',headers=headers,data=request,auth=('wservice', 'h3r3d14'))
	
		response = requests.post('http://portal.oceanica-cr.com:9003/orawsv/WSERVICE/PR_WEBSERVICE/DEV_MARCA_VEH',headers=headers,data=request,auth=('wservice', 'h3r3d14'))
		#print (response.content)

		#prepped_requ = req.prepare()
		#s = requests.Session()
		#http_response = s.send(prepped_requ)
		#print(http_response)
		#print(requests)

	def parseXML(self, xml):
		obj = untangle.parse(StringIO(xml))
		return obj

	#returns a dictionary with default values
	def getDefaultQuotation(self):
		result = {}

		result['Pago Anual'] = 'Consultar' 
		result['Pago Semestral'] = 'Consultar'  
		result['Pago Trimestral'] = 'Consultar' 
		result['Pago Mensual'] = 'Consultar' 

		result['Responsabilidad Civil Personas'] = 'Máximo de ₡%s por persona y máximo de ₡%s por accidente' % ('300,000,000.00', '300,000,000.00')
		result['Responsabilidad Civil Bienes'] = 'Máximo de ₡%s por persona y máximo de ₡%s por accidente' % ('300,000,000.00', '300,000,000.00')
		result['Responsabilidad Civil Alcohol'] = 'No Amparada' 
		result['Responsabilidad Civil Daño Moral'] =  'Hasta 25% del monto asegurado en conciliaciones, hasta 100% del monto asegurado contra Fallo en Firme.'

		result['Colisión y vuelco'] =  '₡' + '12,500,000.00'
		result['Hurto y Robo'] = '₡' + '12,500,000.00'
		result['Riesgos Adicionales y Rotura de Cristales'] = '₡' + '12,500,000.00'
		result['Servicios médicos'] = '₡' + '10,000,000.00'
		result['Gastos funerarios'] =  '₡' + '2,000,000.00'
		result['Gastos legales'] = 'No Amparada'

		return result
		
	def apiRequest(self, url, headers, method, request, auth):
		response=''
		try:
			if method == 'POST':
				response = requests.post(url,headers=headers,data=request,auth=auth,timeout=60)
				#print(response.content)

		except HTTPError as http_err:
			#print(f'HTTP error occurred: {http_err}')  # Python 3.6
			print('Error in ApiAdapter')
		except Exception as err:
			#print(f'Other error occurred: {err}')  # Python 3.6
			print('Error in ApiAdapter')	
		#else:
		#	print('Success!')
		#return self.parseXML(response.content.decode('utf-8'))
		if(response):
			#print('Response from endpoint: {res}'.format(res=response.text))
			return response.content
		else:
			print('Response from endpoint is empty')
			return ''


	def registerPayment(self,policy,agent):

		if(len(policy)>11):
			policy=policy[2:13] 
			print('modified policy {pol}'.format(pol=policy))

		wsdl = 'https://www.qualitas.co.cr:8203/wsPagos/wsPagos?wsdl'		
		client = Client(wsdl=wsdl, wsse=UsernameToken('wsuser', '123456',use_digest=True))
		
		result=client.service.getEdoCtaCRRecibos(policy,agent)
		
		if('ERROR ID' in result):
			fixed_result=str(result).replace('<ERROR ID=\"1\">','')
			new_dict=xmltodict.parse(fixed_result)
			print('step1: sending payment registration issue mail')
		
		else:

			root = ET.fromstring(result)
			monto = root[0][3].text
			numero_recibo=root[0][4].text
			
			result2=client.service.getEdoCtaCRPagosRcibo('0',monto,numero_recibo)

			if('ERROR ID' in result2):
				print('step2: sending payment registration issue mail')
			else:
				#print(result2)
				my_dict = xmltodict.parse(result2)
				partial = json.dumps(my_dict)
				partial2 = json.loads(partial)
				message = partial2['RECIBO']['DATOS_DEL_RECIBO']['ESTADO_RECIBO']
				print('resut: {res}'.format(res=message))


#api = ApiAdapter()
#api.registerPayment('040000162171000000','*')

