import re,string

class NumberFormatter:
	def __init__(self):
		pass

	def formatValue(self,str_value):
		#print('received value {re}'.format(re=str_value))
		#regex = re.compile('[%s]' % re.escape(string.punctuation))
		#f_string=regex.sub('', str(str_value))
		float_value=float(str_value.replace(',',''))
		format_float='₡{:,.2f}'.format(float_value)
		#print('modified value {re}'.format(re=str(format_float)))
		return str(format_float)

	def formatValueOceanica(self,str_value):
		#print('received value {re}'.format(re=str_value))
		float_value=float(str_value.replace(',','.'))
		#float_value=float(f_string)
		format_float='₡{:,.2f}'.format(float_value)
		#print('modified value {re}'.format(re=str(format_float)))
		return str(format_float)