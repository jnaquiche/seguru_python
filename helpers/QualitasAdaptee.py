	
import requests,sys,traceback
from xml.etree import ElementTree
from helpers.ApiAdapter import ApiAdapter
from helpers.NumberFormatter import NumberFormatter
from django.conf import settings
from datetime import *
from dateutil.relativedelta import *

class QualitasAdaptee(ApiAdapter,NumberFormatter):
	def __init__(self):
		pass
	
	##
	def getQualitasRequest(self, amis, year, desc, value, full):#, issueDate, startDate, endDate):
		#print(amis)
		validatorDigit = self.getValidatorCode(amis)
		
		url='http://www.qualitas.co.cr:8103/WsEmision/services/WsEmision.WsEmisionHttpSoap12Endpoint/'
		method = 'POST'
		auth=''
		headers = {'content-type': 'application/soap+xml'} 
		now = datetime.now()
		futureDate = now + relativedelta(years=1)
		currentDate=str(now.strftime('%Y-%m-%d'))
		issueDate = currentDate
		startDate = currentDate
		endDate = str(futureDate.strftime('%Y-%m-%d'))
		ten_percent = str(float(value)*0.1)
		twenty_percent = str(float(value)*0.2)


		if(full):
			print("cotizacion amplia")
			#
			requestTemplate='''
			<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:web="http://web"><soap:Header/><soap:Body><web:obtenerNuevaEmision><!--Optional:-->
				<web:xmlEmision>				  
				<![CDATA[
				<Movimientos>
				    <Movimiento TipoMovimiento="2" NoNegocio="63" NoPoliza="" NoCotizacion="" NoEndoso="" TipoEndoso="" NoOtra="">
				        <DatosAsegurado NoAsegurado="">
				            <Nombre />
				            <Direccion />
				            <Colonia />
				            <Poblacion />
				            <Estado>1</Estado>
				            <CodigoPostal />
				            <NoEmpleado />
				            <Agrupador />
				        </DatosAsegurado>
				        <DatosVehiculo NoInciso="1">
				            <!-- CAMBIO ENVIAR EL NUMERO DE AMIS -->
				            <ClaveAmis>%s</ClaveAmis>
				            <!-- CAMBIO ENVIAR AÑO DEL VEHICULO -->
				            <Modelo>%s</Modelo>
				            <!-- CAMBIO ENVIAR DESCRIPCION DEL VEHICULO -->
				            <DescripcionVehiculo>%s</DescripcionVehiculo>
				            <Uso>1</Uso>
				            <Servicio>1</Servicio>
				            <Paquete>1</Paquete>
				            <Motor />
				            <Serie />
							<!--4	Responsabilidad Civil-->
				            <Coberturas NoCobertura="4">
				            <SumaAsegurada>20000000|50000000</SumaAsegurada>
				            <Deducible />
				            <TipoSuma>25</TipoSuma>
				            <Prima />
				            </Coberturas>
				            <!--1	Daños materiales-->
				            <Coberturas NoCobertura="1">
				                <SumaAsegurada>%s</SumaAsegurada>
				                <Deducible>4</Deducible>
				                <TipoSuma>2</TipoSuma>
				                <Prima />
				            </Coberturas>
				            <!--3	Robo total-->
				            <Coberturas NoCobertura="3">
				            <SumaAsegurada>%s</SumaAsegurada>
				            <Deducible>10</Deducible>
				            <TipoSuma>2</TipoSuma>
				            <Prima />
				            </Coberturas>
							<!--5	Gastos Medicos Ocupantes-->
				            <Coberturas NoCobertura="5">
				            <SumaAsegurada>10000000</SumaAsegurada>
				            <Deducible />
				            <TipoSuma>2</TipoSuma>
				            <Prima />
				            </Coberturas>
				            <!-- CAMBIO COBERTURA OPCIONAL GASTOS FUNERARIOS POR MUERTE AL CONDUCTOR -->
				            <Coberturas NoCobertura="6">
				            <SumaAsegurada>2000000.0</SumaAsegurada>
				            <Deducible />
				            <TipoSuma>2</TipoSuma>
				            <Prima />
				            </Coberturas>
							<!--7	Gastos Legales-->
				            <Coberturas NoCobertura="7">
				            <SumaAsegurada>1</SumaAsegurada>
				            <Deducible />
				            <TipoSuma>1</TipoSuma>
				            <Prima />
				            </Coberturas>
				            <!-- CAMBIO COBERTURA OPCIONAL EXTENCION RC -->
				            <!--Monto vehiculo ingresado por el usuario-->
				            <Coberturas NoCobertura="11">
				            <SumaAsegurada>%s</SumaAsegurada>
				            <Deducible />
				            <TipoSuma>2</TipoSuma>
				            <Prima />
				            </Coberturas>
							<!--14	Asistencia Vial-->
				            <Coberturas NoCobertura="14">
				            <SumaAsegurada>02</SumaAsegurada>
				            <Deducible />
				            <TipoSuma>2</TipoSuma>
				            <Prima />
				            </Coberturas>
				            <!--20 porciento del valor del vehiculo-->
							<!--15	Robo Parcial-->
				            <Coberturas NoCobertura="15">
				                <SumaAsegurada>x|%s</SumaAsegurada>
				                <Deducible>10</Deducible>
				                <TipoSuma>2</TipoSuma>
				                <Prima />
				            </Coberturas>
							<!--18	Responsabilidad Civil Personas-->
				            <Coberturas NoCobertura="18">
				                <SumaAsegurada>50000000</SumaAsegurada>
				                <Deducible>200000</Deducible>
				                <TipoSuma>2</TipoSuma>
				                <Prima />
				            </Coberturas>
							<!--19	Responsabilidad Civil Bienes-->
				            <Coberturas NoCobertura="19">
				                <SumaAsegurada>20000000</SumaAsegurada>
				                <Deducible>200000</Deducible>
				                <TipoSuma>2</TipoSuma>
				                <Prima />
				            </Coberturas>
							<!--20	Responsabilidad Civil Complementaria-->
				            <Coberturas NoCobertura="20">
				                <SumaAsegurada>50000000</SumaAsegurada>
				                <Deducible />
				                <TipoSuma>2</TipoSuma>
				                <Prima />
				            </Coberturas>
							<!--22	BIS RC. Daños a Ocupantes-->
				            <Coberturas NoCobertura="22">
				            <SumaAsegurada>10000000</SumaAsegurada>
				            <Deducible />
				            <TipoSuma>2</TipoSuma>
				            <Prima />
				            </Coberturas>
							<!--28	Gastos Transp PP-->
				            <Coberturas NoCobertura="28">
				            <SumaAsegurada>15000</SumaAsegurada>
				            <Deducible />
				            <TipoSuma>7</TipoSuma>
				            <Prima />
				            </Coberturas>
				            <!--monto ingresado por el usuario-->
							<!--46	Riesgos Adicionales-->
				            <Coberturas NoCobertura="46">
				                <SumaAsegurada>%s</SumaAsegurada>
				                <Deducible>4</Deducible>
				                <TipoSuma>2</TipoSuma>
				                <Prima />
				            </Coberturas>
				            <!--10 porciento del monto ingresado por el usuario-->
							<!--48	Ruptura de Cristales -->
				            <Coberturas NoCobertura="48">
				                <SumaAsegurada>%s</SumaAsegurada>
				                <Deducible>20</Deducible>
				                <TipoSuma>2</TipoSuma>
				                <Prima />
				            </Coberturas>
				            <ConsideracionesAdicionalesDV NoConsideracion="37">
				            <TipoRegla />
				            <ValorRegla>N</ValorRegla>
				            </ConsideracionesAdicionalesDV>
				            <ConsideracionesAdicionalesDV NoConsideracion="109">
				            <TipoRegla />
				            <ValorRegla />
				            </ConsideracionesAdicionalesDV>
				            <ConsideracionesAdicionalesDV NoConsideracion="110">
				            <TipoRegla />
				            <ValorRegla />
				            </ConsideracionesAdicionalesDV>
				        </DatosVehiculo>
				        <DatosGenerales>
				            <!-- CAMBIO ENVIAR LA FECHAS CORRECTAS -->
				            <FechaEmision>%s</FechaEmision>
				            <FechaInicio>%s</FechaInicio>
				            <FechaTermino>%s</FechaTermino>
				            <!-- CAMBIO ENVIAR 0 SI ES COLONES 1 SI ES DOLARES -->
				            <Moneda>0</Moneda>
				            <!-- CAMBIO YA QUE SON DOS TIPOS DE AGENTES -->
				            <Agente>9</Agente>
				            <FormaPago>C</FormaPago>
				            <TarifaValores>1801</TarifaValores>
				            <TarifaCuotas>1801</TarifaCuotas>
				            <TarifaDerechos>1801</TarifaDerechos>
				            <Plazo>10</Plazo>
				            <Agencia />
				            <Contrato />
				            <PorcentajeDescuento>40</PorcentajeDescuento>
				            <ConsideracionesAdicionalesDG NoConsideracion="01">
				            <TipoRegla>1</TipoRegla>
				            <ValorRegla>%s</ValorRegla>
				            </ConsideracionesAdicionalesDG>
				            <!-- -->
				            <ConsideracionesAdicionalesDG NoConsideracion="04">
				            <TipoRegla>1</TipoRegla>
				            <ValorRegla>01</ValorRegla>
				            </ConsideracionesAdicionalesDG>
				            <ConsideracionesAdicionalesDG NoConsideracion="5">
				            <TipoRegla />
				            <ValorRegla>10</ValorRegla>
				            </ConsideracionesAdicionalesDG>
				            <ConsideracionesAdicionalesDG NoConsideracion="8">
				            <TipoRegla>12</TipoRegla>
				            <ValorRegla>1300.0</ValorRegla>
				            </ConsideracionesAdicionalesDG>
				            <ConsideracionesAdicionalesDG NoConsideracion="13">
				            <TipoRegla />
				            <ValorRegla>S</ValorRegla>
				            </ConsideracionesAdicionalesDG>
				            <ConsideracionesAdicionalesDG NoConsideracion="14">
				            <TipoRegla />
				            <ValorRegla>X000001801</ValorRegla>
				            </ConsideracionesAdicionalesDG>
				            <ConsideracionesAdicionalesDG NoConsideracion="15">
				            <TipoRegla>69</TipoRegla>
				            <ValorRegla>9</ValorRegla>
				            </ConsideracionesAdicionalesDG>
				            <ConsideracionesAdicionalesDG NoConsideracion="20">
				            <TipoRegla />
				            <ValorRegla />
				            </ConsideracionesAdicionalesDG>
				            <!--ConsideracionesAdicionalesDG NoConsideracion="23"> <TipoRegla>4</TipoRegla> <ValorRegla>x</ValorRegla> </ConsideracionesAdicionalesDG-->
				            <ConsideracionesAdicionalesDG NoConsideracion="26">
				            <TipoRegla />
				            <ValorRegla>62</ValorRegla>
				            </ConsideracionesAdicionalesDG>
				            <ConsideracionesAdicionalesDG NoConsideracion="35">
				            <TipoRegla>2</TipoRegla>
				            <ValorRegla>0.0</ValorRegla>
				            </ConsideracionesAdicionalesDG>
				            <ConsideracionesAdicionalesDG NoConsideracion="47">
				            <TipoRegla />
				            <ValorRegla>NADA</ValorRegla>
				            </ConsideracionesAdicionalesDG>
				            <ConsideracionesAdicionalesDG NoConsideracion="55">
				            <TipoRegla />
				            <ValorRegla />
				            </ConsideracionesAdicionalesDG>
				        </DatosGenerales>
				        <Primas>
				            <PrimaNeta />
				            <Derecho>10600</Derecho>
				            <Recargo />
				            <Impuesto />
				            <PrimaTotal />
				            <Comision />
				        </Primas>
				    </Movimiento>
				</Movimientos>
				]]>
			</web:xmlEmision></web:obtenerNuevaEmision></soap:Body></soap:Envelope>
			''' % (amis,year,desc,value,value,value,twenty_percent,value,ten_percent,issueDate,issueDate,endDate,validatorDigit)
		else:
			print("cotizacion basica")
			requestTemplate='''
			<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:web="http://web"><soap:Header/><soap:Body><web:obtenerNuevaEmision><!--Optional:-->
				<web:xmlEmision>
				<![CDATA[
				<Movimientos>
				    <Movimiento TipoMovimiento="2" NoNegocio="63" NoPoliza="" NoCotizacion="" NoEndoso="" TipoEndoso="" NoOtra="">
                        <DatosAsegurado NoAsegurado="">
                                        <Nombre />
                                        <Direccion />
                                        <Colonia />
                                        <Poblacion />
                                        <Estado>1</Estado>
                                        <CodigoPostal />
                                        <NoEmpleado />
                                        <Agrupador />
                        </DatosAsegurado>
                        <DatosVehiculo NoInciso="1">
                                        <!-- CAMBIO ENVIAR EL NUMERO DE AMIS -->
                                        <ClaveAmis>%s</ClaveAmis>
                                        <!-- CAMBIO ENVIAR AÑO DEL VEHICULO -->
                                        <Modelo>%s</Modelo>
                                        <!-- CAMBIO ENVIAR DESCRIPCION DEL VEHICULO -->
                                        <DescripcionVehiculo>%s</DescripcionVehiculo>
                                        <Uso>1</Uso>
                                        <Servicio>1</Servicio>
                                        <Paquete>4</Paquete>
                                        <Motor />
                                        <Serie />
                                        <Coberturas NoCobertura="4">
                                                        <SumaAsegurada>30000000|100000000</SumaAsegurada>
                                                        <Deducible />
                                                        <TipoSuma>25</TipoSuma>
                                                        <Prima />
                                        </Coberturas>
                                        <Coberturas NoCobertura="5">
                                                        <SumaAsegurada>10000000.0</SumaAsegurada>
                                                        <Deducible />
                                                        <TipoSuma>2</TipoSuma>
                                                        <Prima />
                                        </Coberturas>
                                        <!-- CAMBIO COBERTURA OPCIONAL GASTOS FUNERARIOS POR MUERTE AL CONDUCTOR -->
                                        <Coberturas NoCobertura="6">
                                                        <SumaAsegurada>2000000.0</SumaAsegurada>
                                                        <Deducible />
                                                        <TipoSuma>2</TipoSuma>
                                                        <Prima />
                                        </Coberturas>
                                        <Coberturas NoCobertura="7">
                                                        <SumaAsegurada>1</SumaAsegurada>
                                                        <Deducible />
                                                        <TipoSuma>1</TipoSuma>
                                                        <Prima />
                                        </Coberturas>
                                        <!-- CAMBIO COBERTURA OPCIONAL EXTENCION RC -->
                                        <Coberturas NoCobertura="11">
                                                        <SumaAsegurada>%s</SumaAsegurada>
                                                        <Deducible />
                                                        <TipoSuma>2</TipoSuma>
                                                        <Prima />
                                        </Coberturas>
                                        <Coberturas NoCobertura="14">
                                                        <SumaAsegurada>02</SumaAsegurada>
                                                        <Deducible />
                                                        <TipoSuma>2</TipoSuma>
                                                        <Prima />
                                        </Coberturas>
                                        <Coberturas NoCobertura="18">
                                                        <SumaAsegurada>100000000</SumaAsegurada>
                                                        <Deducible>150000</Deducible>
                                                        <TipoSuma>2</TipoSuma>
                                                        <Prima />
                                        </Coberturas>
                                        <Coberturas NoCobertura="19">
                                                        <SumaAsegurada>30000000</SumaAsegurada>
                                                        <Deducible>150000</Deducible>
                                                        <TipoSuma>2</TipoSuma>
                                                        <Prima />
                                        </Coberturas>
                                        <Coberturas NoCobertura="20">
                                                        <SumaAsegurada>100000000</SumaAsegurada>
                                                        <Deducible />
                                                        <TipoSuma>2</TipoSuma>
                                                        <Prima />
                                        </Coberturas>
                                        <Coberturas NoCobertura="22">
                                                        <SumaAsegurada>10000000</SumaAsegurada>
                                                        <Deducible />
                                                        <TipoSuma>2</TipoSuma>
                                                        <Prima />
                                        </Coberturas>
                                        <Coberturas NoCobertura="28">
                                                        <SumaAsegurada>15000</SumaAsegurada>
                                                        <Deducible />
                                                        <TipoSuma>7</TipoSuma>
                                                        <Prima />
                                        </Coberturas>
                                        <ConsideracionesAdicionalesDV NoConsideracion="37">
                                                        <TipoRegla />
                                                        <ValorRegla>N</ValorRegla>
                                        </ConsideracionesAdicionalesDV>
                                        <ConsideracionesAdicionalesDV NoConsideracion="109">
                                                        <TipoRegla />
                                                        <ValorRegla />
                                        </ConsideracionesAdicionalesDV>
                                        <ConsideracionesAdicionalesDV NoConsideracion="110">
                                                        <TipoRegla />
                                                        <ValorRegla />
                                        </ConsideracionesAdicionalesDV>
                        </DatosVehiculo>
                        <DatosGenerales>
                                        <!-- CAMBIO ENVIAR LA FECHAS CORRECTAS -->
                                        <FechaEmision>%s</FechaEmision>
                                        <FechaInicio>%s</FechaInicio>
                                        <FechaTermino>%s</FechaTermino>
                                        <!-- CAMBIO ENVIAR 0 SI ES COLONES 1 SI ES DOLARES -->
                                        <Moneda>0</Moneda>
                                        <!-- CAMBIO YA QUE SON DOS TIPOS DE AGENTES -->
                                        <Agente>9</Agente>
                                        <FormaPago>C</FormaPago>
                                        <TarifaValores>1801</TarifaValores>
                                        <TarifaCuotas>1801</TarifaCuotas>
                                        <TarifaDerechos>1801</TarifaDerechos>
                                        <Plazo>10</Plazo>
                                        <Agencia />
                                        <Contrato />
                                        <PorcentajeDescuento>25</PorcentajeDescuento>
                                        <ConsideracionesAdicionalesDG NoConsideracion="01">
                                                        <TipoRegla>1</TipoRegla>
                                                        <ValorRegla>%s</ValorRegla>
                                        </ConsideracionesAdicionalesDG>
                                        <!-- -->
                                        <ConsideracionesAdicionalesDG NoConsideracion="04">
                                                        <TipoRegla>1</TipoRegla>
                                                        <ValorRegla>01</ValorRegla>
                                        </ConsideracionesAdicionalesDG>
                                        <ConsideracionesAdicionalesDG NoConsideracion="5">
                                                        <TipoRegla />
                                                        <ValorRegla>10</ValorRegla>
                                        </ConsideracionesAdicionalesDG>
                                        <ConsideracionesAdicionalesDG NoConsideracion="8">
                                                        <TipoRegla>12</TipoRegla>
                                                        <ValorRegla>1300.0</ValorRegla>
                                        </ConsideracionesAdicionalesDG>
                                        <ConsideracionesAdicionalesDG NoConsideracion="13">
                                                        <TipoRegla />
                                                        <ValorRegla>S</ValorRegla>
                                        </ConsideracionesAdicionalesDG>
                                        <ConsideracionesAdicionalesDG NoConsideracion="14">
                                                        <TipoRegla />
                                                        <ValorRegla>X000001801</ValorRegla>
                                        </ConsideracionesAdicionalesDG>
                                        <ConsideracionesAdicionalesDG NoConsideracion="15">
                                                        <TipoRegla>69</TipoRegla>
                                                        <ValorRegla>9</ValorRegla>
                                        </ConsideracionesAdicionalesDG>
                                        <ConsideracionesAdicionalesDG NoConsideracion="20">
                                                        <TipoRegla />
                                                        <ValorRegla />
                                        </ConsideracionesAdicionalesDG>
                                        <!--ConsideracionesAdicionalesDG NoConsideracion="23"> <TipoRegla>4</TipoRegla> <ValorRegla>x</ValorRegla> </ConsideracionesAdicionalesDG-->
                                        <ConsideracionesAdicionalesDG NoConsideracion="26">
                                                        <TipoRegla />
                                                        <ValorRegla>62</ValorRegla>
                                        </ConsideracionesAdicionalesDG>
                                        <ConsideracionesAdicionalesDG NoConsideracion="35">
                                                        <TipoRegla>2</TipoRegla>
                                                        <ValorRegla>0.0</ValorRegla>
                                        </ConsideracionesAdicionalesDG>
                                        <ConsideracionesAdicionalesDG NoConsideracion="47">
                                                        <TipoRegla />
                                                        <ValorRegla>NADA</ValorRegla>
                                        </ConsideracionesAdicionalesDG>
                                        <ConsideracionesAdicionalesDG NoConsideracion="55">
                                                        <TipoRegla />
                                                        <ValorRegla />
                                        </ConsideracionesAdicionalesDG>
                        </DatosGenerales>
                        <Primas>
                                        <PrimaNeta />
                                        <Derecho>10600</Derecho>
                                        <Recargo />
                                        <Impuesto />
                                        <PrimaTotal />
                                        <Comision />
                        </Primas>
                </Movimiento>
				</Movimientos>
				]]>
			</web:xmlEmision></web:obtenerNuevaEmision></soap:Body></soap:Envelope>
			''' % (amis,year,desc,value,issueDate,issueDate,endDate,validatorDigit)
			


		print('*************************************************')
		print('body for request: {bd}'.format(bd=requestTemplate))
		print('*************************************************')

		result = self.apiRequest(url, headers, method, requestTemplate.encode('utf-8'), auth)		
		return result



	def getQualitasData(self, amis, year, desc, value,full):
		result = {}
		try:
			responseQualitas = self.getQualitasRequest(amis, year, desc, value,full)

			#print('response for quote {res}'.format(res=responseQualitas))
			dom = ElementTree.fromstring(responseQualitas)

			namespaces = {
			  'soapenv': 'http://www.w3.org/2003/05/soap-envelope',
			  'ns':'http://web'
			}

			# reference the namespace mappings here by `<name>:`
			cdataElement = dom.findall(
				'./soapenv:Body'
				'/ns:obtenerNuevaEmisionResponse'
				'/ns:return',
				namespaces,
			)

			cdata = ''
			for child in cdataElement:
				cdata = child.text
			
			domQ = ElementTree.fromstring(cdata)

			#get values
			coverage5 = self.getCoverage(domQ, '5')
			coverage6 = self.getCoverage(domQ, '6')
			coverage7 = self.getCoverage(domQ, '7')

			rule13 = self.getGeneralRule(domQ, '13')
			rule13Text = 'No Amparada'
			if(rule13['ValorRegla'] == 'S'):
				rule13Text = 'No Amparada'
			#if settings.DEBUG:
				#print('Asistencia en carretera', rule13Text)

			consideracion37 = domQ.find
			('Movimiento/DatosVehiculo/ConsideracionesAdicionalesDV[@NoConsideracion="37"]')

			rule13 = self.getGeneralRule(domQ, '13')
			rule37Text = 'No Amparada'
			if(rule13['ValorRegla'] == 'N'):
				rule37Text = 'Cubiertos'

			coverage18 = self.getCoverage(domQ, '18')
			coverage19 = self.getCoverage(domQ, '19')


			coverage11 = self.getCoverage(domQ, '11')

			primas = self.getPrima(domQ)
			prima = primas['PrimaTotal']
			print('Contenido de prima {pri}'.format(pri=prima))


			#recibo1 = self.getRecibo(domQ,'4')
			recibo1 = self.getRecibo(domQ,'1')

			result['Pago Anual'] = self.formatValue(prima)
			result['Pago Semestral'] = self.formatValue(str(float(prima)/2))
			result['Pago Trimestral'] = self.formatValue(str(float(prima)/4))
			result['Pago Mensual'] = self.formatValue(str(float(prima)/12))


			result['Responsabilidad Civil Personas'] = 'Máximo de %s por persona y máximo de %s por accidente' % (self.formatValue(coverage11['SumaAsegurada']), self.formatValue(coverage11['SumaAsegurada']))
			result['Responsabilidad Civil Bienes'] = self.formatValue(coverage11['SumaAsegurada'])
			result['Responsabilidad Civil Alcohol'] = 'Amparada' 
			result['Responsabilidad Civil Daño Moral'] =  'Máximo %s independiente de que haya Conciliación o Sentencia en Firme.' % self.formatValue(coverage11['SumaAsegurada'])

			result['Colisión y vuelco'] =  self.formatValue(coverage6['SumaAsegurada'])
			result['Hurto y Robo'] = self.formatValue(coverage19['SumaAsegurada'])
			result['Riesgos Adicionales y Rotura de Cristales'] = self.formatValue(coverage19['SumaAsegurada'])
			result['Servicios médicos'] = self.formatValue(coverage5['SumaAsegurada'])
			result['Gastos funerarios'] =  self.formatValue(coverage6['SumaAsegurada'])
			result['Gastos legales'] = rule37Text

		except Exception:
			result=self.getDefaultQuotation()
			stk_trace=traceback.print_exc(file=sys.stdout)
			print ('Error in process: {er}'.format(er=stk_trace))
		
		return result

	def exampleRequest2():
		# define namespace mappings to use as shorthand below
		namespaces = {
		  'soapenv': 'http://www.w3.org/2003/05/soap-envelope',
		  'ns':'http://web'
		 
		}
		dom = ElementTree.fromstring(responseExample)

		# reference the namespace mappings here by `<name>:`
		cdataElement = dom.findall(
		  './soapenv:Body'
		  '/ns:obtenerNuevaEmisionResponse'
		  '/ns:return',
		  namespaces,
		)

		cdata = ''
		for child in cdataElement:
			 cdata = child.text

		dom = ElementTree.fromstring(cdata)

		dict2 = getChild(dom, 'Movimiento/Recibos[@NoRecibo="1"]')
		if settings.DEBUG:
			print(dict2['PrimaNeta'])

		coverage22 = getCoverage(dom, '22')
		if settings.DEBUG:
			print('Cobertura 22', coverage22['SumaAsegurada'])

		coverage5 = getCoverage(dom, '5')
		if settings.DEBUG:
			print('Cobertura 25', coverage5['SumaAsegurada'])


		movimiento = dom.find('Movimiento')
		#print(movimiento)

		prima = getPrima(dom)

		print('Contenido de prima {pri}'.format(pri=prima))
		
		if settings.DEBUG:
			print('Prima', prima['Recargo'])

		recibo4 = getRecibo(dom,'4')
		#print('Comision recibo4', recibo4['Comision'])

	#return a dictionary with the tag, values
	def getChild(self, dom, path):
		result = {}
		elements = dom.find(path)
		for child in elements:
			#print ("recibo: ", child.tag, child.text)
			result[child.tag] = child.text
		return result

	#find a especific element -> Coberturas NoCobertura="id"
	def getCoverage(self, dom, val):
		result = {}
		datosVehiculo = dom.find('Movimiento/DatosVehiculo')
		#print(datosVehiculo)
		path ='Coberturas[@NoCobertura="'+val+'"]'
		coverage = datosVehiculo.find(path)
		for child in coverage:
			#print (child.tag, child.text)
			result[child.tag] = child.text
		return result

	def getGeneralRule(self, dom, val):
		result = {}
		datosGenerales = dom.find('Movimiento/DatosGenerales')
		#print(datosVehiculo)
		path ='ConsideracionesAdicionalesDG[@NoConsideracion="'+val+'"]'
		rule = datosGenerales.find(path)
		for child in rule:
			#print (child.tag, child.text)
			result[child.tag] = child.text
		return result

	def getPrima(self, dom):
		return self.getChild(dom,'Movimiento/Primas')


	def getRecibo(self, dom,val):
		return self.getChild(dom, 'Movimiento/Recibos[@NoRecibo="'+val+'"]')

	#code required for consideration and changes according to AMIS
	def getValidatorCode(self, str):
		str = str.zfill(5)
		result = 0
		odd = 0
		even = 0
		for index in range(len(str)):
			if index % 2 == 0:
				#3 sum all even positions from left
				odd += int(str[index])
			else:
				#1 sum all odd positions from left
				even += int(str[index])
		odd *= 3
		#4 sum both
		result = odd + even
		#5 find a number that convert the result in a multiple of 10
		result = result % 10 * -1
		if(result < 0):
			result += 10
		return result;


# def usageExample():
# 	myApi = QualitasAdaptee()

# 	amis = '4319'
# 	year = '2018'
# 	desc = 'KIA SPORTAGE 2.4L 4X4 GASOLINA'
# 	#amount = '25000000'
# 	print(myApi.getQualitasData( amis, year, desc))
  

#usageExample()
#id_type_numer 1 for fisica 2 for juridica

	def createInsurance(self,full_name,address,codelect,amis,year,vehicle_desc,motor_number,serial,vehicle_plate,
	creditor,creditor_id,credit_amount,phone,id_type,birth_date,profession,comertial_activity,id_number,
	county,name,lastname,second_lastname,state_code,county_code,id_type_number,district_code,barrio_code,
	income,gender,company_name,marital_status,business,profession_code,mail,payment_type):
		print('creating insurace for qualitas ...')
		
		now = datetime.now()
		futureDate = now + relativedelta(years=1)
		currentDate =str(now.strftime('%Y-%m-%d'))
		issueDate = currentDate
		startDate = currentDate
		endDate = str(futureDate.strftime('%Y-%m-%d'))
		validatorDigit = self.getValidatorCode(amis)
		date_time_obj = datetime.strptime(birth_date, '%m/%d/%Y')
		birth_date = date_time_obj.strftime("%d-%m-%Y")

		print('formatted birthdate is: {bd}'.format(bd=birth_date))

		url='http://www.qualitas.co.cr:8103/WsEmision/services/WsEmision.WsEmisionHttpSoap12Endpoint/'
		method = 'POST'
		auth=''
		headers = {'content-type': 'application/soap+xml'}

		#validations
		vehicle_plate=vehicle_plate.upper()
		if(gender == 'M'):
			gender = 'H'
		if(gender == 'F'):
			gender = 'M'

		#parts = birth_date.split('/')
		#birth_date = parts[1] + '-' + parts[0] +  '-' + parts[2]

		print('STATE CODE : {st} - COUNTY CODE: {ct}'.format(st=state_code,ct=county_code))


		print(full_name,address,codelect,amis,year,vehicle_desc,motor_number,serial,vehicle_plate,
	creditor,creditor_id,credit_amount,phone,id_type,birth_date,profession,comertial_activity,id_number,
	county,name,lastname,second_lastname,state_code,county_code,id_type_number,district_code,barrio_code,
	income,gender,company_name,marital_status,business,profession_code,mail)

		#profession= Class.objects.get(pk=this_object_id)

		if(creditor_id):

			print('----------------------------------------------')
			print('formatted birthdate is: {bd}'.format(bd=birth_date))
			requestTemplate='''<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:web="http://web"><soap:Header/><soap:Body><web:obtenerNuevaEmision><!--Optional:-->
					<web:xmlEmision>
					  
					<![CDATA[
			<Movimientos>
		  <!--CHAVA-SE OBTIENE EL CANTON DEL ASEGURADO EN FORMATO DE SISE-->
		  <Movimiento TipoMovimiento="21" NoNegocio="63" NoPoliza="" NoCotizacion="" NoEndoso="" TipoEndoso="" NoOtra="">
			 <DatosAsegurado NoAsegurado="">
				<Nombre>%s</Nombre>
				<Direccion>%s</Direccion>
				<Colonia></Colonia>
				<!--<Poblacion>ALAJUELITA></Poblacion>-->
				<Poblacion>%s</Poblacion>
				<Estado>1</Estado>
				<CodigoPostal>0</CodigoPostal>
				<NoEmpleado/>
				<Agrupador/>
			 </DatosAsegurado>
			 <DatosVehiculo NoInciso="1">
				<ClaveAmis>%s</ClaveAmis>
				<Modelo>%s</Modelo>
				<DescripcionVehiculo>%s</DescripcionVehiculo>
				<Uso>1</Uso>
				<Servicio>1</Servicio>
				<Paquete>1</Paquete>
				<Motor>%s</Motor>
				<Serie>%s</Serie>
				<Coberturas NoCobertura="4">
				  <SumaAsegurada>50000000|100000000</SumaAsegurada>
				  <Deducible/>
				  <TipoSuma>25</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="1">
				  <SumaAsegurada>24651000</SumaAsegurada>
				  <Deducible>4</Deducible>
				  <TipoSuma>2</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="3">
				  <SumaAsegurada>24651000</SumaAsegurada>
				  <Deducible>10</Deducible>
				  <TipoSuma>2</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="7">
				  <SumaAsegurada>1</SumaAsegurada>
				  <Deducible/>
				  <TipoSuma>1</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="14">
				  <SumaAsegurada>02</SumaAsegurada>
				  <Deducible/>
				  <TipoSuma>2</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="15">
				  <SumaAsegurada>x|2465100.0></SumaAsegurada>
				  <Deducible>10</Deducible>
				  <TipoSuma>2</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="18">
				  <SumaAsegurada>100000000</SumaAsegurada>
				  <Deducible>300000</Deducible>
				  <TipoSuma>2</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="19">
				  <SumaAsegurada>50000000</SumaAsegurada>
				  <Deducible>300000</Deducible>
				  <TipoSuma>2</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="20">
				  <SumaAsegurada>100000000</SumaAsegurada>
				  <Deducible/>
				  <TipoSuma>2</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="22">
				  <SumaAsegurada>10000000</SumaAsegurada>
				  <Deducible/>
				  <TipoSuma>2</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="46">
				  <SumaAsegurada>24651000</SumaAsegurada>
				  <Deducible>4</Deducible>
				  <TipoSuma>2</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="48">
				  <SumaAsegurada>2465100.0</SumaAsegurada>
				  <Deducible>20</Deducible>
				  <TipoSuma>2</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="5">
				  <SumaAsegurada>10000000.0</SumaAsegurada>
				  <Deducible />
				  <TipoSuma>2</TipoSuma>
				<Prima />
				</Coberturas>
				<Coberturas NoCobertura="28">
					<SumaAsegurada>15000</SumaAsegurada>
					<Deducible />
					<TipoSuma>7</TipoSuma>
					<Prima />
				</Coberturas>
				<ConsideracionesAdicionalesDV NoConsideracion="06">
				  <TipoRegla>109</TipoRegla>
				  <ValorRegla>T109 PARTICIPACON DEL ASEGURADO EN PERDIDA TOTAL||ADDENDUM QUE FORMA PARTE INTEGRANTE DE LAS CONDICIONES|PARTICULARES DE LA POLIZA A LA CUAL SE ANEXA.||EL ASEGURADO Y LA COMPAÑIA CONVIENEN QUE EN CASO DE|DAÑOS DEL VEHICULO ASEGURADO LUEGO DE ESTABLECER EL|IMPORTE DE REPARACION, INCLUYENDO MANO DE OBRA, REFRAC|CIONES Y MATERIALES NECESARIOS PARA SU REPARACION CON|FORME AL PRESUPUESTO APROBADO POR LA COMPAÑIA, EXCEDA|EL 70%% DE LA SUMA CONTRATADA DEL VEHICULO ASEGURADO, SE|CONSIDERARA PERDIDA TOTAL, PARA LO QUE EL ASEGURADO PAR|TICIPARA CON EL 10%% DE LA SUMA CONTRATADA.||LAS DE MAS CONDICIONES GENERALES Y ESPECIALES DE LA POLI|ZA, CONTINUARAN VIGENTES Y SERAN APLICABLES A ESTE|ANEXO, EXCEPTO EN LO QUE SEAN MODIFICADAS POR EL MISMO.|-------------------------------------------------------||</ValorRegla>
				</ConsideracionesAdicionalesDV>
				<ConsideracionesAdicionalesDV NoConsideracion="06">
				  <TipoRegla>114</TipoRegla>
				  <ValorRegla>LIMITES DE SUMA ASEGURADA AL RIESGO MORAL||ADDENDUM QUE FORMA PARTE INTEGRAL DE LAS CONDICIONES|PARTICULARES DE LA POLIZA A LA CUAL SE ANEXA.||NO OBSTANTE LO ESTABLECIDO EN LA COBERTURA DE RESPONSA|BILIDAD CIVIL, EL DANO MORAL PROBADO QUE CAUSE EL ASEGU|RADO O CONDUCTOR QUE CON LA AUTORIZACION DEL ASEGURADO|USE EL VEHICULO Y QUE A CONSECUENCIA DE DICHO USO CAUSE|DANOS A TERCEROS EN SUS PERSONAS, SE AMPARA HASTA POR|UN MAXIMO DE 12.500.000,00 DE COLONES POR EVENTO, TANTO|PARA EL CASO DE SENTENCIA EN FIRME COMO PARA EL|ACUERDO CONCILIATORIO EXTRAJUDICIAL.||LOS DEMAS TERMINOS Y CONDICIONES DE ESTA POLIZA LE SERA|N APLICABLES AL PRESENTE ADDENDUM.|...........................................................</ValorRegla>
				</ConsideracionesAdicionalesDV>
				<!--CHAVA-TEXTO DE ACREEDOR-->
				<ConsideracionesAdicionalesDV NoConsideracion="06">
				  <TipoRegla>1</TipoRegla>
				  <ValorRegla>T001 ADDENDUM QUE FORMA PARTE INTEGRAL DE ESTA POLIZA||CLAUSULA DE ACREENCIA||SE HACE CONSTAR QUE EN CASO DE SINIESTRO DEL VEHICULO|AMPARADO POR LA PRESENTE POLIZA Y QUE AMERITE|INDEMNIZACION SE PAGARA PREFERENTEMENTE HASTA EL INTERES|QUE LE CORRESPONDA A:|PLACA: %s||ACREEDOR: %s||CEDULA: %s||ACREENCIA: %s %%||SIN EMBARGO Y PARA PODER DAR POR CANCELADA LA PRESENTE|POLIZA, ES NECESARIO QUE EL ASEGURADO PRESENTE POR|ESCRITO EL CONSENTIMIENTO DEL ACREEDOR PRENDARIO.|...........................................................|</ValorRegla>
				</ConsideracionesAdicionalesDV>
				<ConsideracionesAdicionalesDV NoConsideracion="17">
				  <TipoRegla>20</TipoRegla>
				  <ValorRegla>01</ValorRegla>
				</ConsideracionesAdicionalesDV>
				<ConsideracionesAdicionalesDV NoConsideracion="58">
				  <TipoRegla/>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDV>
				<ConsideracionesAdicionalesDV NoConsideracion="36">
				  <TipoRegla/>
				  <ValorRegla>CON RTV</ValorRegla>
				</ConsideracionesAdicionalesDV>
				<ConsideracionesAdicionalesDV NoConsideracion="17">
				  <TipoRegla>82</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDV>
				<ConsideracionesAdicionalesDV NoConsideracion="37">
				  <TipoRegla/>
				  <ValorRegla>N</ValorRegla>
				</ConsideracionesAdicionalesDV>
				<ConsideracionesAdicionalesDV NoConsideracion="109">
				  <TipoRegla/>
				  <ValorRegla/>
				</ConsideracionesAdicionalesDV>
				<ConsideracionesAdicionalesDV NoConsideracion="110">
				  <TipoRegla/>
				  <ValorRegla/>
				</ConsideracionesAdicionalesDV>
			 </DatosVehiculo>
			 <DatosGenerales>
				<FechaEmision>%s</FechaEmision>
				<FechaInicio>%s</FechaInicio>
				<FechaTermino>%s</FechaTermino>
				<Moneda>0</Moneda>
				<Agente>9</Agente>
				<FormaPago>%s</FormaPago>
				<TarifaValores>1705</TarifaValores>
				<TarifaCuotas>1705</TarifaCuotas>
				<TarifaDerechos>1705</TarifaDerechos>
				<!--<Plazo>10</Plazo>-->
				<Plazo>0</Plazo>
				<Agencia/>
				<Contrato/>
				<PorcentajeDescuento>3</PorcentajeDescuento>
				<ConsideracionesAdicionalesDG NoConsideracion="01">
				  <TipoRegla>1</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!--     -->
				<ConsideracionesAdicionalesDG NoConsideracion="04">
				  <TipoRegla>1</TipoRegla>
				  <ValorRegla>01</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="5">
				  <TipoRegla/>
				  <ValorRegla>10</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="8">
				  <TipoRegla>12</TipoRegla>
				  <ValorRegla>1300.0</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="8">
				  <TipoRegla>56</TipoRegla>
				  <ValorRegla>%s %s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="9">
				  <TipoRegla>177</TipoRegla>
				  <ValorRegla>09-0010</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="14">
				  <TipoRegla/>
				  <ValorRegla>X000001705</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="15">
				  <TipoRegla>69</TipoRegla>
				  <ValorRegla>9</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="20">
				  <TipoRegla/>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!--ConsideracionesAdicionalesDG NoConsideracion="23">
						<TipoRegla>4</TipoRegla>
						<ValorRegla>x</ValorRegla>
					</ConsideracionesAdicionalesDG-->
				<ConsideracionesAdicionalesDG NoConsideracion="26">
				  <TipoRegla/>
				  <ValorRegla>62</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="35">
				  <TipoRegla>2</TipoRegla>
				  <ValorRegla>0.0</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="31">
				  <TipoRegla/>
				  <ValorRegla/>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="32">
				  <TipoRegla/>
				  <!--<ValorRegla>84865329|N||1986-08-12|1|INFORMATICO(A)|SERVICIOS|||112880373</ValorRegla>-->
				  <ValorRegla>%s|%s||%s|1|%s|%s|%s||%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>1</TipoRegla>
				  <ValorRegla/>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>2</TipoRegla>
				  <ValorRegla/>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>3</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>4</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>5</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>6</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>19</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>23</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>25</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>41</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>68</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Nombre -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>200</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- TODO Definir que parte de la direccion es (Direccion) -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>201</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- TODO Definir que parte de la direccion es (Direccion) -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>202</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Provincia -->
				<!-- CHAVA- -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>203</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Canton -->
				<!-- CHAVA- -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>204</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Tipo de persona (1=fisica, 2=juridica) -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>205</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Cedula (DI)-->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>206</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Actividad o profesion -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>207</TipoRegla>
				  <!--<ValorRegla>115</ValorRegla>-->
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Kermy distrito -->
				<!-- CHAVA- -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>208</TipoRegla>
				  <!--<ValorRegla>115</ValorRegla>-->
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Kermy colonia -->
				<!-- CHAVA- -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>209</TipoRegla>
				  <!--<ValorRegla>115</ValorRegla>-->
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- CHAVA-DATO DEL D.I. DEL ASEGURADO PARA PERSONA FISICA -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>210</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Kermy Ingresos Tomador -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>213</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- CHAVA-OTROS TELEFONOS -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>30</TipoRegla>
				  <ValorRegla>8888888*</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- SEXO-->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>214</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- KERMY ENVIAR PATRONO TOMADOR -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>215</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!--ESTADO CIVIL-->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>216</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!--Exoneracion-->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
						<TipoRegla>217</TipoRegla>
						<ValorRegla>N</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!--NUMERO DOCUMENTO-->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>225</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>230</TipoRegla>
				  <ValorRegla>00009</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- CHAVA-DATOS IMPACTADOS DIRECTAMENTE EN MASEG PARA PERSONAS FISICAS Y JURIDICAS -->
				<!-- CHAVA-DATO DEL D.I. DEL ASEGURADO (MASEG 54) Y NUMERO DE DOCUMENTO (MASEG 32) PARA PERSONA FISICA -->
				<!--<ConsideracionesAdicionalesDG NoConsideracion="9">
						<TipoRegla>54</TipoRegla>
						<ValorRegla>%s</ValorRegla>
					</ConsideracionesAdicionalesDG>
					<ConsideracionesAdicionalesDG NoConsideracion="9">
						<TipoRegla>32</TipoRegla>
						<ValorRegla>%s</ValorRegla>
					</ConsideracionesAdicionalesDG>-->
				<!-- CHAVA-DOCUMENTACION COMPLETA (MASEG 64) -->
				<!--<ConsideracionesAdicionalesDG NoConsideracion="9">
						<TipoRegla>64</TipoRegla>
						<ValorRegla>%s</ValorRegla>
					</ConsideracionesAdicionalesDG>-->
				<!-- Actividad o profesion -->
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>1</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Estructura de Propiedad (Nacionalidad) -->
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>2</TipoRegla>
				  <ValorRegla>2</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>3</TipoRegla>
				  <ValorRegla>2</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>4</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>5</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>6</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>7</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Producto que utiliza (Tipo de paquete / Tipo de poliza) -->
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>8</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Tipo y monto de transaccion (Tipo de monto) -->
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>9</TipoRegla>
				  <ValorRegla>2</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!--NUEVA PREGUNTA, SE RECORRIERON LAS SIGUIENTES-->
				<!-- MATRIZ DE RIESGOS-Cantidad de productos -->
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>10</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- TODO validar: Pais de nacimiento (Nacionalidad) -->
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>11</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- TODO validar: Pais de origen (Nacionalidad) -->
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>12</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- TODO validar: Domicilio Pais (Pais)-->
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>13</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- TODO validar: Domicilio Pais con relacion comercial (Pais) -->
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>14</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Kermy Actividad o profesion -->
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>15</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="47">
				  <TipoRegla/>
				  <ValorRegla>NADA</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="55">
				  <TipoRegla/>
				  <ValorRegla/>
				</ConsideracionesAdicionalesDG>
			 </DatosGenerales>
			 <Primas>
				<PrimaNeta/>
				<Derecho>10600</Derecho>
				<Recargo/>
				<Impuesto/>
				<PrimaTotal/>
				<Comision/>
			 </Primas>
		  </Movimiento>
					</Movimientos>
					]]>
					
					
					</web:xmlEmision></web:obtenerNuevaEmision></soap:Body></soap:Envelope>
			''' % (full_name,address,codelect,amis,year,vehicle_desc,motor_number,serial,vehicle_plate,
				creditor,creditor_id,credit_amount,vehicle_plate,full_name,currentDate,currentDate,endDate,payment_type,
				validatorDigit,creditor_id,creditor,phone,id_type,birth_date,profession,comertial_activity,mail,id_number,
				'COSTA RICA',name,lastname,second_lastname,profession, profession,mail,full_name,address,address,state_code,county_code,
				id_type_number,id_number,profession,district_code,barrio_code,id_number,
				str(float(income)*100),gender,company_name,marital_status,id_number,
				id_number,id_number,marital_status,business,profession_code)
		
		else:			
			print('--------------------NO CREDITOR--------------------------')
			print('formatted birthdate is: {bd}'.format(bd=birth_date))

			requestTemplate='''<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:web="http://web"><soap:Header/><soap:Body><web:obtenerNuevaEmision><!--Optional:-->
					<web:xmlEmision>
					  
					<![CDATA[
			<Movimientos>
		  <!--CHAVA-SE OBTIENE EL CANTON DEL ASEGURADO EN FORMATO DE SISE-->
		  <Movimiento TipoMovimiento="21" NoNegocio="63" NoPoliza="" NoCotizacion="" NoEndoso="" TipoEndoso="" NoOtra="">
			 <DatosAsegurado NoAsegurado="">
				<Nombre>%s</Nombre>
				<Direccion>%s</Direccion>
				<Colonia></Colonia>
				<!--<Poblacion>ALAJUELITA></Poblacion>-->
				<Poblacion>%s</Poblacion>
				<Estado>1</Estado>
				<CodigoPostal>0</CodigoPostal>
				<NoEmpleado/>
				<Agrupador/>
			 </DatosAsegurado>
			 <DatosVehiculo NoInciso="1">
				<ClaveAmis>%s</ClaveAmis>
				<Modelo>%s</Modelo>
				<DescripcionVehiculo>%s</DescripcionVehiculo>
				<Uso>1</Uso>
				<Servicio>1</Servicio>
				<Paquete>1</Paquete>
				<Motor>%s</Motor>
				<Serie>%s</Serie>
				<Coberturas NoCobertura="4">
				  <SumaAsegurada>50000000|100000000</SumaAsegurada>
				  <Deducible/>
				  <TipoSuma>25</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="1">
				  <SumaAsegurada>24651000</SumaAsegurada>
				  <Deducible>4</Deducible>
				  <TipoSuma>2</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="3">
				  <SumaAsegurada>24651000</SumaAsegurada>
				  <Deducible>10</Deducible>
				  <TipoSuma>2</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="7">
				  <SumaAsegurada>1</SumaAsegurada>
				  <Deducible/>
				  <TipoSuma>1</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="14">
				  <SumaAsegurada>02</SumaAsegurada>
				  <Deducible/>
				  <TipoSuma>2</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="15">
				  <SumaAsegurada>x|2465100.0></SumaAsegurada>
				  <Deducible>10</Deducible>
				  <TipoSuma>2</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="18">
				  <SumaAsegurada>100000000</SumaAsegurada>
				  <Deducible>300000</Deducible>
				  <TipoSuma>2</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="19">
				  <SumaAsegurada>50000000</SumaAsegurada>
				  <Deducible>300000</Deducible>
				  <TipoSuma>2</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="20">
				  <SumaAsegurada>100000000</SumaAsegurada>
				  <Deducible/>
				  <TipoSuma>2</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="22">
				  <SumaAsegurada>10000000</SumaAsegurada>
				  <Deducible/>
				  <TipoSuma>2</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="46">
				  <SumaAsegurada>24651000</SumaAsegurada>
				  <Deducible>4</Deducible>
				  <TipoSuma>2</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="48">
				  <SumaAsegurada>2465100.0</SumaAsegurada>
				  <Deducible>20</Deducible>
				  <TipoSuma>2</TipoSuma>
				  <Prima/>
				</Coberturas>
				<Coberturas NoCobertura="5">
				  <SumaAsegurada>10000000.0</SumaAsegurada>
				  <Deducible />
				  <TipoSuma>2</TipoSuma>
				<Prima />
				</Coberturas>
				<Coberturas NoCobertura="28">
					<SumaAsegurada>15000</SumaAsegurada>
					<Deducible />
					<TipoSuma>7</TipoSuma>
					<Prima />
				</Coberturas>
				<ConsideracionesAdicionalesDV NoConsideracion="06">
				  <TipoRegla>109</TipoRegla>
				  <ValorRegla>T109 PARTICIPACON DEL ASEGURADO EN PERDIDA TOTAL||ADDENDUM QUE FORMA PARTE INTEGRANTE DE LAS CONDICIONES|PARTICULARES DE LA POLIZA A LA CUAL SE ANEXA.||EL ASEGURADO Y LA COMPAÑIA CONVIENEN QUE EN CASO DE|DAÑOS DEL VEHICULO ASEGURADO LUEGO DE ESTABLECER EL|IMPORTE DE REPARACION, INCLUYENDO MANO DE OBRA, REFRAC|CIONES Y MATERIALES NECESARIOS PARA SU REPARACION CON|FORME AL PRESUPUESTO APROBADO POR LA COMPAÑIA, EXCEDA|EL 70%% DE LA SUMA CONTRATADA DEL VEHICULO ASEGURADO, SE|CONSIDERARA PERDIDA TOTAL, PARA LO QUE EL ASEGURADO PAR|TICIPARA CON EL 10%% DE LA SUMA CONTRATADA.||LAS DE MAS CONDICIONES GENERALES Y ESPECIALES DE LA POLI|ZA, CONTINUARAN VIGENTES Y SERAN APLICABLES A ESTE|ANEXO, EXCEPTO EN LO QUE SEAN MODIFICADAS POR EL MISMO.|-------------------------------------------------------||</ValorRegla>
				</ConsideracionesAdicionalesDV>
				<ConsideracionesAdicionalesDV NoConsideracion="06">
				  <TipoRegla>114</TipoRegla>
				  <ValorRegla>LIMITES DE SUMA ASEGURADA AL RIESGO MORAL||ADDENDUM QUE FORMA PARTE INTEGRAL DE LAS CONDICIONES|PARTICULARES DE LA POLIZA A LA CUAL SE ANEXA.||NO OBSTANTE LO ESTABLECIDO EN LA COBERTURA DE RESPONSA|BILIDAD CIVIL, EL DANO MORAL PROBADO QUE CAUSE EL ASEGU|RADO O CONDUCTOR QUE CON LA AUTORIZACION DEL ASEGURADO|USE EL VEHICULO Y QUE A CONSECUENCIA DE DICHO USO CAUSE|DANOS A TERCEROS EN SUS PERSONAS, SE AMPARA HASTA POR|UN MAXIMO DE 12.500.000,00 DE COLONES POR EVENTO, TANTO|PARA EL CASO DE SENTENCIA EN FIRME COMO PARA EL|ACUERDO CONCILIATORIO EXTRAJUDICIAL.||LOS DEMAS TERMINOS Y CONDICIONES DE ESTA POLIZA LE SERA|N APLICABLES AL PRESENTE ADDENDUM.|...........................................................</ValorRegla>
				</ConsideracionesAdicionalesDV>
				<!--CHAVA-TEXTO DE ACREEDOR-->
				<!--
				<ConsideracionesAdicionalesDV NoConsideracion="06">
				  <TipoRegla>1</TipoRegla>
				  <ValorRegla>T001 ADDENDUM QUE FORMA PARTE INTEGRAL DE ESTA POLIZA||CLAUSULA DE ACREENCIA||SE HACE CONSTAR QUE EN CASO DE SINIESTRO DEL VEHICULO|AMPARADO POR LA PRESENTE POLIZA Y QUE AMERITE|INDEMNIZACION SE PAGARA PREFERENTEMENTE HASTA EL INTERES|QUE LE CORRESPONDA A:|PLACA: %s||ACREEDOR: %s||CEDULA: %s||ACREENCIA: %s %%||SIN EMBARGO Y PARA PODER DAR POR CANCELADA LA PRESENTE|POLIZA, ES NECESARIO QUE EL ASEGURADO PRESENTE POR|ESCRITO EL CONSENTIMIENTO DEL ACREEDOR PRENDARIO.|...........................................................|</ValorRegla>
				</ConsideracionesAdicionalesDV>
				-->
				<ConsideracionesAdicionalesDV NoConsideracion="17">
				  <TipoRegla>20</TipoRegla>
				  <ValorRegla>01</ValorRegla>
				</ConsideracionesAdicionalesDV>
				<ConsideracionesAdicionalesDV NoConsideracion="58">
				  <TipoRegla/>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDV>
				<ConsideracionesAdicionalesDV NoConsideracion="36">
				  <TipoRegla/>
				  <ValorRegla>CON RTV</ValorRegla>
				</ConsideracionesAdicionalesDV>
				<ConsideracionesAdicionalesDV NoConsideracion="17">
				  <TipoRegla>82</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDV>
				<ConsideracionesAdicionalesDV NoConsideracion="37">
				  <TipoRegla/>
				  <ValorRegla>N</ValorRegla>
				</ConsideracionesAdicionalesDV>
				<ConsideracionesAdicionalesDV NoConsideracion="109">
				  <TipoRegla/>
				  <ValorRegla/>
				</ConsideracionesAdicionalesDV>
				<ConsideracionesAdicionalesDV NoConsideracion="110">
				  <TipoRegla/>
				  <ValorRegla/>
				</ConsideracionesAdicionalesDV>
			 </DatosVehiculo>
			 <DatosGenerales>
				<FechaEmision>%s</FechaEmision>
				<FechaInicio>%s</FechaInicio>
				<FechaTermino>%s</FechaTermino>
				<Moneda>0</Moneda>
				<Agente>9</Agente>
				<FormaPago>%s</FormaPago>
				<TarifaValores>1705</TarifaValores>
				<TarifaCuotas>1705</TarifaCuotas>
				<TarifaDerechos>1705</TarifaDerechos>
				<!--<Plazo>10</Plazo>-->
				<Plazo>0</Plazo>
				<Agencia/>
				<Contrato/>
				<PorcentajeDescuento>3</PorcentajeDescuento>
				<ConsideracionesAdicionalesDG NoConsideracion="01">
				  <TipoRegla>1</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!--     -->
				<ConsideracionesAdicionalesDG NoConsideracion="04">
				  <TipoRegla>1</TipoRegla>
				  <ValorRegla>01</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="5">
				  <TipoRegla/>
				  <ValorRegla>10</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="8">
				  <TipoRegla>12</TipoRegla>
				  <ValorRegla>1300.0</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="8">
				  <TipoRegla>56</TipoRegla>
				  <ValorRegla>%s %s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="9">
				  <TipoRegla>177</TipoRegla>
				  <ValorRegla>09-0010</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="14">
				  <TipoRegla/>
				  <ValorRegla>X000001705</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="15">
				  <TipoRegla>69</TipoRegla>
				  <ValorRegla>9</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="20">
				  <TipoRegla/>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!--ConsideracionesAdicionalesDG NoConsideracion="23">
						<TipoRegla>4</TipoRegla>
						<ValorRegla>x</ValorRegla>
					</ConsideracionesAdicionalesDG-->
				<ConsideracionesAdicionalesDG NoConsideracion="26">
				  <TipoRegla/>
				  <ValorRegla>62</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="35">
				  <TipoRegla>2</TipoRegla>
				  <ValorRegla>0.0</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="31">
				  <TipoRegla/>
				  <ValorRegla/>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="32">
				  <TipoRegla/>
				  <!--<ValorRegla>84865329|N||1986-08-12|1|INFORMATICO(A)|SERVICIOS|||112880373</ValorRegla>-->
				  <ValorRegla>%s|%s||%s|1|%s|%s|%s||%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>1</TipoRegla>
				  <ValorRegla/>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>2</TipoRegla>
				  <ValorRegla/>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>3</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>4</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>5</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>6</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>19</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>23</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>25</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>41</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>68</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Nombre -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>200</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- TODO Definir que parte de la direccion es (Direccion) -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>201</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- TODO Definir que parte de la direccion es (Direccion) -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>202</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Provincia -->
				<!-- CHAVA- -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>203</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Canton -->
				<!-- CHAVA- -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>204</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Tipo de persona (1=fisica, 2=juridica) -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>205</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Cedula (DI)-->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>206</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Actividad o profesion -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>207</TipoRegla>
				  <!--<ValorRegla>115</ValorRegla>-->
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Kermy distrito -->
				<!-- CHAVA- -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>208</TipoRegla>
				  <!--<ValorRegla>115</ValorRegla>-->
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Kermy colonia -->
				<!-- CHAVA- -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>209</TipoRegla>
				  <!--<ValorRegla>115</ValorRegla>-->
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- CHAVA-DATO DEL D.I. DEL ASEGURADO PARA PERSONA FISICA -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>210</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Kermy Ingresos Tomador -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>213</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- CHAVA-OTROS TELEFONOS -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>30</TipoRegla>
				  <ValorRegla>8888888*</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- SEXO-->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>214</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- KERMY ENVIAR PATRONO TOMADOR -->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>215</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!--ESTADO CIVIL-->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>216</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!--Exoneracion-->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
						<TipoRegla>217</TipoRegla>
						<ValorRegla>N</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!--NUMERO DOCUMENTO-->
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>225</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="40">
				  <TipoRegla>230</TipoRegla>
				  <ValorRegla>00009</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- CHAVA-DATOS IMPACTADOS DIRECTAMENTE EN MASEG PARA PERSONAS FISICAS Y JURIDICAS -->
				<!-- CHAVA-DATO DEL D.I. DEL ASEGURADO (MASEG 54) Y NUMERO DE DOCUMENTO (MASEG 32) PARA PERSONA FISICA -->
				<!--<ConsideracionesAdicionalesDG NoConsideracion="9">
						<TipoRegla>54</TipoRegla>
						<ValorRegla>%s</ValorRegla>
					</ConsideracionesAdicionalesDG>
					<ConsideracionesAdicionalesDG NoConsideracion="9">
						<TipoRegla>32</TipoRegla>
						<ValorRegla>%s</ValorRegla>
					</ConsideracionesAdicionalesDG>-->
				<!-- CHAVA-DOCUMENTACION COMPLETA (MASEG 64) -->
				<!--<ConsideracionesAdicionalesDG NoConsideracion="9">
						<TipoRegla>64</TipoRegla>
						<ValorRegla>%s</ValorRegla>
					</ConsideracionesAdicionalesDG>-->
				<!-- Actividad o profesion -->
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>1</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Estructura de Propiedad (Nacionalidad) -->
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>2</TipoRegla>
				  <ValorRegla>2</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>3</TipoRegla>
				  <ValorRegla>2</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>4</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>5</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>6</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>7</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Producto que utiliza (Tipo de paquete / Tipo de poliza) -->
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>8</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Tipo y monto de transaccion (Tipo de monto) -->
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>9</TipoRegla>
				  <ValorRegla>2</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!--NUEVA PREGUNTA, SE RECORRIERON LAS SIGUIENTES-->
				<!-- MATRIZ DE RIESGOS-Cantidad de productos -->
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>10</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- TODO validar: Pais de nacimiento (Nacionalidad) -->
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>11</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- TODO validar: Pais de origen (Nacionalidad) -->
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>12</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- TODO validar: Domicilio Pais (Pais)-->
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>13</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- TODO validar: Domicilio Pais con relacion comercial (Pais) -->
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>14</TipoRegla>
				  <ValorRegla>1</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<!-- Kermy Actividad o profesion -->
				<ConsideracionesAdicionalesDG NoConsideracion="96">
				  <TipoRegla>15</TipoRegla>
				  <ValorRegla>%s</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="47">
				  <TipoRegla/>
				  <ValorRegla>NADA</ValorRegla>
				</ConsideracionesAdicionalesDG>
				<ConsideracionesAdicionalesDG NoConsideracion="55">
				  <TipoRegla/>
				  <ValorRegla/>
				</ConsideracionesAdicionalesDG>
			 </DatosGenerales>
			 <Primas>
				<PrimaNeta/>
				<Derecho>10600</Derecho>
				<Recargo/>
				<Impuesto/>
				<PrimaTotal/>
				<Comision/>
			 </Primas>
		  </Movimiento>
					</Movimientos>
					]]>
					
					
					</web:xmlEmision></web:obtenerNuevaEmision></soap:Body></soap:Envelope>
			''' % (full_name,address,codelect,amis,year,vehicle_desc,motor_number,serial,vehicle_plate,
				creditor,creditor_id,credit_amount,vehicle_plate,full_name,currentDate,currentDate,endDate,payment_type,
				validatorDigit,creditor_id,creditor,phone,id_type,birth_date,profession,comertial_activity,mail,id_number,
				'COSTA RICA',name,lastname,second_lastname,profession, profession,mail,full_name,address,address,state_code,county_code,id_type_number,
				id_number,profession,district_code,barrio_code,id_number,str(float(income)*100),gender,company_name,marital_status,id_number,id_number,id_number,marital_status,business,profession_code)


		print('****** START OF TEMPLATE ******')
		print(requestTemplate)
		print('****** END OF TEMPLATE ******')
		result = self.apiRequest(url, headers, method, requestTemplate.encode('utf-8'), auth)
		print(result)		
		return result


#myApi = QualitasAdaptee()
#myApi.createInsurance('leroy bernard','100n del marriot','10403','4319','2018','KIA SPORTAGE 2.4L 4X4 GASOLINA','motor_number','serial','vehicle_plate','creditor','creditor_id','credit_amount','phone','id_type','01/23/1979','profession','comertial_activity','110250602','county','name','lastname','second_lastname','state_code','county_code','id_type_number','district_code','barrio_code','1000000','M','its','D','business','profession_code','leroy.bernard@gmail.com')