import time

from django.core.management.utils import get_random_secret_key
from fabric.api import env, run, cd, prefix
from fabric.contrib.files import upload_template
from fabtools.python import virtualenv

from seguru_website.config.get_config import Config


def production():

	global requirements_file, config_file, settings_env, current_dir, venv_dir

	CONFIG = Config("production")

	env.user = 'deploy'
	env.hosts = [CONFIG.get('server', 'HOST')]
	env.forward_agent = True

	requirements_file = 'requirements/production.txt'
	config_file = 'seguru_website/config/production.env'
	settings_env = 'seguru_website.settings.production'
	current_dir = '/home/deploy/seguru/current/'
	venv_dir = '/home/deploy/envs/seguru/'



def createsuperuser():

	with cd(current_dir):
		run('source {0}bin/activate'.format(venv_dir))

		with virtualenv(venv_dir):

			with prefix('export DJANGO_SETTINGS_MODULE=%s' % settings_env):

				run('python manage.py createsuperuser')


def loadfixture(fixture):

	with cd(current_dir):
		run('source {0}bin/activate'.format(venv_dir))

		with virtualenv(venv_dir):

			with prefix('export DJANGO_SETTINGS_MODULE=%s' % settings_env):

				run('python -Wi manage.py loaddata {0}'.format(fixture))


def runcommand(command):

	with cd(current_dir):
		run('source {0}bin/activate'.format(venv_dir))

		with virtualenv(venv_dir):

			with prefix('export DJANGO_SETTINGS_MODULE=%s' % settings_env):

				run('python manage.py {0}'.format(command))



def deploy(branch_name = None):
	now = str(int(time.time()))
	release_dir = '/home/deploy/seguru/releases/deploy_%s' % now
	repo_url = "git@github.com:trambia/seguru-website.git"

	if branch_name:
		run('git clone -b %s --depth 1 %s %s' % (branch_name, repo_url, release_dir))

	else:
		run('git clone  --depth 1 %s %s' % (repo_url, release_dir))

	with cd(release_dir):
		run('source {0}bin/activate'.format(venv_dir))
		upload_template(config_file, 'seguru_website/config')

		with virtualenv(venv_dir):
			run('pip install -r {0}/{1}'.format(release_dir, requirements_file))

			with prefix('export DJANGO_SETTINGS_MODULE=%s' % settings_env):
				run('python manage.py makemigrations --merge')
				run('python manage.py migrate')
				run('python manage.py collectstatic --no-input')

	run('ln -sfn {0}/* {1}'.format(release_dir, current_dir))
	run('sudo /etc/init.d/supervisor restart')


def generate_secret_key():
	print(get_random_secret_key())



def local():

	global requirements_file, config_file, settings_env, current_dir, venv_dir

	CONFIG = Config("local")

	#env.user = 'deploy'
	#env.hosts = [CONFIG.get('server', 'HOST')]
	#env.forward_agent = True

	requirements_file = 'requirements/local.txt'
	config_file = 'seguru_website/config/local.env'
	settings_env = 'seguru_website.settings.local'
	current_dir = '/home/deploy/seguru/current/'
	venv_dir = 'project_env/'



def deploy_local():
	#now = str(int(time.time()))
	#release_dir = '/home/deploy/seguru/releases/deploy_%s' % now
	#repo_url = "git@github.com:trambia/seguru-website.git"
	settings_env = 'seguru_website.settings.local'
	#if branch_name:
	#	run('git clone -b %s --depth 1 %s %s' % (branch_name, repo_url, release_dir))

	#else:
	#	run('git clone  --depth 1 %s %s' % (repo_url, release_dir))

	#with cd(release_dir):
	#run('source {0}bin/activate'.format(venv_dir))
	#upload_template('seguru_website/config/local.env', 'seguru_website/config')

		#with virtualenv(venv_dir):
		#run('pip install -r {0}/{1}'.format(release_dir, requirements_file))
	with prefix('export DJANGO_SETTINGS_MODULE=%s' % settings_env):
		run('python manage.py makemigrations --merge')
		run('python manage.py migrate')
		run('python manage.py collectstatic --no-input')
	
	#run('ln -sfn {0}/* {1}'.format(release_dir, current_dir))
	#run('sudo /etc/init.d/supervisor restart')