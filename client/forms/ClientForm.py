from django.forms import ModelForm

from insurance.models import Client


class ClientForm(ModelForm):
	class Meta:
		model = Client

		fields = ['first_name', 'last_name', 'email', 'phone', ]

		labels = {
			'first_name': 'Nombres',
			'last_name': 'Apellidos',
			'email': 'Correo Electrónico',
			'phone': 'Teléfono',
		}
