from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from client.views import *

urlpatterns = [
	path('', IndexView.as_view(), name='client.index'),
	path('pages/<slug:page_slug>/', PageSingleView.as_view(), name='client.page.single'),

	# Calculator URL'S
	path('calculator/', CalculatorIndexView.as_view(), name='client.calculator.index'),

	path('calculator/life/', CalculatorLifeView.as_view(), name='client.calculator.life'),
	path('calculator/life-results/', CalculatorLifeResultView.as_view(), name='client.calculator.life_result'),
	path('calculator/<int:insurance_id>/life-request/', CalculatorLifeRequestView.as_view(),
		 name='client.calculator.life_request'),

	path('calculator/medical/', CalculatorMedicalView.as_view(), name='client.calculator.medical'),
	path('calculator/medical-results/', CalculatorMedicalResultView.as_view(), name='client.calculator.medical_result'),
	path('calculator/<int:insurance_id>/medical-request/', CalculatorMedicalRequestView.as_view(),
		 name='client.calculator.medical_request'),

	path('calculator/home/', CalculatorHomeView.as_view(), name='client.calculator.home'),
	path('calculator/home-results/', CalculatorHomeResultView.as_view(), name='client.calculator.home_result'),
	path('calculator/<int:insurance_id>/home-request/', CalculatorHomeRequestView.as_view(),
		 name='client.calculator.home_request'),

	#client/calculator/calculator_vehicle.htm
	path('calculator/vehicle/', CalculatorVehicleView.as_view(), name='client.calculator.vehicle'),
	path('calculator/vehicle-results/', CalculatorVehicleResultView.as_view(), name='client.calculator.vehicle_result'),
	path('calculator/vehicle-client-info/', CalculatorVehicleClientInfoView.as_view(), name='client.calculator.vehicle_client_info'),
	path('calculator/vehicle-client-info-result/', CalculatorVehicleClientInfoResultView.as_view(), name='client.calculator.vehicle_client_info_result'),
	
	path('calculator/vehicle-info/', CalculatorVehicleInfoView.as_view(), name='client.calculator.vehicle_info'),
	path('calculator/vehicle-info-result/', CalculatorVehicleInfoResultView.as_view(), name='client.calculator.vehicle_info_result'),
	path('calculator/vehicle-sugese-info/', CalculatorVehicleSugeseInfoView.as_view(), name='client.calculator.vehicle_sugese_info'),
	#path('calculator/vehicle-sugese-info-result/', CalculatorVehicleSugeseInfoResultView.as_view(), name='client.calculator.calculator_payment_oceanica'),

	path('calculator/get-brand-model-by-plate/', GetBrandModelbyPlate.as_view(), name='client.calculator.get_brand_model_by_plate'),
	
	#payments
	#CalculatorPaymentTypeView
	path('calculator/payments-type/',CalculatorPaymentTypeView.as_view(), name='client.calculator.payments_type'),
	#path('calculator/payments-Oceanica/',CalculatorOceanicaPaymentsView.as_view(), name='client.calculator.payment_oceanica'),
	path('calculator/payments-results/',CalculatorPaymentResults.as_view(), name='client.calculator.payment_result'),
	path('calculator/payments-simulation/',CalculatorPaymentSimulation.as_view(), name='client.calculator.payment_simulation'),
	path('calculator/payments-success/',CalculatorPaymentSuccess.as_view(), name='client.calculator.payment_success'),
	path('calculator/payments-confirm/',CalculatorPaymentConfirmView.as_view(), name='client.calculator.payment_confirm'),


	path('calculator/<int:insurance_id>/vehicle-request/', CalculatorVehicleRequestView.as_view(),
		 name='client.calculator.vehicle_request'),

	path('calculator/sent-request/', CalculatorSentRequestView.as_view(), name='client.calculator.sent_request'),

	# Contact Form URL'S
	path('contact-form/', ContactFormSendView.as_view(), name='client.contact_form'),
]

if settings.DEBUG:
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
