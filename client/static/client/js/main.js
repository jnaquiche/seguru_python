(function(){

	var nav_is_open 		= false;
	var search_is_open 		= false;

	var nav_open_class 		= 'opened';
	var stop_scroll_class 	= 'stop-scroll';
	var scrolled_class 		= 'scrolled';

	var $document		= $(document);
	var $body			= $('body');
	var $main 			= $('main');
	var $nav 			= $('nav');
	var $open_nav 		= $('.open-nav');

	var $header 			= $('header');
	var $notification_bar	= $('.notification-bar');

	$open_nav.click(function(e){
		if(nav_is_open) {
			close_nav();
		} else {
			open_nav();
		}

		e.preventDefault();
	});

	$document.scroll(function() {
		if($document.scrollTop() > 200) {
			$header.addClass(scrolled_class);
			$notification_bar.addClass(scrolled_class);
		} else {
			$header.removeClass(scrolled_class);
			$notification_bar.removeClass(scrolled_class);
		}
	});

	function open_nav() {
		$nav.addClass(nav_open_class);
		$open_nav.addClass(nav_open_class);
		$main.addClass(nav_open_class);

		$body.addClass(stop_scroll_class);

		$main.on("click", close_nav);
		$document.on("keyup", handle_nav_key_up);

		nav_is_open = true;
	}

	function handle_nav_key_up(e) {
		if (e.keyCode == 27) {
			close_nav();
		}
	}

	function close_nav() {
		$nav.removeClass(nav_open_class);
		$open_nav.removeClass(nav_open_class);
		$main.removeClass(nav_open_class);

		$body.removeClass(stop_scroll_class);

		$main.on("click", close_nav);
		$document.off("keyup", handle_nav_key_up);

		nav_is_open = false;
	}

	/* Set ScrollReveal Defaults */
	window.sr = ScrollReveal({
		reset: true,
		duration: 800,
		distance: '40px',
		viewFactor: 0.45,
		scale: 1
	});

	/* Setup Reveals */
	sr.reveal('.reveal');
	sr.reveal('.reveal-300', {
		delay: 300
	});
	sr.reveal('.reveal-500', {
		delay: 500
	});

})();