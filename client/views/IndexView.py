from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

from insurance.models import SectionHomeTemplate, SectionIcon, InsuranceInfo


#class IndexView(LoginRequiredMixin, UserPassesTestMixin,View):
class IndexView(View):
	template_name = 'client/index/index.html'

	def get(self, request):
		home_sections = SectionHomeTemplate.objects.all()

		home_sections_icon = SectionIcon.objects.all()

		section_one = None
		section_three = None
		section_four = None

		section_life = None
		section_medical = None
		section_home = None
		section_vehicle = None

		for section in home_sections:
			if section.home_tag == 'section_one':
				section_one = section

			if section.home_tag == 'section_three':
				section_three = section

			if section.home_tag == 'section_four':
				section_four = section

		for section in home_sections_icon:
			if section.home_tag == 'section_life':
				section_life = section

			if section.home_tag == 'section_medical':
				section_medical = section

			if section.home_tag == 'section_home':
				section_home = section

			if section.home_tag == 'section_vehicle':
				section_vehicle = section

		context = {
			'section_one': section_one,
			'section_three': section_three,
			'section_four': section_four,
			'section_life': section_life,
			'section_medical': section_medical,
			'section_home': section_home,
			'section_vehicle': section_vehicle,
			'index': True,
			'insurance_categories': InsuranceInfo.INSURANCE_CATEGORY_OPTIONS,
		}

		return render(request, self.template_name, context=context)

	def test_func(self):
		#post = self.get_object()
		if self.request.user.is_authenticated:
			print("user is is_authenticated")
			return True
		return False
