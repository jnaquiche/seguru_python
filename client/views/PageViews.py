from django.shortcuts import render
from django.views import View

from insurance.models import Post


class PageSingleView(View):
	template_name = 'client/page/page_single.html'

	def get(self, request, page_slug):
		page = Post.get_by_slug(post_slug=page_slug)

		context = {
			'page': page,
		}

		return render(request, self.template_name, context=context)
