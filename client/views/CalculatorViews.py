

import urllib.parse  
import urllib.request
import ssl
from django.http import HttpResponseRedirect
from django.http import HttpResponse, Http404
from hashlib import md5

import requests

from django.db import transaction, IntegrityError
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import TemplateView
from django.http import JsonResponse

from client.forms import ClientForm

from insurance.models import ClientLifeResponse, Insurance, VehicleBrand, VehicleModel, VehicleYear, InsuranceInfo, \
	ConstructionType, Feature, InsCatalogModel, OceanicaCatalogModel, ClientMedicalResponse, \
	Profession, Padron, Color

from insurance.models.Color import Color
from insurance.models.Profession import Profession


from insurance.models.ClientInfo import ClientInfo
from insurance.models.InsState import InsState
from insurance.models.InsCounty import InsCounty
from insurance.models.InsDistrict import InsDistrict

from insurance.models.QualitasState import QualitasState
from insurance.models.QualitasCounty import QualitasCounty
from insurance.models.QualitasDistrict import QualitasDistrict
from insurance.models.QualitasBarrio import QualitasBarrio

from insurance.models.OceanicaState import OceanicaState
from insurance.models.OceanicaCounty import OceanicaCounty
from insurance.models.OceanicaDistrict import OceanicaDistrict

from insurance.models.Profession import Profession

from insurance.models.QualitasCatalogModel import QualitasCatalogModel

from django.core import serializers
import json,logging
from pprint import pprint
from decimal import *
from datetime import *
from helpers.ApiAdapter import ApiAdapter
from helpers.QualitasAdaptee import QualitasAdaptee
from helpers.OceanicaAdaptee import OceanicaAdaptee
from helpers.InsAdaptee import InsAdaptee

from helpers.credid import Credid

from django.conf import settings
from dateutil.relativedelta import *
from django.contrib import messages
import re, string, os, time, uuid

def date_diff_in_Seconds(dt2, dt1):
  timedelta = dt2 - dt1
  return timedelta.days * 24 * 3600 + timedelta.seconds

class CalculatorIndexView(TemplateView):
	template_name = 'client/calculator/calculator_index.html'


class CalculatorLifeView(View):
	template_name = 'client/calculator/calculator_life.html'
	
	def get(self, request):
		context = {
			'coverage_options': ClientLifeResponse.COVERAGE_OPTIONS,
			'gender_options': ClientLifeResponse.GENDER_OPTIONS,
		}
		
		return render(request, self.template_name, context=context)
	
	def post(self, request):
		response_life_dict = {
			'age': request.POST.get('age', None),
			'gender': request.POST.get('gender', None),
			'coverage_value': request.POST.get('coverage_value', None),
			'is_smoker': request.POST.get('is_smoker', False),
		}
		
		# Set values selected by client in Session
		request.session['response_life_dict'] = response_life_dict
		
		return redirect(reverse_lazy('client.calculator.life_result'))


class CalculatorLifeResultView(View):
	template_name = 'client/calculator/calculator_result.html'
	
	def get(self, request):
		# Get values selected by client in Session
		response_life_dict = request.session.get('response_life_dict', None)
		
		context = {
			'title_section': 'Resultados Calculador Seguro de Vida'
		}
		
		if response_life_dict:
			age = response_life_dict.get('age')
			gender = response_life_dict.get('gender')
			coverage_value = response_life_dict.get('coverage_value')
			is_smoker = response_life_dict.get('is_smoker')
			
			filter_dict = {
				'life_responses__age': age,
				'life_responses__gender': gender,
				'life_responses__coverage_value': coverage_value,
				'life_responses__is_smoker': is_smoker,
				'insurance_info__category': InsuranceInfo.INSURANCE_CATEGORY_LIFE,
			}
			
			# Getting options according client answers to display results
			insurance_result_list = Insurance.objects \
				.filter(**filter_dict) \
				.prefetch_related('insurance_info', 'insurance_info__insurance_info_property').distinct()
			
			property_dict = {}
			
			for insurance in insurance_result_list:
				for info_property in insurance.insurance_info.insurance_info_property.all():
					property_dict[info_property.insurance_property_id] = {
						'property_title': info_property.insurance_property.__str__(),
						'insurances': {
						
						}
					}
			
			for property_id, property_item in property_dict.items():
				for insurance in insurance_result_list:
					property_item['insurances'][insurance.id] = {
						'name': insurance,
						'value': None,
					}
			for insurance in insurance_result_list:
				for info_property in insurance.insurance_info.insurance_info_property.all():
					property_dict[info_property.insurance_property_id]['insurances'][insurance.id][
						'value'] = info_property.value
			
			context['insurance_result_list'] = insurance_result_list
			
			context['property_dict'] = property_dict
		
		return render(request, self.template_name, context=context)


class CalculatorLifeRequestView(View):
	template_name = 'client/calculator/calculator_request_insurance.html'
	
	def get(self, request, insurance_id):
		response_life_dict = request.session.get('response_life_dict', None)
		
		insurance = Insurance.get_by_id(insurance_id=insurance_id)
		
		client_form = ClientForm()
		
		context = {
			'title_section': 'Solicitud de Seguro de Vida',
			'title_insurance': 'Seguro de Vida',
			'response_life_dict': response_life_dict,
			'insurance': insurance,
			'client_form': client_form,
		}
		
		return render(request, self.template_name, context=context)
	
	def post(self, request, insurance_id):
		response_life_dict = request.session.get('response_life_dict', None)
		
		insurance = Insurance.get_by_id(insurance_id=insurance_id)
		
		client_form = ClientForm(request.POST)
		
		# Saving Client responses
		if client_form.is_valid() and response_life_dict:
			with transaction.atomic():
				try:
					client_instance = client_form.save()
					
					client_response = client_instance.life_responses.create(age=response_life_dict['age'],
																			gender=response_life_dict['gender'],
																			coverage_value=response_life_dict[
																				'coverage_value'],
																			is_smoker=response_life_dict['is_smoker'])
					
					client_response.insurance.add(insurance)
					
					del request.session['response_life_dict']
					
					return redirect(reverse_lazy('client.calculator.sent_request'))
				
				except IntegrityError:
					pass
		
		context = {
			'title_section': 'Solicitud de Seguro de Vida',
			'title_insurance': 'Seguro de Vida',
			'response_life_dict': response_life_dict,
			'insurance': insurance,
			'client_form': client_form,
		}
		
		return render(request, self.template_name, context=context)


class CalculatorMedicalView(View):
	template_name = 'client/calculator/calculator_medical.html'
	
	def get(self, request):
		context = {
			'coverage_options': ClientMedicalResponse.MEDICAL_COVERAGE_OPTIONS,
			'gender_options': ClientMedicalResponse.GENDER_OPTIONS,
		}
		
		return render(request, self.template_name, context=context)
	
	def post(self, request):
		dependents = request.POST.getlist('dependents', [])
		
		response_medical_dict = {
			'age': request.POST.get('age', None),
			'gender': request.POST.get('gender', None),
			'coverage_value': request.POST.get('coverage_value', None),
			'dependents': dependents,
			'spouse_age': request.POST.get('spouse_age', None),
		}
		
		# Set values selected by client in Session
		request.session['response_medical_dict'] = response_medical_dict
		
		return redirect(reverse_lazy('client.calculator.medical_result'))


class CalculatorMedicalResultView(View):
	template_name = 'client/calculator/calculator_result.html'
	
	def get(self, request):
		# Get values selected by client in Session
		response_medical_dict = request.session.get('response_medical_dict', None)
		
		context = {
			'title_section': 'Resultados Calculador Seguro Médico'
		}
		
		if response_medical_dict:
			age = response_medical_dict.get('age')
			gender = response_medical_dict.get('gender')
			coverage_value = response_medical_dict.get('coverage_value')
			dependents = response_medical_dict.get('dependents', [])
			spouse_age = response_medical_dict.get('spouse_age')
			
			dependents_number = len(dependents)
			
			filter_dict = {
				'medical_responses__age': age,
				'medical_responses__gender': gender,
				'medical_responses__coverage_value': coverage_value,
				'insurance_info__category': InsuranceInfo.INSURANCE_CATEGORY_MEDICAL,
			}
			
			if spouse_age:
				filter_dict['medical_responses__spouse_age'] = spouse_age
			
			if dependents_number > 0:
				filter_dict['medical_responses__dependents_number'] = dependents_number
			
			else:
				filter_dict['medical_responses__dependents_number__isnull'] = True
			
			# Getting options according client answers
			insurance_result_list = Insurance.objects \
				.filter(**filter_dict) \
				.prefetch_related('insurance_info', 'insurance_info__insurance_info_property').distinct()
			
			for dependent in dependents:
				if dependent:
					insurance_result_list = insurance_result_list.filter(medical_responses__dependents__age=dependent)
			
			property_dict = {}
			
			for insurance in insurance_result_list:
				for info_property in insurance.insurance_info.insurance_info_property.all():
					property_dict[info_property.insurance_property_id] = {
						'property_title': info_property.insurance_property.__str__(),
						'insurances': {
						
						}
					}
			
			for property_id, property_item in property_dict.items():
				for insurance in insurance_result_list:
					property_item['insurances'][insurance.id] = {
						'name': insurance,
						'value': None,
					}
			for insurance in insurance_result_list:
				for info_property in insurance.insurance_info.insurance_info_property.all():
					property_dict[info_property.insurance_property_id]['insurances'][insurance.id][
						'value'] = info_property.value
			
			context['insurance_result_list'] = insurance_result_list
			
			context['property_dict'] = property_dict
		
		return render(request, self.template_name, context=context)


class CalculatorMedicalRequestView(View):
	template_name = 'client/calculator/calculator_request_insurance.html'
	
	def get(self, request, insurance_id):
		response_medical_dict = request.session.get('response_medical_dict', None)
		
		insurance = Insurance.get_by_id(insurance_id=insurance_id)
		
		client_form = ClientForm()
		
		context = {
			'title_section': 'Solicitud de Seguro Médico',
			'title_insurance': 'Seguro de Médico',
			'response_medical_dict': response_medical_dict,
			'insurance': insurance,
			'client_form': client_form,
		}
		
		return render(request, self.template_name, context=context)
	
	def post(self, request, insurance_id):
		response_medical_dict = request.session.get('response_medical_dict', None)
		
		insurance = Insurance.get_by_id(insurance_id=insurance_id)
		
		client_form = ClientForm(request.POST)
		
		# Saving Client responses
		if client_form.is_valid() and response_medical_dict:
			with transaction.atomic():
				try:
					client_instance = client_form.save()
					
					dependents = response_medical_dict.get('dependents', [])
					
					dependents_number = None
					
					if len(dependents) > 0:
						dependents_number = len(dependents)
					
					client_response = client_instance \
						.medical_responses \
						.create(age=response_medical_dict['age'],
								gender=response_medical_dict['gender'],
								coverage_value=response_medical_dict['coverage_value'],
								dependents_number=dependents_number,
								spouse_age=response_medical_dict['spouse_age'])
					
					client_response.insurance.add(insurance)
					
					for dependent in dependents:
						client_response.dependents.create(age=dependent)
					
					del request.session['response_medical_dict']
					
					return redirect(reverse_lazy('client.calculator.sent_request'))
				
				except IntegrityError:
					pass
		
		context = {
			'title_section': 'Solicitud de Seguro de Vida',
			'title_insurance': 'Seguro de Vida',
			'response_medical_dict': response_medical_dict,
			'insurance': insurance,
			'client_form': client_form,
		}
		
		return render(request, self.template_name, context=context)


class CalculatorHomeView(View):
	template_name = 'client/calculator/calculator_home.html'
	
	def get(self, request):
		construction_type_list = ConstructionType.objects.all().order_by('name')
		
		feature_list = Feature.objects.all().order_by('name')
		
		context = {
			'construction_type_list': construction_type_list,
			'feature_list': feature_list,
		}
		
		return render(request, self.template_name, context=context)
	
	def post(self, request):
		response_home_dict = {
			'construction_type': request.POST.get('construction_type', None),
			'feature': request.POST.getlist('feature', []),
			'building_age': request.POST.get('building_age', None),
			'fire_protection': request.POST.get('fire_protection', None),
			'rivers_near': request.POST.get('rivers_near', None),
			'claims_last_3_years': request.POST.get('claims_last_3_years', None),
		}
		
		# Set values selected by client in Session
		request.session['response_home_dict'] = response_home_dict
		
		return redirect(reverse_lazy('client.calculator.home_result'))


class CalculatorHomeResultView(View):
	template_name = 'client/calculator/calculator_result.html'
	
	def get(self, request):
		# Get values selected by client in Session
		response_home_dict = request.session.get('response_home_dict', None)
		
		context = {
			'title_section': 'Resultados Calculador Seguro de Casa'
		}
		
		if response_home_dict:
			construction_type = response_home_dict.get('construction_type')
			feature = response_home_dict.get('feature', [])
			fire_protection = response_home_dict.get('fire_protection')
			rivers_near = response_home_dict.get('rivers_near')
			claims_last_3_years = response_home_dict.get('claims_last_3_years')
			
			filter_dict = {
				'home_responses__construction_type': construction_type,
				'home_responses__features__in': feature,
				'home_responses__fire_protection': fire_protection,
				'home_responses__rivers_near': rivers_near,
				'home_responses__claims_last_3_years': claims_last_3_years,
				'insurance_info__category': InsuranceInfo.INSURANCE_CATEGORY_HOME,
			}
			
			# Getting options according client answers
			insurance_result_list = Insurance.objects \
				.filter(**filter_dict) \
				.prefetch_related('insurance_info', 'insurance_info__insurance_info_property').distinct()
			
			property_dict = {}
			
			for insurance in insurance_result_list:
				for info_property in insurance.insurance_info.insurance_info_property.all():
					property_dict[info_property.insurance_property_id] = {
						'property_title': info_property.insurance_property.__str__(),
						'insurances': {
						
						}
					}
			
			for property_id, property_item in property_dict.items():
				for insurance in insurance_result_list:
					property_item['insurances'][insurance.id] = {
						'name': insurance,
						'value': None,
					}
			for insurance in insurance_result_list:
				for info_property in insurance.insurance_info.insurance_info_property.all():
					property_dict[info_property.insurance_property_id]['insurances'][insurance.id][
						'value'] = info_property.value
			
			context['insurance_result_list'] = insurance_result_list
			
			context['property_dict'] = property_dict
		
		return render(request, self.template_name, context=context)


class CalculatorHomeRequestView(View):
	template_name = 'client/calculator/calculator_request_insurance.html'
	
	def get(self, request, insurance_id):
		response_home_dict = request.session.get('response_home_dict', None)
		
		insurance = Insurance.get_by_id(insurance_id=insurance_id)
		
		client_form = ClientForm()
		
		context = {
			'title_section': 'Solicitud de Seguro de Casa',
			'title_insurance': 'Seguro de Casa',
			'response_home_dict': response_home_dict,
			'insurance': insurance,
			'client_form': client_form,
		}
		
		return render(request, self.template_name, context=context)
	
	def post(self, request, insurance_id):
		response_home_dict = request.session.get('response_home_dict', None)
		
		insurance = Insurance.get_by_id(insurance_id=insurance_id)
		
		client_form = ClientForm(request.POST)
		
		# Saving Client responses
		if client_form.is_valid() and response_home_dict:
			with transaction.atomic():
				try:
					client_instance = client_form.save()
					
					client_response = client_instance \
						.home_responses \
						.create(building_age=response_home_dict['building_age'],
								fire_protection=response_home_dict['fire_protection'],
								rivers_near=response_home_dict['rivers_near'],
								claims_last_3_years=response_home_dict['claims_last_3_years'])
					
					client_response.insurance.add(insurance)
					
					construction_type_list = ConstructionType \
						.objects \
						.filter(id__in=[response_home_dict['construction_type']])
					
					for construction_type in construction_type_list:
						client_response.construction_type.add(construction_type)
					
					feature_list = Feature.objects.filter(id__in=response_home_dict['feature'])
					
					for feature in feature_list:
						client_response.features.add(feature)
					
					del request.session['response_home_dict']
					
					return redirect(reverse_lazy('client.calculator.sent_request'))
				
				except IntegrityError:
					pass
		
		context = {
			'title_section': 'Solicitud de Seguro de Carro',
			'title_insurance': 'Seguro Carro',
			'response_medical_dict': response_home_dict,
			'insurance': insurance,
			'client_form': client_form,
		}
		
		return render(request, self.template_name, context=context)


class CalculatorVehicleView(View):
	template_name = 'client/calculator/calculator_vehicle.html'
	
	def get(self, request):
		vehicle_brand_list = VehicleBrand.objects.all().order_by('name')
		years_list={}
		now = datetime.now()

		for x in range(0,49):
			previous = now - relativedelta(years=x)
			years_list['year{y}'.format(y=x)] = str(previous.strftime('%Y'))

		show_dialog="False"

		currency={
		"colones":"C",
		"dolares":"D",
		}

        #in case of vehicles recently saled
		plate={
		"con placa":"S",
		"sin placa":"N"
		}

		context = {
			'vehicle_brand_list': vehicle_brand_list,
			'years_list':years_list,
			'show_dialog':show_dialog,
			'currency':currency,
			'plate':plate
		}
		
		return render(request, self.template_name, context=context)
	
	#this dictionary will be used to bypass data from one template to another.
	def post(self, request):

		#messages.info(request, 'Por favor espere')

		#show_dialog="True"
		response_vehicle_dict = {
		    'vehicle_brand': request.POST.get('vehicle_brand', None),
			'vehicle_model': request.POST.get('vehicle_model', None),
			'vehicle_year': request.POST.get('vehicle_year', None),
			'vehicle_value': request.POST.get('vehicle_value', None),
			'vehicle_plate_id': request.POST.get('plate_id', None),
			'vehicle_plate_id': request.POST.get('plate_id', None),
			"credid_propietario": request.POST.get('credid_propietario', None),
            "credid_brand": request.POST.get('credid_brand', None),
            "credid_model": request.POST.get('credid_model', None),
            "credid_year": request.POST.get('credid_year', None),
            "credid_motor": request.POST.get('credid_motor', None),
            "credid_chasis": request.POST.get('credid_chasis', None),
            "credid_type": request.POST.get('credid_type', None),
            "credid_cilinders": request.POST.get('credid_cilinders', None),
            "credid_cc": request.POST.get('credid_cc', None),
            "credid_weight": request.POST.get('credid_weight', None),
            "credid_sits": request.POST.get('credid_sits', None)
		}
		
		# Set values selected by client in Session
		request.session['response_vehicle_dict'] = response_vehicle_dict
		request.session['newCalculatorRequest'] = True # clean the next view the first
		request.session['ins_coverage'] = "ins_914"
		request.session['qua_coverage'] = "qua_basica"
		return redirect(reverse_lazy('client.calculator.vehicle_result'))


class CalculatorVehicleResultView(View):
	template_name = 'client/calculator/calculator_result.html'
	
	def get(self, request):
		qualitasDictionary={}
		insDictionary={}
		oceanicaDictionary={}
		qualitasDictionary2={}
		insDictionary2={}

		response_vehicle_dict = request.session.get('response_vehicle_dict', None)
		
		context = {
			'title_section': 'Resultados Calculador Seguro de Vehículos'
		}
		
		new_calculator_request = request.session['newCalculatorRequest']

		if new_calculator_request:
			request.session['newCalculatorRequest'] = False
			if response_vehicle_dict:
				vehicle_brand_id = response_vehicle_dict['vehicle_brand']
				vehicle_model_id = response_vehicle_dict['vehicle_model']
				year = response_vehicle_dict['vehicle_year']			
				vehicle_value = response_vehicle_dict['vehicle_value'].replace('₡','',1)

				regex = re.compile('[%s]' % re.escape(string.punctuation))

				vehicle_value = regex.sub('', vehicle_value)
				
				#values required to create request
				brand_description = VehicleBrand.get_by_id(vehicle_brand_id)
				model_description = VehicleModel.get_by_id(vehicle_model_id)
				

				vehicle_info={}
				vehicle_info['brand'] = brand_description.name
				vehicle_info['model'] = model_description.name			
				vehicle_info['year'] = year
				vehicle_info['vehicle_value'] = vehicle_value
				vehicle_info['ins_model_code'] =model_description.ins_model_code
				vehicle_info['ins_brand_code'] = brand_description.ins_brand_code
				vehicle_info['amis'] = model_description.amis
				vehicle_info['oceanica_model_code'] = model_description.oceanica_model_code
				vehicle_info['oceanica_brand_code'] = brand_description.oceanica_brand_code 
				vehicle_info['vehicle_plate_id'] = response_vehicle_dict['vehicle_plate_id']
				vehicle_info['credid_propietario'] = response_vehicle_dict['credid_propietario']
				vehicle_info['credid_brand'] = response_vehicle_dict['credid_brand']
				vehicle_info['credid_model'] = response_vehicle_dict['credid_model']
				vehicle_info['credid_year'] = response_vehicle_dict['credid_year']
				vehicle_info['credid_motor'] = response_vehicle_dict['credid_motor']
				vehicle_info['credid_chasis'] = response_vehicle_dict['credid_chasis']
				vehicle_info['credid_type'] = response_vehicle_dict['credid_type']
				vehicle_info['credid_cilinders'] = response_vehicle_dict['credid_cilinders']
				vehicle_info['credid_cc'] = response_vehicle_dict['credid_cc']
				vehicle_info['credid_weight'] = response_vehicle_dict['credid_weight']
				vehicle_info['credid_sits'] = response_vehicle_dict['credid_sits']
				 
				request.session['vehicle_info'] = vehicle_info

				
				api = ApiAdapter()
				
				qualitasAdaptee = QualitasAdaptee()
				insAdaptee = InsAdaptee()
				oceanicaAdaptee = OceanicaAdaptee()

				#Qualitas Initial date
				date1 = datetime.now()
				#queryset of different amis & descriptions for Qualitas
				qualitas_catalog_set = QualitasCatalogModel.objects.filter(marca2=brand_description.name, subtipo=model_description.name);
				qualitas_quote_list=[]
				qualitas_quote_list2=[]
				qualitasDictionary={}
				qualitasDictionary2={}
				qualitas_versions={}
				oceanica_versions={}
				
				print('********** QUALITAS QUOTE **********')
				print('searching for brand {brand}'.format(brand=brand_description.name))
				print('searching for model {mod}'.format(mod=model_description.name))
				print('catalog {cat}'.format(cat=qualitas_catalog_set))
				
				for catalog in qualitas_catalog_set:
					qualitas_versions[catalog.amis]=catalog.vers.replace('\"','')
					print(' Creating qualitas quote for amis: {am}, descripcion: {desc}'.format(am=catalog.amis, desc=catalog.vers))
					temp_dictionary=qualitasAdaptee.getQualitasData(str(catalog.amis), year, catalog.vers, vehicle_value,False)
					temp_dictionary2=qualitasAdaptee.getQualitasData(str(catalog.amis), year, catalog.vers, vehicle_value,True)
					
					if(temp_dictionary["Pago Anual"] != "Consultar"):
						qualitas_quote_list.append(temp_dictionary)
						qualitas_quote_list2.append(temp_dictionary2)
					else:
						print('Excluded amis: {amis}'.format(amis=str(catalog.amis)))
					
					#Fprint(temp_dictionary)
					print('************************************')	
				print('********** END QUALITAS QUOTE **********')
				#Qualitas End Date
				date2 = datetime.now()
				print("***TIME QUALITAS TOTAL **** \n%d seconds" %(date_diff_in_Seconds(date2, date1)))
				time_qualitas = date_diff_in_Seconds(date2, date1)

				#Oceanica Initial date
				date1 = datetime.now()

				try:
					oceanica_versions = oceanicaAdaptee.getVersionCodes(brand_description.oceanica_brand_code,model_description.oceanica_model_code)
					print('Oceanica versions: {ver}'.format(ver=oceanica_versions))

				except:
					print("Error retrieving oceanica Versions. Brand: " + brand_description.oceanica_brand_code + " Model: " + model_description.oceanica_model_code );
				
				oceanica_quote_list=[]
				oceanica_quote_info_list=[]
				oceanica_quotes={}
				oceanica_quotes_ids={}
				
				for key, value in oceanica_versions.items():
					if(value != '.'):
						print(' Creating Oceanica quote for code: {am}, descripcion: {desc}'.format(am=key, desc=value))
						temp_dictionary=oceanicaAdaptee.getOceanicaData( brand_description.oceanica_brand_code,model_description.oceanica_model_code,str(key), year, vehicle_value, 'Cotizador', 'Seguro', 'TST000')
						oceanica_quote_list.append(temp_dictionary["quotation"])
						print('Adding Oceanica quote #{num}'.format(num=temp_dictionary['id']))
						oceanica_quote_info_list.append(temp_dictionary['id'])
						oceanica_quotes[str(key)]=temp_dictionary["quotation"]
						oceanica_quotes_ids[str(key)]=temp_dictionary['id']
				
				#Oceanica End Date
				date2 = datetime.now()
				print("***TIME OCEANICA TOTAL **** \n%d seconds" %(date_diff_in_Seconds(date2, date1)))
				time_oceanica = date_diff_in_Seconds(date2, date1)

				#TODO : add validation in case no dictionary exists a default dictionary is required
				if(len(qualitas_quote_list)>0):
					qualitasDictionary = qualitas_quote_list[0]
				if(len(qualitas_quote_list2)>0):
					qualitasDictionary2 = qualitas_quote_list2[0]
				
				if(len( oceanica_quote_list)>0):
					oceanicaDictionary = oceanica_quote_list[0]
				else:
					oceanicaDictionary={}

				#INS Initial date
				date1 = datetime.now()
				print('*********** GETTING INS INFO **********')
				insDictionary = insAdaptee.getInsDataAutoPercent(year,vehicle_value,brand_description.ins_brand_code,model_description.ins_model_code)
				insDictionary2 = insAdaptee.getInsDataAutoFixed(year,vehicle_value,brand_description.ins_brand_code,model_description.ins_model_code)
				#INS End Date
				date2 = datetime.now()
				print("***TIME INS TOTAL **** \n%d seconds" %(date_diff_in_Seconds(date2, date1)))
				time_ins = date_diff_in_Seconds(date2, date1)

				quotation_info={}

				if(len(oceanica_quote_info_list)>0):
					quotation_info['oceanicaQuotationId'] = oceanica_quote_info_list[0]
				
				request.session['quotation_info'] = quotation_info
				request.session['qualitas_versions']=qualitas_versions
				request.session['qualitas_quote_list']=qualitas_quote_list

				request.session['oceanica_versions']=oceanica_versions
				request.session['oceanica_quotes']=oceanica_quotes
				request.session['oceanica_quotes_ids']=oceanica_quotes_ids
				

				##STORE the values so we do need need to request again next time
				request.session['qualitasDictionary'] = qualitasDictionary
				request.session['qualitasDictionary2'] = qualitasDictionary2
				request.session['insDictionary'] = insDictionary
				request.session['insDictionary2'] = insDictionary2
				request.session['oceanicaDictionary'] = oceanicaDictionary

				request.session['time_ins'] = time_ins
				request.session['time_qualitas'] = time_qualitas
				request.session['time_oceanica'] = time_oceanica

		deductibles = {}
		deductibles['Responsabilidad Civil Personas']=['Sin Deducible 1er evento por Semestre (aplican restricciones)','₡150,000.00 fijos por evento','Sin Deducible']
		deductibles['Responsabilidad Civil Bienes']=['Sin Deducible 1er evento por Semestre (aplican restricciones)','₡150,000.00 fijos por evento','₡300,000.00 fijos por evento']
		deductibles['Colisión, Vuelco, Riesgos adicionales']=['Deducible fijo de ₡300,000.00','2% del Monto Asegurado','10% del Monto Asegurado']
		deductibles['Hurto, Robo total o parcial']=['Deducible fijo de ₡300,000.00','10% del Monto Asegurado','10% del Monto Asegurado']
		deductibles['Rotura de cristales']=['Deducible fijo de ₡300,000.00','20% de la pérdida','₡300,000.00 fijos por evento']
		deductibles['Gastos médicos por accidente']=['Sin Deducible','Sin Deducible','Sin Deducible']
		deductibles['Asistencia en carretera']=['Sin Deducible','Sin Deducible','Sin Deducible']
		deductibles['Asistencia en carretera Extendida']=['Sin Deducible','No Amparada','No Amparada']
		deductibles['Auto sustituto']=['Sin Deducible','Sin Deducible','Sin Deducible']
		deductibles['Gastos legales']=['No Amparada','Sin deducible','No amparada']


		insurance_companies = ["Ins","Qualitas","Oceánica"]
		context['deductibles'] = deductibles
		context['insurance_companies'] = insurance_companies
		context['time_ins'] = request.session['time_ins']
		context['time_qualitas'] = request.session['time_qualitas']
		context['time_oceanica']=request.session['time_oceanica']

				#print('Qualitas dictionary items:')
				#for k, v in qDict.items():
				#	print(k, v)
				#print('***********************')
				
				#print('Oceanica dictionary items:')
				#for k, v in oDict.items():
				#	print(k, v)
				#print('***********************')

				#print('INS dictionary items:')
				#for k, v in iDict.items():
				#	print(k, v)

				#print('***********************')
			
		#obtain info from selectors
		ins_coverage = request.session['ins_coverage']
		qua_coverage = request.session['qua_coverage']

		context['ins_coverage_value']=request.session['ins_coverage']
		context['qualitas_coverage_value']=request.session['qua_coverage']
		
		print('********** COVERAGE **********')
		print('Selected Coverage ins:{ins} qua:{qua}'.format(ins=str(ins_coverage),qua=str(qua_coverage)))
		
		#seleccionar dependiendo de los ids de los drops
		if ins_coverage == 'ins_914':
			iDict=request.session['insDictionary']
		else:
			iDict=request.session['insDictionary2']

		if qua_coverage == 'qua_basica':
			qDict=request.session['qualitasDictionary']
		else:
			qDict=request.session['qualitasDictionary2']
		
		oDict=request.session['oceanicaDictionary']
		
		simplified = {}
	
		#order is ins, qualitas, oceanica
		#validation in case dictionary is empty
		if(len(oDict)>0):
			print('Oceanica is reference')
			for key, value in oDict.items():
				
				values=[]
				
				if key in iDict.keys():
					values.append(iDict[key])
				else:
					values.append('-')
				if key in qDict.keys():
					values.append(qDict[key])
				else:
					values.append('-')
				values.append(value)
				
				simplified[key]= values
				values=[]

		elif(len(qDict)>0):
			print('Qualitas is reference')
			for key, value in qDict.items():
				
				values=[]
				
				if key in iDict.keys():
					values.append(iDict[key])
				else:
					values.append('-')

				values.append(value)

				if key in oDict.keys():
					values.append(oDict[key])
				else:
					values.append('-')
				
				
				simplified[key]= values
				values=[]

		elif(len(iDict)>0):
			
			print('INS is reference')
			for key, value in iDict.items():
				
				values=[]

				values.append(value)
				
				if key in qDict.keys():
					values.append(qDict[key])
				else:
					values.append('-')

				if key in oDict.keys():
					values.append(oDict[key])
				else:
					values.append('-')
					
				simplified[key]= values
				values=[]



		print('********** SIMPLIFIED ITEMS **********')
		for k, v in simplified.items():
			print(k, v)
		
		context['simplified'] = simplified
		context['vehicle_info'] = request.session['vehicle_info']
		
		return render(request, self.template_name, context=context)

	def post(self,request):
		
		coverage_changed = request.POST.get("coverage_changed")
		print(' valor de ins coverage: {val}'.format(val=request.POST.get("ins_coverage")))
		print(' valor de coverage changed: {val}'.format(val=request.POST.get("coverage_changed")))
		
		if coverage_changed == "True":
			#if request.POST.get("ins_coverage"):
			request.session['ins_coverage'] = request.POST.get("ins_coverage")
			#return redirect(reverse_lazy('client.calculator.vehicle_result'))
			#if request.POST.get("qua_coverage"):
			request.session['qua_coverage'] = request.POST.get("qua_coverage")
			return redirect(reverse_lazy('client.calculator.vehicle_result'))
			
		company_info = {}

		if request.POST.get("submit_Ins"):
			company_info['selected_company'] = 'ins'
		if request.POST.get("submit_Oceánica"):
			company_info['selected_company'] = 'oceanica'
		if request.POST.get("submit_Qualitas"):
			company_info['selected_company'] = 'qualitas'

		request.session['company_info'] = company_info

		print('company info contents')
		for k, v in company_info.items():
			print(k, v)

		# if request.user.is_superuser:
			return redirect(reverse_lazy('client.calculator.vehicle_client_info'))

		# else: 
		# 	return redirect(reverse_lazy('client.contact_form'))


class CalculatorVehicleClientInfoView(View):

	template_name = 'client/calculator/calculator_vehicle_client_info.html'
	
	def geIdTypes(self,company):
		if(company == 'qualitas'):
			return {
				"Cédula física":"N",
				"Cédula jurídica":"J",
				"DIMEX":"D"
				}
		else:
			return {
			"Cédula física":"N",
			"Cédula jurídica":"J",
			"DIMEX":"R"
			}
		

	def get(self,request):
		
		show_dialog="False"
		company_info = request.session['company_info']
		
		for k, v in company_info.	items():
			print(k, v)

		print('Selected company is: {company}'.format(company=company_info['selected_company']))
		id_types=self.geIdTypes(company_info['selected_company'])
		
		context={
			'show_dialog':show_dialog,
			'id_types':id_types
		}
		return render(request, self.template_name,context=context)


	def post(self,request):

		#details = PostForm(request.POST)
		client_id_type = request.POST.get('client_id_type', None)
		client_category_id=client_id_type


		if client_id_type == 'N':
			client_category_id='S'
		if client_id_type == 'R':
			client_category_id='N'
		#Juridica queda igual

		client_info = {
		'client_id': request.POST.get('client_id', None),
		'client_id_type' : client_id_type,
		'client_category_id':client_category_id,
		}

		print(client_info)

		if client_info["client_id"]:
			request.session['client_info'] = client_info
			return redirect(reverse_lazy('client.calculator.vehicle_client_info_result'))
		else:
			print("invalid form")


class CalculatorVehicleClientInfoResultView(View):
	template_name = 'client/calculator/calculator_vehicle_client_info_result.html'

	def get(self,request):
		#print("here!!")
		show_dialog="False"
		
		context={
			'show_dialog':show_dialog,
		}

		client_info = request.session.get('client_info', None)
		print(client_info)
		state_list = InsState.objects.all().order_by('name')
		

		company_info = request.session['company_info']
		selected_company=company_info['selected_company']

		state_list = None
		#value by default for statelist
		if selected_company == 'ins':
			state_list = InsState.objects.all().order_by('name')
		elif selected_company == 'oceanica':
			state_list = OceanicaState.objects.all().order_by('name')
		elif selected_company == 'qualitas':
			state_list = QualitasState.objects.all().order_by('name')
		else: #default
			state_list = InsState.objects.all().order_by('name')
		

		if client_info:
			client_id = client_info['client_id']
			
			#Credid Initial date
			date1 = datetime.now()
			credid = Credid()
			info=credid.getIdInfo(client_id)
			#Credid End Date
			date2 = datetime.now()
			print("***TIME Credid Client TOTAL **** \n%d seconds" %(date_diff_in_Seconds(date2, date1)))
			
			# if(clientInfo):
			# 	if clientInfo["type"]=="Nacional":
			# 		print(clientInfo["name"])
			# 		print((clientInfo["lastname1"]))
			# 		print((clientInfo["lastname2"]))
			# 		print((clientInfo["birthDate"]))
			# 	elif clientInfo["type"]=="Juridica":
			# 		#
			# 		print(clientInfo["name"])

			#client = ClientInfo.get_by_id(client_id=client_id)
			
			gender={
			"Masculino":"M",
			"Femenino":"F"
			}

			marital_status={
			"Soltero(a)":"S",
			"Casado(a)":"C",
			"Divorciado(a)":"D",
			"Viudo(a)":"V",
			"No Aplica":"N"
			}

			study_level={
			"No Aplica":"00",
			"Primaria Incompleta":"01",
			"Primaria":"02",
			"Secundaria Incompleta":"03",
			"Secundaria":"04",
			"Egresado Universitario":"05",
			"Técnico":"06",
			"Bachiller Universitario":"07",
			"Licenciado":"08",
			"Maestría":"09",
			"Doctorado":"10"
			}
		

		client_id_type = client_info['client_id_type']
		client_category_id = client_info['client_category_id']

		if info:

			# client.nombre=client.nombre.lower().capitalize()
			# client.apellido1=client.apellido1.lower().capitalize()
			# client.apellido2=client.apellido2.lower().capitalize()
			if info["type"] == "Nacional":
				#
				if str(info["gender"]) == "1":
					gender={"Masculino":"M"}
				else:
					gender={"Femenino":"F"}

				if str(info["relation"])=="Casado(a)":
					marital_status={
					"Casado(a)":"C",
					}
				elif str(info["relation"])=="Soltero":
					marital_status={
					"Soltero(a)":"S",
					}
				elif str(info["relation"])=="Divorciado(a)":
					marital_status={
						"Divorciado(a)":"D",
					}
				else:
					marital_status={
						"Viudo(a)":"V",
					}


				client_info={
				'client_id':client_id,
				'name': info["name"],
				'lastName': info["lastname1"] + ' ' + info["lastname2"],
				'lastName1': info["lastname1"],
				'lastName2': info["lastname2"],
				'gender':None,
				'DueDate': info["dueDate"],
				'phone' : None,
				'address' :None,
				'email' : None,
				'birthDate':info["birthDate"],
				'client_id_type':client_id_type,
				'client_category_id':client_category_id
				}

				context={
				'name':info["name"],
				'lastname1':info["lastname1"],
				'lastname2':info["lastname2"],
				'birth_date':info["birthDate"],
				'state_list' : state_list,
				'marital_status':marital_status,
				'gender':gender,
				'study_level':study_level,
				'selected_company':selected_company,
				'DueDate': info["dueDate"],		
				}

		else:
			client_info={
			'client_id':client_id,
			'name': '',
			'lastName': None,
			'lastName1': None,
			'lastName2': None,
			'gender':None,
			'DueDate': '20220101',
			'phone' : None,
			'address' :None,
			'email' : None,
			'birthDate':None,
			'client_id_type':client_id_type,
			'client_category_id':client_category_id
			}

			context={
			'name':'',
			'lastname1':'',
			'lastname2':'',
			'birth_date':'',
			'state_list' : state_list,
			'marital_status':marital_status,
			'gender':gender,
			'study_level':study_level,
			'selected_company':selected_company,
			'DueDate': '',		
			}

		request.session['client_info'] = client_info

		return render(request, self.template_name,context=context)
	
	def post(self,request):

		#GEt all client info and pass to the next step
		client_info = request.session['client_info']
		
		if(not client_info["name"] and not client_info["lastName1"] and not client_info["lastName2"]):
			print('filling name lastName1 lastName2')
			client_info["name"] = request.POST.get('client_name', None)
			client_info["lastName1"] =request.POST.get('client_lastname1', None)
			client_info["lastName2"] =request.POST.get('client_lastname2', None)

		client_state = request.POST.get('client_state', None)
		client_county = request.POST.get('client_county', None)
		client_district = request.POST.get('client_district', None)
		client_barrio =   request.POST.get('client_barrio', None)
		client_address =   request.POST.get('client_address', None)
		client_phone =   request.POST.get('client_phone', None)
		client_email =   request.POST.get('client_email', None)
		client_birthdate =   request.POST.get('datepicker', None)
		client_marital_status=request.POST.get('client_marital_status', None)
		client_gender=request.POST.get('client_gender', None)
		client_study_level=request.POST.get('client_study_level', None)


		company_info = request.session['company_info']
		selected_company=company_info['selected_company']

		state_list = None
		#value by default for statelist
		if selected_company == 'qualitas':
			print('=========== From selection ===========')
			print('State id : {cd}'.format(cd=client_state))
			print('County id : {cd}'.format(cd=client_county))
			print('District id : {cd}'.format(cd=client_district))
			print('Barrio code id: {cd}'.format(cd=client_barrio))	

			state_name    = QualitasState.get_by_id(client_state).name
			county_name   = QualitasCounty.get_by_id(client_county).name
			district_name = QualitasDistrict.get_by_id(client_district).name

			if(client_barrio):
				barrio_name   = QualitasBarrio.get_by_id(client_barrio).name	

			print('=========== From DB ===========')
			print('State Name: {cd}'.format(cd=state_name))
			print('County Name: {cd}'.format(cd=county_name))
			print('District Name: {cd}'.format(cd=district_name))

			if(client_barrio):
				print('Barrio Name: {cd}'.format(cd=barrio_name))

			client_info["state"] = state_name
			client_info["county"] = county_name
			client_info["district"] = district_name

			get_code=False

			if(client_barrio):
				client_info["barrio"] = barrio_name
				barrio_code   = QualitasBarrio.get_by_id(client_barrio).code
				print('Barrio code: {cd}'.format(cd=barrio_code))
				get_code =True 

			state_code    = QualitasState.get_by_id(client_state).code
			county_code   = QualitasCounty.get_by_id(client_county).code
			district_code = QualitasDistrict.get_by_id(client_district).code



			print('=========== From DB ===========')
			print('State code: {cd}'.format(cd=state_code))
			print('County code: {cd}'.format(cd=county_code))
			print('District code: {cd}'.format(cd=district_code))

			full_state_code='0'+str(state_code)
			client_info["full_state_code"]   = full_state_code

			print('=========== Full County FIXED ===========')
			if(int(county_code))< 10:
				full_county_code='0'+str(state_code)+'00'+str(county_code)
			else:
				full_county_code='0'+str(state_code)+'0'+str(county_code)

			print('Full County code : {cd}'.format(cd=full_county_code))

			print('=========== Full District ===========')

			if(int(county_code))< 10:
				full_district_code='0'+str(state_code)+'0'+str(county_code)
			else:
				full_district_code='0'+str(state_code)+str(county_code)

			if(int(district_code))< 10:
				full_district_code = full_district_code + '0' + district_code
			else:
				full_district_code = full_district_code + district_code

			print('Full district code : {cd}'.format(cd=full_district_code))

			print('=========== Full Barrio ===========')

			full_barrio_code=''

			if(get_code):
				if(int(barrio_code))< 10:
					full_barrio_code = full_district_code + '0' + barrio_code
				else:
					full_barrio_code = full_district_code + barrio_code

				client_info["full_barrio_code"]   = full_barrio_code
				print('Full barrio code : {cd}'.format(cd=full_barrio_code))

			client_info["full_county_code"]   = full_county_code
			client_info["full_district_code"] = full_district_code
	
		else:
			print('***********COMPANY QUALITAS**************')
			client_info["state"] = client_state
			client_info["county"] = client_county
			client_info["district"] = client_district
			client_info["barrio"] = client_barrio


		client_info["phone"] = client_phone
		client_info["address"] = client_address
		client_info["email"] = client_email
		client_info["birthDate"] =client_birthdate
		client_info["client_marital_status"] =client_marital_status
		client_info["client_gender"] =client_gender
		client_info["client_study_level"] =client_study_level
		client_info["idCategory"] =client_study_level
		print(client_info)
		
		request.session['client_info'] = client_info


		return redirect(reverse_lazy('client.calculator.vehicle_info'))

	def lower(value):
		return value.title()

	# def phone_number(number):
	# 	first = number[0:3]
	# 	second = number[3:6]
	# 	third = number[6:10]
	# 	return '(' + first + ')' + ' ' + second + '-' + third

class GetBrandModelbyPlate(View):
	def get(self, request):
		plate_id = request.GET.get('plate_id', None)
		print ("***Credid Vehicle %s **** \n" %(plate_id) )
		vehicle_info_by_plate = {}
		vehicle_info_by_plate['plate'] = plate_id

		#Credid Initial date
		date1 = datetime.now()
		credid = Credid()
		carInfo=credid.getData(plate_id)
		#Credid End Date
		date2 = datetime.now()
		print("***TIME Credid Vehicle TOTAL **** \n%d seconds" %(date_diff_in_Seconds(date2, date1)))
		
		if(carInfo):
			print("Got vehicle info!")
			print("The Array is: ", carInfo)
			#vehicle_info[''] = plate
			vehicle_info_by_plate['propietario'] = carInfo["propietario"]
			vehicle_info_by_plate['brand'] = carInfo["brand"].title()
			vehicle_info_by_plate['model'] = carInfo["model"].title()
			vehicle_info_by_plate['year'] = carInfo["year"]

			vehicle_info_by_plate['motor'] = carInfo["motor"]
			vehicle_info_by_plate['chasis'] = carInfo["vin"]
			vehicle_info_by_plate['type'] = carInfo["type"]
			vehicle_info_by_plate['cilinders'] = carInfo["cilinders"]
			vehicle_info_by_plate['cc'] = carInfo["cc"]
			vehicle_info_by_plate['weight'] = carInfo["weight"]
			vehicle_info_by_plate['sits'] = carInfo["seats"]

		return JsonResponse(vehicle_info_by_plate, safe=False)

class CalculatorVehicleInfoView(View):

	template_name = 'client/calculator/calculator_vehicle_info.html'

	
	def get(self,request):
		show_dialog="False"

		
		context={
			'show_dialog':show_dialog,
		}
		return render(request, self.template_name,context=context)


	def post(self,request):
		
		vehicle_info = request.session['vehicle_info']
		plate = request.POST.get('plate_id', None)
		vehicle_info['plate'] = plate

		#Credid Initial date
		date1 = datetime.now()
		credid = Credid()
		carInfo=credid.getData(plate)
		#Credid End Date
		date2 = datetime.now()
		print("***TIME Credid Vehicle TOTAL **** \n%d seconds" %(date_diff_in_Seconds(date2, date1)))
		
		if(carInfo):
			print("Got vehicle info!")
			#vehicle_info[''] = plate
			vehicle_info['brand'] = carInfo["brand"]
			vehicle_info['model'] = carInfo["model"]
			vehicle_info['year'] = carInfo["year"]

			vehicle_info['motor'] = carInfo["motor"]
			vehicle_info['chasis'] = carInfo["vin"]
			vehicle_info['type'] = carInfo["type"]
			vehicle_info['cilinders'] = carInfo["cilinders"]
			vehicle_info['cc'] = carInfo["cc"]
			vehicle_info['weight'] = carInfo["weight"]
			vehicle_info['sits'] = carInfo["seats"]


		request.session['vehicle_info'] = vehicle_info
		return redirect(reverse_lazy('client.calculator.vehicle_info_result'))

class CalculatorVehicleInfoResultView(View):
	template_name = 'client/calculator/calculator_vehicle_info_result.html'

	def get(self,request):
		show_dialog="False"	
		vehicle_info = request.session['vehicle_info'] 
		damage_answer_list = ['No','Sí']
		company_info = request.session['company_info']
		selected_company=company_info['selected_company']
		print('selected company: {sel}'.format(sel=selected_company))
		
		#vehicle_color_list = ['Azul','Amarillo','rojo','verde','negro','blanco','rosa','cafe']
		vehicle_combustible_list = { #ins
			"Gasolina":"01",
			"Diesel":"02",
			"Eléctrico":"05",
			}

		vehicle_transmition_list = ['Manual', 'Automático']

		if selected_company == 'oceanica':
			vehicle_combustible_list = { #oceanica
				"Gasolina / Gas LP":"1",
				"Diesel":"2",
				"Híbrido":"3",
				"Eléctrico":"4",
				}
			
			versions=request.session['oceanica_versions']
			vehicle_color_list = Color.objects.filter(oceanica_color_code__isnull=False).order_by('name')
		
		if selected_company == 'qualitas':
			versions = request.session['qualitas_versions']
			vehicle_color_list = Color.objects.filter(qualitas_color_code__isnull=False).order_by('name')
		

		if selected_company == 'ins':
			vehicle_color_list = Color.objects.filter(ins_color_code__isnull=False).order_by('name')
			print(vehicle_color_list)

			context={
				'vehicle':vehicle_info,
				'vehicle_color_list':vehicle_color_list,
				'vehicle_combustible_list':vehicle_combustible_list,
				'vehicle_transmition_list':vehicle_transmition_list,
				'damage_answer_list':damage_answer_list,
				'selected_company':selected_company,
			}
		else:
			context={
				'vehicle':vehicle_info,
				'vehicle_color_list':vehicle_color_list,
				'vehicle_combustible_list':vehicle_combustible_list,
				'vehicle_transmition_list':vehicle_transmition_list,
				'damage_answer_list':damage_answer_list,
				'selected_company':selected_company,
				'versions':versions
			}


		return render(request, self.template_name,context=context)

	def post(self, request):
		#get all data and store in session
		vehicle_info = request.session['vehicle_info']
		
		vehicle_info['motor'] = request.POST.get('vehicle_motor', None)
		vehicle_info['chasis'] = request.POST.get('vehicle_chasis', None)
		vehicle_info['type'] = request.POST.get('vehicle_type', None)
		vehicle_info['cilinders'] = request.POST.get('vehicle_cilinders', None)
		vehicle_info['cc'] = request.POST.get('vehicle_cc', None)		
		vehicle_info['sits'] = request.POST.get('vehicle_sits', None)
		vehicle_info['weight'] = request.POST.get('vehicle_weight', None)
		
		vehicle_info['color'] = request.POST.get('vehicle_color', None)
		print('selected color is {col}'.format(col=vehicle_info['color']))

		vehicle_info['combustible'] = request.POST.get('vehicle_combustible', None)
		print('selected combustible {com}'.format(com=vehicle_info['combustible']))

		vehicle_info['version'] = request.POST.get('vehicle_version', None)
		vehicle_info['kms'] = request.POST.get('vehicle_kms', None)
		vehicle_info['has_debt'] = request.POST.get('vehicle_has_debt', None)	
		vehicle_info['creditor_name'] = request.POST.get('creditor_name', None)
		vehicle_info['creditor_id'] = request.POST.get('creditor_id', None)
		vehicle_info['credit_percentage'] = request.POST.get('credit_percentage', None)
		vehicle_info['has_damage'] = request.POST.get('vehicle_has_damage', None)

		#update qulitas amis
		company_info = request.session['company_info']
		selected_company=company_info['selected_company']
		quote_info ={}

		#print("Combustible =====================>" + vehicle_info['combustible'])

		if selected_company == 'qualitas':
			print('amis updated to {sel}'.format(sel = request.POST.get('vehicle_version')))
			vehicle_info = request.session['vehicle_info']
			print('=========== VEHICLE INFO ===========')
			print(request.session['qualitas_versions'][request.POST.get('vehicle_version')])
			
			vehicle_info['amis']= request.POST.get('vehicle_version')
			vehicle_info['version'] = request.session['qualitas_versions'][request.POST.get('vehicle_version')]
			request.session['vehicle_info'] = vehicle_info
		
		if selected_company == 'oceanica' :
			vehicle_info['version']= request.POST.get('vehicle_version')
			color_code = Color.objects.filter(oceanica_color_code__isnull=False).get(name= vehicle_info['color']).oceanica_color_code
			
		request.session['vehicle_info'] = vehicle_info
		return redirect(reverse_lazy('client.calculator.vehicle_sugese_info'))

class CalculatorVehicleSugeseInfoView(View):

	template_name = 'client/calculator/calculator_vehicle_sugese_info.html'

	def get(self,request):
		show_dialog="False"

		no_yes_answer_list = ['No','Sí']

		company_info = request.session['company_info']
		selected_company=company_info['selected_company']

		business={}

		if selected_company == 'qualitas':
			business={
			"1":"Financiero",
			"2":"Servicios",
			"3":"Comercial",
			"4":"Industrial",
			"5":"Turismo",
			"6":"Otro",
			}

		elif selected_company == 'oceanica':
			business ={
			"001":"Asalariado(a)",
			"002":"Comerciante de servicios",
			"003":"Comerciante de productos",
			"004":"Actividades artísticas, de entretenimiento y recreativas",
			"005":"Actividades de alojamiento y de servicio de comidas",
			"006":"Actividades de atención de la salud humana y de asistencia social",
			"007":"Actividades de los hogares como empleadores",
			"008":"Actividades de organizaciones y órganos extraterritoriales",
			"009":"Actividades de servicios administrativos y de apoyo",
			"010":"Actividades financieras y de seguros",
			"011":"Actividades inmobiliarias",
			"012":"Actividades profesionales, científicas y técnicas",
			"013":"Administracion pública y defensa; planes de seguridad social",
			"014":"Administradores de fondos de terceros",
			"015":"Agricultura, ganadería, silvicultura y pesca",
			"017":"Comercio al por mayor y al por menor",
			"018":"Construcción",
			"019":"Deporte",
			"020":"Enseñanza",
			"021":"Explotación de minas y canteras",
			"022":"Industrias manufactureras",
			"023":"Información y comunicaciones",
			"024":"Legal",
			"025":"Otras actividades de servicios",
			"026":"Pensionado(a)",
			"027":"Manejo aguas residuales, desechos y descontaminación",
			"028":"Suministro de electricidad, gas, vapor y aire acondicionado",
			"029":"Transporte y almacenamiento"
			}
		else:
			business ={}

		profession_list = Profession.objects.all().order_by('name')

		origin= {
		"001":"Asalariado",
		"002":"Personas físicas sin ingresos y personas jurídicas sin actividad comercial",
		"003":"No asalariado",
		}
		print(selected_company)

		context={
			'show_dialog':show_dialog,
			'no_yes_answer_list':no_yes_answer_list,
			'business':business,
			'origin':origin,
			'selected_company':selected_company,
			'profession_list':profession_list
		}

		return render(request, self.template_name,context=context)


	def post(self,request):
		
		sugese_info = {
			'work_phone' : request.POST.get('sugese_work_phone', None),
			'company' : request.POST.get('sugese_company_name', None),
			'comertial_activity' : request.POST.get('sugese_comertial_activity', None),
			'position' : request.POST.get('sugese_position', None),
			'income' : request.POST.get('sugese_income', None),
			'funds_source' : request.POST.get('sugese_funds_source', None),
			'thirdparty_funds' : request.POST.get('sugese_thirdparty_funds', None),
			'work_phone' : request.POST.get('sugese_politically_exposed', None),
		}
		
		request.session['sugese_info'] = sugese_info


		#Emit
		client = request.session['client_info']
		vehicle_info = request.session['vehicle_info']
		company_info = request.session['company_info']

		print('vehicle_info ' , vehicle_info)
		print('company_info ' ,company_info)
		print('client ' ,client)

		#next view 
		view=None

		if(company_info and company_info['selected_company'] == 'oceanica'):
			view = 'client.calculator.payments_type'
			#view = 'client.calculator.payment_oceanica'

		if(company_info and company_info['selected_company'] == 'qualitas'):
			view = 'client.calculator.payments_type'
	
		if(company_info and company_info['selected_company'] == 'ins'):
			print("ins")
			view = 'client.calculator.payments_type'

		context={}
		
		#this should go to payment module or review info
		#return render(request, template_name,context=context)
		return redirect(reverse_lazy(view))

class CalculatorPaymentSimulation(View):
	template_name = 'client/calculator/calculator_payment_simulation.html'

	def get(self, request):
		context={}
		return render(request,self.template_name, context=context)

	def post(self,request):
		return redirect(reverse_lazy('client.calculator.payment_success'))

class CalculatorPaymentSuccess(View):
	template_name = 'client/calculator/calculator_payment_success.html'

	def get(self, request):
		context={}
		return render(request,self.template_name, context=context)

	def post(self, request):
		path='poliza_demo.pdf'
		file_path = os.path.join(settings.MEDIA_ROOT, path)
		print('looking for pdf at {dir}'.format(dir=file_path))
		if os.path.exists(file_path):
			print('file exists')
			with open(file_path, 'rb') as fh:
				response = HttpResponse(fh.read(), content_type="application/pdf")
				print('here')
				response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
				return response
		raise Http404

class CalculatorPaymentTypeView(View):

	template_name = 'client/calculator/calculator_payment_type.html'
	
	def get(self,request):
		print('here')
		company_info = request.session['company_info']
		
		if(company_info and company_info['selected_company'] == 'qualitas'):
			payment_type={
				"M":"Mensual",
				"T":"Trimestral",
				"S":"Semestral",
				"C":"Anual"
				}
		else:
			payment_type={
				"M":"Mensual",
				"T":"Trimestral",
				"S":"Semestral",
				"A":"Anual"
				}

		anual=''
		semestral=''
		trimestral=''
		mensual=''

		if(company_info and company_info['selected_company'] == 'qualitas'):
			print('payment data for qualitas')
			vehicle= request.session['vehicle_info']
			amis = vehicle['amis']
			versions = request.session['qualitas_versions']
			qua_coverage = request.session['qua_coverage']

			if qua_coverage == "qua_basica":
				qualitas_quote_list=request.session['qualitasDictionary']
			else:
				qualitas_quote_list=request.session['qualitasDictionary2']

			for k,v in versions.items():
				if k == amis:
					quote_dict=qualitas_quote_list
					break
				index=index+1

			anual=quote_dict['Pago Anual']
			semestral= quote_dict['Pago Semestral']
			trimestral=quote_dict['Pago Trimestral']
			mensual=quote_dict['Pago Mensual']

		elif(company_info and company_info['selected_company'] == 'oceanica'):

			print('payment data for oceanica')
			vehicle_info = request.session['vehicle_info']	
			id_version = vehicle_info['version']
			oceanica_quotes= request.session['oceanica_quotes']
			oceanica_quotes_ids = request.session['oceanica_quotes_ids']

			anual=oceanica_quotes[id_version]['Pago Anual']
			semestral=oceanica_quotes[id_version]['Pago Semestral']
			trimestral=oceanica_quotes[id_version]['Pago Trimestral']
			mensual=oceanica_quotes[id_version]['Pago Mensual']

		else:
			print('payment data for INS')
			ins_coverage = request.session['ins_coverage']
			if ins_coverage == 'ins_914':
				iDict=request.session['insDictionary']
			else:
				iDict=request.session['insDictionary2']

			anual=iDict['Pago Anual']
			semestral= iDict['Pago Semestral']
			trimestral=iDict['Pago Trimestral']
			mensual=iDict['Pago Mensual']


		context={ 
		'payment_type':payment_type,
		'anual':anual,
		'semestral':semestral,
		'trimestral':trimestral,
		'mensual':mensual
		}
		
		return render(request, self.template_name,context=context)

	def post(self, request):
		context={}

		#Dynamically generate a number from DB
		order='0000000001'

		#from previous view
		payment_type = request.POST.get('payment_type', None)	
		client  = request.session['client_info']
		vehicle = request.session['vehicle_info']
		sugese  = request.session['sugese_info']
		company_info = request.session['company_info']

		if(company_info and company_info['selected_company'] == 'qualitas'):
			print("qualitas")

			qualitasAdaptee = QualitasAdaptee()
			full_name=client["name"].upper()+' '+client["lastName1"].upper() + ' ' + client["lastName2"].upper()
			
			#def createInsurance(self,
			#full_name,address,district,amis,year,
			#vehicle_desc,motor_number,serial,vehicle_plate,
			# creditor,creditor_id,credit_amount,phone,id_type,birth_date,
			#profession,comertial_activity,id_number,
			# county,name,lastname,second_lastname,state_code,county_code,id_type_number,district_code,barrio_code,
			# income,gender,company_name,marital_status,business,profession_code):

			#state =QualitasState.get_by_id()
			
			county  = client["county"]
			district= client["district"]

			business={
			"1":"Financiero",
			"2":"Servicios",
			"3":"Comercial",
			"4":"Industrial",
			"5":"Turismo",
			"6":"Otro",
			}

			for k, v in business.items():
				if k == sugese["comertial_activity"]:
					activity = v.upper()
			

			profession_detail = sugese["position"]
			profession = Profession.objects.get(name=profession_detail)

			address=client["address"].upper()
			print('Address is {ad}'.format(ad=address))

			vehicle_motor=vehicle["motor"].upper()
			print('vehicle_motor is {ad}'.format(ad=vehicle_motor))

			vehicle_chasis=vehicle["chasis"].upper()
			print('vehicle_chasis is {ad}'.format(ad=vehicle_chasis))

			vehicle_plate = vehicle["plate"].upper()
			print('vehicle_plate is {ad}'.format(ad=vehicle_plate))

			vehicle_creditor = vehicle["creditor_name"].upper()
			print('vehicle_creditor is {ad}'.format(ad=vehicle_creditor))

			name=client["name"].upper()
			print('name is {ad}'.format(ad=name))

			lastname1=client["lastName1"].upper()
			print('lastname1 is {ad}'.format(ad=lastname1))

			lastname2=client["lastName2"].upper()
			print('lastname2 is {ad}'.format(ad=lastname2))

			company = sugese["company"].upper()
			print('company is {ad}'.format(ad=company))

			mail=client["email"].upper()
			print('mail is {ad}'.format(ad=mail))

			position = sugese["position"].upper()
			comertial_activity=sugese["comertial_activity"].upper()

			print('**** STATE IS {st} ****'.format(st=client["state"]))

			#TODO: There is need to add verification for returned string

			qualitasAdaptee.createInsurance(
				full_name,address,client["full_county_code"],vehicle["amis"],vehicle["year"],
				vehicle['version'], vehicle_motor,vehicle_chasis,vehicle_plate,
				vehicle_creditor,vehicle["creditor_id"], vehicle["credit_percentage"],client["phone"],
				client["client_id_type"],client["birthDate"],position,activity,client["client_id"],
				county, name,lastname1,lastname2,client["full_state_code"],client["full_county_code"],'1',client["full_district_code"], client["full_barrio_code"],
				sugese["income"].replace('₡','',1).replace(',','',6),client["client_gender"],company,client["client_marital_status"],
				comertial_activity,profession.qualitas_profession_code,mail,payment_type
			)

			#Getting quote amount
			amis = vehicle['amis']
			versions = request.session['qualitas_versions']
			qua_coverage = request.session['qua_coverage']

			if qua_coverage == "qua_basica":
				qualitas_quote_list=request.session['qualitasDictionary']
			else:
				qualitas_quote_list=request.session['qualitasDictionary2']

			quote_dict={}

			#index=0

			for k,v in versions.items():
				if k == amis:
					#quote_dict=qualitas_quote_list[index]
					quote_dict=qualitas_quote_list
				#index=index+1

			if(payment_type == 'C'):
				monto = quote_dict['Pago Anual']
			elif(payment_type == 'S'):
				monto = quote_dict['Pago Semestral']
			elif(payment_type == 'T'):
				monto = quote_dict['Pago Trimestral']
			elif(payment_type == 'M'):
				monto = quote_dict['Pago Mensual']

			print('monto {monto}'.format(monto=monto))
			monto_real = monto
			monto = '₡5.00'  #solo para pruebas Jorge

			payment_data={
			'monto':monto.replace('₡','').replace(',',''),   
			'order':order,
			'redirect':'http://seguru.cr/calculator/payments-results/',
			'payment_type':payment_type,
			'moneda' : 'CO',
			'des_monto' : monto,
			'des_monto_real' : monto_real,  #Pruebas JORGE
			'type':payment_type
			}

			request.session['payment_data']=payment_data

			return redirect(reverse_lazy('client.calculator.payment_confirm'))			
		
		elif(company_info and company_info['selected_company'] == 'oceanica'):

			print('post paymets view is: {req}'.format(req=request))
			#sacar el valor del tipos de pago
			#payment_type = request.POST.get('payment_type', None)

			client = request.session['client_info']
			vehicle_info = request.session['vehicle_info']
			company_info = request.session['company_info']
			id_version = vehicle_info['version']
			
			oceanica_quotes= request.session['oceanica_quotes']
			oceanica_quotes_ids = request.session['oceanica_quotes_ids']
			poliza = oceanica_quotes_ids[id_version]
			monto = 0 
			
			if (payment_type == 'A'):
				monto=oceanica_quotes[id_version]['Pago Anual']
			if (payment_type == 'S'):
				monto=oceanica_quotes[id_version]['Pago Semestral']
			if (payment_type == 'T'):
				monto=oceanica_quotes[id_version]['Pago Trimestral']
			if (payment_type == 'M'):
				monto=oceanica_quotes[id_version]['Pago Mensual']
			#k == 'Pago Anual' or k == 'Pago Semestral' or k == 'Pago Mensual' or k == 'Pago Trimestral' 

			print (payment_type, poliza, monto)
			monto = '₡5.00'  #solo para pruebas Jorge
				
			payment_data={
			'monto':monto.replace('₡','').replace(',',''),
			'order':order,
			'indredirect':'S',
			#'poliza':policy['PolicyNumber'],
			#'redirect':'http://seguru.cr/calculator/payments-results/',
			'redirect':'http://127.0.0.1:8000/calculator/payments-results/', #Para pruebas en local Jorge
			'payment_type':payment_type,
			'moneda' : 'CO',
			'des_monto' : monto,
			'type':payment_type
			}

			request.session['payment_data']=payment_data
			
			#return requests.post(path, mydata)
			return redirect(reverse_lazy('client.calculator.payment_confirm'))

		else:
			#add INS emision info prior to payment
			#return render(request, self.template_name,context=context)
			print('INS selected payment_type: {payment_type}'.format(payment_type=payment_type))
			
			#get correct value
			ins_coverage = request.session['ins_coverage']
			if ins_coverage == 'ins_914':
				iDict=request.session['insDictionary']
			else:
				iDict=request.session['insDictionary2']

			anual=iDict['Pago Anual']
			semestral= iDict['Pago Semestral']
			trimestral=iDict['Pago Trimestral']
			mensual=iDict['Pago Mensual']

			if (payment_type == 'A'):
				monto=iDict['Pago Anual']
			if (payment_type == 'S'):
				monto=iDict['Pago Semestral']
			if (payment_type == 'T'):
				monto=iDict['Pago Trimestral']
			if (payment_type == 'M'):
				monto=iDict['Pago Mensual']
			
			monto = '₡5.00'  #solo para pruebas Jorge
			
			payment_data={
			'monto':monto.replace('₡','').replace(',',''),
			'order':order,
			#'redirect':'http://seguru.cr/calculator/payments-results/',
			'redirect':'http://127.0.0.1:8000/calculator/payments-results/', #Para pruebas en local Jorge
			'payment_type':payment_type,
			'moneda' : 'CO',
			'des_monto' : monto,
			'type':payment_type
			}

			request.session['payment_data']=payment_data

			return redirect(reverse_lazy('client.calculator.payment_confirm'))

class CalculatorPaymentConfirmView(View):

	template_name = 'client/calculator/calculator_payment_confirm.html'
	
	def get(self,request):

		context={}

		payment_data =request.session['payment_data']

		company_info = request.session['company_info']
		selected_company=company_info['selected_company']

		client  = request.session['client_info']
		vehicle = request.session['vehicle_info']

		response_vehicle_dict = request.session.get('response_vehicle_dict', None)

		if response_vehicle_dict:
			vehicle_brand_id = response_vehicle_dict['vehicle_brand']
			vehicle_model_id = response_vehicle_dict['vehicle_model']
			year = response_vehicle_dict['vehicle_year']
			vehicle_value = response_vehicle_dict['vehicle_value']

		brand_description = VehicleBrand.get_by_id(vehicle_brand_id)
		model_description = VehicleModel.get_by_id(vehicle_model_id)		

		payment_type={
		"M":"Mensual",
		"T":"Trimestral",
		"S":"Semestral",
		"A":"Anual"
		}

		#case for QUALITAS
		if(company_info and company_info['selected_company'] == 'qualitas'):

			payment_type={
				"M":"Mensual",
				"T":"Trimestral",
				"S":"Semestral",
				"C":"Anual"
				}
			
			#Hash generation
			# t0 = datetime(1970, 1, 1)
			# now = datetime.utcnow()
			# seconds = (now - t0).total_seconds()
			
			seconds=int(int(round(time.time() * 1000)) /1000)
			seconds = 1199
			# print('seconds: {sec}'.format(sec=seconds))
			orderid = str(payment_data['order'])
			amount=str(payment_data['monto'])

			key= '9ZAjZkr748bc8a69n8TwR4ST4E52BxAX'
			
			processor_id = '11085083'
			
			s= orderid + '|' + str(amount) + '|' + str(seconds) + '|' + key
			encoding='utf-8'	
			md5_hash=md5(s.encode(encoding)).hexdigest()

			test= 'test' +'|'+ '1.00' +'|'+ '1279302634' + '|' + '23232332222222222222222222222222' 
			md5_hash2=md5(test.encode(encoding)).hexdigest()

			print('MD5: {md5_hash}'.format(md5_hash=md5_hash))
			print('MD5: {md5_hash}'.format(md5_hash=md5_hash2))
			
			
			context={
			#digital payment
			'hash' : md5_hash,
			'time' : seconds,
			#'amount' : payment_data['monto'],
			'amount' : amount,
			'type' : 'sale',
			'key_id' : '11775721',
			'processor_id' : processor_id,
			'order_id' : orderid,			
			'redirect' : payment_data['redirect'],
			
			#car
			'brand' : brand_description,
			'model' : model_description,
			'year' : year,
			'value' : vehicle_value,
			
			#client data
			'name' : client["name"].lower().capitalize() +' '+client["lastName1"].lower().capitalize() +' '+client["lastName2"].lower().capitalize(),
			'id' : client["client_id"],
			'email' : client["email"],
			'phone':  client["phone"],
			
			#insurance
			'selected_company' : selected_company.capitalize(),
			'des_monto' : payment_data['des_monto'],
			'des_monto_real' : payment_data['des_monto_real'],
			'payment_type': payment_type[payment_data['payment_type']]
			}

		#case for OCEANICA
		elif(company_info and company_info['selected_company'] == 'oceanica'):
			
			context={
			#'monto' : payment_data['monto'],
			'monto' : 5.00,
			'order' : payment_data['order'],
			'indredirect' : payment_data['indredirect'],
			'redirect' : payment_data['redirect'], 
			#'poliza' : payment_data['poliza'], 
			'moneda' : payment_data['moneda'],
			'selected_company' : selected_company.capitalize(),
			'brand' : brand_description,
			'model' : model_description,
			'year' : year,
			'value' : vehicle_value,
			'name' : client["name"].lower().capitalize() +' '+client["lastName1"].lower().capitalize() +' '+client["lastName2"].lower().capitalize(),
			'id' : client["client_id"],
			'email' : client["email"],
			'phone':  client["phone"],
			'des_monto' : payment_data['des_monto'],
			'payment_type': payment_type[payment_data['payment_type']]
			}

		#case for INS
		else:
			client = request.session['client_info']
			vehicle_info = request.session['vehicle_info']
			color_code = Color.objects.filter(ins_color_code__isnull=False).filter(name= vehicle_info['color']).latest('ins_color_code')
			
			now = datetime.now() # current date and time
			startDate = now.strftime("%d/%m/%Y")
			oneyear = now + timedelta(days=365)
			endDate = oneyear.strftime("%Y%m%d")

			insAdaptee = InsAdaptee()
			name = client["name"].lower().capitalize() +' '+client["lastName1"].lower().capitalize() +' '+client["lastName2"].lower().capitalize()
					#emitInsPolicy(solicitante, fromDate, idType, clientID,birdDate, PHOSINBUC, SERIERSGO, plateType, CLASEPLACA2RSGO,plateNumber,year, 
					#address, amount,CLASEPLACARSGO,MONTO1,TIPDEDCOBK,toDate, fullName,
					#stateId,countyId, districtId, TIPOTARIFRSGO, 
					#brandId, ModelId, Gender,FACEXP,VALORMERC,PORCDESCCOB, 
					#email,capacity, cilinders, colorId, cc, 
					#MONTO8,weigth, vinNumber, motorSerial,
					#loadType, combustibleId,vehicleTypeId,firstName,middleName,lastName, secondLastName):	
			result = insAdaptee.emitInsPolicy(914, name, startDate, '0', client["client_id"],client["birthDate"], '', '001','N', '00',vehicle_info['plate'],vehicle_info['year'],
			client["address"],vehicle_info['vehicle_value'],'PAR',0,'B',endDate,name,
			client["state"],client["county"],client["district"],'15',
			vehicle_info['ins_brand_code'],vehicle_info['ins_model_code'],client["client_gender"],'-45','P','00', 
			client["email"],vehicle_info['sits'], vehicle_info['cilinders'] , color_code,vehicle_info['cc'],
			'04',vehicle_info['weight'],vehicle_info['chasis'],vehicle_info['motor'],
			'05',vehicle_info['combustible'],'02',client["name"],'',client["lastName1"],client["lastName2"])
			print('result emit Ins ' + str(result) )

			context={
			#digital payment
			'amount' : payment_data['monto'],
			'order_id' : payment_data['order'],			
			'redirect' : payment_data['redirect'],
			
			#car
			'brand' : brand_description,
			'model' : model_description,
			'year' : year,
			'value' : vehicle_value,
			
			#client data
			'name' : client["name"].lower().capitalize() +' '+client["lastName1"].lower().capitalize() +' '+client["lastName2"].lower().capitalize(),
			'id' : client["client_id"],
			'email' : client["email"],
			'phone':  client["phone"],
			
			#insurance
			'selected_company' : selected_company.capitalize(),
			'des_monto' : payment_data['des_monto'],
			'payment_type': payment_type[payment_data['payment_type']]
			}


		return render(request, self.template_name,context=context)

	def post(self,request):
		print('inside CalculatorPaymentConfirmView post ...')
		api = ApiAdapter()
		
		url='http://201.195.233.245/api/v1/pagos/cargostarjeta'
		auth={}
		method='POST'
		headers = {'content-type': 'application/soap+xml'}

		body={
			"numeroTarjeta": "SNhQBIvG9cZND3P0kFwzKA==",
			"montoTransaccion": "wVDoEsWkT5SOSbi1wR4mUA==",
			"cvv": "tVM5fkwSlxK4miLvt5yPlA",
			"mesExpiracion": "reLAyrNMUGuFujtdRfWMZQ",
			"anioExpiracion": "W9w9DNjTDSxa7bCmLOPsxw",
			"moneda": "GNVRxAF5+L+4SKQBkr8P7w==",
			"detalleCargo": "0mlL01n/iAuUc+gOaLvUVQ==",
			"correoElectronico": "OAH6UpFE1pEhLbEBBxq/EA=="
		}

		response=api.apiRequest(url, headers, method, request, auth)

		print('INS payments response {res}'.format(res=response))

		return redirect(reverse_lazy('client.calculator.payment_success'))

class CalculatorPaymentResults(View):

	template_name = 'client/calculator/calculator_payment_success.html'

	def get(self,request):

		#print('get paymets request is: {req}'.format(req=request))

		company_info = request.session['company_info']
		client = request.session['client_info']
		vehicle_info = request.session['vehicle_info']
		payment_data=request.session['payment_data']
		client_id = client['client_id'] 
		payment_data=request.session['payment_data']

		approved=True

		#if(company_info and company_info['selected_company'] == 'qualitas'):

		if(company_info and company_info['selected_company'] == 'oceanica'):
		
			response=request.GET['response']
			response_text=request.GET['responsetext']
			authcode=request.GET['authcode']
			#reponse_code=request.GET['reponse_code']
			#response=1&responsetext=APROBADA&orderid=0000000001&authcode=674791&response_code=100
			print('authorization code is: {code}'.format(code=authcode))
			print('response text is: {text}'.format(text=response_text))
			print('response is: {text}'.format(text=response))

			if(str(response_text) == 'APROBADA'):
				oceanicaAdaptee = OceanicaAdaptee()
			
				canalVenta= '001'
				tipocer = 'P' #Personal
				brokerCode = '000025'
				intermediaryCode = '014120' # '016123'

				now = datetime.now() # current date and time
				startDate = now.strftime("%Y%m%d")
				oneyear = now + timedelta(days=365)
				endDate = oneyear.strftime("%Y%m%d")
				
				print('from ' + startDate + ' to ' + endDate) 
				quotation_info = request.session['quotation_info']
				print('Quotation ' ,quotation_info)

				clientID = None
				#register client

				state_code=OceanicaState.objects.get(id=client["state"]).code
				county_code=OceanicaCounty.objects.get(id=client["county"]).code
				district_code= OceanicaDistrict.objects.get(id=client["district"]).code

				birthDate = datetime.strptime(client["birthDate"], "%d/%m/%Y").strftime('%Y%m%d')
				DueDate = datetime.strptime(client["DueDate"], "%d/%m/%Y").strftime('%Y%m%d')

				try:
					clientID = oceanicaAdaptee.createClient(
						client["name"], 
						client["lastName1"] + ' ' + client["lastName2"], 
						state_code, county_code, district_code, client["address"], 
						client["client_marital_status"], client["email"], birthDate, 
						DueDate, client["client_id"], client["client_category_id"], 
						client["client_study_level"], client["client_gender"], client["phone"], 
						client["phone"],client["client_id_type"], tipocer)
					
				except:
					print('errror')
					#return redirect(reverse_lazy('INTRODUCIR_PAGINA_DE_ERROR'))
				
				print('Client created with id ', clientID)
				
				request.session['client_id']=clientID
				
				#create policy
				policy={}
				
				#Que es primero el huevo o la gallina Este valor viene del resultado del pago, pero el pago requiere la creacion de la poliza primero
				#NIDTRANSACC = authcode

				vehicle_motor=vehicle_info["motor"].upper()
				print('vehicle_motor is {ad}'.format(ad=vehicle_motor))

				vehicle_chasis=vehicle_info["chasis"].upper()
				print('vehicle_chasis is {ad}'.format(ad=vehicle_chasis))

				vehicle_plate = vehicle_info["plate"].upper()
				print('vehicle_plate is {ad}'.format(ad=vehicle_plate))

				color_code = Color.objects.filter(oceanica_color_code__isnull=False).get(name= vehicle_info['color']).oceanica_color_code

				policy = oceanicaAdaptee.createInsurancePolicy(canalVenta,clientID,
					brokerCode,
					intermediaryCode,
					payment_data['type'],
					startDate,
					endDate,
					quotation_info['oceanicaQuotationId'], 
					'CO', #currency colones
					authcode,
					'-',
					color_code,
					vehicle_plate,
					vehicle_chasis,
					vehicle_motor
					)

				print("policy created with Id: " + policy['PolicyNumber'])
			else:
				approved=False
				print('pago denegado')


			# oceanicaAdaptee = OceanicaAdaptee()
			
			# now = datetime.now() # current date and time
			# startDate = now.strftime("%Y%m%d")
			# oneyear = now + timedelta(days=365)
			# endDate = oneyear.strftime("%Y%m%d")
			# color_code = Color.objects.filter(oceanica_color_code__isnull=False).get(name= vehicle_info['color']).oceanica_color_code
			
			# cert = oceanicaAdaptee.createInsuranceCert(
			# 	vehicle_info['year'],
			# 	'COA3;6',#codCober
			# 	'P/0/375000',#codDeduc
			# 	vehicle_info['oceanica_brand_code'], 
			# 	vehicle_info['oceanica_model_code'],
			# 	'500',#codPlan
			# 	'APTV',#codProd
			# 	vehicle_info['version'],
			# 	color_code, #color
			# 	vehicle_info['combustible'], #combustible

			# 	authcode,#numContrato
			# 	vehicle_info['motor'], 
			# 	vehicle_info['plate'],
			# 	'005',#revPlan 005 en colones 011 en dolares
			# 	vehicle_info['chasis'],
			# 	'VC',#valuacion

			# 	startDate, #DateInit 
			# 	vehicle_info['kms'], 
			# 	payment_data['poliza'], 
			# 	vehicle_info['vehicle_value'], 
				
			# 	'N',#clas
			# 	client_id,
			# 	"",#factClient vacio si el pagado es el asegurado
			# 	'N',#indactivar
			# 	'S',#tomadorAcredor
			# 	'N',#salvar
			# 	'N',#sumaQEsp
			# 	'PER')
			# print('OCEANICA CERTIFICATE IS {cert}'.format(cert=cert))
			# print('returning payment success for oceanica ...')

		if(company_info and company_info['selected_company'] == 'qualitas'):

			response=request.GET['response']
			#response_text=request.GET['responsetext']
			authcode=request.GET['authcode']

			if(str(response) == '1'):
				print('aprobada transaccion qualitas')
			else:
				approved=False

				#Prueba para Emisión - Jorge
				qualitasAdaptee = QualitasAdaptee()

				#myApi.createInsurance('leroy bernard','100n del marriot','10403','4319','2018',
				# 'KIA SPORTAGE 2.4L 4X4 GASOLINA','motor_number','serial','vehicle_plate','creditor','creditor_id','credit_amount','phone',
				# 'id_type','01/23/1979','profession','comertial_activity','110250602','county','name','lastname','second_lastname','state_code',
				# 'county_code','id_type_number','district_code','barrio_code','1000000','M','its','D','business',
				# 'profession_code','leroy.bernard@gmail.com')

				#createInsurance(self,  full_name,address,codelect,amis,year,
				# vehicle_desc,motor_number,serial,vehicle_plate,creditor,creditor_id,credit_amount,phone,
				# id_type,birth_date,profession,comertial_activity,id_number,county,name,lastname,second_lastname,state_code,
				# county_code,id_type_number,district_code,barrio_code,	income,gender,company_name,marital_status,business,
				# profession_code,mail,payment_type)


				qualitasAdaptee = QualitasAdaptee()
				full_name=client["name"].upper()+' '+client["lastName1"].upper() + ' ' + client["lastName2"].upper()
				
				#state =QualitasState.get_by_id()
				
				county  = client["county"]
				district= client["district"]

				business={
				"1":"Financiero",
				"2":"Servicios",
				"3":"Comercial",
				"4":"Industrial",
				"5":"Turismo",
				"6":"Otro",
				}

				for k, v in business.items():
					if k == sugese["comertial_activity"]:
						activity = v.upper()
				

				profession_detail = sugese["position"]
				profession = Profession.objects.get(name=profession_detail)

				address=client["address"].upper()
				print('Address is {ad}'.format(ad=address))

				vehicle_motor=vehicle["motor"].upper()
				print('vehicle_motor is {ad}'.format(ad=vehicle_motor))

				vehicle_chasis=vehicle["chasis"].upper()
				print('vehicle_chasis is {ad}'.format(ad=vehicle_chasis))

				vehicle_plate = vehicle["plate"].upper()
				print('vehicle_plate is {ad}'.format(ad=vehicle_plate))

				vehicle_creditor = vehicle["creditor_name"].upper()
				print('vehicle_creditor is {ad}'.format(ad=vehicle_creditor))

				name=client["name"].upper()
				print('name is {ad}'.format(ad=name))

				lastname1=client["lastName1"].upper()
				print('lastname1 is {ad}'.format(ad=lastname1))

				lastname2=client["lastName2"].upper()
				print('lastname2 is {ad}'.format(ad=lastname2))

				company = sugese["company"].upper()
				print('company is {ad}'.format(ad=company))

				mail=client["email"].upper()
				print('mail is {ad}'.format(ad=mail))

				position = sugese["position"].upper()
				comertial_activity=sugese["comertial_activity"].upper()

				print('**** STATE IS {st} ****'.format(st=client["state"]))

				#TODO: There is need to add verification for returned string

				qualitasAdaptee.createInsurance(
					full_name,address,client["full_county_code"],vehicle["amis"],vehicle["year"],
					vehicle['version'], vehicle_motor,vehicle_chasis,vehicle_plate,
					vehicle_creditor,vehicle["creditor_id"], vehicle["credit_percentage"],client["phone"],
					client["client_id_type"],client["birthDate"],position,activity,client["client_id"],
					county, name,lastname1,lastname2,client["full_state_code"],client["full_county_code"],'1',client["full_district_code"], client["full_barrio_code"],
					sugese["income"].replace('₡','',1).replace(',','',6),client["client_gender"],company,client["client_marital_status"],
					comertial_activity,profession.qualitas_profession_code,mail,payment_type
				)

				# full_name = client["name"] + ' ' + client["lastName1"] + ' ' + client["lastName2"]
				# address = client["address"]
				# codelect = '10403'  # Buscar INFO
				# amis = vehicle_info['amis']
				# year = vehicle_info['year']
				# vehicle_desc = "Vehiculo de Prueba"
				# motor_number = vehicle_info['motor'].upper()
				# serial = vehicle_info['chasis'].upper()
				# vehicle_plate = vehicle_info["plate"].upper()

				# result = qualitasAdaptee.createInsurance(full_name, address, amis, year,
				# 			vehicle_desc, motor_number, serial, vehicle_plate

				# print("policy created with Id: " + policy['PolicyNumber'])

		if(company_info and company_info['selected_company'] == 'ins'):
			print('ins payment result')

		request.session['approved']=approved
		
		context={
			#'show_dialog':show_dialog,
			'approved': approved
		}
		
		return render(request, self.template_name,context=context)


	def post(self,request):

		approved = request.session['approved']

		if(approved):
			return redirect(reverse_lazy('client.calculator.vehicle'))
		else:
			return redirect(reverse_lazy('client.calculator.payment_confirm'))


class CalculatorVehicleRequestView(View):
	template_name = 'client/calculator/calculator_request_insurance.html'
	
	def get(self, request, insurance_id):
		response_vehicle_dict = request.session.get('response_vehicle_dict', None)
		
		insurance = Insurance.get_by_id(insurance_id=insurance_id)
		
		client_form = ClientForm()
		
		context = {
			'title_section': 'Solicitud de Seguro de Vehículos',
			'title_insurance': 'Seguro de Vehículos',
			'response_vehicle_dict': response_vehicle_dict,
			'insurance': insurance,
			'client_form': client_form,
		}
		
		return render(request, self.template_name, context=context)
	
	def post(self, request, insurance_id):
		response_vehicle_dict = request.session.get('response_vehicle_dict', None)
		
		insurance = Insurance.get_by_id(insurance_id=insurance_id)
		
		client_form = ClientForm(request.POST)
		
		if client_form.is_valid() and response_vehicle_dict:
			with transaction.atomic():
				try:
					client_instance = client_form.save()
					
					client_response = client_instance \
						.vehicle_responses \
						.create(vehicle_model_id=response_vehicle_dict['vehicle_model'],
								vehicle_year_id=response_vehicle_dict['vehicle_year'],
								vehicle_value=response_vehicle_dict['vehicle_value'])
					
					client_response.insurance.add(insurance)
					
					del request.session['response_vehicle_dict']
					
					return redirect(reverse_lazy('client.calculator.sent_request'))
				
				except IntegrityError:
					pass
		
		context = {
			'title_section': 'Solicitud de Seguro de Vehículos',
			'title_insurance': 'Seguro de Vehículos',
			'response_life_dict': response_vehicle_dict,
			'insurance': insurance,
			'client_form': client_form,
		}
		
		return render(request, self.template_name, context=context)

class CalculatorSentRequestView(TemplateView):
	template_name = 'client/calculator/calculator_sent_request.html'
