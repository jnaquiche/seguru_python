from django.conf import settings
from django.shortcuts import render
from django.views import View

from seguru_website.libraries.EmailThread import EmailThread


class ContactFormSendView(View):
	template_name = 'client/contact_form/contact_form.html'

	def get(self, request):
		return render(request, self.template_name)

	def post(self, request):
		
		# EmailThread(subject='Formulario de Contacto',
		# 			email_data=request.POST,
		# 			recipient_list=[settings.DEFAULT_FROM_EMAIL]).start()

		return render(request, self.template_name, context={'success_message': 'Su mensaje se ha enviado con éxito'})
