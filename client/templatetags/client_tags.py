from django import template
from django.urls import reverse_lazy

from insurance.models import Post, InsuranceInfo

register = template.Library()


@register.inclusion_tag('client/layout/menu.html')
def display_menu():
	pages_in_menu = Post.posts_in_menu()

	menu_pages = []

	menu_pages.append({
		'title': 'Inicio',
		'page_url': reverse_lazy('client.index'),
	})

	for page in pages_in_menu:
		menu_pages.append({
			'title': page.title,
			'page_url': reverse_lazy('client.page.single', kwargs={'page_slug': page.slug})
		})

	return {
		'menu_pages': menu_pages,
	}


@register.filter()
def request_url(insurance):
	if insurance.insurance_info.category == InsuranceInfo.INSURANCE_CATEGORY_LIFE:
		return reverse_lazy('client.calculator.life_request', kwargs={'insurance_id': insurance.id})

	elif insurance.insurance_info.category == InsuranceInfo.INSURANCE_CATEGORY_VEHICLE:
		return reverse_lazy('client.calculator.vehicle_request', kwargs={'insurance_id': insurance.id})

	elif insurance.insurance_info.category == InsuranceInfo.INSURANCE_CATEGORY_MEDICAL:
		return reverse_lazy('client.calculator.medical_request', kwargs={'insurance_id': insurance.id})

	elif insurance.insurance_info.category == InsuranceInfo.INSURANCE_CATEGORY_HOME:
		return reverse_lazy('client.calculator.home_request', kwargs={'insurance_id': insurance.id})
