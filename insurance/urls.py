from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from insurance.views import *

urlpatterns = [
	path('', PostListView.as_view(), name='insurance.index'),

	# Posts URL'S
	path('post/', PostListView.as_view(), name='insurance.post.list'),
	path('post/add/', PostAddView.as_view(), name='insurance.post.add'),
	path('post/<int:pk>/edit/', PostEditView.as_view(), name='insurance.post.edit'),
	path('post/<int:pk>/delete/', PostDeleteView.as_view(), name='insurance.post.delete'),
	path('post/sort/', PostSortView.as_view(), name='insurance.post.sort'),

	# Post Image URL'S
	path('post-image/<int:post_id>/add/', PostImageAddView.as_view(), name='insurance.post_image.add'),
	path('post-image/<int:post_image_id>/delete/', PostImageDeleteView.as_view(), name='insurance.post_image.delete'),

	# Section Home URL'S
	path('section-home/', SectionHomeTemplateListView.as_view(), name='insurance.section_home_template.list'),
	path('section-home/add/', SectionHomeTemplateAddView.as_view(), name='insurance.section_home_template.add'),
	path('section-home/<int:pk>/edit/', SectionHomeTemplateEditView.as_view(),
		 name='insurance.section_home_template.edit'),

	path('section-icon/add/', SectionIconAddView.as_view(), name='insurance.section_icon.add'),
	path('section-icon/<int:pk>/edit/', SectionIconEditView.as_view(), name='insurance.section_icon.edit'),

	# Company URL'S
	path('company/', CompanyListView.as_view(), name='insurance.company.list'),
	path('company/add/', CompanyAddView.as_view(), name='insurance.company.add'),
	path('company/<int:pk>/edit/', CompanyEditView.as_view(), name='insurance.company.edit'),

	# # Insurance Category URL'S
	# path('insurance-category/', InsuranceCategoryListView.as_view(), name='insurance.insurance_category.list'),
	# path('insurance-category/add/', InsuranceCategoryAddView.as_view(), name='insurance.insurance_category.add'),
	# path('insurance-category/<int:pk>/edit/', InsuranceCategoryEditView.as_view(),
	# 	 name='insurance.insurance_category.edit'),

	# Insurance Property URL'S
	path('insurance-property/', InsurancePropertyListView.as_view(), name='insurance.insurance_property.list'),
	path('insurance-property/add/', InsurancePropertyAddView.as_view(), name='insurance.insurance_property.add'),
	path('insurance-property/<int:pk>/edit/', InsurancePropertyEditView.as_view(),
		 name='insurance.insurance_property.edit'),
	path('insurance-property/sort/', InsurancePropertySortView.as_view(), name='insurance.insurance_property.sort'),

	# Insurance Info URL'S
	path('insurance-info/', InsuranceInfoListView.as_view(), name='insurance.insurance_info.list'),
	path('insurance-info/add/', InsuranceInfoAddView.as_view(), name='insurance.insurance_info.add'),
	path('insurance-info/<int:pk>/detail/', InsuranceInfoDetailView.as_view(),
		 name='insurance.insurance_info.detail'),
	path('insurance-info/<int:pk>/edit/', InsuranceInfoEditView.as_view(), name='insurance.insurance_info.edit'),

	path('insurance-info/<int:insurance_info_id>/add-property/', InsuranceInfoPropertyAddView.as_view(),
		 name='insurance.insurance_info.add_property'),
	path('insurance-info/<int:insurance_info_id>/add-insurance/', InsuranceAddFromInfoView.as_view(),
		 name='insurance.insurance_info.add_insurance'),

	# Insurance Info Property URL'S
	path('insurance-property-info/<int:pk>/edit/', InsuranceInfoPropertyEditView.as_view(),
		 name='insurance.insurance_info_property_edit'),
	path('insurance-property-info/<int:pk>/delete/', InsuranceInfoProperyDeleteView.as_view(),
		 name='insurance.insurance_info_property.delete'),

	# Insurance URL'S
	path('insurance/<int:pk>/edit/', InsuranceEditView.as_view(), name='insurance.insurance.edit'),


	#client info 
	path('client-info/get-counties/', GetCountyView.as_view(), name='insurance.client-info.get_models'),
	path('client-info/get-districts/', GetDistrictView.as_view(), name='insurance.client-info.get_districts'),
	path('client-info/get-barrios/', GetBarrioView.as_view(), name='insurance.client-info.get_barrios'),


	# Brand URL'S
	path('vehicle-brand/', VehicleBrandListView.as_view(), name='insurance.vehicle_brand.list'),
	path('vehicle-brand/add/', VehicleBrandAddView.as_view(), name='insurance.vehicle_brand.add'),
	path('vehicle-brand/<int:pk>/detail/', VehicleBrandDetailView.as_view(), name='insurance.vehicle_brand.detail'),
	path('vehicle-brand/<int:pk>/edit/', VehicleBrandEditView.as_view(), name='insurance.vehicle_brand.edit'),
	path('vehicle-brand/get-models/', GetModelbyBrandView.as_view(), name='insurance.vehicle_brand.get_models'),
	path('vehicle-brand/get-years/', GetYearByModel.as_view(), name='insurance.vehicle_brand.get_years'),

	path('vehicle-brand/<int:vehicle_brand_id>/add-model/', VehicleModelAddView.as_view(),
		 name='insurance.vehicle_brand.add_model'),
	path('vehicle-brand/<int:pk>/edit-model/', VehicleModelEditView.as_view(),
		 name='insurance.vehicle_brand.edit_model'),

	# Insurance Life Response URL'S
	path('insurance-life-response/', LifeResponseListView.as_view(),
		 name='insurance.insurance_life_response.list'),
	path('insurance-life-response/add/', LifeResponseAddView.as_view(),
		 name='insurance.insurance_life_response.add'),
	path('insurance-life-response/<int:pk>/edit/', LifeResponseEditView.as_view(),
		 name='insurance.insurance_life_response.edit'),

	# Insurance Vehicle Response URL'S
	path('insurance-vehicle-response/', VehicleResponseListView.as_view(),
		 name='insurance.insurance_vehicle_response.list'),
	path('insurance-vehicle-response/add/', VehicleResponseAddView.as_view(),
		 name='insurance.insurance_vehicle_response.add'),
	path('insurance-vehicle-response/<int:pk>/edit/', VehicleResponseEditView.as_view(),
		 name='insurance.insurance_vehicle_response.edit'),

	# Insurance Medical Response URL'S
	path('insurance-medical-response/', MedicalResponseListView.as_view(),
		 name='insurance.insurance_medical_response.list'),
	path('insurance-medical-response/add/', MedicalResponseAddView.as_view(),
		 name='insurance.insurance_medical_response.add'),
	path('insurance-medical-response/<int:pk>/edit/', MedicalResponseEditView.as_view(),
		 name='insurance.insurance_medical_response.edit'),
	path('insurance-medical-response/<int:medical_response_id>/add-dependent/',
		 MedicalResponseAddDependentView.as_view(), name='insurance.insurance_medical_response.add_dependent'),
	path('insurance-medical-response/<int:dependent_id>/delete-dependent/',
		 MedicalResponseDeleteDependentView.as_view(), name='insurance.insurance_medical_response.delete_dependent'),

	# Insurance Home Response URL'S
	path('insurance-home-response/', HomeResponseListView.as_view(), name='insurance.insurance_home_response.list'),
	path('insurance-home-response/add/', HomeResponseAddView.as_view(), name='insurance.insurance_home_response.add'),
	path('insurance-home-response/<int:pk>/edit/', HomeResponseEditView.as_view(),
		 name='insurance.insurance_home_response.edit'),

	# Construction Type URL'S
	path('construction-type/', ConstructionTypeListView.as_view(), name='insurance.construction_type.list'),
	path('construction-type/add/', ConstructionTypeAddView.as_view(), name='insurance.construction_type.add'),
	path('construction-type/<int:pk>/edit/', ConstructionTypeEditView.as_view(),
		 name='insurance.construction_type.edit'),

	# Feauture URL'S
	path('feature/', FeatureListView.as_view(), name='insurance.feature.list'),
	path('feature/add/', FeatureAddView.as_view(), name='insurance.feature.add'),
	path('feature/<int:pk>/edit/', FeatureEditView.as_view(), name='insurance.feature.edit'),

	# Client Response URL'S
	path('client-response/', ClientResponseListView.as_view(), name='insurance.client_response.list'),
	path('client-response/<int:pk>/detail/', ClientResponseDetail.as_view(), name='insurance.client_response.detail'),

	# Vehicle Year URL'S
	path('vehicle-year/', VehicleYearListView.as_view(), name='insurance.vehicle_year.list'),
	path('vehicle-year/add/', VehicleYearAddView.as_view(), name='insurance.vehicle_year.add'),
	path('vehicle-year/<int:pk>/edit/', VehicleYearEditView.as_view(), name='insurance.vehicle_year.edit'),
]

if settings.DEBUG:
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
