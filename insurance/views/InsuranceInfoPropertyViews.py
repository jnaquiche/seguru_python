from django.contrib.auth.decorators import login_required, permission_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, UpdateView, DeleteView

from insurance.forms import InsuranceInfoPropertyForm
from insurance.models import InsuranceInfoProperty


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.add_insuranceinfoproperty'), name='dispatch')
class InsuranceInfoPropertyAddView(CreateView):
	model = InsuranceInfoProperty

	template_name = 'insurance/layout/generic_form.html'

	form_class = InsuranceInfoPropertyForm

	def get_success_url(self):
		insurance_info_id = self.kwargs.get('insurance_info_id')

		return reverse_lazy('insurance.insurance_info.detail', kwargs={'pk': insurance_info_id})

	def get_context_data(self, **kwargs):
		data = super(InsuranceInfoPropertyAddView, self).get_context_data(**kwargs)

		insurance_info_id = self.kwargs.get('insurance_info_id')

		data['title_form'] = 'Asociar Propiedad de Seguro'

		data['title_section'] = 'Data de Seguro'

		data['reverse_url'] = reverse_lazy('insurance.insurance_info.detail', kwargs={'pk': insurance_info_id})

		return data

	def get_initial(self):
		initial = super(InsuranceInfoPropertyAddView, self).get_initial()

		insurance_info_id = self.kwargs.get('insurance_info_id')

		initial['insurance_info'] = insurance_info_id

		return initial


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_insuranceinfoproperty'), name='dispatch')
class InsuranceInfoPropertyEditView(UpdateView):
	model = InsuranceInfoProperty

	template_name = 'insurance/layout/generic_form.html'

	form_class = InsuranceInfoPropertyForm

	def get_success_url(self):
		return reverse_lazy('insurance.insurance_info.detail', kwargs={'pk': self.object.insurance_info_id})

	def get_context_data(self, **kwargs):
		data = super(InsuranceInfoPropertyEditView, self).get_context_data(**kwargs)

		data['title_form'] = self.object.insurance_property

		data['title_section'] = 'Data de Seguro'

		data['reverse_url'] = reverse_lazy('insurance.insurance_info.detail',
										   kwargs={'pk': self.object.insurance_info_id})

		return data


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.delete_insuranceinfoproperty'), name='dispatch')
class InsuranceInfoProperyDeleteView(DeleteView):
	model = InsuranceInfoProperty

	template_name = 'insurance/layout/confirm_delete.html'

	def get_success_url(self):
		return reverse_lazy('insurance.insurance_info.detail', kwargs={'pk': self.object.insurance_info_id})

	def get_context_data(self, **kwargs):
		data = super(InsuranceInfoProperyDeleteView, self).get_context_data(**kwargs)

		data['title_form'] = 'Eliminar {0}'.format(self.object.insurance_property)

		data['title_card'] = self.object.insurance_property

		data['reverse_url'] = reverse_lazy('insurance.insurance_info.detail',
										   kwargs={'pk': self.object.insurance_info_id})

		data['text_delete'] = '¿Está seguro de eliminar esta propiedad?'

		return data
