from django.contrib.auth.decorators import login_required, permission_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, UpdateView

from insurance.forms import SectionIconForm
from insurance.models import SectionIcon


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.add_sectionicon'), name='dispatch')
class SectionIconAddView(CreateView):
	model = SectionIcon

	template_name = 'insurance/section_icon/section_icon_form.html'

	form_class = SectionIconForm

	def get_success_url(self):
		return reverse_lazy('insurance.section_home_template.list')

	def get_context_data(self, **kwargs):
		data = super(SectionIconAddView, self).get_context_data(**kwargs)

		data['title_form'] = 'Nueva Sección'

		data['reverse_url'] = reverse_lazy('insurance.section_home_template.list')

		return data


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_sectionicon'), name='dispatch')
class SectionIconEditView(UpdateView):
	model = SectionIcon

	template_name = 'insurance/section_icon/section_icon_form.html'

	form_class = SectionIconForm

	context_object_name = 'section_home'

	def get_context_data(self, **kwargs):
		data = super(SectionIconEditView, self).get_context_data(**kwargs)

		data['title_form'] = self.object

		data['reverse_url'] = reverse_lazy('insurance.section_home_template.list')

		return data

	def get_success_url(self):
		return reverse_lazy('insurance.section_home_template.list')
