from django.contrib.auth.decorators import login_required, permission_required
from django.db import transaction, IntegrityError
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, CreateView, DetailView, UpdateView

from insurance.forms import InsuranceInfoForm, InsuranceForm
from insurance.models import InsuranceInfo


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_insuranceinfo'), name='dispatch')
class InsuranceInfoListView(ListView):
	model = InsuranceInfo

	template_name = 'insurance/insurance_info/insurance_info_list.html'

	context_object_name = 'insurance_info_list'

	paginate_by = 10

	ordering = 'name'


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.add_insuranceinfo'), name='dispatch')
class InsuranceInfoAddView(CreateView):
	model = InsuranceInfo

	template_name = 'insurance/insurance_info/insurance_info_form.html'

	form_class = InsuranceInfoForm

	def get_success_url(self):
		return reverse_lazy('insurance.insurance_info.detail', kwargs={'pk': self.object.id})

	def get_context_data(self, **kwargs):
		data = super(InsuranceInfoAddView, self).get_context_data(**kwargs)

		data['title_form'] = 'Nueva Información de Seguros'

		data['reverse_url'] = reverse_lazy('insurance.insurance_info.list')

		data['insurance_form'] = InsuranceForm(prefix='insurance')

		return data

	def form_valid(self, form):
		with transaction.atomic():
			try:
				response = super(InsuranceInfoAddView, self).form_valid(form)

				insurance_data_request = self.request.POST.copy()

				insurance_data_request['insurance-insurance_info'] = self.object.id

				insurance_form = InsuranceForm(insurance_data_request, prefix='insurance')

				if insurance_form.is_valid():
					insurance_form.save()

				else:
					response = super(InsuranceInfoAddView, self).form_invalid(form)

			except IntegrityError:
				response = super(InsuranceInfoAddView, self).form_invalid(form)

			return response


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_insuranceinfo'), name='dispatch')
class InsuranceInfoDetailView(DetailView):
	model = InsuranceInfo

	template_name = 'insurance/insurance_info/insurance_info_detail.html'

	context_object_name = 'insurance_info'

	def get_context_data(self, **kwargs):
		data = super(InsuranceInfoDetailView, self).get_context_data(**kwargs)

		data['reverse_url'] = reverse_lazy('insurance.insurance_info.list')

		return data


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_insuranceinfo'), name='dispatch')
class InsuranceInfoEditView(UpdateView):
	model = InsuranceInfo

	template_name = 'insurance/insurance_info/insurance_info_form.html'

	form_class = InsuranceInfoForm

	context_object_name = 'insurance_info'

	def get_success_url(self):
		return reverse_lazy('insurance.insurance_info.detail', kwargs={'pk': self.object.id})

	def get_context_data(self, **kwargs):
		data = super(InsuranceInfoEditView, self).get_context_data(**kwargs)

		data['title_form'] = self.object

		data['reverse_url'] = reverse_lazy('insurance.insurance_info.detail', kwargs={'pk': self.object.id})

		return data
