from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView

from insurance.forms import InsuranceCategoryForm
from insurance.models import InsuranceCategory


class InsuranceCategoryListView(ListView):
	model = InsuranceCategory

	template_name = 'insurance/insurance_category/insurance_category_list.html'

	context_object_name = 'insurance_category_list'

	paginate_by = 10

	ordering = 'name'


class InsuranceCategoryAddView(CreateView):
	model = InsuranceCategory

	template_name = 'insurance/layout/generic_form.html'

	form_class = InsuranceCategoryForm

	def get_success_url(self):
		return reverse_lazy('insurance.insurance_category.list')

	def get_context_data(self, **kwargs):
		data = super(InsuranceCategoryAddView, self).get_context_data(**kwargs)

		data['title_form'] = 'Nueva Categoría de Seguros'

		data['title_section'] = 'Categoría de Seguros'

		data['reverse_url'] = reverse_lazy('insurance.insurance_category.list')

		return data


class InsuranceCategoryEditView(UpdateView):
	model = InsuranceCategory

	template_name = 'insurance/layout/generic_form.html'

	form_class = InsuranceCategoryForm

	def get_success_url(self):
		return reverse_lazy('insurance.insurance_category.list')

	def get_context_data(self, **kwargs):
		data = super(InsuranceCategoryEditView, self).get_context_data(**kwargs)

		data['title_form'] = self.object

		data['title_section'] = 'Categoría de Seguros'

		data['reverse_url'] = reverse_lazy('insurance.insurance_category.list')

		return data
