from django.contrib.auth.decorators import permission_required, login_required
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import ListView, CreateView, UpdateView

from insurance.forms import InsurancePropertyForm
from insurance.models import InsuranceProperty


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_insuranceproperty'), name='dispatch')
class InsurancePropertyListView(ListView):
	model = InsuranceProperty

	template_name = 'insurance/insurance_property/insurance_property_list.html'

	context_object_name = 'insurance_property_list'

	paginate_by = 10

	ordering = 'order'


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.add_insuranceproperty'), name='dispatch')
class InsurancePropertyAddView(CreateView):
	model = InsuranceProperty

	template_name = 'insurance/layout/generic_form.html'

	form_class = InsurancePropertyForm

	def get_success_url(self):
		return reverse_lazy('insurance.insurance_property.list')

	def get_context_data(self, **kwargs):
		data = super(InsurancePropertyAddView, self).get_context_data(**kwargs)

		data['title_form'] = 'Nueva Propiedad de Seguros'

		data['title_section'] = 'Propiedades de Seguros'

		data['reverse_url'] = reverse_lazy('insurance.insurance_property.list')

		return data


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_insuranceproperty'), name='dispatch')
class InsurancePropertyEditView(UpdateView):
	model = InsuranceProperty

	template_name = 'insurance/layout/generic_form.html'

	form_class = InsurancePropertyForm

	def get_success_url(self):
		return reverse_lazy('insurance.insurance_property.list')

	def get_context_data(self, **kwargs):
		data = super(InsurancePropertyEditView, self).get_context_data(**kwargs)

		data['title_form'] = self.object

		data['title_section'] = 'Propiedades de Seguros'

		data['reverse_url'] = reverse_lazy('insurance.insurance_property.list')

		return data


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_insuranceproperty'), name='dispatch')
class InsurancePropertySortView(View):
	template_name = 'insurance/insurance_property/insurance_property_sort.html'

	def get(self, request):
		context = {
			'insurance_property_list': InsuranceProperty.get_sort_properties()
		}

		return render(request, self.template_name, context=context)

	def post(self, request):
		insurance_property_ids = request.POST.get('insurance_property_ids', None)

		if insurance_property_ids:
			insurance_property_ids = insurance_property_ids.split(',')

			pos = 0

			for insurance_property_id in insurance_property_ids:
				try:
					insurance_property = InsuranceProperty.objects.get(id=insurance_property_id)

					insurance_property.order = pos

					insurance_property.save()

					pos += 1

				except InsuranceProperty.DoesNotExist:
					pass

		return redirect(reverse_lazy('insurance.insurance_property.list'))
