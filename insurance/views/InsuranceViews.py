from django.contrib.auth.decorators import login_required, permission_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, UpdateView

from insurance.forms import InsuranceForm
from insurance.models import Insurance


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.add_insurance'), name='dispatch')
class InsuranceAddFromInfoView(CreateView):
	model = Insurance

	template_name = 'insurance/layout/generic_form.html'

	form_class = InsuranceForm

	def get_success_url(self):
		return reverse_lazy('insurance.insurance_info.detail', kwargs={'pk': self.object.insurance_info_id})

	def get_context_data(self, **kwargs):
		data = super(InsuranceAddFromInfoView, self).get_context_data(**kwargs)

		insurance_info_id = self.kwargs.get('insurance_info_id')

		data['title_form'] = 'Asociar Seguro'

		data['title_section'] = 'Data de Seguro'

		data['reverse_url'] = reverse_lazy('insurance.insurance_info.detail', kwargs={'pk': insurance_info_id})

		return data

	def get_initial(self):
		initial = super(InsuranceAddFromInfoView, self).get_initial()

		insurance_info_id = self.kwargs.get('insurance_info_id')

		initial['insurance_info'] = insurance_info_id

		return initial


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_insurance'), name='dispatch')
class InsuranceEditView(UpdateView):
	model = Insurance

	template_name = 'insurance/layout/generic_form.html'

	form_class = InsuranceForm

	def get_success_url(self):
		return reverse_lazy('insurance.insurance_info.detail', kwargs={'pk': self.object.insurance_info_id})

	def get_context_data(self, **kwargs):
		data = super(InsuranceEditView, self).get_context_data(**kwargs)

		data['title_form'] = self.object

		data['title_section'] = 'Data de Seguro'

		data['reverse_url'] = reverse_lazy('insurance.insurance_info.detail',
										   kwargs={'pk': self.object.insurance_info_id})

		return data
