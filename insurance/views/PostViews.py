import os
from django.conf import settings
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from insurance.forms import PostForm
from insurance.models import Post


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_post'), name='dispatch')
class PostListView(ListView):
	model = Post

	template_name = 'insurance/post/post_list.html'

	paginate_by = 10

	context_object_name = 'post_list'

	ordering = 'title'

	def get_context_data(self, *, object_list=None, **kwargs):
		data = super(PostListView, self).get_context_data(**kwargs)

		params = self.request.GET.copy()

		if 'page' in params:
			params.pop('page')

		data['params'] = params.urlencode()

		return data


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.add_post'), name='dispatch')
class PostAddView(CreateView):
	model = Post

	template_name = 'insurance/post/post_form.html'

	form_class = PostForm

	def get_context_data(self, **kwargs):
		data = super(PostAddView, self).get_context_data(**kwargs)

		data['title_form'] = 'Nueva Página'

		data['reverse_url'] = reverse_lazy('insurance.post.list')

		return data

	def get_success_url(self):
		return reverse_lazy('insurance.post.edit', kwargs={'pk': self.object.id})


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_post'), name='dispatch')
class PostEditView(UpdateView):
	model = Post

	template_name = 'insurance/post/post_form.html'

	form_class = PostForm

	context_object_name = 'post'

	def get_context_data(self, **kwargs):
		data = super(PostEditView, self).get_context_data(**kwargs)

		data['title_form'] = self.object

		data['reverse_url'] = reverse_lazy('insurance.post.list')

		path_image_post = os.path.join(settings.MEDIA_ROOT, 'images/post')

		if not os.path.exists(path_image_post):
			os.makedirs(path_image_post)

		return data

	def get_success_url(self):
		return reverse_lazy('insurance.post.edit', kwargs={'pk': self.object.id})


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.delete_post'), name='dispatch')
class PostDeleteView(DeleteView):
	model = Post

	template_name = 'insurance/layout/confirm_delete.html'

	def get_context_data(self, **kwargs):
		data = super(PostDeleteView, self).get_context_data(**kwargs)

		data['title_form'] = 'Eliminar {0}'.format(self.object)

		data['title_card'] = self.object

		data['reverse_url'] = reverse_lazy('insurance.post.edit', kwargs={'pk': self.object.id})

		data['text_delete'] = '¿Está seguro de eliminar esta página?'

		return data

	def get_success_url(self):
		return reverse_lazy('insurance.post.list')


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_post'), name='dispatch')
class PostSortView(View):
	template_name = 'insurance/post/post_sort.html'

	def get(self, request):
		exclude_posts = Post.exclude_posts()

		posts_in_menu = Post.posts_in_menu()

		context = {
			'exclude_posts': exclude_posts,
			'posts_in_menu': posts_in_menu,
		}

		return render(request, self.template_name, context=context)

	def post(self, request):
		ids_sort_posts = request.POST.get('ids_sort_posts')

		ids_exclude_posts = request.POST.get('ids_exclude_posts')

		if ids_exclude_posts:
			ids_exclude_posts = ids_exclude_posts.split(',')

			Post.objects.filter(id__in=ids_exclude_posts).update(order_in_menu=None)

		if ids_sort_posts:
			ids_sort_posts = ids_sort_posts.split(',')

			pos = 0

			for post_id in ids_sort_posts:
				try:
					post = Post.objects.get(id=post_id)

					post.order_in_menu = pos

					post.save()

					pos += 1

				except Post.DoesNotExist:
					pass

		return redirect(reverse_lazy('insurance.post.list'))
