from django.contrib.auth.decorators import login_required, permission_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, CreateView, UpdateView

from insurance.forms import LifeResponseForm
from insurance.models import LifeResponse


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_liferesponse'), name='dispatch')
class LifeResponseListView(ListView):
	model = LifeResponse

	template_name = 'insurance/insurance_life_response/insurance_life_response_list.html'

	context_object_name = 'insurance_life_response_list'

	paginate_by = 10

	def get_queryset(self):
		return LifeResponse.objects.filter(delete_at__isnull=True).order_by('id')


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.add_liferesponse'), name='dispatch')
class LifeResponseAddView(CreateView):
	model = LifeResponse

	template_name = 'insurance/insurance_life_response/form.html'

	form_class = LifeResponseForm

	def get_success_url(self):
		return reverse_lazy('insurance.insurance_life_response.list')

	def get_context_data(self, **kwargs):
		data = super(LifeResponseAddView, self).get_context_data(**kwargs)

		data['title_form'] = 'Nueva Respuesta para Seguro de Vida'

		data['title_section'] = 'Respuestas Seguro de Vida'

		data['reverse_url'] = reverse_lazy('insurance.insurance_life_response.list')

		return data


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_liferesponse'), name='dispatch')
class LifeResponseEditView(UpdateView):
	model = LifeResponse

	template_name = 'insurance/insurance_life_response/form.html'

	form_class = LifeResponseForm

	def get_success_url(self):
		return reverse_lazy('insurance.insurance_life_response.list')

	def get_context_data(self, **kwargs):
		data = super(LifeResponseEditView, self).get_context_data(**kwargs)

		data['title_form'] = 'Editar Respuesta'

		data['title_section'] = 'Respuestas Seguro de Vida'

		data['reverse_url'] = reverse_lazy('insurance.insurance_life_response.list')

		return data
