from django.contrib.auth.decorators import login_required, permission_required
from django.http import JsonResponse
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View

from insurance.models import Post, PostImage


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.add_postimage'), name='dispatch')
class PostImageAddView(View):
	def post(self, request, post_id):
		post = Post.get_by_id(post_id=post_id)

		response = {}

		if post is not None:
			post_image_file = request.FILES.get('post_image_file', None)

			post_image = post.images.create(image=post_image_file)

			response = {
				'message': 'La imagen se ha subido con éxito',
				'image_url': post_image.image.url,
				'post_image_delete_url': reverse_lazy('insurance.post_image.delete',
													  kwargs={'post_image_id': post_image.id})
			}

		else:
			return JsonResponse({'message': 'La página no existe'}, status=404)

		return JsonResponse(response, status=200)


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.delete_postimage'), name='dispatch')
class PostImageDeleteView(View):
	def get(self, request, post_image_id):
		post_image = PostImage.get_by_id(post_image_id=post_image_id)

		if post_image:
			post_image.delete()

			return redirect(reverse_lazy('insurance.post.edit', kwargs={'pk': post_image.post_id}))

		else:
			return redirect(reverse_lazy('insurance.post.list'))
