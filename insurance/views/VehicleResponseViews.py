from django.contrib.auth.decorators import login_required, permission_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, CreateView, UpdateView

from insurance.forms import VehicleResponseForm
from insurance.models import VehicleResponse, VehicleModel


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_clientvehicleresponse'), name='dispatch')
class VehicleResponseListView(ListView):
	model = VehicleResponse

	template_name = 'insurance/insurance_vehicle_response/insurance_vehicle_response_list.html'

	context_object_name = 'insurance_vehicle_response_list'

	paginate_by = 10


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.add_clientvehicleresponse'), name='dispatch')
class VehicleResponseAddView(CreateView):
	model = VehicleResponse

	template_name = 'insurance/insurance_vehicle_response/insurance_vehicle_response_form.html'

	form_class = VehicleResponseForm

	def get_success_url(self):
		return reverse_lazy('insurance.insurance_vehicle_response.list')

	def get_context_data(self, **kwargs):
		data = super(VehicleResponseAddView, self).get_context_data(**kwargs)

		data['title_form'] = 'Nueva Respuesta para Seguro de Vehículos'

		data['title_section'] = 'Respuestas de Seguro de Vehículos'

		data['reverse_url'] = reverse_lazy('insurance.insurance_vehicle_response.list')

		return data


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_clientvehicleresponse'), name='dispatch')
class VehicleResponseEditView(UpdateView):
	model = VehicleResponse

	template_name = 'insurance/insurance_vehicle_response/insurance_vehicle_response_form.html'

	form_class = VehicleResponseForm

	context_object_name = 'insurance_vehicle_response'

	def get_success_url(self):
		return reverse_lazy('insurance.insurance_vehicle_response.list')

	def get_context_data(self, **kwargs):
		data = super(VehicleResponseEditView, self).get_context_data(**kwargs)

		data['title_form'] = 'Editar Respuesta'

		data['title_section'] = 'Respuestas de Seguro de Vehículos'

		data['reverse_url'] = reverse_lazy('insurance.insurance_vehicle_response.list')

		return data
