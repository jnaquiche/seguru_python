from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import ListView, CreateView, UpdateView

from insurance.forms import MedicalResponseForm
from insurance.forms.MedicalResponseDependentsForm import MedicalResponseDependentsForm
from insurance.models import MedicalResponse, MedicalResponseDependents


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_medicalresponse'), name='dispatch')
class MedicalResponseListView(ListView):
	model = MedicalResponse

	template_name = 'insurance/insurance_medical_response/insurance_medical_response_list.html'

	context_object_name = 'insurance_medical_response_list'

	paginate_by = 10

	ordering = 'id'


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.add_medicalresponse'), name='dispatch')
class MedicalResponseAddView(CreateView):
	model = MedicalResponse

	template_name = 'insurance/insurance_medical_response/insurance_medical_form.html'

	form_class = MedicalResponseForm

	def get_success_url(self):
		return reverse_lazy('insurance.insurance_medical_response.edit', kwargs={'pk': self.object.id})

	def get_context_data(self, **kwargs):
		data = super(MedicalResponseAddView, self).get_context_data(**kwargs)

		data['title_form'] = 'Nueva Respuesta para Seguro Médico'

		data['title_section'] = 'Respuestas Seguro Médico'

		data['reverse_url'] = reverse_lazy('insurance.insurance_medical_response.list')

		return data


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_medicalresponse'), name='dispatch')
class MedicalResponseEditView(UpdateView):
	model = MedicalResponse

	template_name = 'insurance/insurance_medical_response/insurance_medical_form.html'

	form_class = MedicalResponseForm

	context_object_name = 'medical_response'

	def get_success_url(self):
		return reverse_lazy('insurance.insurance_medical_response.list')

	def get_context_data(self, **kwargs):
		data = super(MedicalResponseEditView, self).get_context_data(**kwargs)

		data['title_form'] = 'Editar Respuesta'

		data['title_section'] = 'Respuestas Seguro Médico'

		data['reverse_url'] = reverse_lazy('insurance.insurance_medical_response.list')

		return data


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.add_medicalresponsedependents'), name='dispatch')
class MedicalResponseAddDependentView(CreateView):
	model = MedicalResponseDependents

	template_name = 'insurance/layout/generic_form.html'

	form_class = MedicalResponseDependentsForm

	def get_success_url(self):
		return reverse_lazy('insurance.insurance_medical_response.edit', kwargs={'pk': self.object.medical_response_id})

	def get_context_data(self, **kwargs):
		medical_response_id = self.kwargs.get('medical_response_id', None)

		data = super(MedicalResponseAddDependentView, self).get_context_data(**kwargs)

		data['title_form'] = 'Agregar Dependiente'

		data['title_section'] = 'Respuestas Seguro Médico'

		data['reverse_url'] = reverse_lazy('insurance.insurance_medical_response.edit',
										   kwargs={'pk': medical_response_id})

		return data

	def get_initial(self):
		initial = super(MedicalResponseAddDependentView, self).get_initial()

		medical_response_id = self.kwargs.get('medical_response_id', None)

		initial['medical_response'] = medical_response_id

		return initial

	def form_valid(self, form):
		response = super(MedicalResponseAddDependentView, self).form_valid(form)

		form.instance.medical_response.dependents_number = form.instance.medical_response.dependents.count()

		form.instance.medical_response.save()

		return response


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.delete_medicalresponsedependents'), name='dispatch')
class MedicalResponseDeleteDependentView(View):
	def get(self, request, dependent_id):
		dependent = MedicalResponseDependents.get_by_id(dependent_id=dependent_id)

		if dependent:
			dependent.delete()

			dependent.medical_response.dependents_number = dependent.medical_response.dependents.count()
			dependent.medical_response.save()

			return redirect(
				reverse_lazy('insurance.insurance_medical_response.edit', kwargs={'pk': dependent.medical_response_id}))

		return redirect(reverse_lazy('insurance.insurance_medical_response.list'))
