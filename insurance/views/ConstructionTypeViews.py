from django.contrib.auth.decorators import login_required, permission_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, CreateView, UpdateView

from insurance.forms import ConstructionTypeForm
from insurance.models import ConstructionType


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_constructiontype'), name='dispatch')
class ConstructionTypeListView(ListView):
	model = ConstructionType

	template_name = 'insurance/construction_type/construction_type_list.html'

	context_object_name = 'construction_type_list'

	paginate_by = 10

	ordering = 'name'


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.add_constructiontype'), name='dispatch')
class ConstructionTypeAddView(CreateView):
	model = ConstructionType

	template_name = 'insurance/layout/generic_form.html'

	form_class = ConstructionTypeForm

	def get_success_url(self):
		return reverse_lazy('insurance.construction_type.list')

	def get_context_data(self, **kwargs):
		data = super(ConstructionTypeAddView, self).get_context_data(**kwargs)

		data['title_form'] = 'Nuevo Tipo de Construcción'

		data['title_section'] = 'Tipos de Construccion'

		data['reverse_url'] = reverse_lazy('insurance.construction_type.list')

		return data


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_constructiontype'), name='dispatch')
class ConstructionTypeEditView(UpdateView):
	model = ConstructionType

	template_name = 'insurance/layout/generic_form.html'

	form_class = ConstructionTypeForm

	def get_success_url(self):
		return reverse_lazy('insurance.construction_type.list')

	def get_context_data(self, **kwargs):
		data = super(ConstructionTypeEditView, self).get_context_data(**kwargs)

		data['title_form'] = self.object

		data['title_section'] = 'Tipos de Construccion'

		data['reverse_url'] = reverse_lazy('insurance.construction_type.list')

		return data
