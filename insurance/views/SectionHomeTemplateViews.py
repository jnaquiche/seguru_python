from django.contrib.auth.decorators import login_required, permission_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, CreateView, UpdateView

from insurance.forms import SectionHomeTemplateForm
from insurance.models import SectionHomeTemplate, SectionIcon


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_sectionhometemplate'), name='dispatch')
class SectionHomeTemplateListView(ListView):
	model = SectionHomeTemplate

	context_object_name = 'section_list'

	template_name = 'insurance/section_home_template/section_home_template_list.html'

	def get_context_data(self, *, object_list=None, **kwargs):
		data = super(SectionHomeTemplateListView, self).get_context_data(**kwargs)

		data['section_icon_list'] = SectionIcon.objects.all()

		return data


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.add_sectionhometemplate'), name='dispatch')
class SectionHomeTemplateAddView(CreateView):
	model = SectionHomeTemplate

	template_name = 'insurance/section_home_template/section_home_template_form.html'

	form_class = SectionHomeTemplateForm

	def get_context_data(self, **kwargs):
		data = super(SectionHomeTemplateAddView, self).get_context_data(**kwargs)

		data['title_form'] = 'Nueva Sección'

		data['reverse_url'] = reverse_lazy('insurance.section_home_template.list')

		return data

	def get_success_url(self):
		return reverse_lazy('insurance.section_home_template.list')


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_sectionhometemplate'), name='dispatch')
class SectionHomeTemplateEditView(UpdateView):
	model = SectionHomeTemplate

	template_name = 'insurance/section_home_template/section_home_template_form.html'

	form_class = SectionHomeTemplateForm

	context_object_name = 'section_home'

	def get_context_data(self, **kwargs):
		data = super(SectionHomeTemplateEditView, self).get_context_data(**kwargs)

		data['title_form'] = self.object

		data['reverse_url'] = reverse_lazy('insurance.section_home_template.list')

		return data

	def get_success_url(self):
		return reverse_lazy('insurance.section_home_template.list')
