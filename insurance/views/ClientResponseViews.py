from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView

from insurance.models import Client

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_client'), name='dispatch')
class ClientResponseListView(ListView):
	model = Client

	template_name = 'insurance/client_response/list.html'

	context_object_name = 'client_response_list'

	paginate_by = 10

	ordering = '-created_at'

@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_client'), name='dispatch')
class ClientResponseDetail(DetailView):
	model = Client

	template_name = 'insurance/client_response/detail.html'

	context_object_name = 'client_response'
