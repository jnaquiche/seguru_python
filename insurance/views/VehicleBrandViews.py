from django.contrib.auth.decorators import login_required, permission_required
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import ListView, CreateView, DetailView, UpdateView

from insurance.forms import VehicleBrandForm, VehicleModelForm
from insurance.models import VehicleBrand, VehicleModel


from insurance.models.ClientInfo import ClientInfo
from insurance.models.InsState import InsState
from insurance.models.InsCounty import InsCounty
from insurance.models.InsDistrict import InsDistrict

from insurance.models.QualitasState import QualitasState
from insurance.models.QualitasCounty import QualitasCounty
from insurance.models.QualitasDistrict import QualitasDistrict
from insurance.models.QualitasBarrio import QualitasBarrio

from insurance.models.OceanicaState import OceanicaState
from insurance.models.OceanicaCounty import OceanicaCounty
from insurance.models.OceanicaDistrict import OceanicaDistrict



@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_vehiclebrand'), name='dispatch')
class VehicleBrandListView(ListView):
	model = VehicleBrand

	template_name = 'insurance/vehicle_brand/vehicle_brand_list.html'

	context_object_name = 'vehicle_brand_list'

	paginate_by = 10

	ordering = 'name'


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.add_vehiclebrand'), name='dispatch')
class VehicleBrandAddView(CreateView):
	model = VehicleBrand

	form_class = VehicleBrandForm

	template_name = 'insurance/layout/generic_form.html'

	def get_success_url(self):
		return reverse_lazy('insurance.vehicle_brand.detail', kwargs={'pk': self.object.id})

	def get_context_data(self, **kwargs):
		data = super(VehicleBrandAddView, self).get_context_data(**kwargs)

		data['title_form'] = 'Nueva Marca de Vehículo'

		data['reverse_url'] = reverse_lazy('insurance.vehicle_brand.list')

		data['title_section'] = 'Marcas de Vehículos'

		return data


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_vehiclebrand'), name='dispatch')
class VehicleBrandDetailView(DetailView):
	model = VehicleBrand

	template_name = 'insurance/vehicle_brand/vehicle_brand_detail.html'

	context_object_name = 'vehicle_brand'


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_vehiclebrand'), name='dispatch')
class VehicleBrandEditView(UpdateView):
	model = VehicleBrand

	template_name = 'insurance/layout/generic_form.html'

	form_class = VehicleBrandForm

	def get_success_url(self):
		return reverse_lazy('insurance.vehicle_brand.detail', kwargs={'pk': self.object.id})

	def get_context_data(self, **kwargs):
		data = super(VehicleBrandEditView, self).get_context_data(**kwargs)

		data['title_form'] = self.object

		data['title_section'] = 'Marcas de Vehículos'

		data['reverse_url'] = reverse_lazy('insurance.vehicle_brand.detail', kwargs={'pk': self.object.id})

		return data


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.add_vehiclemodel'), name='dispatch')
class VehicleModelAddView(CreateView):
	model = VehicleModel

	template_name = 'insurance/layout/generic_form.html'

	form_class = VehicleModelForm

	def get_success_url(self):
		return reverse_lazy('insurance.vehicle_brand.detail', kwargs={'pk': self.object.vehicle_brand_id})

	def get_context_data(self, **kwargs):
		data = super(VehicleModelAddView, self).get_context_data(**kwargs)

		vehicle_brand_id = self.kwargs.get('vehicle_brand_id', None)

		data['title_form'] = 'Nuevo Modelo'

		data['title_section'] = 'Marcas de Vehículos'

		data['reverse_url'] = reverse_lazy('insurance.vehicle_brand.detail', kwargs={'pk': vehicle_brand_id})

		return data

	def get_initial(self):
		initial = super(VehicleModelAddView, self).get_initial()

		vehicle_brand_id = self.kwargs.get('vehicle_brand_id', None)

		initial['vehicle_brand'] = vehicle_brand_id

		return initial


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_vehiclemodel'), name='dispatch')
class VehicleModelEditView(UpdateView):
	model = VehicleModel

	template_name = 'insurance/layout/generic_form.html'

	form_class = VehicleModelForm

	def get_success_url(self):
		return reverse_lazy('insurance.vehicle_brand.detail', kwargs={'pk': self.object.vehicle_brand_id})

	def get_context_data(self, **kwargs):
		data = super(VehicleModelEditView, self).get_context_data(**kwargs)

		data['title_form'] = self.object

		data['title_section'] = 'Marcas de Vehículos'

		data['reverse_url'] = reverse_lazy('insurance.vehicle_brand.detail',
										   kwargs={'pk': self.object.vehicle_brand_id})

		return data


class GetModelbyBrandView(View):
	def get(self, request):
		vehicle_brand_id = request.GET.get('vehicle_brand_id', None)

		vehicle_brand = VehicleBrand.get_by_id(vehicle_brand_id=vehicle_brand_id)

		vehicle_model_list = []

		if vehicle_brand:
			vehicle_model_list = vehicle_brand.sorted_vehicle_models.values('id', 'name')

			vehicle_model_list = list(vehicle_model_list)

			for vehicle_model in vehicle_model_list:
				vehicle_model['name'] = vehicle_model['name'].title()

		return JsonResponse(vehicle_model_list, safe=False)


###
class GetCountyView(View):
	def get(self, request):
		state_id = request.GET.get('state_id', None)

		company_info = request.session['company_info']
		
		state = None
		#value by default for statelist
		if company_info['selected_company'] == 'ins':
			state = InsState.get_by_id(state_id=state_id)
		elif company_info['selected_company'] == 'oceanica':
			state = OceanicaState.get_by_id(state_id=state_id)
		elif company_info['selected_company'] == 'qualitas':
			state = QualitasState.get_by_id(state_id=state_id)
		else: #default
			state = InsState.get_by_id(state_id=state_id)

		
		county_list = []

		if state:
			county_list = state.sorted_state_county.values('id', 'name')
			#print(county_list)
			county_list = list(county_list)

			for ccounty in county_list:
				ccounty['name'] = ccounty['name'].title()

		return JsonResponse(county_list, safe=False)

class GetDistrictView(View):
	def get(self, request):
		county_id = request.GET.get('county_id', None)

		company_info = request.session['company_info']
		county = None
		#value by default for statelist
		if company_info['selected_company'] == 'ins':
			county = InsCounty.get_by_id(county_id=county_id)
		elif company_info['selected_company'] == 'oceanica':
			county = OceanicaCounty.get_by_id(county_id=county_id)
		elif company_info['selected_company'] == 'qualitas':
			county = QualitasCounty.get_by_id(county_id=county_id)
		else: #default
			county = InsCounty.get_by_id(county_id=county_id)

		district_list = []
		print(company_info['selected_company'],' County: ', county_id )
		if county:
			district_list = county.sorted_county_districts.values('id', 'name')
			 
			district_list = list(district_list)

			for ddistrict in district_list:
				ddistrict['name'] = ddistrict['name'].title()

		return JsonResponse(district_list, safe=False)

class GetBarrioView(View):
	def get(self, request):
		district_id = request.GET.get('district_id', None)
		

		company_info = request.session['company_info']
		
		state = None
		barrio_list = []
		if company_info['selected_company'] == 'qualitas':
			district = QualitasDistrict.get_by_id(district_id=district_id)
			if district:
				barrio_list = district.sorted_district_barrios.values('id', 'name')
				barrio_list = list(barrio_list)

				for dbarrio in barrio_list:
					dbarrio['name'] = dbarrio['name'].title()

		return JsonResponse(barrio_list, safe=False)



class GetYearByModel(View):
	def get(self, request):
		vehicle_model_id = request.GET.get('vehicle_model_id', None)

		vehicle_year_list = []

		vehicle_model = VehicleModel.get_by_id(vehicle_model_id=vehicle_model_id)

		if vehicle_model:
			vehicle_year_list = vehicle_model.linked_years.values('id', 'year')

			vehicle_year_list = list(vehicle_year_list)

		return JsonResponse(vehicle_year_list, safe=False)
