from django.contrib.auth.decorators import permission_required, login_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, CreateView, UpdateView

from insurance.forms import FeatureForm
from insurance.models import Feature


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_feature'), name='dispatch')
class FeatureListView(ListView):
	model = Feature

	template_name = 'insurance/feature/feature_list.html'

	context_object_name = 'feature_list'

	paginate_by = 10

	ordering = 'name'


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.add_feature'), name='dispatch')
class FeatureAddView(CreateView):
	model = Feature

	template_name = 'insurance/layout/generic_form.html'

	form_class = FeatureForm

	def get_success_url(self):
		return reverse_lazy('insurance.feature.list')

	def get_context_data(self, **kwargs):
		data = super(FeatureAddView, self).get_context_data(**kwargs)

		data['title_form'] = 'Nueva Característica'

		data['title_section'] = 'Características'

		data['reverse_url'] = reverse_lazy('insurance.feature.list')

		return data


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_feature'), name='dispatch')
class FeatureEditView(UpdateView):
	model = Feature

	template_name = 'insurance/layout/generic_form.html'

	form_class = FeatureForm

	def get_success_url(self):
		return reverse_lazy('insurance.feature.list')

	def get_context_data(self, **kwargs):
		data = super(FeatureEditView, self).get_context_data(*kwargs)

		data['title_form'] = self.object

		data['title_section'] = 'Características'

		data['reverse_url'] = reverse_lazy('insurance.feature.list')

		return data
