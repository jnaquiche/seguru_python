from django.contrib.auth.decorators import permission_required, login_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, CreateView, UpdateView

from insurance.forms import HomeResponseForm
from insurance.models import HomeResponse


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_homeresponse'), name='dispatch')
class HomeResponseListView(ListView):
	model = HomeResponse

	template_name = 'insurance/home_response/home_response_list.html'

	context_object_name = 'home_response_list'

	paginate_by = 10

	ordering = 'id'


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.add_homeresponse'), name='dispatch')
class HomeResponseAddView(CreateView):
	model = HomeResponse

	template_name = 'insurance/layout/generic_form_insurance_response.html'

	form_class = HomeResponseForm

	def get_success_url(self):
		return reverse_lazy('insurance.insurance_home_response.list')

	def get_context_data(self, **kwargs):
		data = super(HomeResponseAddView, self).get_context_data(**kwargs)

		data['title_form'] = 'Nueva Respuesta para Seguro de Casa'

		data['title_section'] = 'Respuestas de Seguro de Casa'

		data['reverse_url'] = reverse_lazy('insurance.insurance_home_response.list')

		return data


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_homeresponse'), name='dispatch')
class HomeResponseEditView(UpdateView):
	model = HomeResponse

	template_name = 'insurance/layout/generic_form_insurance_response.html'

	form_class = HomeResponseForm

	def get_success_url(self):
		return reverse_lazy('insurance.insurance_home_response.list')

	def get_context_data(self, **kwargs):
		data = super(HomeResponseEditView, self).get_context_data(**kwargs)

		data['title_form'] = 'Editar Respuesta para Seguro de Casa'

		data['title_section'] = 'Respuestas de Seguro de Casa'

		data['reverse_url'] = reverse_lazy('insurance.insurance_home_response.list')

		return data
