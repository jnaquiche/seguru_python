from django.contrib.auth.decorators import login_required, permission_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, CreateView, UpdateView

from insurance.forms import CompanyForm
from insurance.models import Company


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_company'), name='dispatch')
class CompanyListView(ListView):
	model = Company

	template_name = 'insurance/company/company_list.html'

	paginate_by = 10

	ordering = 'name'

	context_object_name = 'company_list'


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.add_company'), name='dispatch')
class CompanyAddView(CreateView):
	model = Company

	template_name = 'insurance/layout/generic_form.html'

	form_class = CompanyForm

	def get_success_url(self):
		return reverse_lazy('insurance.company.list')

	def get_context_data(self, **kwargs):
		data = super(CompanyAddView, self).get_context_data(**kwargs)

		data['title_form'] = 'Nueva Compañia'

		data['title_section'] = 'Compañias'

		data['reverse_url'] = reverse_lazy('insurance.company.list')

		return data


@method_decorator(login_required, name='dispatch')
@method_decorator(permission_required('insurance.change_company'), name='dispatch')
class CompanyEditView(UpdateView):
	model = Company

	template_name = 'insurance/layout/generic_form.html'

	form_class = CompanyForm

	def get_success_url(self):
		return reverse_lazy('insurance.company.list')

	def get_context_data(self, **kwargs):
		data = super(CompanyEditView, self).get_context_data(**kwargs)

		data['title_form'] = self.object

		data['title_section'] = 'Compañias'

		data['reverse_url'] = reverse_lazy('insurance.company.list')

		return data
