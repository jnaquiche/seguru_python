from django.db import models

from insurance.models.MedicalResponseDependentsTemplate import MedicalResponseDependentsTemplate


class MedicalResponseDependents(MedicalResponseDependentsTemplate):
	medical_response = models.ForeignKey('insurance.MedicalResponse',
										 related_name='dependents',
										 on_delete=models.CASCADE)
