from django.db import models

from insurance.models.VehicleResponseTemplate import VehicleResponseTemplate


class ClientVehicleResponse(VehicleResponseTemplate):
	client = models.ForeignKey('insurance.Client', related_name='vehicle_responses', on_delete=models.CASCADE)

	insurance = models.ManyToManyField('insurance.Insurance', related_name='client_vehicle_responses')
