from django.db import models

from insurance.models.ResponseTemplate import ResponseTemplate


class LifeResponseTemplate(ResponseTemplate):
	is_smoker = models.BooleanField(default=False)

	class Meta:
		abstract = True
