from django.db import models


class Color(models.Model):
	name = models.CharField(max_length=255,null=True)
	qualitas_color_code = models.CharField(max_length=255,default=None,null=True)
	ins_color_code = models.CharField(max_length=5,default=None,null=True)
	oceanica_color_code = models.CharField(max_length=255,default=None,null=True)
	created_at = models.DateTimeField(auto_now_add=True,null=True)
	updated_at = models.DateTimeField(auto_now=True,null=True)

	def __str__(self):
		return self.name.title()

	@classmethod
	def get_by_id(cls, color_id):
		try:
			color = cls.objects.get(id=color_id)

		except cls.DoesNotExist:
			color = None

		return color

	