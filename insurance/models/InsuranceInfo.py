from django.db import models


class InsuranceInfo(models.Model):
	# Category Options
	INSURANCE_CATEGORY_LIFE = 1
	INSURANCE_CATEGORY_MEDICAL = 2
	INSURANCE_CATEGORY_HOME = 3
	INSURANCE_CATEGORY_VEHICLE = 4

	INSURANCE_CATEGORY_OPTIONS = (
		(INSURANCE_CATEGORY_LIFE, 'Vida'),
		(INSURANCE_CATEGORY_MEDICAL, 'Médico'),
		(INSURANCE_CATEGORY_HOME, 'Casa'),
		(INSURANCE_CATEGORY_VEHICLE, 'Vehículos'),
	)

	name = models.CharField(max_length=255)

	company = models.ManyToManyField('insurance.Company', related_name='insurance_info', through='insurance.Insurance')

	category = models.IntegerField(choices=INSURANCE_CATEGORY_OPTIONS, default=INSURANCE_CATEGORY_LIFE)

	insurance_properties = models.ManyToManyField('insurance.InsuranceProperty',
												  through='insurance.InsuranceInfoProperty',
												  related_name='insurance_info')

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name.title()
