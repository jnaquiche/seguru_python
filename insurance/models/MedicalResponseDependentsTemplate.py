from django.db import models


class MedicalResponseDependentsTemplate(models.Model):
	age = models.IntegerField()

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	@classmethod
	def get_by_id(cls, dependent_id):
		try:
			dependent = cls.objects.get(id=dependent_id)

		except cls.DoesNotExist:
			dependent = None

		return dependent

	class Meta:
		abstract = True
