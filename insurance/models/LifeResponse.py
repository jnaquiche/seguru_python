from django.db import models

from insurance.models.LifeResponseTemplate import LifeResponseTemplate


class LifeResponse(LifeResponseTemplate):
	insurance = models.ManyToManyField('insurance.Insurance', related_name='life_responses')

	@property
	def insurances_linked(self):
		insurance_list = self.insurance.all().values_list('name', flat=True)

		insurance_list = ', '.join(insurance_list)

		return insurance_list
