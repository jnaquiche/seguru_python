from django.db import models

from insurance.models.ResponseTemplate import ResponseTemplate


class MedicalResponseTemplate(ResponseTemplate):
	COVERAGE_UNLIMITED = 8

	MEDICAL_COVERAGE_OPTIONS = ResponseTemplate.COVERAGE_OPTIONS + ((COVERAGE_UNLIMITED, 'Ilimitado'),)

	coverage_value = models.IntegerField(choices=MEDICAL_COVERAGE_OPTIONS,
										 default=ResponseTemplate.COVERAGE_100_k)

	dependents_number = models.IntegerField(null=True)

	spouse_age = models.IntegerField(null=True, blank=True)

	class Meta:
		abstract = True
