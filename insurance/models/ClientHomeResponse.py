from django.db import models

from insurance.models.HomeResponseTemplate import HomeResponseTemplate


class ClientHomeResponse(HomeResponseTemplate):
	client = models.ForeignKey('insurance.Client', related_name='home_responses', on_delete=models.CASCADE)

	insurance = models.ManyToManyField('insurance.Insurance', related_name='client_home_responses')
