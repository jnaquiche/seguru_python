from django.db import models


class Padron(models.Model):
	cedula = models.IntegerField(default=None)
	codelect = models.IntegerField(default=None)
	sexo = models.IntegerField(default=None)
	fechacaduc = models.IntegerField(default=None)
	junta = models.CharField(max_length=255,null=True)
	nombre = models.CharField(max_length=255,default=None,null=True)
	apellido1 = models.CharField(max_length=255,default=None,null=True)
	apellido2 = models.CharField(max_length=255,default=None,null=True)
	
	def __str__(self):
		return self.name.title()

	
	@classmethod
	def get_by_id(cls, county_id):
		try:
			padron = cls.objects.get(id=county_id)

		except cls.DoesNotExist:
			padron = None

		return padron
