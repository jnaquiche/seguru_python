from django.db import models


class QualitasState(models.Model):
	name = models.CharField(max_length=255,null=True)
	code = models.CharField(max_length=5,default=None,null=True)
	created_at = models.DateTimeField(auto_now_add=True,null=True)
	updated_at = models.DateTimeField(auto_now=True,null=True)

	def __str__(self):
		return self.name.title()

	@classmethod
	def get_by_id(cls, state_id):
		try:
			state = cls.objects.get(id=state_id)

		except cls.DoesNotExist:
			state = None

		return state

	@property
	def sorted_state_county(self):
		return self.state_counties.all().order_by('name')
