from django.db import models


class VehicleBrand(models.Model):
	name = models.CharField(max_length=255,null=True)
	qualitas_brand_code = models.CharField(max_length=255,default=None,null=True)
	ins_brand_code = models.CharField(max_length=255,default=None,null=True)
	oceanica_brand_code = models.CharField(max_length=255,default=None,null=True)
	created_at = models.DateTimeField(auto_now_add=True,null=True)
	updated_at = models.DateTimeField(auto_now=True,null=True)

	def __str__(self):
		return self.name.title()

	@classmethod
	def get_by_id(cls, vehicle_brand_id):
		try:
			vehicle_brand = cls.objects.get(id=vehicle_brand_id)

		except cls.DoesNotExist:
			vehicle_brand = None

		return vehicle_brand

	@property
	def sorted_vehicle_models(self):
		return self.vehicle_models.all().order_by('name')
