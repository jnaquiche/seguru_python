from django.db import models


class County(models.Model):
	name = models.CharField(max_length=255,null=True)
	qualitas_county_code = models.CharField(max_length=255,default=None,null=True)
	oceanica_county_code = models.CharField(max_length=255,default=None,null=True)
	ins_county_code = models.CharField(max_length=5,default=None,null=True)
	state = models.ForeignKey('insurance.State', related_name='state_counties', on_delete=True,null=True)
	created_at = models.DateTimeField(auto_now_add=True,null=True)
	updated_at = models.DateTimeField(auto_now=True,null=True)

	def __str__(self):
		return self.name.title()

	
	@classmethod
	def get_by_id(cls, county_id):
		try:
			county = cls.objects.get(id=county_id)

		except cls.DoesNotExist:
			county = None

		return county

	@property
	def sorted_county_districts(self):
		return self.county_districts.all().order_by('name')
