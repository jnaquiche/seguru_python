from django.db import models


class VehicleModel(models.Model):
	name = models.CharField(max_length=255,null=True)
	amis = models.CharField(max_length=255,default=None,null=True)
	oceanica_model_code = models.CharField(max_length=255,default=None,null=True)
	ins_model_code = models.CharField(max_length=255,default=None,null=True)
	vehicle_brand = models.ForeignKey('insurance.VehicleBrand', related_name='vehicle_models', on_delete=True,null=True)
	years = models.CharField(max_length=255,null=True)
	created_at = models.DateTimeField(auto_now_add=True,null=True)
	updated_at = models.DateTimeField(auto_now=True,null=True)

	def __str__(self):
		return self.name.title()

	#@property
	#def linked_years(self):
	#	return self.years.order_by('year')

	@classmethod
	def get_by_id(cls, vehicle_model_id):
		try:
			vehicle_model = cls.objects.get(id=vehicle_model_id)

		except cls.DoesNotExist:
			vehicle_model = None

		return vehicle_model
