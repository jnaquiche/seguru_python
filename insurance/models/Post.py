import itertools
from django.db import models
from django.template.defaultfilters import slugify


class Post(models.Model):
	title = models.CharField(max_length=255)

	subtitle = models.CharField(max_length=150, null=True, blank=True)

	content = models.TextField()

	slug = models.SlugField(max_length=255)

	order_in_menu = models.IntegerField(null=True)

	primary_button_text = models.CharField(max_length=20, null=True, blank=True)
	primary_button_url = models.CharField(max_length=255, null=True, blank=True)
	secondary_button_text = models.CharField(max_length=20, null=True, blank=True)
	secondary_button_url = models.CharField(max_length=255, null=True, blank=True)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.title.title()

	def set_slug(self):
		if self.id is None:
			max_length = Post._meta.get_field('slug').max_length

			self.slug = slugify(self.title)[:max_length]

			for x in itertools.count(1):
				if not Post.objects.filter(slug=self.slug).exists():
					break

				self.slug = '%s-%d' % (self.slug, x)

	@classmethod
	def exclude_posts(cls):
		return cls.objects.filter(order_in_menu__isnull=True).order_by('title')

	@classmethod
	def posts_in_menu(cls):
		return cls.objects.filter(order_in_menu__isnull=False).order_by('order_in_menu')

	@classmethod
	def get_by_id(cls, post_id):
		try:
			post = cls.objects.get(id=post_id)

		except cls.DoesNotExist:
			post = None

		return post

	@classmethod
	def get_by_slug(cls, post_slug):
		try:
			post = cls.objects.get(slug=post_slug)

		except cls.DoesNotExist:
			post = None

		return post

	def save(self, *args, **kwargs):
		self.set_slug()

		super(Post, self).save(*args, **kwargs)

	def delete(self, *args, **kwargs):
		for post_image in self.images.all():
			post_image.image.delete()

		super(Post, self).delete(*args, **kwargs)
