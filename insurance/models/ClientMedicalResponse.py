from django.db import models

from insurance.models.MedicalResponseTemplate import MedicalResponseTemplate


class ClientMedicalResponse(MedicalResponseTemplate):
	client = models.ForeignKey('insurance.Client', related_name='medical_responses', on_delete=models.CASCADE)

	insurance = models.ManyToManyField('insurance.Insurance', related_name='client_medical_responses')
