from django.db import models


class QualitasBarrio(models.Model):
	name = models.CharField(max_length=255,null=True)
	code = models.CharField(max_length=255,default=None,null=True)
	district = models.ForeignKey('insurance.QualitasDistrict', related_name='district_barrios', on_delete=True,null=True)
	created_at = models.DateTimeField(auto_now_add=True,null=True)
	updated_at = models.DateTimeField(auto_now=True,null=True)

	def __str__(self):
		return self.name.title()

	@classmethod
	def get_by_id(cls, barrio_id):
		try:
			barrio = cls.objects.get(id=barrio_id)

		except cls.DoesNotExist:
			barrio = None

		return barrio
	