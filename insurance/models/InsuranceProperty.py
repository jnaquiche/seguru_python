from django.db import models


class InsuranceProperty(models.Model):
	name = models.CharField(max_length=255)
	order = models.IntegerField(default=0)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name.capitalize()

	@classmethod
	def get_sort_properties(cls):
		return cls.objects.all().order_by('order')
