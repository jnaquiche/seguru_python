from django.db import models


class Profession(models.Model):
	name = models.CharField(max_length=255,null=True)
	qualitas_profession_code = models.CharField(max_length=255,default=None,null=True)
	oceanica_profession_code = models.CharField(max_length=255,default=None,null=True)
	ins_profession_code = models.CharField(max_length=255,default=None,null=True)
	created_at = models.DateTimeField(auto_now_add=True,null=True)
	updated_at = models.DateTimeField(auto_now=True,null=True)

	def __str__(self):
		return self.name.title()

	
	@classmethod
	def get_by_id(cls, profession_id):
		try:
			profession = cls.objects.get(id=profession_id)

		except cls.DoesNotExist:
			profession = None

		return profession
