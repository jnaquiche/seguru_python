from django.db import models


class Client(models.Model):
	first_name = models.CharField(max_length=255)
	last_name = models.CharField(max_length=255)
	email = models.EmailField(max_length=255, null=True, blank=True)
	phone = models.CharField(max_length=50, null=True, blank=True)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return '{0} {1}'.format(self.first_name.title(), self.last_name.title())

	@property
	def get_insurance_type(self):
		if self.life_responses.exists():
			return 'Vida'

		elif self.medical_responses.exists():
			return 'Médico'

		elif self.vehicle_responses.exists():
			return 'Vehículos'

		elif self.home_responses.exists():
			return 'Casas'
