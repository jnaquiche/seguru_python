from django.db import models


class ResponseTemplate(models.Model):
	# Gender Options
	GENDER_FEMALE = 1
	GENDER_MALE = 2

	GENDER_OPTIONS = (
		(GENDER_FEMALE, 'Femenino'),
		(GENDER_MALE, 'Masculino'),
	)

	# Coverage Options
	COVERAGE_100_k = 1
	COVERAGE_200_k = 2
	COVERAGE_250_k = 3
	COVERAGE_500_k = 4
	COVERAGE_750_k = 5
	COVERAGE_1_M = 6
	COVERAGE_2_M = 7

	COVERAGE_OPTIONS = (
		(COVERAGE_100_k, '$100.000,00'),
		(COVERAGE_200_k, '$200.000,00'),
		(COVERAGE_250_k, '$250.000,00'),
		(COVERAGE_500_k, '$500.000,00'),
		(COVERAGE_750_k, '$750.000,00'),
		(COVERAGE_1_M, '$1.000.000,00'),
		(COVERAGE_2_M, '$2.000.000,00'),
	)

	age = models.IntegerField()

	gender = models.IntegerField(choices=GENDER_OPTIONS, default=GENDER_FEMALE)

	coverage_value = models.IntegerField(choices=COVERAGE_OPTIONS, default=COVERAGE_100_k)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	delete_at = models.DateTimeField(null=True)

	class Meta:
		abstract = True
