from django.db import models


class OceanicaCatalogModel(models.Model):
	brandcode = models.CharField(max_length=255,null=True)
	brandDescription = models.CharField(max_length=255,null=True)
	brandModelCode = models.CharField(max_length=255,null=True)
	brandModelDescription = models.CharField(max_length=255,default=None, null=True)