from django.db import models


class PostImage(models.Model):
	title = models.CharField(max_length=255, null=True, blank=True)

	image = models.ImageField(upload_to='images/post')

	post = models.ForeignKey('insurance.Post', related_name='images', on_delete=models.CASCADE)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	@classmethod
	def get_by_id(cls, post_image_id):
		try:
			post_image = cls.objects.get(id=post_image_id)

		except cls.DoesNotExist:
			post_image = None

		return post_image

	def delete(self, *args, **kwargs):
		self.image.delete()

		super(PostImage, self).delete(*args, **kwargs)

	class Meta:
		db_table = 'insurance_post_image'
