from django.db import models


class InsCatalogModel(models.Model):
	tagxml = models.CharField(max_length=255,null=True)
	codigo = models.CharField(max_length=255,null=True)
	descripcion = models.CharField(max_length=255,null=True)
	valorref1 = models.CharField(max_length=255,default=None, null=True)
	valorref2 = models.CharField(max_length=255,default=None, null=True)
	valorref3 = models.CharField(max_length=255,default=None, null=True)
	valorref4 = models.CharField(max_length=255,default=None, null=True)
	#created_at = models.DateTimeField(auto_now_add=True)
	#updated_at = models.DateTimeField(auto_now=True)
	
	#TAGXML	CODIGO	DESCRIPCION	VALORREF1	VALORREF2	VALORREF3	VALORREF4
	#def __str__(self):
	#	return self.tagxml