from django.db import models


class VehicleResponseTemplate(models.Model):
	vehicle_model = models.ForeignKey('insurance.VehicleModel', related_name='%(class)s', on_delete=models.CASCADE)
	#vehicle_year = models.ForeignKey('insurance.VehicleYear', related_name='%(class)s', on_delete=models.CASCADE,
	#								 null=True)
	vehicle_year=models.DecimalField(max_digits=12, decimal_places=2, null=True)
	vehicle_value = models.DecimalField(max_digits=12, decimal_places=2, null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		abstract = True
