from django.db import models

from insurance.models.LifeResponseTemplate import LifeResponseTemplate


class ClientLifeResponse(LifeResponseTemplate):
	client = models.ForeignKey('insurance.Client', related_name='life_responses', on_delete=models.CASCADE)

	insurance = models.ManyToManyField('insurance.Insurance', related_name='client_life_responses')
