from django.db import models


class ClientInfo(models.Model):
	
	cedula = models.IntegerField(primary_key=True)
	codelect = models.IntegerField()
	sexo =  models.IntegerField()
	fechacaduc = models.IntegerField()
	junta = models.CharField(max_length=10,null=True)
	nombre = models.CharField(max_length=50,null=True)
	apellido1 = models.CharField(max_length=50,null=True)
	apellido2 = models.CharField(max_length=50,null=True)

	# def __str__(self):
	# 	return self.cedula.value()

	@classmethod
	def get_by_id(cls,client_id):
		try:
			client_info = cls.objects.get(cedula=client_id)

		except cls.DoesNotExist:
			client_info = None

		return client_info

	#@property
	#def sorted_client_models(self):
	#	return self.vehicle_models.all().order_by('name')