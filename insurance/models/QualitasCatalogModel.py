from django.db import models


class QualitasCatalogModel(models.Model):

	#tarifa	AMIS	CLAVE	MARCA1	MARCA2	SUBTIPO	DESCRIPCIONMARCA	cate	car	tras	
	#cil	ptas	ocup	ton	subr	tarvig	gdm	grt	grc	marca	tipo	vers
	
	tarifa = models.IntegerField()
	amis = models.IntegerField()
	clave = models.IntegerField()
	
	marca1 = models.CharField(max_length=255)
	marca2 = models.CharField(max_length=255)
	subtipo = models.CharField(max_length=255)
	descripcionmarca = models.CharField(max_length=255)
	cate = models.CharField(max_length=255)
	
	car = models.IntegerField()
	tras = models.IntegerField()
	
	cil = models.CharField(max_length=255)
	
	ptas = models.IntegerField()
	ocup = models.IntegerField()
	ton = models.IntegerField()
	
	subr = models.CharField(max_length=255)
	
	tarvig = models.IntegerField()
	gdm = models.IntegerField()
	grt = models.IntegerField()
	grc = models.IntegerField()
	marca = models.CharField(max_length=255)
	
	tipo = models.CharField(max_length=255)
	vers = models.CharField(max_length=255)

	def __str__(self):
		return str(self.amis)

	# @classmethod
	# def get_catalog_model(cls, marca, modelo):
	# 	try:
	# 		print('buscando marca  {mr} ....'.format(mr=marca))
	# 		print('buscando modelo {md} ....'.format(md=modelo))
	# 		catalog_model = cls.objects.get(id=vehicle_model_id)

	# 	except cls.DoesNotExist:
	# 		catalog_model = None

	# 	return catalog_model
