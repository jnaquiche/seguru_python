from django.db import models


class SectionIcon(models.Model):
	title = models.CharField(max_length=25)
	subtitle = models.CharField(max_length=50)
	summary = models.CharField(max_length=75)

	button_text = models.CharField(max_length=20)
	button_url = models.CharField(max_length=255)

	home_tag = models.CharField(max_length=20, null=True, blank=True)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.title.title()
