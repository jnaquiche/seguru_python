from django.db import models


class InsuranceInfoProperty(models.Model):
	insurance_property = models.ForeignKey('insurance.InsuranceProperty',
										   on_delete=models.CASCADE,
										   related_name='insurance_info_property')

	insurance_info = models.ForeignKey('insurance.InsuranceInfo',
									   on_delete=models.CASCADE,
									   related_name='insurance_info_property')

	value = models.TextField()

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
