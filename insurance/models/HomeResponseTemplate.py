from django.db import models


class HomeResponseTemplate(models.Model):
	building_age = models.IntegerField()

	fire_protection = models.BooleanField(default=False)

	rivers_near = models.BooleanField(default=False)

	claims_last_3_years = models.BooleanField(default=False)

	construction_type = models.ManyToManyField('insurance.ConstructionType', related_name='%(class)s')

	features = models.ManyToManyField('insurance.Feature', related_name='%(class)s')

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	@property
	def construction_type_linked(self):
		construction_type_list = self.construction_type.all().values_list('name', flat=True)

		construction_type_list = ', '.join(construction_type_list)

		return construction_type_list

	@property
	def features_linked(self):
		features_list = self.features.all().values_list('name', flat=True)

		features_list = ', '.join(features_list)

		return features_list

	class Meta:
		abstract = True
