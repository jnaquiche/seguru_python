from django.db import models


class VehicleYear(models.Model):
	year = models.IntegerField(unique=True)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return str(self.year)

	@classmethod
	def get_by_id(cls, vehicle_year_id):
		try:
			vehicle_year = cls.objects.get(id=vehicle_year_id)

		except cls.DoesNotExist:
			vehicle_year = None

		return vehicle_year
