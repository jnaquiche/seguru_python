from django.db import models


class InsDistrict(models.Model):
	name = models.CharField(max_length=255,null=True)
	code = models.CharField(max_length=5,default=None,null=True)
	county = models.ForeignKey('insurance.InsCounty', related_name='county_districts', on_delete=True,null=True)
	created_at = models.DateTimeField(auto_now_add=True,null=True)
	updated_at = models.DateTimeField(auto_now=True,null=True)

	def __str__(self):
		return self.name.title()

	
	@classmethod
	def get_by_id(cls, district_id):
		try:
			district = cls.objects.get(id=district_id)

		except cls.DoesNotExist:
			district = None

		return district
