from django.db import models

from insurance.models.MedicalResponseDependentsTemplate import MedicalResponseDependentsTemplate


class ClientMedicalResponseDependents(MedicalResponseDependentsTemplate):
	client_medical_response = models.ForeignKey('insurance.ClientMedicalResponse',
												related_name='dependents',
												on_delete=models.CASCADE)
