from django.db import models


class Insurance(models.Model):
	name = models.CharField(max_length=255)
	price = models.DecimalField(max_digits=12, decimal_places=2)

	insurance_info = models.ForeignKey('insurance.InsuranceInfo', related_name='insurance', on_delete=models.CASCADE)
	company = models.ForeignKey('insurance.Company', related_name='insurance', on_delete=models.CASCADE)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return '{0} - {1}'.format(self.company, self.name.title())

	@classmethod
	def get_by_category(cls, category):
		return cls.objects.filter(insurance_info__category=category).order_by('name')

	@classmethod
	def get_by_id(cls, insurance_id):
		try:
			insurance = cls.objects.get(id=insurance_id)

		except cls.DoesNotExist:
			insurance = None

		return insurance
