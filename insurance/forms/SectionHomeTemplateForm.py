from django.contrib.admin.widgets import AdminFileWidget
from django.forms import ModelForm, TextInput, Textarea

from insurance.models import SectionHomeTemplate


class SectionHomeTemplateForm(ModelForm):
	class Meta:
		model = SectionHomeTemplate

		fields = ['title', 'subtitle', 'summary', 'primary_button_text', 'primary_button_url', 'secondary_button_text',
				  'secondary_button_url', 'background_image', 'home_tag']

		widgets = {
			'title': TextInput(attrs={'class': 'form-control'}),
			'subtitle': TextInput(attrs={'class': 'form-control'}),
			'summary': Textarea(attrs={'class': 'form-control'}),
			'primary_button_text': TextInput(attrs={'class': 'form-control'}),
			'primary_button_url': TextInput(attrs={'class': 'form-control'}),
			'secondary_button_text': TextInput(attrs={'class': 'form-control'}),
			'secondary_button_url': TextInput(attrs={'class': 'form-control'}),
			'home_tag': TextInput(attrs={'class': 'form-control'}),
			'background_image': AdminFileWidget(attrs={'class': 'form-control'}),
		}

		labels = {
			'title': 'Título',
			'subtitle': 'Subtítulo',
			'summary': 'Extracto',
			'primary_button_text': 'Texto Botón Primario',
			'primary_button_url': 'URL Botón Primario',
			'secondary_button_text': 'Texto Botón Secundario',
			'secondary_button_url': 'URL Botón Secundario',
			'background_image': 'Imagen de Fondo',
			'home_tag': 'Tag',
		}
