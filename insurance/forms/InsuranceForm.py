from django.forms import ModelForm, TextInput, Select, NumberInput, HiddenInput

from insurance.models import Insurance


class InsuranceForm(ModelForm):
	class Meta:
		model = Insurance

		fields = ['name', 'company', 'price', 'insurance_info']

		widgets = {
			'name': TextInput(attrs={'class': 'form-control'}),
			'company': Select(attrs={'class': 'form-control'}),
			'price': NumberInput(attrs={'class': 'form-control'}),
			'insurance_info': HiddenInput(),
		}

		labels = {
			'name': 'Nombre del Seguro',
			'company': 'Compañia',
			'price': 'Precio',
		}
