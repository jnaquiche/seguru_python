from django.forms import ModelForm, NumberInput, HiddenInput

from insurance.models import MedicalResponseDependents


class MedicalResponseDependentsForm(ModelForm):
	class Meta:
		model = MedicalResponseDependents

		fields = ['age', 'medical_response']

		widgets = {
			'age': NumberInput(attrs={'class': 'form-control'}),
			'medical_response': HiddenInput(),
		}

		labels = {
			'age': 'Edad',
		}
