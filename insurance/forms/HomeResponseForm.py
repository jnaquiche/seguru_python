from django.forms import ModelForm, NumberInput, CheckboxSelectMultiple

from insurance.models import HomeResponse, Insurance, InsuranceInfo


class HomeResponseForm(ModelForm):
	def __init__(self, *args, **kwargs):
		super(HomeResponseForm, self).__init__(*args, **kwargs)

		insurance_life = Insurance.get_by_category(category=InsuranceInfo.INSURANCE_CATEGORY_HOME)

		self.fields['insurance'].queryset = insurance_life

	class Meta:
		model = HomeResponse

		fields = ['construction_type', 'features', 'building_age', 'fire_protection', 'rivers_near',
				  'claims_last_3_years', 'insurance']

		widgets = {
			'building_age': NumberInput(attrs={'class': 'form-control'}),
			'construction_type': CheckboxSelectMultiple(),
			'features': CheckboxSelectMultiple(),
			'insurance': CheckboxSelectMultiple(),
		}

		labels = {
			'building_age': 'Antigüedad de la edificación',
			'construction_type': 'Tipo de construcción',
			'features': 'Características',
			'fire_protection': 'Medidas contra incendio',
			'rivers_near': 'Hay ríos cerca',
			'claims_last_3_years': 'Siniestros en los últimos 3 años',
			'insurance': 'Seguros',
		}
