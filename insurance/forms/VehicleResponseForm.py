from django import forms
from django.forms import ModelForm, Select, CheckboxSelectMultiple, NumberInput

from insurance.models import VehicleResponse, Insurance, InsuranceInfo, VehicleModel, VehicleBrand


class VehicleResponseForm(ModelForm):
	vehicle_brand_queryset = VehicleBrand.objects.all().order_by('name')
	vehicle_brand = forms.ModelChoiceField(queryset=vehicle_brand_queryset,
										   empty_label='Seleccione una Marca',
										   label='Marca',
										   widget=Select(attrs={'class': 'form-control'}))

	def __init__(self, *args, **kwargs):
		super(VehicleResponseForm, self).__init__(*args, **kwargs)

		insurance_life = Insurance.get_by_category(category=InsuranceInfo.INSURANCE_CATEGORY_VEHICLE)

		self.fields['insurance'].queryset = insurance_life

		self.fields['vehicle_model'].empty_label = 'Seleccione un Modelo'

		self.fields['vehicle_year'].empty_label = 'Digite un Año'

		if self.instance.id:
			self.fields['vehicle_brand'].initial = self.instance.vehicle_model.vehicle_brand

		self.fields['vehicle_model'].queryset = VehicleModel.objects.all()

	class Meta:
		model = VehicleResponse

		fields = ['vehicle_brand', 'vehicle_model', 'vehicle_year', 'vehicle_value', 'insurance']

		widgets = {
			'vehicle_model': Select(attrs={'class': 'form-control'}),
			'insurance': CheckboxSelectMultiple(),
			'vehicle_year': NumberInput(attrs={'class': 'form-control'}),
			'vehicle_value': NumberInput(attrs={'class': 'form-control'}),
		}

		labels = {
			'vehicle_model': 'Modelo',
			'insurance': 'Seguros',
			'vehicle_year': 'Año',
			'vehicle_value': 'Valor del Vehículo',
		}
