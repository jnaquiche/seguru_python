from django.forms import ModelForm, TextInput, Textarea

from insurance.models import SectionIcon


class SectionIconForm(ModelForm):
	class Meta:
		model = SectionIcon

		fields = ['title', 'subtitle', 'summary', 'button_text', 'button_url', 'home_tag']

		widgets = {
			'title': TextInput(attrs={'class': 'form-control'}),
			'subtitle': TextInput(attrs={'class': 'form-control'}),
			'summary': Textarea(attrs={'class': 'form-control'}),
			'button_text': TextInput(attrs={'class': 'form-control'}),
			'button_url': TextInput(attrs={'class': 'form-control'}),
			'home_tag': TextInput(attrs={'class': 'form-control'}),
		}

		labels = {
			'title': 'Título',
			'subtitle': 'Subtítulo',
			'summary': 'Extracto',
			'button_text': 'Texto Botón',
			'button_url': 'URL Botón',
			'home_tag': 'Tag',
		}
