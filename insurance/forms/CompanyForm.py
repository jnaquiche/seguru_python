from django.forms import ModelForm, TextInput

from insurance.models import Company


class CompanyForm(ModelForm):
	class Meta:
		model = Company

		fields = ['name']

		widgets = {
			'name': TextInput(attrs={'class': 'form-control'}),
		}

		labels = {
			'name': 'Nombre',
		}
