from django.forms import ModelForm, TextInput, HiddenInput, CheckboxSelectMultiple

from insurance.models import VehicleModel, VehicleYear


class VehicleModelForm(ModelForm):
	def __init__(self, *args, **kwargs):
		super(VehicleModelForm, self).__init__(*args, **kwargs)

		self.fields['years'].queryset = VehicleYear.objects.all().order_by('year')

	class Meta:
		model = VehicleModel

		fields = ['name','amis','vehicle_brand', 'years']

		widgets = {
			'name': TextInput(attrs={'class': 'form-control'}),
			'amis': TextInput(attrs={'class': 'form-control'}),
			'vehicle_brand': HiddenInput(),
			'years': TextInput(attrs={'class': 'form-control'}),
		}

		labels = {
			'name': 'Nombre',
			'years': 'Años',
		}
