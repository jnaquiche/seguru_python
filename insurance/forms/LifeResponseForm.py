from django.forms import ModelForm, Select, CheckboxSelectMultiple, TextInput

from insurance.models import LifeResponse, Insurance, InsuranceInfo


class LifeResponseForm(ModelForm):
	def __init__(self, *args, **kwargs):
		super(LifeResponseForm, self).__init__(*args, **kwargs)

		insurance_life = Insurance.get_by_category(category=InsuranceInfo.INSURANCE_CATEGORY_LIFE)

		self.fields['insurance'].queryset = insurance_life

	class Meta:
		model = LifeResponse

		fields = ['age', 'gender', 'coverage_value', 'is_smoker', 'insurance']

		widgets = {
			'age': TextInput(attrs={'class': 'form-control'}),
			'gender': Select(attrs={'class': 'form-control'}),
			'coverage_value': Select(attrs={'class': 'form-control'}),
			'insurance': CheckboxSelectMultiple(),
		}

		labels = {
			'age': 'Edad',
			'gender': 'Género',
			'coverage_value': 'Cobertura',
			'insurance': 'Seguros',
			'is_smoker': '¿Es fumador?',
		}
