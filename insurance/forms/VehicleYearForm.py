from django.forms import ModelForm, TextInput

from insurance.models import VehicleYear


class VehicleYearForm(ModelForm):
	class Meta:
		model = VehicleYear

		fields = ['year']

		widgets = {
			'year': TextInput(attrs={'class': 'form-control'}),
		}

		labels = {
			'year': 'Año',
		}

		error_messages={
			'year': {
				'unique': 'Ya existe un año registrado con este valor.'
			}
		}
