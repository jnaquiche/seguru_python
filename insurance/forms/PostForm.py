from django.forms import ModelForm, TextInput, Textarea

from insurance.models import Post


class PostForm(ModelForm):
	class Meta:
		model = Post

		fields = ['title', 'subtitle', 'content', 'primary_button_text', 'primary_button_url', 'secondary_button_text',
				  'secondary_button_url']

		widgets = {
			'title': TextInput(attrs={'class': 'form-control'}),
			'subtitle': TextInput(attrs={'class': 'form-control'}),
			'content': Textarea(attrs={'class': 'form-control'}),
			'primary_button_text': TextInput(attrs={'class': 'form-control'}),
			'primary_button_url': TextInput(attrs={'class': 'form-control'}),
			'secondary_button_text': TextInput(attrs={'class': 'form-control'}),
			'secondary_button_url': TextInput(attrs={'class': 'form-control'}),
		}

		labels = {
			'title': 'Título',
			'subtitle': 'Subtítulo',
			'content': 'Contenido',
			'primary_button_text': 'Texto Botón Primario',
			'primary_button_url': 'URL Botón Primario',
			'secondary_button_text': 'Texto Botón Secundario',
			'secondary_button_url': 'URL Botón Secundario',
		}
