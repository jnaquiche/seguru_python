from django.forms import ModelForm, TextInput

from insurance.models import Feature


class FeatureForm(ModelForm):
	class Meta:
		model = Feature

		fields = ['name']

		widgets = {
			'name': TextInput(attrs={'class': 'form-control'})
		}

		labels = {
			'name': 'Nombre',
		}
