from django.forms import ModelForm, TextInput

from insurance.models import ConstructionType


class ConstructionTypeForm(ModelForm):
	class Meta:
		model = ConstructionType

		fields = ['name']

		widgets = {
			'name': TextInput(attrs={'class': 'form-control'})
		}

		labels = {
			'name': 'Nombre',
		}
