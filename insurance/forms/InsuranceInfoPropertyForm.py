from django.forms import ModelForm, Select, HiddenInput, Textarea

from insurance.models import InsuranceInfoProperty


class InsuranceInfoPropertyForm(ModelForm):
	class Meta:
		model = InsuranceInfoProperty

		fields = ['insurance_property', 'insurance_info', 'value']

		widgets = {
			'insurance_property': Select(attrs={'class': 'form-control'}),
			'insurance_info': HiddenInput(),
			'value': Textarea(attrs={'class': 'form-control', 'rows': 5}),
		}

		labels = {
			'insurance_property': 'Propiedad de Seguro',
			'value': 'Valor',
		}
