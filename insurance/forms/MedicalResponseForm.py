from django.forms import ModelForm, TextInput, Select, CheckboxSelectMultiple, NumberInput

from insurance.models import MedicalResponse, Insurance, InsuranceInfo


class MedicalResponseForm(ModelForm):
	def __init__(self, *args, **kwargs):
		super(MedicalResponseForm, self).__init__(*args, **kwargs)

		insurance_medical = Insurance.get_by_category(category=InsuranceInfo.INSURANCE_CATEGORY_MEDICAL)

		self.fields['insurance'].queryset = insurance_medical

	class Meta:
		model = MedicalResponse

		fields = ['age', 'gender', 'coverage_value', 'spouse_age', 'insurance']

		widgets = {
			'age': TextInput(attrs={'class': 'form-control'}),
			'gender': Select(attrs={'class': 'form-control'}),
			'coverage_value': Select(attrs={'class': 'form-control'}),
			'insurance': CheckboxSelectMultiple(),
			'spouse_age': NumberInput(attrs={'class': 'form-control'}),
		}

		labels = {
			'age': 'Edad',
			'gender': 'Género',
			'coverage_value': 'Cobertura',
			'insurance': 'Seguros',
			'spouse_age': 'Edad del Cónyuge (Dejar vacío si no tiene)',
		}
