from django.forms import ModelForm, TextInput

from insurance.models import InsuranceCategory


class InsuranceCategoryForm(ModelForm):
	class Meta:
		model = InsuranceCategory

		fields = ['name']

		widgets = {
			'name': TextInput(attrs={'class': 'form-control'}),
		}

		labels = {
			'name': 'Nombre',
		}
