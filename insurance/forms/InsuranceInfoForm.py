from django.forms import ModelForm, Select, CheckboxSelectMultiple, TextInput

from insurance.models import InsuranceInfo


class InsuranceInfoForm(ModelForm):
	class Meta:
		model = InsuranceInfo

		fields = ['name', 'category']

		widgets = {
			'name': TextInput(attrs={'class': 'form-control'}),
			'category': Select(attrs={'class': 'form-control'}),
		}

		labels = {
			'name': 'Nombre (Etiqueta)',
			'category': 'Categoria del Seguro',
		}
