from django.forms import ModelForm, TextInput

from insurance.models import VehicleBrand


class VehicleBrandForm(ModelForm):
	class Meta:
		model = VehicleBrand

		fields = ['name']

		widgets = {
			'name': TextInput(attrs={'class': 'form-control'}),
		}

		labels = {
			'name': 'Nombre',
		}
