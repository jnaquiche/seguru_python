from django.forms import ModelForm, TextInput

from insurance.models import InsuranceProperty


class InsurancePropertyForm(ModelForm):
	class Meta:
		model = InsuranceProperty

		fields = ['name']

		widgets = {
			'name': TextInput(attrs={'class': 'form-control'}),
		}

		labels = {
			'name': 'Nombre',
		}
