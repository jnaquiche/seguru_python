# Generated by Django 2.0.2 on 2019-06-18 06:21

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('insurance', '0030_auto_20190617_2212'),
    ]

    operations = [
        migrations.RenameField(
            model_name='district',
            old_name='state',
            new_name='county',
        ),
    ]
