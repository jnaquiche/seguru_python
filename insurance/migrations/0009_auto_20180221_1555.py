# Generated by Django 2.0.2 on 2018-02-21 21:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('insurance', '0008_auto_20180221_1438'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='email',
            field=models.EmailField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='phone',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
