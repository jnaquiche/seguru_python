# Generated by Django 2.0.2 on 2018-02-20 23:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('insurance', '0002_vehicleyear'),
    ]

    operations = [
        migrations.AddField(
            model_name='vehiclemodel',
            name='years',
            field=models.ManyToManyField(related_name='vehicle_models', to='insurance.VehicleYear'),
        ),
    ]
