# Generated by Django 2.0.2 on 2019-06-23 01:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('insurance', '0032_auto_20190622_1931'),
    ]

    operations = [
        migrations.CreateModel(
            name='Color',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, null=True)),
                ('qualitas_color_code', models.CharField(default=None, max_length=255, null=True)),
                ('ins_color_code', models.CharField(default=None, max_length=5, null=True)),
                ('oceanica_color_code', models.CharField(default=None, max_length=255, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
            ],
        ),
    ]
