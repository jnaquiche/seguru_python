# Generated by Django 2.0.2 on 2019-06-04 02:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('insurance', '0017_auto_20190602_1316'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='vehiclemodel',
            name='years',
        ),
        migrations.AddField(
            model_name='vehiclemodel',
            name='years',
            field=models.CharField(default=2000, max_length=255),
            preserve_default=False,
        ),
    ]
