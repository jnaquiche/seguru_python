"""
Django settings for seguru_website project.

Generated by 'django-admin startproject' using Django 2.0.2.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.0/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
from seguru_website.config.get_config import Config

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROJECT_ROOT = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
CONFIG = Config('production')
SECRET_KEY = CONFIG.get('secrets', 'SECRET_KEY'),

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['*']

# Application definition

INSTALLED_APPS = [
	'django.contrib.admin',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	'django.contrib.humanize',
	'insurance',
	'client',
	'accounts',
]

MIDDLEWARE = [
	'django.middleware.security.SecurityMiddleware',
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.common.CommonMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'seguru_website.urls'

TEMPLATES = [
	{
		'BACKEND': 'django.template.backends.django.DjangoTemplates',
		'DIRS': [],
		'APP_DIRS': True,
		'OPTIONS': {
			'context_processors': [
				'django.template.context_processors.debug',
				'django.template.context_processors.request',
				'django.contrib.auth.context_processors.auth',
				'django.contrib.messages.context_processors.messages',
			],
		},
	},
]

WSGI_APPLICATION = 'seguru_website.wsgi.application'

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.mysql',
		'NAME': CONFIG.get('database', 'DATABASE_NAME'),
		'USER': CONFIG.get('database', 'DATABASE_USER'),
		'PASSWORD': CONFIG.get('database', 'DATABASE_PASSWORD'),
		'HOST': CONFIG.get('database', 'DATABASE_HOST'),
		'PORT': '',
	}
}

# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
	{
		'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
	},
	{
		'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
	},
	{
		'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
	},
	{
		'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
	},
]

# Auth Settings
LOGIN_URL = 'accounts.login'
LOGIN_REDIRECT_URL = 'insurance.post.list'
LOGOUT_REDIRECT_URL = 'accounts.login'

# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'America/Costa_Rica'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')

MEDIA_URL = '/uploads/'
MEDIA_PATH = os.path.abspath(os.path.dirname(__name__))
MEDIA_ROOT = os.path.join(MEDIA_PATH, 'uploads/')

DEFAULT_FROM_EMAIL = 'info@seguru.com'

if DEBUG:
	EMAIL_HOST = '127.0.0.1'
	EMAIL_HOST_USER = ''
	EMAIL_HOST_PASSWORD = ''
	EMAIL_PORT = 1025
	EMAIL_USE_TLS = False

LOGGING = {
	'version': 1,
	'disable_existing_loggers': False,
	'handlers': {
		# Include the default Django email handler for errors
		# This is what you'd get without configuring logging at all.
		'mail_admins': {
			'class': 'django.utils.log.AdminEmailHandler',
			'level': 'ERROR',
			# But the emails are plain text by default - HTML is nicer
			'include_html': True,
		},
		# Log to a text file that can be rotated by logrotate
		'logfile': {
			'class': 'logging.handlers.WatchedFileHandler',
			'filename': '/home/deploy/seguru/current/logs/seguru_errors.log'
		},
	},
	'loggers': {
		# Again, default Django configuration to email unhandled exceptions
		'django.request': {
			'handlers': ['logfile'],
			'level': 'ERROR',
			'propagate': True,
		},
		# Might as well log any errors anywhere else in Django
		'django': {
			'handlers': ['logfile'],
			'level': 'ERROR',
			'propagate': False,
		},
		# Your own app - this assumes all your logger names start with "myapp."
		'weizman.logger': {
			'handlers': ['logfile'],
			'level': 'INFO',  # Or maybe INFO or DEBUG
			'propagate': False
		},
	},
}
