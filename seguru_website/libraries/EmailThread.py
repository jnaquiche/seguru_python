import threading

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string


class EmailThread(threading.Thread):
	from_address = settings.DEFAULT_FROM_EMAIL
	template = 'client/layout/email_template.html'

	def __init__(self, subject, email_data, recipient_list):
		self.subject = subject
		self.recipient_list = recipient_list
		self.email_data = email_data

		threading.Thread.__init__(self)

	def run(self):
		email_html = render_to_string(self.template, {
			'email_data': self.email_data
		})

		email = EmailMultiAlternatives(subject=self.subject,
									   body=email_html,
									   from_email=settings.DEFAULT_FROM_EMAIL,
									   to=self.recipient_list)

		email.attach_alternative(email_html, 'text/html')

		email.send()
